# Contribuer au développement de GeRaid

Le projet est placé sous la licence libre et à code source ouvert [MIT Licence](LICENSE).

## Contraintes de développement

- Le code compilé doit être exeécutable par Java 8 ou supérieur.

- Il n'y a, pour l'instant, pas d'utilisation de moteur de production type `gradle`.

- L'archive produite `gecoraid.jar` contient les bibliothèques du dossier `lib` sans le sous-dossier `dev`. 

- L'archive doit être exécutable sur Linux, MacOS et Windows.
