# GeCoRaid

## Crédits

GeCoRaid est un dérivé de l'excellent projet [GeRaid](http://t.porret.free.fr/lienlogiciel.php?idmenu=10) développé par Thierry Porret.
Il intègre aussi un dérivé de l'autre excellent projet [GecoSI](https://github.com/sdenier/GecoSI) développé par Simon Denier pour interfacer
le système de chronométrage [SPORTident](https://www.sportident.com/).

## Téléchargement et lancement du logiciel

### Prérequis

Le logiciel nécessite l'installation préalable de [Java 8 ou supérieur](https://www.java.com/) sur
l'ordinateur hôte.

### Installation

1. Téléchargez [l'archive zip (compatible Linux / Windows / MacOS)](https://gitlab.com/clouardregis/gecoraid/-/releases)
2. Dézippez l'archive à l'endroit de votre choix
3. Dans le dossier `bin` du logiciel, double-cliquez sur le fichier `gecoraid` sur Linux ou `gecoraid.bat` sur Windows.
4. Configurez le logiciel avec vos préférences (menu `GeCoRaid`)

## Présentation

GeCoRaid est un logiciel de gestion électronique de course d'orientation et raids multisports.
Le logiciel interface le système SPORTIdent pour le chronométrage des courses par doigts électroniques.

Les principales fonctionnalités sont :

- Gestion de plusieurs parcours
- Gestion de plusieurs étapes par parcours
- Gestion de plusieurs épreuves par étape
- Paramétrage fin des épreuves
- Gestion des équipes par parcours
- Import des balises à partir d'OCAD
- Lecture de toutes les puces actuelles
- Impression des différents résultats
- Visualisation immédiate des résultats
- Modification des résultats
- Gestion des pénalités hors épreuves

Aperçu de l'interface :

![gecoraid](assets/gecoraid.jpg)

## Objectif du projet

Le projet est le support de développements réalisés par des étudiants en spécialité informatique
de l'école publique d'ingénieurs [ENSICAEN](https://www.ensicaen.fr).

# Licence

Le projet est placé sous la licence libre et à code source ouvert [MIT](LICENSE).

# Contribution

Voir le fichier [CONTRIBUTING.md](CONTRIBUTING.md)

# Mainteneurs

- Régis CLOUARD
- Martin FÉAUX DE LA CROIX