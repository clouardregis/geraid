# Journal des modifications

Tous les changements notables apportés à ce projet seront documentés dans ce dossier.

Le format est basé sur celui décrit dans [Keep a Changelog](https://keepachangelog.com/en/1.0.0/index.html),
et le projet suit le versionnage sémantique [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

# [0.0.2] 2025/01/01

- Ajout d'un mode "exclusion" qui permet de prendre une épreuve sur deux (celle qui à était le mieux réussi).

## [0.0.1] 2023/02/01

- Adaptation à Linux et MacOS
- Révision de l'interface graphique

## [2.3.0] 2020/02/06
- Originale version de GeRaid