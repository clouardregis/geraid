/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi;

import net.gecosi.dataframe.SportIdentDataFrame;

public interface SportIdentListener {
    void handleEcard( SportIdentDataFrame dataFrame );

    void notify( CommStatus status );

    void notify( CommStatus errorStatus, String errorMessage );
}
