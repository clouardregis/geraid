/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi;

public enum CommStatus {
    OFF, STARTING, READY, ON, PROCESSING, PROCESSING_ERROR, FATAL_ERROR
}
