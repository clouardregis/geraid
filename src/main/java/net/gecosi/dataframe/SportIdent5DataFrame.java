/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.dataframe;

import net.gecosi.internal.SportIdentMessage;

import java.util.Arrays;

public class SportIdent5DataFrame extends SportIdentAbstractDataFrame {
    private static final int SI5_TIMED_PUNCHES = 30;

    public SportIdent5DataFrame( SportIdentMessage message ) {
        setDataFrame(extractDataFrame(message));
        siNumber = extractSiNumber();
    }

    @Override
    public SportIdentDataFrame startingAt( long zeroHour ) {
        startTime = advanceTimePast(rawStartTime(), zeroHour);
        checkTime = advanceTimePast(rawCheckTime(), zeroHour);
        long refTime = newRefTime(zeroHour, startTime);
        punches = computeShiftedPunches(refTime);
        if (punches.length > 0) {
            SportIdentPunch lastTimedPunch = punches[nbTimedPunches(punches) - 1];
            refTime = newRefTime(refTime, lastTimedPunch.timestamp());
        }
        finishTime = advanceTimePast(rawFinishTime(), refTime);
        return this;
    }

    public long advanceTimePast( long timestamp, long refTime ) {
        return advanceTimePast(timestamp, refTime, TWELVE_HOURS);
    }

    @Override
    public String getSiSeries() {
        return "SiCard 5";
    }

    private byte[] extractDataFrame( SportIdentMessage message ) {
        return Arrays.copyOfRange(message.sequence(), 5, 133);
    }

    private SportIdentPunch[] computeShiftedPunches( long startTime ) {
        int nbPunches = rawNbPunches();
        SportIdentPunch[] punches = new SportIdentPunch[nbPunches];
        int nbTimedPunches = nbTimedPunches(punches);
        long refTime = startTime;
        for (int i = 0; i < nbTimedPunches; i++) {
            // shift each punch time after the previous
            long punchTime = advanceTimePast(getPunchTime(i), refTime);
            punches[i] = new SportIdentPunch(getPunchCode(i), punchTime);
            refTime = newRefTime(refTime, punchTime);
        }
        for (int i = 0; i < nbPunches - SI5_TIMED_PUNCHES; i++) {
            punches[i + SI5_TIMED_PUNCHES] = new SportIdentPunch(getNoTimePunchCode(i), NO_TIME);
        }
        return punches;
    }

    private int nbTimedPunches( SportIdentPunch[] punches ) {
        return Math.min(punches.length, SI5_TIMED_PUNCHES);
    }

    private String extractSiNumber() {
        int siNumber = wordAt(0x04);
        int cns = byteAt(0x06);
        if (cns > 0x01) {
            siNumber = siNumber + cns * 100000;
        }
        return Integer.toString(siNumber);
    }

    private int rawNbPunches() {
        return byteAt(0x17) - 1;
    }

    private long rawStartTime() {
        return timestampAt(0x13);
    }

    private long rawFinishTime() {
        return timestampAt(0x15);
    }

    private long rawCheckTime() {
        return timestampAt(0x19);
    }

    protected int punchOffset( int i ) {
        return 0x21 + (i / 5) * 0x10 + (i % 5) * 0x03;
    }

    private int getPunchCode( int i ) {
        return byteAt(punchOffset(i));
    }

    private int getNoTimePunchCode( int i ) {
        return byteAt(0x20 + i * 0x10);
    }

    private long getPunchTime( int i ) {
        return timestampAt(punchOffset(i) + 1);
    }
}
