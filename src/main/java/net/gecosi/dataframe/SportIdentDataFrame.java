/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.dataframe;

public interface SportIdentDataFrame {
    long NO_TIME = -1;

    SportIdentDataFrame startingAt( long zeroHour );

    int getNbPunches();

    String getSiNumber();

    String getSiSeries();

    long getStartTime();

    long getFinishTime();

    long getCheckTime();

    SportIdentPunch[] getPunches();
}