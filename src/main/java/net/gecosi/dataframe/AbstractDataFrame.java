/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.dataframe;

public abstract class AbstractDataFrame implements SportIdentDataFrame {
    protected String siNumber;
    protected long checkTime;
    protected long startTime;
    protected long finishTime;
    protected SportIdentPunch[] punches;

    @Override
    public String getSiNumber() {
        return siNumber;
    }

    @Override
    public long getStartTime() {
        return startTime;
    }

    @Override
    public long getFinishTime() {
        return finishTime;
    }

    @Override
    public long getCheckTime() {
        return checkTime;
    }

    @Override
    public int getNbPunches() {
        return punches.length;
    }

    @Override
    public SportIdentPunch[] getPunches() {
        return punches;
    }
}