/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.dataframe;

import net.gecosi.internal.SportIdentMessage;

public class SportIdent8PlusDataFrame extends SportIdent6PlusAbstractDataFrame {
    public static final int PAGE_SIZE = 4;
    private static final int SI_NUMBER_PAGE = 6 * PAGE_SIZE;
    public static final int NB_PUNCHES_INDEX = 5 * PAGE_SIZE + 2;
    private final SiPlusSeries siSeries;

    public enum SiPlusSeries {
        SI8_SERIES("SiCard 8", 34),
        SI9_SERIES("SiCard 9", 14),
        SI10PLUS_SERIES("SiCard 10/11/SIAC", 32),
        PCARD_SERIES("pCard", 44),
        UNKNOWN_SERIES("Unknown", 0);

        private final String _ident;
        private final int _punchesPageIndex;

        SiPlusSeries( String ident, int punchesPageIndex ) {
            _ident = ident;
            _punchesPageIndex = punchesPageIndex;
        }

        public String ident() {
            return _ident;
        }

        public int punchesPageStartIndex() {
            return _punchesPageIndex;
        }
    }

    public SportIdent8PlusDataFrame( SportIdentMessage[] dataMessages ) {
        super(dataMessages);
        this.siSeries = extractSiSeries();
    }

    @Override
    public String getSiSeries() {
        return siSeries.ident();
    }

    @Override
    protected int siNumberIndex() {
        return SI_NUMBER_PAGE + 1;
    }

    @Override
    protected int startTimeIndex() {
        return 3 * PAGE_SIZE;
    }

    @Override
    protected int finishTimeIndex() {
        return 4 * PAGE_SIZE;
    }

    @Override
    protected int checkTimeIndex() {
        return 2 * PAGE_SIZE;
    }

    @Override
    protected int nbPunchesIndex() {
        return NB_PUNCHES_INDEX;
    }

    @Override
    protected SportIdentPunch[] extractPunches( long startTime ) {
        SportIdentPunch[] punches = new SportIdentPunch[rawNbPunches()];
        int punchesStart = siSeries.punchesPageStartIndex();
        long refTime = startTime;
        for (int i = 0; i < punches.length; i++) {
            int punchIndex = (punchesStart + i) * PAGE_SIZE;
            long punchTime = advanceTimePast(extractFullTime(punchIndex), refTime);
            punches[i] = new SportIdentPunch(extractCode(punchIndex), punchTime);
            refTime = newRefTime(refTime, punchTime);
        }
        return punches;
    }

    private SiPlusSeries extractSiSeries() {
        switch (byteAt(SI_NUMBER_PAGE) & 15) {
            case 2:
                return SiPlusSeries.SI8_SERIES;
            case 1:
                return SiPlusSeries.SI9_SERIES;
            case 4:
                return SiPlusSeries.PCARD_SERIES;
            case 15:
                return SiPlusSeries.SI10PLUS_SERIES;
            default:
                return SiPlusSeries.UNKNOWN_SERIES;
        }
    }
}
