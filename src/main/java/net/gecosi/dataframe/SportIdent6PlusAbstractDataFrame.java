/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.dataframe;

import java.util.Arrays;

import net.gecosi.internal.SportIdentMessage;

public abstract class SportIdent6PlusAbstractDataFrame extends SportIdentAbstractDataFrame {
    protected SportIdent6PlusAbstractDataFrame( SportIdentMessage[] dataMessages ) {
        setDataFrame(extractDataFrame(dataMessages));
        this.siNumber = extractSiNumber();
    }

    protected byte[] extractDataFrame( SportIdentMessage[] dataMessages ) {
        byte[] dataFrame = Arrays.copyOfRange(dataMessages[0].sequence(), 6, dataMessages.length * 128 + 6);
        for (int i = 1; i < dataMessages.length; i++) {
            System.arraycopy(dataMessages[i].sequence(), 6, dataFrame, i * 128, 128);
        }
        return dataFrame;
    }

    @Override
    public SportIdentDataFrame startingAt( long zeroHour ) {
        startTime = advanceTimePast(extractStartTime(), zeroHour);
        checkTime = advanceTimePast(extractCheckTime(), zeroHour);
        long refTime = newRefTime(zeroHour, startTime);
        punches = extractPunches(refTime);
        if (punches.length > 0) {
            SportIdentPunch lastPunch = punches[punches.length - 1];
            refTime = newRefTime(refTime, lastPunch.timestamp());
        }
        finishTime = advanceTimePast(extractFinishTime(), refTime);
        return this;
    }

    public long advanceTimePast( long timestamp, long refTime ) {
        return advanceTimePast(timestamp, refTime, ONE_DAY);
    }

    protected int rawNbPunches() {
        return byteAt(nbPunchesIndex());
    }

    protected long extractFullTime( int pageStart ) {
        int pmFlag = byteAt(pageStart) & 1;
        return computeFullTime(pmFlag, timestampAt(pageStart + 2));
    }

    protected int extractCode( int punchIndex ) {
        int codeHigh = (byteAt(punchIndex) & 192) << 2;
        return codeHigh + byteAt(punchIndex + 1);
    }

    protected abstract int siNumberIndex();

    protected abstract int startTimeIndex();

    protected abstract int finishTimeIndex();

    protected abstract int checkTimeIndex();

    protected abstract int nbPunchesIndex();

    protected abstract SportIdentPunch[] extractPunches( long startTime );

    private String extractSiNumber() {
        return Integer.toString(block3At(siNumberIndex()));
    }

    private long extractStartTime() {
        return extractFullTime(startTimeIndex());
    }

    private long extractFinishTime() {
        return extractFullTime(finishTimeIndex());
    }

    private long extractCheckTime() {
        return extractFullTime(checkTimeIndex());
    }

    private long computeFullTime( int pmFlag, long twelveHoursTime ) {
        if (twelveHoursTime == NO_SPORT_IDENT_TIME) {
            return NO_SPORT_IDENT_TIME;
        }
        return pmFlag * TWELVE_HOURS + twelveHoursTime;
    }
}