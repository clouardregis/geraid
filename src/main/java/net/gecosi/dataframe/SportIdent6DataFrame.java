/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.dataframe;

import net.gecosi.internal.SportIdentMessage;

public class SportIdent6DataFrame extends SportIdent6PlusAbstractDataFrame {
    private static final int PAGE_SIZE = 16;
    private static final int DOUBLE_WORD = 4;
    public static final int NB_PUNCHES_INDEX = PAGE_SIZE + 2;

    public SportIdent6DataFrame( SportIdentMessage[] dataMessages ) {
        super(dataMessages);
    }

    @Override
    protected int siNumberIndex() {
        return 2 * DOUBLE_WORD + 3;
    }

    @Override
    protected int startTimeIndex() {
        return PAGE_SIZE + 2 * DOUBLE_WORD;
    }

    @Override
    protected int finishTimeIndex() {
        return PAGE_SIZE + DOUBLE_WORD;
    }

    @Override
    protected int checkTimeIndex() {
        return PAGE_SIZE + 3 * DOUBLE_WORD;
    }

    @Override
    protected int nbPunchesIndex() {
        return NB_PUNCHES_INDEX;
    }

    protected int punchesStartIndex() {
        return 8 * PAGE_SIZE;
    }

    @Override
    protected SportIdentPunch[] extractPunches( long startTime ) {
        SportIdentPunch[] punches = new SportIdentPunch[rawNbPunches()];
        int punchesStart = punchesStartIndex();
        long refTime = startTime;
        for (int i = 0; i < punches.length; i++) {
            int punchIndex = punchesStart + (DOUBLE_WORD * i);
            long punchTime = advanceTimePast(extractFullTime(punchIndex), refTime);
            punches[i] = new SportIdentPunch(extractCode(punchIndex), punchTime);
            refTime = newRefTime(refTime, punchTime);
        }
        return punches;
    }

    @Override
    public String getSiSeries() {
        return "SiCard 6";
    }
}
