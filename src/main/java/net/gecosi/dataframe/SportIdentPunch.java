/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.dataframe;

public class SportIdentPunch {
    private final int _code;
    private final long _timestamp;

    public SportIdentPunch( int code, long timestamp ) {
        _code = code;
        _timestamp = timestamp;
    }

    public int code() {
        return _code;
    }

    public long timestamp() {
        return _timestamp;
    }
}
