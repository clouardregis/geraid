/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.adapter.logfile;

import net.gecosi.internal.CommWriter;
import net.gecosi.internal.SportIdentMessageQueue;
import net.gecosi.internal.SportIdentPort;

import java.io.IOException;

public class LogFilePort implements SportIdentPort {
    private final String _logFilename;

    public LogFilePort( String logFilename ) {
        _logFilename = logFilename;
    }

    @Override
    public SportIdentMessageQueue createMessageQueue() throws IOException {
        return new LogFileCommReader(_logFilename).createMessageQueue();
    }

    @Override
    public CommWriter createWriter() {
        return new NullCommWriter();
    }

    @Override
    public void setupHighSpeed() {
        // Nothing to do
    }

    @Override
    public void setupLowSpeed() {
        // Nothing to do
    }

    @Override
    public void close() {
        // Nothing to do
    }
}
