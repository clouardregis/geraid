/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.adapter.logfile;

import net.gecosi.internal.CommWriter;
import net.gecosi.internal.SportIdentMessage;

public class NullCommWriter implements CommWriter {
    @Override
    public void write( SportIdentMessage message ) {
        // Nothing to do
    }
}
