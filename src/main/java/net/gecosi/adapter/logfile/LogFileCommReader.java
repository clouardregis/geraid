/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.adapter.logfile;

import net.gecosi.internal.SportIdentMessage;
import net.gecosi.internal.SportIdentMessageQueue;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Vector;

public class LogFileCommReader {
    private final Vector<SportIdentMessage> _messages;

    public LogFileCommReader( String filename ) throws IOException {
        _messages = read(filename, new Vector<>(100));
    }

    public Vector<SportIdentMessage> read( String filename, Vector<SportIdentMessage> messages ) throws IOException {
        try (BufferedReader fileReader = new BufferedReader(new FileReader(filename))) {
            String line = fileReader.readLine();
            while (line != null) {
                if (line.startsWith("READ")) {
                    String[] bytes = line.split(" ");
                    byte[] seq = new byte[bytes.length - 1];
                    for (int i = 1; i < bytes.length; i++) {
                        seq[i - 1] = (byte) Integer.parseInt(bytes[i], 16);
                    }
                    messages.add(new SportIdentMessage(seq));
                }
                line = fileReader.readLine();
            }
        }
        return messages;
    }

    public SportIdentMessageQueue createMessageQueue() {
        SportIdentMessageQueue queue = new SportIdentMessageQueue(_messages.size());
        queue.addAll(_messages);
        return queue;
    }
}
