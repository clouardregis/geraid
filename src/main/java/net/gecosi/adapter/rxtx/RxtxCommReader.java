/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.adapter.rxtx;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortDataListener;
import com.fazecast.jSerialComm.SerialPortEvent;
import net.gecosi.internal.SportIdentLogger;
import net.gecosi.internal.SportIdentMessage;
import net.gecosi.internal.SportIdentMessageQueue;

public class RxtxCommReader implements SerialPortDataListener {
    public static final int MAX_MESSAGE_SIZE = 139;
    private static final int METADATA_SIZE = 6;
    private final InputStream _input;
    private final SportIdentMessageQueue _messageQueue;
    private byte[] _accumulator;
    private int _accSize;
    private long _lastTime;
    private final int _timeoutDelay;

    public RxtxCommReader( InputStream input, SportIdentMessageQueue messageQueue ) {
        this(input, messageQueue, 500);
    }

    public RxtxCommReader( InputStream input, SportIdentMessageQueue messageQueue, int timeout ) {
        _input = input;
        _messageQueue = messageQueue;
        _timeoutDelay = timeout;
        _lastTime = 0;
    }

    public void serialEvent( SerialPortEvent event ) {
        try {
            checkTimeout();
            accumulate();
            if (_accSize == 1 && _accumulator[0] != 0x02) {
                sendMessage();
            } else {
                checkExpectedLength(_accumulator, _accSize);
            }
        } catch (IOException | InterruptedException e) {
            SportIdentLogger.error(" #serialEvent# " + e);
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public int getListeningEvents() {
        return SerialPort.LISTENING_EVENT_DATA_AVAILABLE;
    }

    private void resetAccumulator() {
        _accumulator = new byte[MAX_MESSAGE_SIZE];
        _accSize = 0;
    }

    private void accumulate() throws IOException {
        _accSize += _input.read(_accumulator, _accSize, MAX_MESSAGE_SIZE - _accSize);
    }

    private void checkTimeout() {
        long currentTime = System.currentTimeMillis();
        if (currentTime > _lastTime + _timeoutDelay) {
            resetAccumulator();
        }
        _lastTime = currentTime;
    }

    private void checkExpectedLength( byte[] accumulator, int accSize ) throws InterruptedException {
        if (completeMessage(accumulator, accSize)) {
            sendMessage();
        } else {
            SportIdentLogger.debug("Fragment");
        }
    }

    private boolean completeMessage( byte[] answer, int nbReadBytes ) {
        return (answer[2] & 0xFF) == nbReadBytes - METADATA_SIZE;
    }

    private void sendMessage() throws InterruptedException {
        queueMessage(extractMessage(_accumulator, _accSize));
        resetAccumulator();
    }

    private void queueMessage( SportIdentMessage message ) throws InterruptedException {
        SportIdentLogger.log("READ", message.toString());
        _messageQueue.put(message);
    }

    private SportIdentMessage extractMessage( byte[] answer, int nbBytes ) {
        return new SportIdentMessage(Arrays.copyOfRange(answer, 0, nbBytes));
    }
}
