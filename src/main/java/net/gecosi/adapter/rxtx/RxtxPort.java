/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.adapter.rxtx;

import com.fazecast.jSerialComm.SerialPort;

import net.gecosi.internal.CommWriter;
import net.gecosi.internal.SportIdentMessageQueue;
import net.gecosi.internal.SportIdentPort;

public class RxtxPort implements SportIdentPort {
    private final SerialPort _port;

    public RxtxPort( SerialPort port ) {
        _port = port;
    }

    public SportIdentMessageQueue createMessageQueue()  {
        SportIdentMessageQueue messageQueue = new SportIdentMessageQueue(10);
        _port.addDataListener(new RxtxCommReader(_port.getInputStream(), messageQueue));
        return messageQueue;
    }

    public CommWriter createWriter() {
        return new RxtxCommWriter(_port.getOutputStream());
    }

    public void setupHighSpeed() {
        setSpeed(38400);
    }

    public void setupLowSpeed() {
        setSpeed(4800);
    }

    public void setSpeed( int baudRate ) {
        _port.setComPortParameters(baudRate, 8, SerialPort.ONE_STOP_BIT, SerialPort.NO_PARITY);
    }

    public void close() {
        _port.closePort();
    }
}
