/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.adapter.rxtx;

import java.io.IOException;
import java.io.OutputStream;

import net.gecosi.internal.CommWriter;
import net.gecosi.internal.SportIdentLogger;
import net.gecosi.internal.SportIdentMessage;

public class RxtxCommWriter implements CommWriter {
    private final OutputStream _output;

    public RxtxCommWriter( OutputStream out ) {
        _output = out;
    }

    public void write( SportIdentMessage message ) throws IOException {
        SportIdentLogger.log("SEND", message.toString());
        if (_output == null) {
            throw new IOException();
        }
        _output.write(message.sequence());
    }
}
