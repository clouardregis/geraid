/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi;

import com.fazecast.jSerialComm.SerialPort;
import com.fazecast.jSerialComm.SerialPortInvalidPortException;
import net.gecosi.adapter.rxtx.RxtxPort;
import net.gecosi.dataframe.SportIdentDataFrame;
import net.gecosi.internal.SportIdentLogger;
import net.gecosi.internal.SportIdentDriver;

import java.io.IOException;
import java.util.concurrent.ArrayBlockingQueue;

public class SportIdentHandler implements Runnable {
    private final ArrayBlockingQueue<SportIdentDataFrame> _dataQueue;
    private final SportIdentListener _sportIdentListener;
    private long _zeroHourInMilliseconds;
    private SportIdentDriver _driver;
    private Thread _thread;

    public SportIdentHandler( SportIdentListener sportIdentListener ) {
        _dataQueue = new ArrayBlockingQueue<>(5);
        _sportIdentListener = sportIdentListener;
    }

    public void setZeroHourInMilliseconds( long hour ) {
        _zeroHourInMilliseconds = hour;
    }

    public void connect( String portName ) throws IOException, SerialPortInvalidPortException {
        SerialPort port = SerialPort.getCommPort(portName);
        port.openPort();
        SportIdentLogger.open("######");
        SportIdentLogger.logTime("Start " + portName);
        start();
        _driver = new SportIdentDriver(new RxtxPort(port), this).start();
    }

    public void start() {
        _thread = new Thread(this);
        _thread.start();
    }

    public void stop() {
        if (_driver != null) {
            _driver.interrupt();
        }
        if (_thread != null) {
            _thread.interrupt();
        }
    }

    public boolean isAlive() {
        return _thread != null && _thread.isAlive();
    }

    public void notify( SportIdentDataFrame data ) {
        data.startingAt(_zeroHourInMilliseconds);
        _dataQueue.offer(data);
    }

    public void notify( CommStatus status ) {
        SportIdentLogger.log("!", status.name());
        _sportIdentListener.notify(status);
    }

    public void notifyError( CommStatus errorStatus, String errorMessage ) {
        SportIdentLogger.error(errorMessage);
        _sportIdentListener.notify(errorStatus, errorMessage);
    }

    public void run() {
        try {
            SportIdentDataFrame dataFrame;
            while ((dataFrame = _dataQueue.take()) != null) {
                _sportIdentListener.handleEcard(dataFrame);
            }
        } catch (InterruptedException e) {
            _dataQueue.clear();
            Thread.currentThread().interrupt();
        }
    }
}
