/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.internal;

import java.io.IOException;

public interface SportIdentPort {
    SportIdentMessageQueue createMessageQueue() throws IOException;

    CommWriter createWriter() throws IOException;

    void setupHighSpeed();

    void setupLowSpeed();

    void close();
}
