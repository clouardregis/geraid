/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.internal;

public class InvalidMessage extends Exception {
    private static final long serialVersionUID = 8582804744437652882L;
    private final SportIdentMessage _receivedMessage;

    public InvalidMessage( SportIdentMessage receivedMessage ) {
        _receivedMessage = receivedMessage;
    }

    public SportIdentMessage receivedMessage() {
        return _receivedMessage;
    }
}
