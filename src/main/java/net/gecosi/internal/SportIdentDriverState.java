/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.internal;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

import net.gecosi.CommStatus;
import net.gecosi.SportIdentHandler;
import net.gecosi.dataframe.SportIdent5DataFrame;
import net.gecosi.dataframe.SportIdent6DataFrame;
import net.gecosi.dataframe.SportIdent8PlusDataFrame;
import net.gecosi.dataframe.SportIdentDataFrame;

public enum SportIdentDriverState {
    STARTUP {
        @Override
        public SportIdentDriverState send( CommWriter writer, SportIdentHandler sportIdentHandler ) throws IOException {
            writer.write(SportIdentMessage.startup_sequence);
            return STARTUP_CHECK;
        }
    },

    STARTUP_CHECK {
        @Override
        public SportIdentDriverState receive( SportIdentMessageQueue queue, CommWriter writer, SportIdentHandler sportIdentHandler )
                throws IOException, InterruptedException, TimeoutException, InvalidMessage {
            pollAnswer(queue, SportIdentMessage.SET_MASTER_MODE);
            return GET_CONFIG.send(writer, sportIdentHandler);
        }
    },

    STARTUP_TIMEOUT {
        @Override
        public boolean isError() {
            return true;
        }

        @Override
        public String status() {
            return "Master station did not answer to startup sequence (high/low baud)";
        }
    },

    GET_CONFIG {
        @Override
        public SportIdentDriverState send( CommWriter writer, SportIdentHandler sportIdentHandler ) throws IOException {
            writer.write(SportIdentMessage.get_protocol_configuration);
            return CONFIG_CHECK;
        }
    },

    CONFIG_CHECK {
        @Override
        public SportIdentDriverState receive( SportIdentMessageQueue queue, CommWriter writer, SportIdentHandler sportIdentHandler )
                throws IOException, InterruptedException, TimeoutException, InvalidMessage {
            SportIdentMessage message = pollAnswer(queue, SportIdentMessage.GET_SYSTEM_VALUE);
            byte cpcByte = message.sequence(6);
            if ((cpcByte & CONFIG_CHECK_MASK) == CONFIG_CHECK_MASK) {
                return GET_SPORT_IDENT_6_CARDBLOCKS.send(writer, sportIdentHandler);
            }
            if ((cpcByte & EXTENDED_PROTOCOL_MASK) == 0) {
                return EXTENDED_PROTOCOL_ERROR;
            } else {
                return HANDSHAKE_MODE_ERROR;
            }
        }
    },

    EXTENDED_PROTOCOL_ERROR {
        @Override
        public boolean isError() {
            return true;
        }

        @Override
        public String status() {
            return "Master station should be configured with extended protocol";
        }
    },

    HANDSHAKE_MODE_ERROR {
        @Override
        public boolean isError() {
            return true;
        }

        @Override
        public String status() {
            return "Master station should be configured in handshake mode (no autosend)";
        }
    },

    GET_SPORT_IDENT_6_CARDBLOCKS {
        @Override
        public SportIdentDriverState send( CommWriter writer, SportIdentHandler sportIdentHandler ) throws IOException {
            writer.write(SportIdentMessage.get_cardblocks_configuration);
            return SI6_CARDBLOCKS_SETTING;
        }
    },

    SI6_CARDBLOCKS_SETTING {
        @Override
        public SportIdentDriverState receive( SportIdentMessageQueue queue, CommWriter writer, SportIdentHandler sportIdentHandler )
                throws IOException, InterruptedException, TimeoutException, InvalidMessage {
            SportIdentMessage message = pollAnswer(queue, SportIdentMessage.GET_SYSTEM_VALUE);
            sport_ident_6_192PunchesMode = (message.sequence(6) & 0xFF) == 0xFF;
            SportIdentLogger.info("SiCard6 192 Punches Mode " + (sport_ident_6_192PunchesMode ? "Enabled" : "Disabled"));
            return STARTUP_COMPLETE.send(writer, sportIdentHandler);
        }
    },

    STARTUP_COMPLETE {
        @Override
        public SportIdentDriverState send( CommWriter writer, SportIdentHandler sportIdentHandler ) throws IOException {
            writer.write(SportIdentMessage.beep_twice);
            sportIdentHandler.notify(CommStatus.ON);
            return DISPATCH_READY;
        }
    },

    DISPATCH_READY {
        @Override
        public SportIdentDriverState receive( SportIdentMessageQueue queue, CommWriter writer, SportIdentHandler sportIdentHandler ) throws IOException, InterruptedException {
            sportIdentHandler.notify(CommStatus.READY);
            SportIdentMessage message = queue.take();
            sportIdentHandler.notify(CommStatus.PROCESSING);
            switch (message.commandByte()) {
                case SportIdentMessage.SPORT_IDENT_CARD_5_DETECTED:
                    return RETRIEVE_SPORT_IDENT_CARD_5_DATA.retrieve(queue, writer, sportIdentHandler);
                case SportIdentMessage.SPORT_IDENT_CARD_6_PLUS_DETECTED:
                    return RETRIEVE_SPORT_IDENT_CARD_6_DATA.retrieve(queue, writer, sportIdentHandler);
                case SportIdentMessage.SPORT_IDENT_CARD_8_PLUS_DETECTED:
                    return dispatchSportIdentCard8Plus(message, queue, writer, sportIdentHandler);
                case SportIdentMessage.BEEP:
                    break;
                case SportIdentMessage.SPORT_IDENT_CARD_REMOVED:
                    SportIdentLogger.debug("Late removal " + message);
                    break;
                default:
                    SportIdentLogger.debug("Unexpected message " + message);
            }
            return DISPATCH_READY;
        }

        private SportIdentDriverState dispatchSportIdentCard8Plus( SportIdentMessage message, SportIdentMessageQueue queue, CommWriter writer, SportIdentHandler sportIdentHandler )
                throws IOException, InterruptedException {
            if (message.sequence(SportIdentMessage.SI3_NUMBER_INDEX) == SportIdentMessage.SPORT_IDENT_CARD_10_PLUS_SERIES) {
                return RETRIEVE_SPORT_IDENT_CARD_10_PLUS_DATA.retrieve(queue, writer, sportIdentHandler);
            } else {
                return RETRIEVE_SPORT_IDENT_CARD_8_9_DATA.retrieve(queue, writer, sportIdentHandler);
            }
        }
    },

    RETRIEVE_SPORT_IDENT_CARD_5_DATA {
        @Override
        public SportIdentDriverState retrieve( SportIdentMessageQueue queue, CommWriter writer, SportIdentHandler sportIdentHandler )
                throws IOException, InterruptedException {
            return retrieveDataMessages(queue, writer, sportIdentHandler, new SportIdentMessage[]{SportIdentMessage.read_sport_ident_card_5}, -1,
                    "Timeout on retrieving SiCard 5 data");
        }

        @Override
        public SportIdent5DataFrame createDataFrame( SportIdentMessage[] dataMessages ) {
            return new SportIdent5DataFrame(dataMessages[0]);
        }
    },

    RETRIEVE_SPORT_IDENT_CARD_6_DATA {
        private final SportIdentMessage[] readoutCommands = new SportIdentMessage[]{
                SportIdentMessage.read_sport_ident_card_6_b0,
                SportIdentMessage.read_sport_ident_card_6_b6,
                SportIdentMessage.read_sport_ident_card_6_b7,
                SportIdentMessage.read_sport_ident_card_6_plus_b2,
                SportIdentMessage.read_sport_ident_card_6_plus_b3,
                SportIdentMessage.read_sport_ident_card_6_plus_b4,
                SportIdentMessage.read_sport_ident_card_6_plus_b5
        };

        @Override
        public SportIdentDriverState retrieve( SportIdentMessageQueue queue, CommWriter writer, SportIdentHandler sportIdentHandler )
                throws IOException, InterruptedException {
            final int nbPunchesIndex = SportIdent6DataFrame.NB_PUNCHES_INDEX + 6;
            return retrieveDataMessages(queue, writer, sportIdentHandler, readoutCommands, nbPunchesIndex,
                    "Timeout on retrieving SiCard 6 data");
        }

        @Override
        public SportIdentDataFrame createDataFrame( SportIdentMessage[] dataMessages ) {
            return new SportIdent6DataFrame(dataMessages);
        }
    },

    RETRIEVE_SPORT_IDENT_CARD_8_9_DATA {
        @Override
        public SportIdentDriverState retrieve( SportIdentMessageQueue queue, CommWriter writer, SportIdentHandler sportIdentHandler )
                throws IOException, InterruptedException {
            return retrieveDataMessages(queue, writer, sportIdentHandler,
                    new SportIdentMessage[]{SportIdentMessage.read_sport_ident_card_8_plus_b0, SportIdentMessage.read_sport_ident_card_8_plus_b1},
                    -1, "Timeout on retrieving SiCard 8/9 data");
        }

        @Override
        public SportIdentDataFrame createDataFrame( SportIdentMessage[] dataMessages ) {
            return new SportIdent8PlusDataFrame(dataMessages);
        }
    },

    RETRIEVE_SPORT_IDENT_CARD_10_PLUS_DATA {
        private final SportIdentMessage[] readoutCommands = new SportIdentMessage[]{
                SportIdentMessage.read_sport_ident_card_10_plus_b0,
                SportIdentMessage.read_sport_ident_card_10_plus_b4,
                SportIdentMessage.read_sport_ident_card_10_plus_b5,
                SportIdentMessage.read_sport_ident_card_10_plus_b6,
                SportIdentMessage.read_sport_ident_card_10_plus_b7
        };

        @Override
        public SportIdentDriverState retrieve( SportIdentMessageQueue queue, CommWriter writer, SportIdentHandler sportIdentHandler )
                throws IOException, InterruptedException {
            final int nbPunchesIndex = SportIdent8PlusDataFrame.NB_PUNCHES_INDEX + 6;
            return retrieveDataMessages(queue, writer, sportIdentHandler, readoutCommands, nbPunchesIndex,
                    "Timeout on retrieving SiCard 10/11/SIAC data");
        }

        @Override
        public SportIdentDataFrame createDataFrame( SportIdentMessage[] dataMessages ) {
            return new SportIdent8PlusDataFrame(dataMessages);
        }
    },

    ACK_READ {
        @Override
        public SportIdentDriverState send( CommWriter writer, SportIdentHandler sportIdentHandler ) throws IOException {
            writer.write(SportIdentMessage.ack_sequence);
            return WAIT_SPORT_IDENT_CARD_REMOVAL;
        }
    },

    WAIT_SPORT_IDENT_CARD_REMOVAL {
        @Override
        public SportIdentDriverState receive( SportIdentMessageQueue queue, CommWriter writer, SportIdentHandler sportIdentHandler ) throws InterruptedException {
            try {
                pollAnswer(queue, SportIdentMessage.SPORT_IDENT_CARD_REMOVED);
                return DISPATCH_READY;
            } catch (TimeoutException e) {
                SportIdentLogger.info("Timeout on SiCard removal");
                return DISPATCH_READY;
            } catch (InvalidMessage e) {
                return errorFallback(sportIdentHandler, "Invalid message: " + e.receivedMessage().toString());
            }
        }
    };

    private static final int EXTENDED_PROTOCOL_MASK = 1;

    private static final int HANDSHAKE_MODE_MASK = 4;

    private static final int CONFIG_CHECK_MASK = EXTENDED_PROTOCOL_MASK | HANDSHAKE_MODE_MASK;

    private static boolean sport_ident_6_192PunchesMode = false;

    public static boolean sport_ident_card6_192PunchesMode() {
        return sport_ident_6_192PunchesMode;
    }

    protected static void setSportIdentCard6_192PunchesMode( boolean flag ) {
        sport_ident_6_192PunchesMode = flag;
    }

    public SportIdentDriverState send( CommWriter writer, SportIdentHandler sportIdentHandler ) throws IOException {
        wrongCall();
        return this;
    }

    public SportIdentDriverState receive( SportIdentMessageQueue queue, CommWriter writer, SportIdentHandler sportIdentHandler ) throws IOException, InterruptedException, TimeoutException, InvalidMessage {
        wrongCall();
        return this;
    }

    public SportIdentDriverState retrieve( SportIdentMessageQueue queue, CommWriter writer, SportIdentHandler sportIdentHandler ) throws IOException, InterruptedException {
        wrongCall();
        return this;
    }

    public SportIdentDataFrame createDataFrame( SportIdentMessage[] dataMessages ) {
        wrongCall();
        return null;
    }

    private void wrongCall() {
        throw new RuntimeException(String.format("This method should not be called on %s", this.name()));
    }

    public boolean isError() {
        return false;
    }

    public String status() {
        return name();
    }

    protected void checkAnswer( SportIdentMessage message, byte command ) throws InvalidMessage {
        if (!message.check(command)) {
            throw new InvalidMessage(message);
        }
    }

    protected SportIdentMessage pollAnswer( SportIdentMessageQueue queue, byte command )
            throws InterruptedException, TimeoutException, InvalidMessage {
        SportIdentMessage message = queue.timeoutPoll();
        checkAnswer(message, command);
        return message;
    }

    protected int extractNumberOfDataBlocks( SportIdentMessage firstBlock, int nbPunchesIndex ) {
        int nbPunches = firstBlock.sequence(nbPunchesIndex) & 0xFF;
        int nbPunchesPerBlock = 32;
        int nbPunchDataBlocks = (nbPunches / nbPunchesPerBlock) + Math.min(1, nbPunches % nbPunchesPerBlock);
        SportIdentLogger.info(String.format("Nb punches/Data blocks: %s/%s", nbPunches, nbPunchDataBlocks));
        return nbPunchDataBlocks + 1;
    }

    protected SportIdentDriverState retrieveDataMessages( SportIdentMessageQueue queue, CommWriter writer, SportIdentHandler sportIdentHandler,
                                                          SportIdentMessage[] readoutCommands, int nbPunchesIndex, String timeoutMessage ) throws IOException, InterruptedException {
        try {
            SportIdentLogger.stateChanged(name());
            SportIdentMessage readoutCommand = readoutCommands[0];
            writer.write(readoutCommand);
            SportIdentMessage firstDataBlock = pollAnswer(queue, readoutCommand.commandByte());
            int nbDataBlocks = (nbPunchesIndex == -1) ?
                    readoutCommands.length :
                    extractNumberOfDataBlocks(firstDataBlock, nbPunchesIndex);
            SportIdentMessage[] dataMessages = new SportIdentMessage[nbDataBlocks];
            dataMessages[0] = firstDataBlock;
            for (int i = 1; i < nbDataBlocks; i++) {
                readoutCommand = readoutCommands[i];
                writer.write(readoutCommand);
                dataMessages[i] = pollAnswer(queue, readoutCommand.commandByte());
            }
            sportIdentHandler.notify(createDataFrame(dataMessages));
            return ACK_READ.send(writer, sportIdentHandler);
        } catch (TimeoutException e) {
            return errorFallback(sportIdentHandler, timeoutMessage);
        } catch (InvalidMessage e) {
            return errorFallback(sportIdentHandler, "Invalid message: " + e.receivedMessage().toString());
        }
    }

    protected SportIdentDriverState errorFallback( SportIdentHandler sportIdentHandler, String errorMessage ) {
        SportIdentLogger.error(errorMessage);
        sportIdentHandler.notify(CommStatus.PROCESSING_ERROR);
        return DISPATCH_READY;
    }
}
