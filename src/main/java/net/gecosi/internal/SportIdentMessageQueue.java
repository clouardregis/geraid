/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.internal;

import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class SportIdentMessageQueue extends ArrayBlockingQueue<SportIdentMessage> {
    private final long _defaultTimeout;

    public SportIdentMessageQueue( int capacity ) {
        this(capacity, 2000);
    }

    public SportIdentMessageQueue( int capacity, long defaultTimeout ) {
        super(capacity);
        _defaultTimeout = defaultTimeout;
    }

    public SportIdentMessage timeoutPoll() throws InterruptedException, TimeoutException {
        SportIdentMessage message = poll(_defaultTimeout, TimeUnit.MILLISECONDS);
        if (message != null) {
            return message;
        } else {
            throw new TimeoutException();
        }
    }
}
