/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.internal;

import net.gecosi.CommStatus;
import net.gecosi.SportIdentHandler;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

public class SportIdentDriver implements Runnable {
    private final SportIdentPort _sportIdentPort;
    private final CommWriter _writer;
    private final SportIdentMessageQueue _messageQueue;
    private final SportIdentHandler _sportIdentHandler;
    private Thread _thread;

    public SportIdentDriver( SportIdentPort sportIdentPort, SportIdentHandler sportIdentHandler ) throws IOException {
        _sportIdentPort = sportIdentPort;
        _messageQueue = sportIdentPort.createMessageQueue();
        _writer = sportIdentPort.createWriter();
        _sportIdentHandler = sportIdentHandler;
    }

    public SportIdentDriver start() {
        _thread = new Thread(this);
        _thread.start();
        return this;
    }

    public void interrupt() {
        _thread.interrupt();
    }

    @Override
    public void run() {
        try {
            SportIdentDriverState currentState = startupBootstrap();
            while (isAlive(currentState)) {
                SportIdentLogger.stateChanged(currentState.name());
                currentState = currentState.receive(_messageQueue, _writer, _sportIdentHandler);
            }
            if (currentState.isError()) {
                _sportIdentHandler.notifyError(CommStatus.FATAL_ERROR, currentState.status());
            }
        } catch (InterruptedException e) {
            // normal way out
            Thread.currentThread().interrupt();
        } catch (Exception e) {
            _sportIdentHandler.notifyError(CommStatus.FATAL_ERROR, null);
            SportIdentLogger.error(" #run# " + e);
        } finally {
            stop();
        }
    }

    private boolean isAlive( SportIdentDriverState currentState ) {
        return !(_thread.isInterrupted() || currentState.isError());
    }

    private SportIdentDriverState startupBootstrap() throws IOException, InterruptedException, InvalidMessage {
        try {
            _sportIdentHandler.notify(CommStatus.STARTING);
            _sportIdentPort.setupHighSpeed();
            return startup();
        } catch (TimeoutException e) {
            try {
                _sportIdentPort.setupLowSpeed();
                return startup();
            } catch (TimeoutException e1) {
                return SportIdentDriverState.STARTUP_TIMEOUT;
            }
        }
    }

    private SportIdentDriverState startup() throws IOException, InterruptedException, TimeoutException, InvalidMessage {
        return SportIdentDriverState.STARTUP.send(_writer, _sportIdentHandler).receive(_messageQueue, _writer, _sportIdentHandler);
    }

    private void stop() {
        _sportIdentPort.close();
        _sportIdentHandler.notify(CommStatus.OFF);
        SportIdentLogger.close();
    }
}
