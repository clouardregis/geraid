/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.internal;

import java.io.IOException;

public interface CommWriter {
    void write( SportIdentMessage message ) throws IOException;
}
