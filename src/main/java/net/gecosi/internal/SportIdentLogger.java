/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.internal;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Date;

public class SportIdentLogger {
    public static class NullWriter extends Writer {
        @Override
        public void close() {
            // Nothing to do
        }

        @Override
        public void flush() {
            // Nothing to do
        }

        @Override
        public void write( char[] cbuf, int off, int len ) {
            // Nothing to do
        }
    }

    public static class OutStreamWriter extends Writer {
        @Override
        public void close() {
            // Nothing to do
        }

        @Override
        public void flush() {
            // Nothing to do
        }

        @Override
        public void write( char[] cbuf, int off, int len ) {
            for (int i = off; i < off + len; i++) {
                System.out.print(cbuf[i]);
            }
        }
    }

    private static Writer logger;

    public static void open() {
        if (logger != null) {
            close();
        }
        String logProp = System.getProperty("GECOSI_LOG", "FILE");
        switch (logProp) {
            case "FILE":
                openFileLogger();
                break;
            case "NONE":
                openNullLogger();
                break;
            case "OUTSTREAM":
                openOutStreamLogger();
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + logProp);
        }
    }

    public static void openFileLogger() {
        // TODO mettre ce fichier dans le dossier .gecoraid ?
        try {
            logger = new BufferedWriter(new FileWriter("gecosi.log", true));
        } catch (IOException e) {
            openOutStreamLogger();
        }
    }

    public static void openNullLogger() {
        logger = new NullWriter();
    }

    public static void openOutStreamLogger() {
        logger = new OutStreamWriter();
    }

    public static void open( String header ) {
        open();
        log(header, "");
    }

    public static void log( String header, String message ) {
        try {
            logger.write(String.format("%s %s%n", header, message));
            logger.flush();
        } catch (IOException e) {
            // Nothing to do
        }
    }

    public static void logTime( String message ) {
        log(new Date().toString(), message);
    }

    public static void stateChanged( String message ) {
        log("-->", message);
    }

    public static void info( String message ) {
        log("[Info]", message);
    }

    public static void debug( String message ) {
        log("[Debug]", message);
    }

    public static void error( String message ) {
        log("[Error]", message);
    }

    public static void close() {
        try {
            logger.close();
            logger = null;
        } catch (IOException e) {
            // Nothing to do
        }
    }
}
