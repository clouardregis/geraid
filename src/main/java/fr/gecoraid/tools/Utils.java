package fr.gecoraid.tools;

import java.io.File;

// TODO clean code
// TODO put in the convenient folder
public class Utils {
    public static String checkExtension( String fichier, String extension ) {
        StringBuilder retour = new StringBuilder();
        if (!fichier.contains(extension)) {
            retour.append(fichier);
            retour.append(extension);
      /*
      int index = fichier.lastIndexOf(".");
      if(index == -1)
      {
        retour.append(fichier);
        retour.append(extension);
       }
      else
      {
        retour.append(fichier.substring(0, index));
        retour.append(extension);
      }
      */
        } else {
            retour.append(fichier);
        }
        return retour.toString();
    }

    public static String getNom( String fichier ) {
        int indexDebut = fichier.lastIndexOf(File.separatorChar);
        int indexFin = fichier.lastIndexOf(".");
        return fichier.substring(indexDebut + 1, indexFin);
    }

    public static int parseInt( String temps ) {
        if (temps.substring(0, 1).compareTo("-") == 0) {
            temps = temps.substring(1);
            return (getHeure(temps) * 3600 + getMinute(temps) * 60 + getSecond(temps)) * (-1);
        } else {
            return getHeure(temps) * 3600 + getMinute(temps) * 60 + getSecond(temps);
        }
    }

    private static int getSecond( String temps ) {
        return Integer.parseInt(temps.substring(temps.lastIndexOf(":") + 1));
    }

    private static int getMinute( String temps ) {
        return Integer.parseInt(temps.substring(temps.indexOf(":") + 1, temps.lastIndexOf(":")));
    }

    private static int getHeure( String temps ) {
        return Integer.parseInt(temps.substring(0, temps.indexOf(":")));
    }

    public static boolean estUnEntier( String chaine ) {
        try {
            Integer.parseInt(chaine);
        } catch (NumberFormatException e) {
            return false;
        }
        return true;
    }
}
