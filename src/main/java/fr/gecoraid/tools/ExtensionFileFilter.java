package fr.gecoraid.tools;

import javax.swing.filechooser.FileFilter;
import java.io.File;
import java.util.Enumeration;
import java.util.Hashtable;

public class ExtensionFileFilter extends FileFilter {
    private Hashtable<String, ExtensionFileFilter> _filters;
    private String _description = null;
    private String _fullDescription = null;

    private ExtensionFileFilter() {
        _filters = new Hashtable<>();
    }

    public ExtensionFileFilter( String extension, String description ) {
        this();
        if (extension != null) {
            addExtension(extension);
        }
        if (description != null) {
            setDescription(description);
        }
    }

    @Override
    public boolean accept( File file ) {
        if (file != null) {
            if (file.isDirectory()) {
                return true;
            }
            String extension = getExtension(file);
            return extension != null && _filters.get(getExtension(file)) != null;
        }
        return false;
    }

    private String getExtension( File file ) {
        if (file != null) {
            String filename = file.getName();
            int i = filename.lastIndexOf('.');
            if (i > 0 && i < filename.length() - 1) {
                return filename.substring(i + 1).toLowerCase();
            }
        }
        return null;
    }

    private void addExtension( String extension ) {
        if (_filters == null) {
            _filters = new Hashtable<>(5);
        }
        _filters.put(extension.toLowerCase(), this);
        _fullDescription = null;
    }

    @Override
    public String getDescription() {
        if (_fullDescription == null) {
            _fullDescription = _description == null ? "(" : _description + " (";
            final Enumeration<String> extensions = _filters.keys();
            if (extensions != null) {
                _fullDescription += "." + extensions.nextElement();
                StringBuilder elements = new StringBuilder();
                while (extensions.hasMoreElements()) {
                    elements.append(", .").append(extensions.nextElement());
                }
                _fullDescription += elements.toString();
            }
            _fullDescription += ")";
        }
        return _fullDescription;
    }

    private void setDescription( String description ) {
        _description = description;
        _fullDescription = null;
    }
}
