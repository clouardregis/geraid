package fr.gecoraid.desktop.widget;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;

import static fr.gecoraid.NimbusTheme.GREEN_1;

public class IconButton extends JButton {
    static final Color HIGHLIGTH_COLOR = new Color(GREEN_1.getRed(), GREEN_1.getGreen(), GREEN_1.getBlue(), 30);

    public IconButton( String iconName ) {
        setOpaque(false);
        setContentAreaFilled(false);
        setPreferredSize(new Dimension(30, 30));
        //noinspection ConstantConditions
        setIcon(new ImageIcon(getClass().getClassLoader().getResource("fr/gecoraid/icon/" + iconName)));
    }

    @Override
    protected void paintComponent( Graphics g ) {
        if (getModel().isPressed()) {
            g.setColor(HIGHLIGTH_COLOR);
        } else {
            g.setColor(getBackground());
        }
        g.fillRect(0, 0, getWidth(), getHeight());
        super.paintComponent(g);
    }
}
