package fr.gecoraid.desktop.widget;

import fr.gecoraid.L10n;

import javax.swing.JOptionPane;

public class ErrorPane {
    private ErrorPane() {
    }

    public static void showMessageDialog( Object message ) {
        JOptionPane.showMessageDialog(null,
                message,
                L10n.getString("error"),
                JOptionPane.ERROR_MESSAGE);
    }
}
