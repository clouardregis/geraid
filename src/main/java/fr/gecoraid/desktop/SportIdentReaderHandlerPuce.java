/**
 * Copyright (c) 2009 Simon Denier
 * Released under the MIT License (see LICENSE file)
 */
package fr.gecoraid.desktop;

import com.fazecast.jSerialComm.SerialPortInvalidPortException;
import fr.gecoraid.desktop.widget.ErrorPane;
import fr.gecoraid.model.Equipe;
import fr.gecoraid.model.Parcours;
import fr.gecoraid.model.Raid;
import net.gecosi.CommStatus;
import net.gecosi.SportIdentHandler;
import net.gecosi.SportIdentListener;
import net.gecosi.dataframe.SportIdentDataFrame;

import javax.swing.JComboBox;
import java.io.IOException;

// TODO code review
// TODO refactor code
// TODO Revoir le nom de la classe
// TODO mettre une visibilite package
public class SportIdentReaderHandlerPuce implements SportIdentListener {
    private String _portName = "";
    private SportIdentHandler _portHandler;
    private final Raid _geraid;
    private final DeskTopPresenter _presenter;

    public SportIdentReaderHandlerPuce( Raid geraid, DeskTopPresenter presenter ) {
        _geraid = geraid;
        _presenter = presenter;
    }

    public boolean start() {
        if (_portHandler == null) {
            _portHandler = new SportIdentHandler(this);
        }
        _portHandler.setZeroHourInMilliseconds(0);
        try {
            _portHandler.connect(_portName);
            return true;
        } catch (IOException | SerialPortInvalidPortException ignored) {
            return false;
        }
    }

    public void stop() {
        if (_portHandler != null) {
            _portHandler.stop();
        }
    }

    @Override
    public void handleEcard( SportIdentDataFrame card ) {
        if (_geraid.existePuce(card.getSiNumber())) {
            // TODO L10n.getString()
            ErrorPane.showMessageDialog("Cette puce existe déjà pour ce raid.");
        } else {
            final JComboBox<Parcours> combo = _presenter.getComboBoxParcours();
            //noinspection ConstantConditions
            Equipe equipe = _geraid.getEquipeSansPuce((Parcours) combo.getSelectedItem(), _presenter.getListEquipes().getSelectedIndex());
            if (equipe != null) {
                equipe.setIdPuce(card.getSiNumber());
                _presenter.getListEquipes().repaint();
            } else {
                // TODO L10n.getString()
                ErrorPane.showMessageDialog("Il n'y a plus d'équipe sans puce pour ce parcours à partir de la sélection.");
            }
        }
    }

    public void setPortName( String portName ) {
        _portName = portName;
    }

    @Override
    public void notify( CommStatus status ) {
        if (status == CommStatus.ON) {
            _presenter.displayStationStatus(SportIdentStationState.READY);
        }
    }

    @Override
    public void notify( CommStatus errorStatus, String errorMessage ) {
        _presenter.displayStationStatus(SportIdentStationState.FAILED);
    }
}
