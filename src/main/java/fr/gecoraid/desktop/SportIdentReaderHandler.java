/**
 * Copyright (c) 2009 Simon Denier
 * Released under the MIT License (see LICENSE file)
 */
package fr.gecoraid.desktop;

import com.fazecast.jSerialComm.SerialPortInvalidPortException;
import fr.gecoraid.model.Partial;
import fr.gecoraid.model.Puce;
import fr.gecoraid.model.ResultatPuce;
import net.gecosi.CommStatus;
import net.gecosi.SportIdentHandler;
import net.gecosi.SportIdentListener;
import net.gecosi.dataframe.SportIdentDataFrame;

import java.io.IOException;
import java.util.Date;

// TODO mettre une visibilite package
// TODO Refactor code
// TODO Revoir le nom de la classe
public class SportIdentReaderHandler implements SportIdentListener {
    private final DeskTopPresenter _presenter;

    private String _portName = "";
    private long _zeroTimeInMilliseconds = 9 * 3600 * 1000L; // 9:00
    private SportIdentHandler _portHandler;

    public SportIdentReaderHandler( DeskTopPresenter presenter ) {
        _presenter = presenter;
    }

    public void setPortName( String portName ) {
        _portName = portName;
    }

    public void setZeroTimeInMilliseconds( int hours, int minutes ) {
        _zeroTimeInMilliseconds = hours * 3600L * 1000L + minutes * 60L * 1000L;
    }

    public boolean start() {
        _portHandler = new SportIdentHandler(this);
        _portHandler.setZeroHourInMilliseconds(_zeroTimeInMilliseconds);
        try {
            _portHandler.connect(_portName);
            return true;
        } catch (IOException | SerialPortInvalidPortException ignored) {
            return false;
        }
    }

    public void stop() {
        if (_portHandler != null) {
            _portHandler.stop();
        }
    }

    @Override
    public void handleEcard( SportIdentDataFrame card ) {
        ResultatPuce result = _presenter.getRaid().getEquipe(card.getSiNumber());
        remplirResultatPuce(card, result);
        _presenter.ihmResultatPuce(result, result.getEquipe() != null, false);
    }

    @Override
    public void notify( CommStatus status ) {
        if (status == CommStatus.ON) {
            _presenter.displayStationStatus(SportIdentStationState.READY);
        }
    }

    @Override
    public void notify( CommStatus errorStatus, String errorMessage ) {
        _presenter.displayStationStatus(SportIdentStationState.FAILED);
    }

    private void remplirResultatPuce( SportIdentDataFrame card, ResultatPuce rp ) {
        final Puce puce = rp.getPuce();
        puce.setIdPuce(card.getSiNumber());
        puce.setErasetime(new Date(card.getCheckTime()));
        puce.setControltime(new Date(card.getCheckTime()));
        puce.setStarttime(new Date(card.getStartTime()));
        puce.setFinishtime(new Date(card.getFinishTime()));
        puce.setReadtime(new Date());
        Partial[] partiels = new Partial[card.getPunches().length];
        for (int i = 0; i < card.getPunches().length; i++) {
            Partial partiel = new Partial();
            partiel.setCode(card.getPunches()[i].code());
            partiel.setTime(new Date(card.getPunches()[i].timestamp()));
            partiels[i] = partiel;
        }
        puce.setPartiels(partiels);
    }
}
