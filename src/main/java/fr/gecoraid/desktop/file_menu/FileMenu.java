package fr.gecoraid.desktop.file_menu;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DeskTopPresenter;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;

public class FileMenu extends JMenu {
    private final DeskTopPresenter _presenter;

    public FileMenu( DeskTopPresenter presenter ) {
        super(L10n.getString("desktop.menu.file.title"));
        _presenter = presenter;
        _initializeUI();
    }

    private void _initializeUI() {
        add(_createMenuItemNewFile());
        add(_createMenuItemOpenFile());
        add(new JSeparator());
        add(_createMenuItemSaveInFile());
        add(_createMenuItemSaveAsInFile());
        add(_createMenuItemBackup());
        add(new JSeparator());
        add(_createMenuItemQuit());
    }

    private JMenuItem _createMenuItemNewFile() {
        JMenuItem menuItem = new JMenuItem(L10n.getString("desktop.menu.file.new"));
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));
        menuItem.addActionListener(event -> _presenter.createNewRaid());
        return menuItem;
    }

    private JMenuItem _createMenuItemOpenFile() {
        JMenuItem menuItem = new JMenuItem(L10n.getString("desktop.menu.file.open"));
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, InputEvent.CTRL_DOWN_MASK));
        menuItem.addActionListener(event -> _presenter.openRaidFile());
        return menuItem;
    }

    private JMenuItem _createMenuItemSaveInFile() {
        JMenuItem menuItem = new JMenuItem(L10n.getString("desktop.menu.file.save"));
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK));
        menuItem.addActionListener(event -> _presenter.saveCurrentRaid());
        return menuItem;
    }

    private JMenuItem _createMenuItemSaveAsInFile() {
        JMenuItem menuItem = new JMenuItem(L10n.getString("desktop.menu.file.save_as"));
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_DOWN_MASK + InputEvent.SHIFT_DOWN_MASK));
        menuItem.addActionListener(event -> _presenter.saveAsCurrentRaid());
        return menuItem;
    }

    private JMenuItem _createMenuItemBackup() {
        JMenuItem menuItem = new JMenuItem(L10n.getString("desktop.menu.file.backup"));
        menuItem.addActionListener(event -> _presenter.backup());
        return menuItem;
    }

    private JMenuItem _createMenuItemQuit() {
        JMenuItem menuItem = new JMenuItem(L10n.getString("desktop.menu.file.quit"));
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_Q, InputEvent.CTRL_DOWN_MASK));
        menuItem.addActionListener(event -> _presenter.quit());
        return menuItem;
    }
}
