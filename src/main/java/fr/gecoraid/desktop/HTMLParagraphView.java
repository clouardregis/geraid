package fr.gecoraid.desktop;

import javax.swing.text.Element;
import javax.swing.text.FlowView;
import javax.swing.text.View;
import javax.swing.text.html.ParagraphView;

// TODO mettre une visibilite package
public class HTMLParagraphView extends ParagraphView {
    private static final int MAX_VIEW_SIZE = 100;
    // TODO code review

    public HTMLParagraphView( Element elem ) {
        super(elem);
        strategy = new HTMLParagraphView.HTMLFlowStrategy();
    }

    public static class HTMLFlowStrategy extends FlowStrategy {
        @Override
        protected View createView( FlowView fv, int startOffset, int spanLeft, int rowIndex ) {
            View res = super.createView(fv, startOffset, spanLeft, rowIndex);
            if (res.getEndOffset() - res.getStartOffset() > MAX_VIEW_SIZE) {
                res = res.createFragment(startOffset, startOffset + MAX_VIEW_SIZE);
            }
            return res;
        }
    }

    @Override
    public int getResizeWeight( int axis ) {
        return 0;
    }
}
