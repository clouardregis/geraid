package fr.gecoraid.desktop;

import fr.gecoraid.L10n;
import fr.gecoraid.model.Equipe;
import fr.gecoraid.model.Etape;
import fr.gecoraid.model.Parcours;
import fr.gecoraid.model.ResultatEquipe;
import fr.gecoraid.model.ResultatPuce;
import fr.gecoraid.desktop.widget.IconButton;

import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import javax.print.attribute.standard.MediaPrintableArea;
import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JEditorPane;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.text.html.HTMLDocument;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;

// TODO mettre une visibilite package
public class IhmResultatPuce extends JDialog {
    private static final long serialVersionUID = 1L;
    private static final Double INITIAL_ZOOM = 1.5;

    private final transient DeskTopPresenter _presenter;

    // TODO séparer la vue du modèle
    private final transient ResultatPuce _resultatPuce;
    private JComboBox<Parcours> _comboBoxParcours;
    private JComboBox<Etape> _comboBoxEtapes;
    private JComboBox<Equipe> _comboBoxEquipes;

    private final boolean _equipeValide;
    private final boolean _visu;
    private final boolean _resultatReduit;
    private JLabel _labelParcours;
    private JLabel _label1;
    private JLabel _label2;
    private JButton _buttonOk;
    private JLabel _label3;
    private JScrollPane _scrollPane;
    private JLabel _label4;
    private JButton _buttonOkPrint;
    private JButton _buttonCreer;
    private JLabel _labelMessage;
    private JEditorPane _editorPaneResultat;
    private JButton _buttonPrint;
    private JButton _buttonRefresh;
    private JLabel _labelPostes;

    public IhmResultatPuce( JFrame parent, DeskTopPresenter presenter, ResultatPuce rp, boolean equipeValide, boolean visu, boolean resultatReduit ) {
        super(parent);
        _presenter = presenter; // TODO review code with MVP architecture
        _resultatPuce = rp;
        _equipeValide = equipeValide;
        _visu = visu;
        _resultatReduit = resultatReduit;
        _initializeUI();
        _buttonCreer.setEnabled(!equipeValide);
        if (visu) {
            // TODO L10n.getString()
            setTitle("Visualisation d'un résultat");
            _labelParcours.setVisible(false);
            _label1.setVisible(false);
            _label2.setVisible(false);
            _label3.setVisible(false);
            _comboBoxParcours.setVisible(false);
            _comboBoxEtapes.setVisible(false);
            _comboBoxEquipes.setVisible(false);
            _labelMessage.setVisible(false);
            _buttonOk.setVisible(false);
            _buttonOkPrint.setVisible(false);
            _buttonPrint.setVisible(true);
            _buttonCreer.setVisible(false);
            _scrollPane.setBounds(5, 5 + _buttonRefresh.getHeight(), 480, 380 - _buttonRefresh.getHeight());
        }
        _comboBoxParcours.setModel(new DefaultComboBoxModel<>(_presenter.getRaid().getParcourss().getParcourss()));
        // TODO L10n.getString()
        _label4.setText("Résultat de la puce : " + rp.getPuce().getIdPuce());
        if (equipeValide) {
            _comboBoxParcours.setSelectedItem(_resultatPuce.getParcours());
            ResultatEquipe re = new ResultatEquipe(rp, 0, 0);
            _labelPostes.setText(re.getListeCodeOkPm().toHtml());
            re.saveHtml(_presenter.getRaid(), resultatReduit);
            try {
                String adresse = new File(".").getCanonicalPath();
                _editorPaneResultat.setPage("file:///" + adresse + "/temp.html");
            } catch (IOException e) {
                // TODO Beep ?
                // TODO L10n.getString()
                System.err.format("Cannot print %s%n", e.getMessage());
            }
            if (_presenter.getRaid().existeResultatPuce(rp) != null) {
                // TODO Beep
                // TODO L10n.getString()
                _labelMessage.setText("<html>Un résultat existe déjà pour cette équipe sur cette étape.<br>" + "En validant ce résultat, vous remplacerez l'ancien.</html>");
                _miseAJourBoutons(false);
            }
        } else {
            _comboBoxParcours.setSelectedIndex(0);
            // TODO L10n.getString()
            StringBuilder message = new StringBuilder("<html>Le numéro de la puce insérée ne correspond à aucune équipe du raid.<br>");
            // TODO L10n.getString()
            message.append("Vous pouvez attribuer ce résultat à une équipe existante ou créer une nouvelle équipe.</html>");
            _labelMessage.setText(message.toString());
            _miseAJourBoutons(false);
            // TODO L10n.getString()
            JOptionPane.showMessageDialog(this, message.toString(), "Puce inconnue", JOptionPane.WARNING_MESSAGE);
        }
    }

    private void _initializeUI() {
        setModal(true);
        // TODO L10n.getString()
        setTitle("Résultat Puce");
        JPanel jContentPane = new JPanel(new BorderLayout());
        jContentPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        jContentPane.add(_getIdentificationResultat(), BorderLayout.NORTH);
        jContentPane.add(_createJPanelContenu(), BorderLayout.CENTER);
        jContentPane.add(_createButtonPanel(), BorderLayout.SOUTH);
        setContentPane(jContentPane);
        pack();
    }

    private JComponent _getIdentificationResultat() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        JPanel panel0 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        // TODO L10n.getString()
        _label3 = new JLabel("<html>Souhaitez-vous attribuer ce résultat à cette équipe<br>pour cette étape de ce parcours?</html>");
        panel0.add(_label3);
        panel.add(panel0);

        GridLayout gridLayout = new GridLayout(3, 2);
        JPanel panel1 = new JPanel(gridLayout);
        // TODO L10n.getString()
        _labelParcours = new JLabel("Parcours :  ");
        panel1.add(_labelParcours);
        panel1.add(_createJComboBoxParcours());
        // TODO L10n.getString()
        _label1 = new JLabel("Étape :  ");
        panel1.add(_label1);
        panel1.add(_createJComboBoxEtapes());
        // TODO L10n.getString()
        _label2 = new JLabel("Équipe :  ");
        panel1.add(_label2);
        panel1.add(_createJComboBoxEquipes());
        panel.add(panel1);
        return panel;
    }

    private JButton _createOkButton() {
        // TODO L10n.getString()
        _buttonOk = new JButton("Valider le résultat");
        _buttonOk.addActionListener(event -> {
            _resultatPuce.setParcours((Parcours) _comboBoxParcours.getSelectedItem());
            _resultatPuce.setEtape((Etape) _comboBoxEtapes.getSelectedItem());
            _resultatPuce.setEquipe((Equipe) _comboBoxEquipes.getSelectedItem());
            _resultatPuce.getEquipe().setIdPuce(_resultatPuce.getPuce().getIdPuce());
            ResultatPuce rp = _presenter.getRaid().existeResultatPuce(_resultatPuce);
            if (rp == null) {
                _presenter.getRaid().getResultatsPuce().addResultatPuce(_resultatPuce);
                _presenter.saveCurrentRaid();
            } else {
                _presenter.getRaid().removeResultatPuce(rp);
                _presenter.getRaid().addResultatPuce(_resultatPuce);
            }
            dispose();
        });
        return _buttonOk;
    }

    private JButton _createCancelButton() {
        JButton button = new JButton(L10n.getString("cancel"));
        button.addActionListener(event -> dispose());
        return button;
    }

    private JPanel _createJPanelContenu() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));

        JPanel panel4 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        // TODO L10n.getString()
        _label4 = new JLabel("Résultat :");
        _label4.setForeground(Color.blue);
        panel4.add(_label4);
        panel4.add(_createJButtonRefresh());
        panel.add(panel4);

        panel.add(_createJScrollPane());

        panel.add(_getScrollPaneRecapitulatif());

        JPanel panelMessage = new JPanel(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        gbc.weightx = 1;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        gbc.gridwidth = GridBagConstraints.REMAINDER;
        _labelMessage = new JLabel("");
        _labelMessage.setForeground(Color.red);
        _labelMessage.setHorizontalAlignment(SwingConstants.LEFT);
        panelMessage.add(_labelMessage, gbc);
        panel.add(panelMessage);

        return panel;
    }

    private JPanel _createButtonPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 30, 5));
        panel.add(_createOkButtonPrint());
        panel.add(_createJButtonPrint());
        panel.add(_createJButtonCreer());
        panel.add(_createOkButton());
        panel.add(_createCancelButton());
        return panel;
    }

    private JComboBox<Parcours> _createJComboBoxParcours() {
        _comboBoxParcours = new JComboBox<>();
        _comboBoxParcours.setPreferredSize(new Dimension(200, 25));
        _comboBoxParcours.addActionListener(event -> {
            if (_comboBoxParcours.getSelectedIndex() != -1) {
                assert (_comboBoxParcours.getSelectedItem() != null);
                _comboBoxEtapes.setModel(new DefaultComboBoxModel<>(((Parcours) (_comboBoxParcours.getSelectedItem())).getEtapes().getEtapes()));
                _comboBoxEquipes.setModel(new DefaultComboBoxModel<>(((Parcours) (_comboBoxParcours.getSelectedItem())).getEquipes().getEquipes()));
                if (_equipeValide) {
                    _comboBoxEtapes.setSelectedItem(_resultatPuce.getEtape());
                } else {
                    _comboBoxEtapes.setSelectedIndex(0);
                }
                _miseAJourResultat();
            }
        });
        return _comboBoxParcours;
    }

    private JComboBox<Etape> _createJComboBoxEtapes() {
        _comboBoxEtapes = new JComboBox<>();
        _comboBoxEtapes.setPreferredSize(new Dimension(200, 25));
        _comboBoxEtapes.addActionListener(event -> {
            if (_equipeValide) {
                _comboBoxEquipes.setSelectedItem(_resultatPuce.getEquipe());
            } else {
                _comboBoxEquipes.setSelectedIndex(0);
            }
            _miseAJourResultat();
        });
        return _comboBoxEtapes;
    }

    private JComboBox<Equipe> _createJComboBoxEquipes() {
        _comboBoxEquipes = new JComboBox<>();
        _comboBoxEquipes.addActionListener(event -> _miseAJourResultat());
        return _comboBoxEquipes;
    }

    private JScrollPane _createJScrollPane() {
        _scrollPane = new JScrollPane();
        _scrollPane.setPreferredSize(new Dimension(300, 450));
        _scrollPane.setViewportView(_createJEditorPaneResultat());
        return _scrollPane;
    }

    private JButton _createOkButtonPrint() {
        _buttonOkPrint = new JButton();
        // TODO L10n.getString()
        _buttonOkPrint.setToolTipText("Valider et imprimer le résultat");
        //noinspection ConstantConditions
        _buttonOkPrint.setIcon(new ImageIcon(getClass().getClassLoader().getResource("fr/gecoraid/icon/okPrint.png")));
        _buttonOkPrint.addActionListener(event -> {
            _printResultat();
            _resultatPuce.setParcours((Parcours) _comboBoxParcours.getSelectedItem());
            _resultatPuce.setEtape((Etape) _comboBoxEtapes.getSelectedItem());
            _resultatPuce.setEquipe((Equipe) _comboBoxEquipes.getSelectedItem());
            _resultatPuce.getEquipe().setIdPuce(_resultatPuce.getPuce().getIdPuce());

            ResultatPuce rp = _presenter.getRaid().existeResultatPuce(_resultatPuce);
            if (rp == null) {
                _presenter.getRaid().getResultatsPuce().addResultatPuce(_resultatPuce);
                _presenter.saveCurrentRaid();
            } else {
                _presenter.getRaid().removeResultatPuce(rp);
                _presenter.getRaid().addResultatPuce(_resultatPuce);
            }
            dispose();
        });
        return _buttonOkPrint;
    }

    private JButton _createJButtonCreer() {
        _buttonCreer = new JButton();
        _buttonCreer.setPreferredSize(new Dimension(50, 50));
        _buttonCreer.setEnabled(false);
        // TODO L10n.getString()
        _buttonCreer.setToolTipText("Créer une nouvelle équipe");
        //noinspection ConstantConditions
        _buttonCreer.setIcon(new ImageIcon(getClass().getClassLoader().getResource("fr/gecoraid/icon/creer.png")));
        _buttonCreer.addActionListener(event -> {
            Equipe equipe = new Equipe(_presenter.getRaid());
            equipe.setIdPuce(_resultatPuce.getPuce().getIdPuce());
            equipe.setNom(_resultatPuce.getPuce().getIdPuce());
            equipe.setDossard(_resultatPuce.getPuce().getIdPuce());
            equipe.setCategorie(_presenter.getRaid().getCategories().getCategories().get(0));

            _resultatPuce.setParcours((Parcours) _comboBoxParcours.getSelectedItem());
            _resultatPuce.setEtape((Etape) _comboBoxEtapes.getSelectedItem());
            _resultatPuce.setEquipe(equipe);
            assert (_comboBoxParcours.getSelectedItem() != null);
            if (((Parcours) _comboBoxParcours.getSelectedItem()).getEquipes().addEquipe(equipe)) {
                _presenter.getRaid().getResultatsPuce().addResultatPuce(_resultatPuce);
                _presenter.updateEquipeList();
                _presenter.saveCurrentRaid();
                dispose();
            } else {
                DesktopView.beep();
                // TODO L10n.getString()
                _labelMessage.setText("<html>Cette puce est déjà attribuée à une équipe.<br>" + "Vous ne pouvez pas créer une nouvelle équipe avec cette puce.</html>");
            }
        });
        return _buttonCreer;
    }

    private void _miseAJourResultat() {
        _resultatPuce.setParcours((Parcours) _comboBoxParcours.getSelectedItem());
        _resultatPuce.setEtape((Etape) _comboBoxEtapes.getSelectedItem());
        if (_equipeValide) {
            _resultatPuce.setEquipe((Equipe) _comboBoxEquipes.getSelectedItem());
        } else {
            _resultatPuce.setEquipe(_comboBoxEquipes.getItemAt(0));
        }
        ResultatEquipe re = new ResultatEquipe(_resultatPuce, 0, 0);
        _labelPostes.setText(re.getListeCodeOkPm().toHtml());
        re.saveHtml(_presenter.getRaid(), _resultatReduit);
        try {
            _editorPaneResultat.setDocument(new HTMLDocument());
            String adresse = new File(".").getCanonicalPath();
            _editorPaneResultat.setPage("file:///" + adresse + "/temp.html");
        } catch (IOException e) {
            System.err.format("Cannot print %s%n", e.getMessage());
        }

        if (_equipeValide) {
            assert (_comboBoxEquipes.getSelectedItem() != null);
            if (_resultatPuce.getPuce().getIdPuce().compareTo(((Equipe) _comboBoxEquipes.getSelectedItem()).getIdPuce()) == 0) {
                if (_presenter.getRaid().existeResultatPuce(_resultatPuce) != null) {
                    ///  RODO Beep ?
                    // TODO L10n.getString()
                    _labelMessage.setText("<html>Un résultat existe déjà pour cette équipe sur cette étape.<br>" + "En validant ce résultat, vous remplacerez l'ancien.</html>");
                    _miseAJourBoutons(false);
                } else {
                    _labelMessage.setText("");
                    _miseAJourBoutons(false);
                }
            } else {
                // TODO L10n.getString()
                _labelMessage.setText("<html>Cette puce ne correspond pas à l'équipe sélectionnée.<br>" + "Vous ne pouvez pas valider ce résultat.</html>");
                _miseAJourBoutons(true);
            }
        } else {
            // TODO L10n.getString()
            _labelMessage.setText("<html>Le numéro de la puce insérée ne correspond à aucune équipe du raid.<br>" + "Vous pouvez attribuer ce résultat à une équipe existante ou créer une nouvelle équipe.</html>");
            _miseAJourBoutons(false);
        }
    }

    private void _miseAJourBoutons( boolean oKImpossible ) {
        if (_equipeValide) {
            if (oKImpossible) {
                _buttonOk.setEnabled(false);
                _buttonOkPrint.setEnabled(false);
                _buttonPrint.setEnabled(false);
            } else {
                _buttonOk.setEnabled(true);
                _buttonOkPrint.setEnabled(true);
                _buttonPrint.setEnabled(true);
            }
            _buttonCreer.setEnabled(false);
        } else {
            _buttonOk.setEnabled(true);
            _buttonOkPrint.setEnabled(true);
            _buttonPrint.setEnabled(true);
            _buttonCreer.setEnabled(true);
        }
        if (_visu) {
            _buttonPrint.setEnabled(true);
        }
    }

    private JEditorPane _createJEditorPaneResultat() {
        _editorPaneResultat = new JEditorPane();
        _editorPaneResultat.setEditable(false);
        _editorPaneResultat.setDocument(new HTMLDocument());
        _editorPaneResultat.setEditorKit(new LargeHTMLEditorKit(INITIAL_ZOOM));
        return _editorPaneResultat;
    }

    private JButton _createJButtonPrint() {
        _buttonPrint = new JButton();
        // TODO L10n.getString()
        _buttonPrint.setToolTipText("Imprimer le résultat");
        _buttonPrint.setPreferredSize(new Dimension(50, 50));
        //noinspection ConstantConditions
        _buttonPrint.setIcon(new ImageIcon(getClass().getClassLoader().getResource("fr/gecoraid/icon/printer.png")));
        _buttonPrint.addActionListener(event -> {
            _printResultat();
            ((LargeHTMLEditorKit) _editorPaneResultat.getEditorKit()).setZoomFactor(INITIAL_ZOOM);
            _buttonRefresh.doClick();
        });
        return _buttonPrint;
    }

    private void _printResultat() {
        ((LargeHTMLEditorKit) _editorPaneResultat.getEditorKit()).setZoomFactor(1.0);
        _buttonRefresh.doClick();
        PrinterJob job = PrinterJob.getPrinterJob();
        PageFormat format = job.validatePage(new PageFormat());
        int largeur = (int) (format.getWidth() - 10);
        PrintRequestAttributeSet pras = new HashPrintRequestAttributeSet();
        pras.add(new MediaPrintableArea(5, 5, largeur, _editorPaneResultat.getHeight(), MediaPrintableArea.MM));
        try {
            _editorPaneResultat.print(null, null, false, null, pras, false);
        } catch (PrinterException e1) {
            System.out.println(e1.getMessage());
        }
    }

    private JButton _createJButtonRefresh() {
        _buttonRefresh = new IconButton("reload.png");
        // TODO L10n.getString()
        _buttonRefresh.setToolTipText("Rafraichir l'affichage");
        _buttonRefresh.addActionListener(event -> {
            try {
                String adresse = new File(".").getCanonicalPath();
                _editorPaneResultat.setPage("file:///" + adresse + "/temp.html");
            } catch (IOException et) {
                System.err.format("Cannot print %s%n", et.getMessage());
            }
        });
        return _buttonRefresh;
    }

    private JComponent _getScrollPaneRecapitulatif() {
        JPanel panel = new JPanel(new BorderLayout());
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
        _labelPostes = new JLabel("");
        _labelPostes.setPreferredSize(new Dimension(480, 80));
        scrollPane.setViewportView(_labelPostes);
        panel.add(scrollPane, BorderLayout.CENTER);
        return panel;
    }
}
