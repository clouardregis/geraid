package fr.gecoraid.desktop.raid_panel;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DesktopView;
import fr.gecoraid.model.GlobalSettings;
import fr.gecoraid.model.Balise;
import fr.gecoraid.model.Epreuve;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

// TODO mettre une visibilite packaqge

// TODO Retourner false or true to the presenter meaning the action is validate or cancel
// TODO In the presenter operates the modification rathter than here

public class BaliseEditor extends JDialog {
    private static final long serialVersionUID = 13L;

    private final DesktopView _parent;

    // TODO séparer la vue du modèle
    private final transient Balise _balise;

    private final boolean _creationFlag;
    private final boolean _defaultBaliseFlag;
    private final transient GlobalSettings _globalSettings;
    private JLabel _codeLabel;
    private JSpinner _codeSpinner;
    private JSpinner _pointsSpinner;
    private JSpinner _heuresSpinner;
    private JSpinner _minutesSpinner;
    private JSpinner _secondesSpinner;
    private JPanel _chronometrePanel;
    private JCheckBox _debutGelChronometreCheckBox;
    private JCheckBox _finGelChronometreCheckBox;
    private JSpinner _pointsParPosteManquantSpinner;
    private JSpinner _heuresParPosteManquantSpinner;
    private JSpinner _minutesParPosteManquantSpinner;
    private JSpinner _secondesParPosteManquantSpinner;
    private JRadioButton _bonificationSiPosteManquantRadioButton;
    private JRadioButton _penaliteSiPosteManquantRadioButton;
    private JRadioButton _bonificationRadioButton;
    private JRadioButton _penaliteRadioButton;
    private boolean _isCancelled = false;

    public BaliseEditor( DesktopView parent,
                         GlobalSettings globalSettings,
                         Balise balise,
                         boolean creationFlag,
                         boolean defaultBaliseFlag ) {
        super(parent);
        _parent = parent;
        _globalSettings = globalSettings;
        _balise = balise;
        _creationFlag = creationFlag;
        _defaultBaliseFlag = defaultBaliseFlag;
        _initializeUI();
        _initializeData();
    }

    public boolean showDialog() {
        setVisible(true);
        return _isCancelled;
    }

    private void _initializeUI() {
        setModal(true);
        setResizable(false);
        setContentPane(_createContentPane());
        if (_creationFlag) {
            setTitle(L10n.getString("balise_editor.title1"));
        } else {
            setTitle(L10n.getString("balise_editor.title2"));
        }
        pack();
        if (_defaultBaliseFlag) {
            _codeLabel.setVisible(false);
            _codeSpinner.setVisible(false);
            _chronometrePanel.setVisible(false);
            // TODO ecrase le précédent .
            setTitle(L10n.getString("balise_editor.title3"));
        }
    }

    private void _initializeData() {
        if (_creationFlag) {
            _balise.setPoints(_globalSettings.getPenaltyPointsDefault());
            _balise.setTemps(_globalSettings.getPenaltyTimeDefault());
            _balise.setPointsPosteManquant(_globalSettings.getMispunchPenaltyPoints());
            _balise.setTempsPosteManquant(_globalSettings.getMispunchPenaltyTimeInS());
        }
        _codeSpinner.setValue(_balise.getNumero());
        _debutGelChronometreCheckBox.setSelected(_balise.isArreterChrono());
        _finGelChronometreCheckBox.setSelected(_balise.isDemarerChrono());
        _pointsSpinner.setValue(Math.abs(_balise.getPoints()));
        _pointsParPosteManquantSpinner.setValue(Math.abs(_balise.getPointsPosteManquant()));
        _initBonifPenalite();
        _initTemps();
    }

    private void _initBonifPenalite() {
        boolean bonif = _balise.getPoints() >= 0 && _balise.getTemps() <= 0;
        _bonificationRadioButton.setSelected(bonif);
        _penaliteRadioButton.setSelected(!bonif);
        bonif = _balise.getPointsPosteManquant() > 0 || _balise.getTempsPosteManquant() < 0;
        _bonificationSiPosteManquantRadioButton.setSelected(bonif);
        _penaliteSiPosteManquantRadioButton.setSelected(!bonif);
    }

    private void _initTemps() {
        int heures = (int) Math.floor(Math.abs(_balise.getTemps()) / 3600.0);
        int minutes = (int) Math.floor((Math.abs(_balise.getTemps()) - heures * 3600.0) / 60.0);
        int secondes = Math.abs(_balise.getTemps()) - heures * 3600 - minutes * 60;
        _heuresSpinner.setValue(heures);
        _minutesSpinner.setValue(minutes);
        _secondesSpinner.setValue(secondes);

        int heuresPosteManquant = (int) Math.floor(Math.abs(_balise.getTempsPosteManquant()) / 3600.0);
        int minutesPosteManquant = (int) Math.floor((Math.abs(_balise.getTempsPosteManquant()) - heuresPosteManquant * 3600.0) / 60.0);
        int secondesPosteManquant = Math.abs(_balise.getTempsPosteManquant()) - heuresPosteManquant * 3600 - minutesPosteManquant * 60;
        _heuresParPosteManquantSpinner.setValue(heuresPosteManquant);
        _minutesParPosteManquantSpinner.setValue(minutesPosteManquant);
        _secondesParPosteManquantSpinner.setValue(secondesPosteManquant);
    }

    private JPanel _createContentPane() {
        JPanel jContentPane = new JPanel(new BorderLayout());
        jContentPane.add(_createJPanelContenu(), BorderLayout.CENTER);
        jContentPane.add(_createButtonPanel(), BorderLayout.SOUTH);
        return jContentPane;
    }

    private JComponent _createCodeBaliseTextField() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        _codeLabel = new JLabel(L10n.getString("balise_editor.code"));
        panel.add(_codeLabel);
        _codeSpinner = new JSpinner(new SpinnerNumberModel(31, 31, 500, 1));
        panel.add(_codeSpinner);
        return panel;
    }

    private int _getTemps() {
        int retour = (int) _heuresSpinner.getValue() * 3600 + (int) _minutesSpinner.getValue() * 60 + (int) _secondesSpinner.getValue();
        if (_bonificationRadioButton.isSelected()) {
            retour = retour * -1;
        }
        return retour;
    }

    private int _getTempsPosteManquant() {
        int retour = (int) _heuresParPosteManquantSpinner.getValue() * 3600 + (int) _minutesParPosteManquantSpinner.getValue() * 60 + (int) _secondesParPosteManquantSpinner.getValue();
        if (_bonificationSiPosteManquantRadioButton.isSelected()) {
            retour = retour * -1;
        }
        return retour;
    }

    private JPanel _createJPanelContenu() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        panel.add(_createCodeBaliseTextField());
        panel.add(_createBonificationSiPosteDecouvert());
        panel.add(_createBonificationSiPosteManquantPanel());
        panel.add(_createGelChronometrePanel());
        return panel;
    }

    private JPanel _createBonificationSiPosteDecouvert() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(new TitledBorder(null, L10n.getString("balise_editor.panel.checked_control"), TitledBorder.LEADING, TitledBorder.TOP));

        JPanel panel1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel1.add(_createBonificationRadioButton());
        panel1.add(_createPenaliteRadioButton());
        panel.add(panel1);

        JPanel panel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel2.add(_createPointsSiPosteDecouvertPanel());
        panel.add(panel2);

        JPanel panel3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel3.add(_createTempsSiPosteDecouvertPanel());
        panel.add(panel3);

        ButtonGroup group = new ButtonGroup();
        group.add(_bonificationRadioButton);
        group.add(_penaliteRadioButton);
        return panel;
    }

    private JSpinner _createPointsSiPosteDecouvertSpinner() {
        _pointsSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 1000, 10));
        return _pointsSpinner;
    }

    private JSpinner _createMinutesParPosteDecouvertSpinner() {
        _minutesSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 59, 1));
        return _minutesSpinner;
    }

    private JPanel _createGelChronometrePanel() {
        _chronometrePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));

        _chronometrePanel.setBorder(new TitledBorder(null, L10n.getString("balise_editor.panel.freeze_chronometer"), TitledBorder.LEADING, TitledBorder.TOP));
        _chronometrePanel.setEnabled(true);
        _chronometrePanel.add(_createDebutGelChronometreCheckbox());
        _chronometrePanel.add(_createArretGelChronometreCheckbox());
        return _chronometrePanel;
    }

    private JCheckBox _createDebutGelChronometreCheckbox() {
        _debutGelChronometreCheckBox = new JCheckBox(L10n.getString("balise_editor.checkbox.start_freeze"));
        _debutGelChronometreCheckBox.addActionListener(event -> {
            if (_debutGelChronometreCheckBox.isSelected()) {
                _finGelChronometreCheckBox.setSelected(false);
            }
        });
        return _debutGelChronometreCheckBox;
    }

    private JCheckBox _createArretGelChronometreCheckbox() {
        _finGelChronometreCheckBox = new JCheckBox(L10n.getString("balise_editor.checkbox.end_freeze"));
        _finGelChronometreCheckBox.addActionListener(event -> {
            if (_finGelChronometreCheckBox.isSelected()) {
                _debutGelChronometreCheckBox.setSelected(false);
            }
        });
        return _finGelChronometreCheckBox;
    }

    private JPanel _createBonificationSiPosteManquantPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(new TitledBorder(null, L10n.getString("balise_editor.panel.miss_control"), TitledBorder.LEADING, TitledBorder.TOP));

        JPanel panel1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel1.add(_createBonificationSiPosteManquantRadioButton());
        panel1.add(_createPenaliteSiPosteManquantRadioButton());
        panel.add(panel1);

        JPanel panel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel2.add(_createPointsSiPosteManquantPanel());
        panel.add(panel2);

        JPanel panel3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel3.add(_createTempsSiPosteManquantPanel());
        panel.add(panel3);

        ButtonGroup bg = new ButtonGroup();
        bg.add(_bonificationSiPosteManquantRadioButton);
        bg.add(_penaliteSiPosteManquantRadioButton);
        return panel;
    }

    private JPanel _createPointsSiPosteDecouvertPanel() {
        JPanel jPanel2 = new JPanel(new FlowLayout());
        jPanel2.add(new JLabel(L10n.getString("balise_editor.checked_control.points")));
        jPanel2.add(_createPointsSiPosteDecouvertSpinner());
        return jPanel2;
    }

    private JSpinner _createSecondesParPosteDecouvertSpinner() {
        _secondesSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 59, 1));
        return _secondesSpinner;
    }

    private JRadioButton _createBonificationRadioButton() {
        _bonificationRadioButton = new JRadioButton(L10n.getString("balise_editor.checked_control.bonus"));
        return _bonificationRadioButton;
    }

    private JPanel _createPointsSiPosteManquantPanel() {
        JPanel panel = new JPanel(new FlowLayout());
        panel.add(new JLabel(L10n.getString("balise_editor.miss_control.points")));
        panel.add(_createPointsParPosteManquantSpinner());
        return panel;
    }

    private JSpinner _createPointsParPosteManquantSpinner() {
        _pointsParPosteManquantSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 1000, 10));
        return _pointsParPosteManquantSpinner;
    }

    private JSpinner _createHeuresParPosteManquantSpinner() {
        _heuresParPosteManquantSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 12, 1));
        return _heuresParPosteManquantSpinner;
    }

    private JSpinner _createMinutesParPosteManquantSpinner() {
        _minutesParPosteManquantSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 59, 1));
        return _minutesParPosteManquantSpinner;
    }

    private JSpinner _createSecondesParPosteManquantSpinner() {
        _secondesParPosteManquantSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 59, 1));
        return _secondesParPosteManquantSpinner;
    }

    private JRadioButton _createBonificationSiPosteManquantRadioButton() {
        _bonificationSiPosteManquantRadioButton = new JRadioButton();
        _bonificationSiPosteManquantRadioButton.setText(L10n.getString("balise_editor.mis_control.bonus"));
        return _bonificationSiPosteManquantRadioButton;
    }

    private JRadioButton _createPenaliteSiPosteManquantRadioButton() {
        _penaliteSiPosteManquantRadioButton = new JRadioButton();
        _penaliteSiPosteManquantRadioButton.setText(L10n.getString("balise_editor.miss_control.penalty"));
        return _penaliteSiPosteManquantRadioButton;
    }

    private JPanel _createTempsSiPosteDecouvertPanel() {
        JPanel jPanelTemps = new JPanel(new FlowLayout(FlowLayout.LEFT));
        jPanelTemps.add(new JLabel(L10n.getString("balise_editor.checked_control.time")));
        jPanelTemps.add(_createHeuresParPosteDecouvertSpinner());
        jPanelTemps.add(new JLabel(L10n.getString("hour_abbreviation")));
        jPanelTemps.add(_createMinutesParPosteDecouvertSpinner());
        jPanelTemps.add(new JLabel(" " + L10n.getString("minute_abbreviation")));
        jPanelTemps.add(_createSecondesParPosteDecouvertSpinner());
        jPanelTemps.add(new JLabel(L10n.getString("second_abbreviation")));
        return jPanelTemps;
    }

    private JSpinner _createHeuresParPosteDecouvertSpinner() {
        _heuresSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 12, 1));
        return _heuresSpinner;
    }

    private JRadioButton _createPenaliteRadioButton() {
        _penaliteRadioButton = new JRadioButton(L10n.getString("balise_editor.checked_control.penalty"));
        return _penaliteRadioButton;
    }

    private JPanel _createTempsSiPosteManquantPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.add(new JLabel(L10n.getString("balise_editor.miss_control.time")));
        panel.add(_createHeuresParPosteManquantSpinner());
        panel.add(new JLabel(L10n.getString("hour_abbreviation")));
        panel.add(_createMinutesParPosteManquantSpinner());
        panel.add(new JLabel(" " + L10n.getString("minute_abbreviation")));
        panel.add(_createSecondesParPosteManquantSpinner());
        panel.add(new JLabel(L10n.getString("second_abbreviation")));
        return panel;
    }

    private JPanel _createButtonPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 5));
        panel.add(_createValidateButton());
        panel.add(_createCancelButton());
        return panel;
    }

    private JButton _createValidateButton() {
        JButton buttonOk;
        if (_creationFlag) {
            buttonOk = new JButton(L10n.getString("balise_editor.validate_button1"));
        } else {
            buttonOk = new JButton(L10n.getString("balise_editor.validate_button2"));
        }
        buttonOk.addActionListener(event -> {
            // TODO Mettre ça dans le controleur appellant
            if (_defaultBaliseFlag) {
                _validateSetDefaultBalise();
            } else {
                int codeNumber = (int) _codeSpinner.getValue();
                if (_creationFlag) {
                    _validateCreateNewBalise(codeNumber);
                } else {
                    _validateModifyBalise(codeNumber);
                }
            }
        });
        getRootPane().setDefaultButton(buttonOk);
        return buttonOk;
    }

    private void _validateModifyBalise( int codeNumber ) {
        _balise.setNumero(codeNumber);
        _balise.setArreterChrono(_debutGelChronometreCheckBox.isSelected());
        _balise.setDemarerChrono(_finGelChronometreCheckBox.isSelected());
        if (_bonificationRadioButton.isSelected()) {
            _balise.setPoints((int) _pointsSpinner.getValue());
        } else {
            _balise.setPoints(-(int) _pointsSpinner.getValue());
        }
        _balise.setTemps(_getTemps());
        if (_bonificationSiPosteManquantRadioButton.isSelected()) {
            _balise.setPointsPosteManquant((int) _pointsParPosteManquantSpinner.getValue());
        } else {
            _balise.setPointsPosteManquant(-(int) _pointsParPosteManquantSpinner.getValue());
        }
        _balise.setTempsPosteManquant(_getTempsPosteManquant());
        _parent.getListeBalises().repaint();
        dispose();
    }

    private void _validateCreateNewBalise( int num ) {
        _balise.setNumero(num);
        _balise.setArreterChrono(_debutGelChronometreCheckBox.isSelected());
        _balise.setDemarerChrono(_finGelChronometreCheckBox.isSelected());
        if (_bonificationRadioButton.isSelected()) {
            _balise.setPoints((int) _pointsSpinner.getValue());
        } else {
            _balise.setPoints(-(int) _pointsSpinner.getValue());
        }
        _balise.setTemps(_getTemps());
        if (_bonificationSiPosteManquantRadioButton.isSelected()) {
            _balise.setPointsPosteManquant((int) _pointsParPosteManquantSpinner.getValue());
        } else {
            _balise.setPointsPosteManquant(-(int) _pointsParPosteManquantSpinner.getValue());
        }
        _balise.setTempsPosteManquant(_getTempsPosteManquant());
        final JList<Epreuve> jListEpreuves = _parent.getListeEpreuves();
        if (jListEpreuves.getSelectedValue().getBalises().addBalise(_balise)) {
            _parent.getListeBalises().setListData(jListEpreuves.getSelectedValue().getBalises().getBalises());
            _parent.getListeBalises().setSelectedIndex(jListEpreuves.getSelectedValue().getBalises().getSize() - 1);
            dispose();
        }
    }

    private void _validateSetDefaultBalise() {
        if (_bonificationRadioButton.isSelected()) {
            _globalSettings.setPenaltyPointsDefault((int) _pointsSpinner.getValue());
        } else {
            _globalSettings.setPenaltyPointsDefault(-(int) _pointsSpinner.getValue());
        }
        _globalSettings.setPenaltyTimeDefault(_getTemps());
        if (_bonificationSiPosteManquantRadioButton.isSelected()) {
            _globalSettings.setMispunchPenaltyPoints((int) _pointsParPosteManquantSpinner.getValue());
        } else {
            _globalSettings.setMispunchPenaltyPoints(-(int) _pointsParPosteManquantSpinner.getValue());
        }
        _globalSettings.setMispunchPenaltyTimeInS(_getTempsPosteManquant());
        dispose();

        _isCancelled = false;
    }

    private JButton _createCancelButton() {
        JButton button = new JButton(L10n.getString("cancel"));
        button.addActionListener(event -> dispose());
        _isCancelled = true;
        return button;
    }
}
