package fr.gecoraid.desktop.raid_panel;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DesktopView;
import fr.gecoraid.model.Epreuve;
import fr.gecoraid.model.TypeLimite;
import fr.gecoraid.tools.TimeManager;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.util.Date;

// TODO mettre avec une visibilité package
 class EpreuveEditor extends JDialog {
    private static final long serialVersionUID = 1L;

    private final DesktopView _parent;

    // TODO séparer la vue du modèle
    private final transient Epreuve _epreuve;

    private final boolean _creationFlag;
    private JTextField _textFieldNom;
    private JLabel _errorLabel;
    private JComboBox<TypeLimite> _typeLimiteComboBox;
    private JPanel _heureLimiteTempsPanel;
    private JSpinner _heureLimiteTempsSpinner;
    private JSpinner _minuteLimiteTempsSpinner;
    private JPanel _heureLimiteHorairePannel;
    private JSpinner _heureLimiteHoraireSpinner;
    private JSpinner _minuteLimiteHoraireSpinner;
    private JPanel _penalitePanel;
    private JSpinner _pointsSpinner;
    private JSpinner _tempsSpinner;
    private JSpinner _penaliteTempsSpinner;
    private JCheckBox _chronometrageApartirDeCheckbox;
    private JCheckBox _finDuChronometrageCheckbox;
    private JCheckBox _courseEnLigneCheckbox;
    private JCheckBox _chronometrageDeEpreuveCheckbox;
    private JCheckBox _epreuveEffectueApresCheckbox;
    private JSpinner _multiSpinner;
    private JSpinner _secondeLimiteTempsSpinner;
    private JCheckBox _createPostesApresEpreuvePrecedenteCheckbox;
    private JCheckBox _createPostesAvantEpreuveSuivanteCheckbox;
    private JCheckBox _exclusiveWithNextEpreuveCheckbox;

// TODO mettre une visibilite packaqge
    // TODO Retourner false or true to the presenter meaning the action is validate or cancel
    // TODO In the presenter operates the modification rathter than here

    public EpreuveEditor( DesktopView parent, Epreuve epreuve, boolean creationFlag ) {
        super(parent);
        _parent = parent;
        _epreuve = epreuve;
        _creationFlag = creationFlag;
        _initializeUI();
        _initializeData();
    }

    public boolean showDialog() {
        // TODO retourner vrai: validate button / faux: cancel button
        setVisible(true);
        return true;
    }

    private void _initializeUI() {
        setModal(true);
        if (_creationFlag) {
            setTitle(L10n.getString("epreuve_editor.title1"));
        } else {
            setTitle(L10n.getString("epreuve_editor.title2"));
        }
        setContentPane(_createContentPane());
        pack();
    }

    private void _initializeData() {
        _textFieldNom.setText(_epreuve.getName());
        _multiSpinner.setValue(_epreuve.getMultiplicateurTemps());
        _typeLimiteComboBox.setModel(new DefaultComboBoxModel<>(TypeLimite.values()));
        _typeLimiteComboBox.setSelectedItem(_epreuve.getTypeLimite());
        _pointsSpinner.setValue(_epreuve.getPenalite());
        _penaliteTempsSpinner.setValue(_epreuve.getPenaliteTemps());
        _tempsSpinner.setValue(_epreuve.getIntervalPenalite());
        switch (_epreuve.getTypeLimite()) {
            case AVECLIMITEHORAIRE:
                _heureLimiteHorairePannel.setVisible(true);
                _heureLimiteTempsPanel.setVisible(false);
                _heureLimiteHoraireSpinner.setValue(TimeManager.getHeures(_epreuve.getHeureLimite()));
                _minuteLimiteHoraireSpinner.setValue(TimeManager.getMinutes(_epreuve.getHeureLimite()));
                break;
            case AVECLIMITETEMPS:
                _heureLimiteHorairePannel.setVisible(false);
                _heureLimiteTempsPanel.setVisible(true); // TODO ?
                _heureLimiteTempsSpinner.setValue(TimeManager.getHeures(_epreuve.getTempsLimite()));
                _minuteLimiteTempsSpinner.setValue(TimeManager.getMinutes(_epreuve.getTempsLimite()));
                _secondeLimiteTempsSpinner.setValue(TimeManager.getSecondes(_epreuve.getTempsLimite()));
                break;
            case SANSLIMITE:
                _heureLimiteHorairePannel.setVisible(false);
                _heureLimiteTempsPanel.setVisible(false);
                break;
            default:
                break;
        }
        _chronometrageDeEpreuveCheckbox.setSelected(_epreuve.isChronometree());
        _courseEnLigneCheckbox.setSelected(_epreuve.isCourseEnLigne());
        _epreuveEffectueApresCheckbox.setSelected(_epreuve.isHorsChrono());
        _chronometrageApartirDeCheckbox.setSelected(_epreuve.isDebutEpreuve());
        _finDuChronometrageCheckbox.setSelected(_epreuve.isFinEpreuve());
        _createPostesApresEpreuvePrecedenteCheckbox.setSelected(_epreuve.isApresEpreuvePrecedente());
        _createPostesAvantEpreuveSuivanteCheckbox.setSelected(_epreuve.isAvantEpreuveSuivante());
        _exclusiveWithNextEpreuveCheckbox.setSelected(_epreuve.isExclusiveWithNextEpreuve());
    }

    private JPanel _createContentPane() {
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        panel.add(_createJPanel(), BorderLayout.NORTH);
        panel.add(_createButtonPanel(), BorderLayout.SOUTH);
        return panel;
    }

    private JPanel _createJPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT, 10, 5));
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));

        JPanel labelPanel = new JPanel(new BorderLayout());
        labelPanel.add(new JLabel(L10n.getString("epreuve_editor.name")), BorderLayout.WEST);
        labelPanel.add(_createJTextFieldNom(), BorderLayout.CENTER);
        panel.add(labelPanel);

        panel.add(_createJPanelTypeLimite());
        panel.add(_createJPanelHeureLimiteTemps());
        panel.add(_createJPanelHeureLimiteHoraire());
        panel.add(_createJPanelPenalite());
        panel.add(_createJPanelChrono());
        panel.add(_createMultiplicateurPanel());
        JPanel errorPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        _errorLabel = new JLabel("");
        _errorLabel.setForeground(Color.red);
        errorPanel.add(_errorLabel);
        panel.add(errorPanel);
        return panel;
    }

    private JTextField _createJTextFieldNom() {
        _textFieldNom = new JTextField();
        return _textFieldNom;
    }

    private JPanel _createJPanelTypeLimite() {
        JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout(FlowLayout.LEFT));
        panel.add(new JLabel(L10n.getString("epreuve_editor.limit")));
        panel.add(_createJComboBoxTypeLimite());
        return panel;
    }

    private JComboBox<TypeLimite> _createJComboBoxTypeLimite() {
        _typeLimiteComboBox = new JComboBox<>();
        _typeLimiteComboBox.addActionListener(event -> {
            assert (_typeLimiteComboBox.getSelectedItem() != null);
            switch ((TypeLimite) _typeLimiteComboBox.getSelectedItem()) {
                case SANSLIMITE:
                    _heureLimiteHorairePannel.setVisible(false);
                    _heureLimiteTempsPanel.setVisible(false);
                    _penalitePanel.setVisible(false);
                    break;
                case AVECLIMITEHORAIRE:
                    _heureLimiteHorairePannel.setVisible(true);
                    _heureLimiteTempsPanel.setVisible(false);
                    _penalitePanel.setVisible(true);
                    _chronometrageDeEpreuveCheckbox.setSelected(true);
                    break;
                case AVECLIMITETEMPS:
                    _heureLimiteHorairePannel.setVisible(false);
                    _heureLimiteTempsPanel.setVisible(true);
                    _penalitePanel.setVisible(true);
                    _chronometrageDeEpreuveCheckbox.setSelected(true);
                    break;
                default:
                    break;
            }
        });
        return _typeLimiteComboBox;
    }

    private JPanel _createJPanelHeureLimiteTemps() {
        _heureLimiteTempsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        _heureLimiteTempsPanel.add(new JLabel(L10n.getString("epreuve_editor.time_limit")));
        _heureLimiteTempsPanel.add(_createJSpinnerHeureLimiteTemps());
        _heureLimiteTempsPanel.add(new JLabel(L10n.getString("hour_abbreviation")));
        _heureLimiteTempsPanel.add(_createJSpinnerMinuteLimiteTemps());
        _heureLimiteTempsPanel.add(new JLabel(L10n.getString("minute_abbreviation")));
        _heureLimiteTempsPanel.add(_createSecondeLimiteTempsSpinner());
        _heureLimiteTempsPanel.add(new JLabel(L10n.getString("second_abbreviation")));
        return _heureLimiteTempsPanel;
    }

    private JSpinner _createJSpinnerHeureLimiteTemps() {
        _heureLimiteTempsSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 23, 1));
        return _heureLimiteTempsSpinner;
    }

    private JSpinner _createJSpinnerMinuteLimiteTemps() {
        _minuteLimiteTempsSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 59, 1));
        return _minuteLimiteTempsSpinner;
    }

    private JPanel _createJPanelHeureLimiteHoraire() {
        _heureLimiteHorairePannel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        _heureLimiteHorairePannel.setVisible(false);
        _heureLimiteHorairePannel.add(new JLabel(L10n.getString("epreuve_editor.hour_limit")));
        _heureLimiteHorairePannel.add(_createJSpinnerHeureLimiteHoraire());
        _heureLimiteHorairePannel.add(new JLabel(L10n.getString("hour_abbreviation")));
        _heureLimiteHorairePannel.add(_createJSpinnerMinuteLimiteHoraire());
        _heureLimiteHorairePannel.add(new JLabel(L10n.getString("minute_abbreviation")));
        return _heureLimiteHorairePannel;
    }

    private JSpinner _createJSpinnerHeureLimiteHoraire() {
        _heureLimiteHoraireSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 48, 1));
        return _heureLimiteHoraireSpinner;
    }

    private JSpinner _createJSpinnerMinuteLimiteHoraire() {
        _minuteLimiteHoraireSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 59, 1));
        return _minuteLimiteHoraireSpinner;
    }

    private JPanel _createJPanelPenalite() {
        _penalitePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        _penalitePanel.add(new JLabel(L10n.getString("epreuve_editor.minus")), null);
        _penalitePanel.add(_createJSpinnerPoints());
        _penalitePanel.add(new JLabel(L10n.getString("epreuve_editor.point_and_plus")));
        _penalitePanel.add(_createJSpinnerPenaliteTemps());
        _penalitePanel.add(new JLabel(L10n.getString("epreuve_editor.mn_by")));
        _penalitePanel.add(_createJSpinnerTemps());
        _penalitePanel.add(new JLabel(L10n.getString("minute_abbreviation")));
        return _penalitePanel;
    }

    private JSpinner _createJSpinnerPoints() {
        _pointsSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 500, 10));
        return _pointsSpinner;
    }

    private JSpinner _createJSpinnerTemps() {
        _tempsSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 1440, 1));
        return _tempsSpinner;
    }

    private JSpinner _createJSpinnerPenaliteTemps() {
        _penaliteTempsSpinner = new JSpinner();
        _penaliteTempsSpinner.setModel(new SpinnerNumberModel(0, 0, 60, 1));
        return _penaliteTempsSpinner;
    }

    private JPanel _createJPanelChrono() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(_createChronomtrageDeLpreuveCheckbox());
        panel.add(_createChronometrageSuccessifCheckbox());
        panel.add(_createFinDuChronometrageCheckbox());
        panel.add(_createCourseEnLigneCheckbox());
        panel.add(_createEpreuveEffectueApresCheckbox());
        panel.add(_createExclusiveWithNextEpreuveCheckbox());
        panel.add(_createPostesApresEpreuvePrecedenteCheckbox());
        panel.add(_createPostesAvantEpreuveSuivanteCheckbox());
        return panel;
    }

    private JCheckBox _createChronomtrageDeLpreuveCheckbox() {
        _chronometrageDeEpreuveCheckbox = new JCheckBox(L10n.getString("epreuve_editor.checkbox1"));
        _chronometrageDeEpreuveCheckbox.addChangeListener(arg0 -> {
            assert (_typeLimiteComboBox.getSelectedItem() != null);
            if ((Integer) _multiSpinner.getValue() > 1
                    || !_typeLimiteComboBox.getSelectedItem().equals(TypeLimite.SANSLIMITE)) {
                _chronometrageDeEpreuveCheckbox.setSelected(true);
            }
            if (_chronometrageDeEpreuveCheckbox.isSelected()) {
                if (_parent.getSelectedEtape().isFirstEpreuve(_epreuve)) {
                    _chronometrageApartirDeCheckbox.setEnabled(false);
                    _chronometrageApartirDeCheckbox.setSelected(false);
                } else {
                    _chronometrageApartirDeCheckbox.setEnabled(true);
                }
                if (_parent.getSelectedEtape().isLastEpreuve(_epreuve)) {
                    _finDuChronometrageCheckbox.setEnabled(false);
                    _finDuChronometrageCheckbox.setSelected(false);
                } else {
                    _finDuChronometrageCheckbox.setEnabled(true);
                }
            } else {
                _finDuChronometrageCheckbox.setSelected(false);
                _chronometrageApartirDeCheckbox.setSelected(false);
                _finDuChronometrageCheckbox.setEnabled(false);
                _chronometrageApartirDeCheckbox.setEnabled(false);
            }
        });
        return _chronometrageDeEpreuveCheckbox;
    }

    private JCheckBox _createChronometrageSuccessifCheckbox() {
        _chronometrageApartirDeCheckbox = new JCheckBox(L10n.getString("epreuve_editor.checkbox2"));
        _chronometrageApartirDeCheckbox.setEnabled(false);
        return _chronometrageApartirDeCheckbox;
    }

    private JCheckBox _createFinDuChronometrageCheckbox() {
        _finDuChronometrageCheckbox = new JCheckBox(L10n.getString("epreuve_editor.checkbox3"));
        _finDuChronometrageCheckbox.setEnabled(false);
        _finDuChronometrageCheckbox.setSelected(true);
        return _finDuChronometrageCheckbox;
    }

    private JCheckBox _createCourseEnLigneCheckbox() {
        _courseEnLigneCheckbox = new JCheckBox(L10n.getString("epreuve_editor.checkbox4"));
        return _courseEnLigneCheckbox;
    }

    private JCheckBox _createEpreuveEffectueApresCheckbox() {
        _epreuveEffectueApresCheckbox = new JCheckBox(L10n.getString("epreuve_editor.checkbox5"));
        return _epreuveEffectueApresCheckbox;
    }

    private JCheckBox _createExclusiveWithNextEpreuveCheckbox() {
        _exclusiveWithNextEpreuveCheckbox = new JCheckBox(L10n.getString("epreuve_editor.checkbox6"));
        return _exclusiveWithNextEpreuveCheckbox;
    }

    private JCheckBox _createPostesApresEpreuvePrecedenteCheckbox() {
        _createPostesApresEpreuvePrecedenteCheckbox = new JCheckBox(L10n.getString("epreuve_editor.checkbox7"));
        _createPostesApresEpreuvePrecedenteCheckbox.addItemListener(arg0 -> {
            if (_createPostesApresEpreuvePrecedenteCheckbox.isSelected()) {
                _createPostesAvantEpreuveSuivanteCheckbox.setSelected(false);
            }
        });
        return _createPostesApresEpreuvePrecedenteCheckbox;
    }

    private JCheckBox _createPostesAvantEpreuveSuivanteCheckbox() {
        _createPostesAvantEpreuveSuivanteCheckbox = new JCheckBox(L10n.getString("epreuve_editor.checkbox8"));
        _createPostesAvantEpreuveSuivanteCheckbox.addItemListener(event -> {
            if (_createPostesAvantEpreuveSuivanteCheckbox.isSelected()) {
                _createPostesApresEpreuvePrecedenteCheckbox.setSelected(false);
            }
        });
        return _createPostesAvantEpreuveSuivanteCheckbox;
    }

    private JSpinner _createMultiSpinner() {
        _multiSpinner = new JSpinner();
        _multiSpinner.addChangeListener(event -> {
            if ((Integer) _multiSpinner.getValue() > 1) {
                _chronometrageDeEpreuveCheckbox.setSelected(true);
            }
        });
        _multiSpinner.setModel(new SpinnerNumberModel(1, 1, null, 1));
        return _multiSpinner;
    }

    private JPanel _createMultiplicateurPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.add(new JLabel(L10n.getString("epreuve_editor.multiplicator")));
        panel.add(_createMultiSpinner());
        return panel;
    }

    private JSpinner _createSecondeLimiteTempsSpinner() {
        _secondeLimiteTempsSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 59, 1));
        return _secondeLimiteTempsSpinner;
    }


    private JPanel _createButtonPanel() {
        JPanel jPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 5));
        jPanel.add(_createValidateButton());
        jPanel.add(_createCancelButton());
        return jPanel;
    }

    private JButton _createValidateButton() {
        JButton button;
        if (_creationFlag) {
            button = new JButton(L10n.getString("epreuve_editor.validate_button1"));
        } else {
            button = new JButton(L10n.getString("epreuve_editor.validate_button2"));
        }
        getRootPane().setDefaultButton(button);
        button.addActionListener(event -> {
            if (_textFieldNom.getText().trim().isEmpty()) {
                DesktopView.beep();
                _errorLabel.setText(L10n.getString("epreuve_editor.error1"));
            } else if (_creationFlag) {
                _validateCreateEpreuve();
            } else {
                _validateModifyEpreuve();
            }
        });
        return button;
    }

    private void _validateModifyEpreuve() {
        if (_parent.getSelectedEtape().existeEpreuve(_textFieldNom.getText(), _epreuve)) {
            DesktopView.beep();
            _errorLabel.setText(L10n.getString("epreuve_editor.error2"));
        } else {
            _epreuve.setNom(_textFieldNom.getText());
            _epreuve.setMultiplicateurTemps((int) _multiSpinner.getValue());
            _epreuve.setTypeLimite((TypeLimite) _typeLimiteComboBox.getSelectedItem());
            _epreuve.setChronometree(_chronometrageDeEpreuveCheckbox.isSelected());
            _epreuve.setCourseEnLigne(_courseEnLigneCheckbox.isSelected());
            _epreuve.setHorsChrono(_epreuveEffectueApresCheckbox.isSelected());
            _epreuve.setDebutEpreuve(_chronometrageApartirDeCheckbox.isSelected());
            _epreuve.setFinEpreuve(_finDuChronometrageCheckbox.isSelected());
            _epreuve.setApresEpreuvePrecedente(_createPostesApresEpreuvePrecedenteCheckbox.isSelected());
            _epreuve.setAvantEpreuveSuivante(_createPostesAvantEpreuveSuivanteCheckbox.isSelected());
            _epreuve.setExclusiveWithNextEpreuve(_exclusiveWithNextEpreuveCheckbox.isSelected());
            if (_epreuve.getTypeLimite().equals(TypeLimite.AVECLIMITEHORAIRE)) {
                _epreuve.setHeureLimite(new Date((Integer) _heureLimiteHoraireSpinner.getValue() * 3600000L +
                        (Integer) _minuteLimiteHoraireSpinner.getValue() * 60000L));
            }
            if (_epreuve.getTypeLimite().equals(TypeLimite.AVECLIMITETEMPS)) {
                _epreuve.setTempsLimite(new Date((Integer) _heureLimiteTempsSpinner.getValue() * 3600000L +
                        (Integer) _minuteLimiteTempsSpinner.getValue() * 60000L + (Integer) _secondeLimiteTempsSpinner.getValue() * 1000L));
            }
            if (!_epreuve.getTypeLimite().equals(TypeLimite.SANSLIMITE)) {
                _epreuve.setPenalite((Integer) _pointsSpinner.getValue());
                _epreuve.setPenaliteTemps((Integer) _penaliteTempsSpinner.getValue());
                _epreuve.setIntervalPenalite((Integer) _tempsSpinner.getValue());
            }
            _parent.getEtapeCombox().repaint(); // TODO en quoi est-il impacté...
            final JList<Epreuve> epreuves = _parent.getListeEpreuves();
            epreuves.setListData(_parent.getSelectedEtape().getEpreuves().getEpreuves());
            epreuves.setSelectedValue(_epreuve, true);
            dispose();
        }
    }

    private void _validateCreateEpreuve() {
        _epreuve.setNom(_textFieldNom.getText());
        _epreuve.setMultiplicateurTemps((int) _multiSpinner.getValue());
        _epreuve.setTypeLimite((TypeLimite) _typeLimiteComboBox.getSelectedItem());
        _epreuve.setChronometree(_chronometrageDeEpreuveCheckbox.isSelected());
        _epreuve.setCourseEnLigne(_courseEnLigneCheckbox.isSelected());
        _epreuve.setHorsChrono(_epreuveEffectueApresCheckbox.isSelected());
        _epreuve.setDebutEpreuve(_chronometrageApartirDeCheckbox.isSelected());
        _epreuve.setFinEpreuve(_finDuChronometrageCheckbox.isSelected());
        _epreuve.setApresEpreuvePrecedente(_createPostesApresEpreuvePrecedenteCheckbox.isSelected());
        _epreuve.setAvantEpreuveSuivante(_createPostesAvantEpreuveSuivanteCheckbox.isSelected());
        _epreuve.setExclusiveWithNextEpreuve(_exclusiveWithNextEpreuveCheckbox.isSelected());
        if (_epreuve.getTypeLimite().equals(TypeLimite.AVECLIMITEHORAIRE)) {
            _epreuve.setHeureLimite(new Date((Integer) _heureLimiteHoraireSpinner.getValue() * 3600000L +
                    (Integer) _minuteLimiteHoraireSpinner.getValue() * 60000L));
        }
        if (_epreuve.getTypeLimite().equals(TypeLimite.AVECLIMITETEMPS)) {
            _epreuve.setTempsLimite(new Date((Integer) _heureLimiteTempsSpinner.getValue() * 3600000L +
                    (Integer) _minuteLimiteTempsSpinner.getValue() * 60000L + (Integer) _secondeLimiteTempsSpinner.getValue() * 1000L));
        }
        if (!_epreuve.getTypeLimite().equals(TypeLimite.SANSLIMITE)) {
            _epreuve.setPenalite((Integer) _pointsSpinner.getValue());
            _epreuve.setPenaliteTemps((Integer) _penaliteTempsSpinner.getValue());
            _epreuve.setIntervalPenalite((Integer) _tempsSpinner.getValue());
        }
        if (_parent.getSelectedEtape().existeEpreuve(_textFieldNom.getText(), _epreuve)) {
            DesktopView.beep();
            _errorLabel.setText(L10n.getString("epreuve_editor.error3"));
        } else {
            _parent.getSelectedEtape().getEpreuves().addEpreuve(_epreuve);
            final JList<Epreuve> jListeEpreuves = _parent.getListeEpreuves();
            jListeEpreuves.setListData(_parent.getSelectedEtape().getEpreuves().getEpreuves());
            jListeEpreuves.setSelectedIndex(_parent.getSelectedEtape().getEpreuves().getSize() - 1);
            dispose();
        }
    }

    private JButton _createCancelButton() {
        JButton button = new JButton(L10n.getString("cancel"));
        button.addActionListener(event -> dispose());
        return button;
    }
}
