package fr.gecoraid.desktop.raid_panel;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DeskTopPresenter;
import fr.gecoraid.model.Epreuve;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import java.awt.BorderLayout;
import java.awt.FlowLayout;

 class MultipleBaliseEditor extends JDialog {
    private static final long serialVersionUID = 6925815924430132616L;
    private static final int START_BALISE_CODE = 31;

    private final transient DeskTopPresenter _presenter;
    private final Integer[] _baliseCodes = new Integer[225];
    private final JList<Integer> _baliseCodesList = new JList<>();

    // TODO mettre une visibilite package
    // TODO Retourner false or true to the presenter meaning the action is validate or cancel
    // TODO In the presenter operates the modification rather than here

    public MultipleBaliseEditor( final Epreuve circuit, DeskTopPresenter presenter ) {
        setModal(true);
        _presenter = presenter;
        setTitle(L10n.getString("multiple_balise_editor.title"));
        _initializeUI(circuit);
        _initializeCodes();
        _baliseCodesList.setListData(_baliseCodes);
    }

    public boolean showDialog() {
        // TODO retourner vrai: validate button / faux: cancel button
        setVisible(true);
        return true;
    }

    private void _initializeUI( Epreuve circuit ) {
        JPanel contentPanel = new JPanel(new BorderLayout());
        contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPanel.add(_createTitleLabel(), BorderLayout.NORTH);
        contentPanel.add(_createBaliseCodeScrollPane(), BorderLayout.CENTER);
        contentPanel.add(_createButtonPanel(circuit), BorderLayout.SOUTH);
        setContentPane(contentPanel);
        pack();
    }

    private JScrollPane _createBaliseCodeScrollPane() {
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(_baliseCodesList);
        return scrollPane;
    }

    private JPanel _createTitleLabel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.add(new JLabel(L10n.getString("multiple_balise_editor.label")));
        return panel;
    }

    private JPanel _createButtonPanel( Epreuve circuit ) {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER));
        panel.add(_createValidateButton(circuit));
        panel.add(_createCancelButton());
        return panel;
    }

    private void _initializeCodes() {
        for (int i = 0; i < 225; i++) {
            _baliseCodes[i] = i + START_BALISE_CODE;
        }
    }

    private JButton _createValidateButton( Epreuve circuit ) {
        JButton button = new JButton(L10n.getString("multiple_balise_editor.validate"));
        button.addActionListener(event -> {
            // TODO _result =
            if (_baliseCodesList.getSelectedIndices().length > 0) {
                circuit.getBalises().addBalises(_baliseCodesList.getSelectedValuesList(),
                        _presenter.getGlobalSettings().getPenaltyPointsDefault(),
                        _presenter.getGlobalSettings().getPenaltyTimeDefault(),
                        _presenter.getGlobalSettings().getMispunchPenaltyPoints(),
                        _presenter.getGlobalSettings().getMispunchPenaltyTimeInS());
            }
            dispose();
        });
        getRootPane().setDefaultButton(button);
        return button;
    }

    private JButton _createCancelButton() {
        JButton button = new JButton(L10n.getString("cancel"));
        button.addActionListener(event -> dispose());
        return button;
    }
}
