package fr.gecoraid.desktop.raid_panel;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DeskTopPresenter;
import fr.gecoraid.desktop.DesktopView;
import fr.gecoraid.model.Balise;
import fr.gecoraid.model.CsvEpreuves;
import fr.gecoraid.model.Epreuve;
import fr.gecoraid.model.Equipe;
import fr.gecoraid.model.Etape;
import fr.gecoraid.model.Parcours;
import fr.gecoraid.tools.ExtensionFileFilter;
import fr.gecoraid.tools.Utils;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import java.io.File;
import java.util.Vector;

class RaidPanelPresenter {
    private static final String BALISES_LABEL = "Balises : ";
    private static final String POINTS_LABEL = " points";

    private final DesktopView _desktopView;
    private final DeskTopPresenter _parent;

    private final JList<Equipe> _equipeList;
    private final JList<Epreuve> _epreuveList;
    private final JList<Balise> _baliseList;
    private final JComboBox<Parcours> _parcoursComboBox;
    private final JComboBox<Etape> _etapeComboBox;
    private final RaidPanelView _raidPanelView;

    RaidPanelPresenter( RaidPanelView raidPanelView,
                        DesktopView desktopView, DeskTopPresenter parent,
                        JList<Equipe> equipeList,
                        JComboBox<Parcours> parcoursComboBox,
                        JList<Epreuve> epreuveList,
                        JComboBox<Etape> etapeComboBox,
                        JList<Balise> baliseList ) {
        _raidPanelView = raidPanelView;
        _desktopView = desktopView;
        _parent = parent;
        _equipeList = equipeList;
        _parcoursComboBox = parcoursComboBox;
        _epreuveList = epreuveList;
        _etapeComboBox = etapeComboBox;
        _baliseList = baliseList;

    }

    JComboBox<Parcours> getParcoursComboBox() {
        return _parcoursComboBox;
    }

    JList<Epreuve> getEpreuveList() {
        return _epreuveList;
    }

    JComboBox<Etape> getEtapeComboBox() {
        return _etapeComboBox;
    }

    JList<Balise> getBaliseList() {
        return _baliseList;
    }

    void selectParcours() {
        if (_parcoursComboBox.getSelectedIndex() != -1) {
            assert (_parcoursComboBox.getSelectedItem() != null);
            Parcours selectedParcours = (Parcours) _parcoursComboBox.getSelectedItem();
            _equipeList.setListData(selectedParcours.getEquipes().getEquipes());
            _equipeList.repaint();
            if (_equipeList.getModel().getSize() > 0) {
                _equipeList.setSelectedIndex(0);
            } else {
                _equipeList.setSelectedIndex(-1);
            }
            _etapeComboBox.setModel(new DefaultComboBoxModel<>(selectedParcours.getEtapes().getEtapes()));
            _etapeComboBox.repaint();
            if (_etapeComboBox.getItemCount() > 0) {
                _etapeComboBox.setSelectedIndex(0);
            } else {
                _etapeComboBox.setSelectedIndex(-1);
            }
            _desktopView.getEquipePanel().updateTeamTotal(selectedParcours.getNombreEquipes());
        } else {
            // Clear the list of parcours
            _equipeList.setListData(new Vector<>());
            _equipeList.repaint();
            _desktopView.getEquipePanel().updateTeamTotal(0);
            _equipeList.setSelectedIndex(-1);
            _etapeComboBox.removeAllItems();
            _etapeComboBox.repaint();
            _etapeComboBox.setSelectedIndex(-1);
            _raidPanelView.setBaliseCount(0);
        }
        _desktopView.refreshResultTable();
    }

    void addParcours() {
        ParcoursEditor parcoursEditor = new ParcoursEditor(
                _desktopView,
                _parent,
                new Parcours(""),
                true);
        parcoursEditor.setLocationRelativeTo(null);
        Parcours result = parcoursEditor.showDialog();
        if (result != null) {
            _parent.getRaid().addParcours(result);
            JComboBox<Parcours> combo = _parent.getComboBoxParcours();
            combo.repaint();
            combo.setSelectedIndex(combo.getItemCount() - 1);
            _parent.setModified(true);
        }
    }

    void duplicateParcours() {
        if (_parcoursComboBox.getSelectedIndex() > -1) {
            assert (_parcoursComboBox.getSelectedItem() != null);
            Parcours parcours = (Parcours) ((Parcours) _parcoursComboBox.getSelectedItem()).clone();
            _parent.getRaid().getParcourss().addParcours(parcours);
            _parcoursComboBox.setModel(new DefaultComboBoxModel<>(_parent.getRaid().getParcourss().getParcourss()));
            _parcoursComboBox.setSelectedIndex(_parcoursComboBox.getItemCount() - 1);
        }
    }

    void deleteParcours() {
        if (_parcoursComboBox.getItemCount() > 0 && _parcoursComboBox.getSelectedIndex() != -1) {
            assert (_parcoursComboBox.getSelectedItem() != null);
            String message = String.format(L10n.getString("raid_panel.parcours.delete.confirmation", _parcoursComboBox.getSelectedItem().toString()));
            int choice = JOptionPane.showConfirmDialog(_raidPanelView, message, L10n.getString("confirmation_title"), JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (choice == JOptionPane.YES_OPTION) {
                _parent.getRaid().removeParcours((Parcours) _parcoursComboBox.getSelectedItem());
                _parcoursComboBox.repaint();
                _parcoursComboBox.setSelectedIndex(_parcoursComboBox.getItemCount() - 1);
            }
        }
    }

    void editParcours() {
        if (_parcoursComboBox.getSelectedIndex() != -1) {
            Parcours parcours = (Parcours) _parcoursComboBox.getSelectedItem();
            assert parcours != null;
            ParcoursEditor parcoursEditor = new ParcoursEditor(
                    _desktopView,
                    _parent,
                    parcours,
                    false);
            parcoursEditor.setLocationRelativeTo(null);
            Parcours result = parcoursEditor.showDialog();
            if (result != null) {
                final JComboBox<Parcours> combo = _parent.getComboBoxParcours();
                combo.repaint();
            }
        }
    }

    void selectEtape() {
        if (_etapeComboBox.getSelectedIndex() != -1) {
            assert (_etapeComboBox.getSelectedItem() != null);
            _epreuveList.setListData(((Etape) _etapeComboBox.getSelectedItem()).getEpreuves().getEpreuves());
            _epreuveList.repaint();
            if (_epreuveList.getModel().getSize() > 0) {
                _epreuveList.setSelectedIndex(0);
                _raidPanelView.setBaliseCount(_epreuveList.getSelectedValue().getNbBalises());
                _raidPanelView.setBaliseTitle(BALISES_LABEL + _epreuveList.getSelectedValue().getTotalPointsEpreuve() + POINTS_LABEL);
            } else {
                _epreuveList.setSelectedIndex(-1);
                _raidPanelView.setBaliseCount(0);
                _raidPanelView.setBaliseTitle(BALISES_LABEL);
            }
        } else {
            _epreuveList.setListData(new Vector<>());
            _epreuveList.repaint();
            _epreuveList.setSelectedIndex(-1);
            _raidPanelView.setBaliseCount(0);
            _raidPanelView.setBaliseTitle(BALISES_LABEL);
        }
        _desktopView.refreshResultTable();
    }

    void addEtape() {
        JComboBox<Parcours> _parcoursComboBox = getParcoursComboBox();
        if (_parcoursComboBox.getSelectedIndex() != -1) {
            EtapeEditor ihm = new EtapeEditor(_desktopView, new Etape(), true);
            ihm.setLocationRelativeTo(null);
            ihm.showDialog(); // TODO ShowDialog()
        }
    }

    void editEtape() {
        JComboBox<Etape> _etapeComboBox = getEtapeComboBox();
        if (_etapeComboBox.getSelectedIndex() != -1) {
            EtapeEditor ihm = new EtapeEditor(_desktopView, ((Etape) _etapeComboBox.getSelectedItem()), false);
            ihm.setLocationRelativeTo(null);
            ihm.showDialog(); // TODO ShowDialog()
        }
    }

    void deleteEtape() {
        JComboBox<Etape> _etapeComboBox = getEtapeComboBox();
        JComboBox<Parcours> _parcoursComboBox = getParcoursComboBox();
        assert (_parcoursComboBox.getSelectedItem() != null);
        if (_parcoursComboBox.getSelectedIndex() != -1
                && ((Parcours) _parcoursComboBox.getSelectedItem()).getEtapes().getSize() != 0
                && _etapeComboBox.getSelectedIndex() != -1) {
            assert (_etapeComboBox.getSelectedItem() != null);
            String message = String.format(L10n.getString("raid_panel.etape.delete.confirmation", _etapeComboBox.getSelectedItem().toString()));
            int reply = JOptionPane.showConfirmDialog(
                    _raidPanelView,
                    message,
                    L10n.getString("confirmation_title"),
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
            if (reply == JOptionPane.YES_OPTION) {
                Etape et = (Etape) _etapeComboBox.getSelectedItem();
                final JComboBox<Parcours> combo = _parent.getComboBoxParcours();
                assert (combo.getSelectedItem() != null);
                ((Parcours) combo.getSelectedItem()).getEtapes().removeEtape(et);
                _parent.getRaid().getResultatsPuce().effacerEtape(et);
                _parent.getRaid().getPenalites().effacerEtape(et);
                _etapeComboBox.repaint();
                if (((Parcours) combo.getSelectedItem()).getEtapes().getSize() == 0) {
                    _etapeComboBox.setSelectedIndex(-1);
                } else {
                    _etapeComboBox.setSelectedIndex(0);
                }
            }
        }
    }

    void moveDownEpreuve() {
        if (_etapeComboBox.getSelectedIndex() >= 0 && _epreuveList.getModel().getSize() > 0) {
            assert (_etapeComboBox.getSelectedItem() != null);
            ((Etape) _etapeComboBox.getSelectedItem()).getEpreuves().downEpreuve(_epreuveList.getSelectedValue());
            _epreuveList.repaint();
            _epreuveList.setSelectedIndex(_epreuveList.getSelectedIndex() + 1);
        }
    }

    void moveUpEpreuve() {
        if (_etapeComboBox.getSelectedIndex() >= 0 && _epreuveList.getModel().getSize() > 0) {
            assert (_etapeComboBox.getSelectedItem() != null);
            ((Etape) _etapeComboBox.getSelectedItem()).getEpreuves().upEpreuve(_epreuveList.getSelectedValue());
            _epreuveList.repaint();
            _epreuveList.setSelectedIndex(_epreuveList.getSelectedIndex() - 1);
        }
    }

    void selectEpreuve() {
        if (_epreuveList.getSelectedIndex() != -1) {
            _baliseList.setListData(_epreuveList.getSelectedValue().getBalises().getBalises());
            _baliseList.repaint();
            if (_baliseList.getModel().getSize() > 0) {
                _baliseList.setSelectedIndex(0);
            } else {
                _baliseList.setSelectedIndex(-1);
            }
            _raidPanelView.setBaliseCount(_epreuveList.getSelectedValue().getNbBalises());
            _raidPanelView.setBaliseTitle(BALISES_LABEL + _epreuveList.getSelectedValue().getTotalPointsEpreuve() + POINTS_LABEL);
        } else {
            _baliseList.setListData(new Vector<>());
            _baliseList.repaint();
            _baliseList.setSelectedIndex(-1);
            _raidPanelView.setBaliseCount(0);
            _raidPanelView.setBaliseTitle(BALISES_LABEL);
        }
        _desktopView.refreshResultTable();
    }

    void deleteEpreuve() {
        assert (_etapeComboBox.getSelectedItem() != null);
        if (_etapeComboBox.getSelectedIndex() != -1 && ((Etape) _etapeComboBox.getSelectedItem()).getEpreuves().getSize() != 0 && _epreuveList.getSelectedIndex() != -1) {
            String message = String.format(L10n.getString("raid_panel.epreuve.delete.confirmation", _epreuveList.getSelectedValue().toString()));
            int reply = JOptionPane.showConfirmDialog(null,
                    message,
                    L10n.getString("confirmation_title"),
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
            if (reply == JOptionPane.YES_OPTION) {
                final JList<Epreuve> epreuveList = _desktopView.getListeEpreuves();
                Epreuve epreuve = epreuveList.getSelectedValue();
                ((Etape) _etapeComboBox.getSelectedItem()).getEpreuves().removeEpreuve(epreuve);
                epreuveList.repaint();
                epreuveList.setListData(((Etape) _etapeComboBox.getSelectedItem()).getEpreuves().getEpreuves());
                if (((Etape) _etapeComboBox.getSelectedItem()).getEpreuves().getSize() == 0) {
                    epreuveList.setSelectedIndex(-1);
                } else {
                    epreuveList.setSelectedIndex(0);
                }
                epreuveList.repaint();
            }
        }
    }

    void importEpreuve() {
        JFileChooser chooser = new JFileChooser();
        ExtensionFileFilter filter = new ExtensionFileFilter("csv", L10n.getString("csv_file.description"));
        chooser.setFileFilter(filter);
        chooser.setCurrentDirectory(new File(_parent.getRaidFolderPath()));

        int returnVal = chooser.showOpenDialog(_desktopView);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            String file = chooser.getSelectedFile().getAbsolutePath();
            CsvEpreuves.importer(_parent.getGlobalSettings(), (Etape) _etapeComboBox.getSelectedItem(), file);
            assert (_etapeComboBox.getSelectedItem() != null);
            _epreuveList.setListData(((Etape) _etapeComboBox.getSelectedItem()).getEpreuves().getEpreuves());
            _epreuveList.repaint();
            if (_epreuveList.getModel().getSize() > 0) {
                _epreuveList.setSelectedIndex(0);
            }
        }
    }

    void exportEpreuve() {
        JFileChooser chooser = new JFileChooser();
        ExtensionFileFilter filter = new ExtensionFileFilter("csv", L10n.getString("csv_file.description"));
        chooser.setFileFilter(filter);
        chooser.setCurrentDirectory(new File(_parent.getRaidFolderPath()));

        int returnVal = chooser.showSaveDialog(_desktopView);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            String file = Utils.checkExtension(chooser.getSelectedFile().getAbsolutePath(), ".csv");
            assert (_etapeComboBox.getSelectedItem() != null);
            CsvEpreuves.exporter(((Etape) _etapeComboBox.getSelectedItem()).getEpreuves(), file);
        }
    }

    void importEpreuveFromOcad() {
        JFileChooser chooser = new JFileChooser();
        ExtensionFileFilter filter = new ExtensionFileFilter("xml", L10n.getString("xml_file.description"));
        chooser.setFileFilter(filter);
        chooser.setCurrentDirectory(new File(_parent.getRaidFolderPath()));

        int returnVal = chooser.showOpenDialog(_desktopView);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            String file = chooser.getSelectedFile().getAbsolutePath();
            OcadParcoursXmlReader.importer(_parent.getGlobalSettings(), _epreuveList.getSelectedValue(), file);
            _baliseList.setListData(_epreuveList.getSelectedValue().getBalises().getBalises());
            _baliseList.repaint();
            if (_baliseList.getModel().getSize() > 0) {
                _baliseList.setSelectedIndex(0);
            } else {
                _baliseList.setSelectedIndex(-1);
            }
            _raidPanelView.setBaliseCount(_epreuveList.getSelectedValue().getNbBalises());
            _raidPanelView.setBaliseTitle(BALISES_LABEL + _epreuveList.getSelectedValue().getTotalPointsEpreuve() + POINTS_LABEL);
        }
    }

    void moveUpBalise() {
        if (_epreuveList.getSelectedIndex() >= 0 && _baliseList.getModel().getSize() > 0) {
            _epreuveList.getSelectedValue().getBalises().upBalise(_baliseList.getSelectedValue());
            _baliseList.repaint();
            _baliseList.setSelectedIndex(_baliseList.getSelectedIndex() - 1);
        }
    }

    void moveDownBalise() {
        if (_epreuveList.getSelectedIndex() >= 0 && _baliseList.getModel().getSize() > 0) {
            _epreuveList.getSelectedValue().getBalises().downBalise(_baliseList.getSelectedValue());
            _baliseList.repaint();
            _baliseList.setSelectedIndex(_baliseList.getSelectedIndex() + 1);
        }
    }

    void addBalise() {
        if (_epreuveList.getSelectedIndex() != -1) {
            assert (_etapeComboBox.getSelectedItem() != null);
            int index = ((Etape) _etapeComboBox.getSelectedItem()).getEpreuves().getDernierNumero();
            Balise balise = new Balise(index + 1);
            BaliseEditor ihm = new BaliseEditor(_desktopView, _parent.getGlobalSettings(), balise, true, false);
            ihm.setLocationRelativeTo(null);
            boolean isCancelled = ihm.showDialog();  // TODO à utiliser ?
            _raidPanelView.setBaliseCount(_epreuveList.getSelectedValue().getNbBalises());
            _raidPanelView.setBaliseTitle(BALISES_LABEL + _epreuveList.getSelectedValue().getTotalPointsEpreuve() + POINTS_LABEL);
        }
    }

    void editBalise() {
        if (_baliseList.getSelectedIndex() != -1) {
            BaliseEditor ihm = new BaliseEditor(
                    _desktopView,
                    _parent.getGlobalSettings(),
                    _baliseList.getSelectedValue(),
                    false,
                    false);
            ihm.setLocationRelativeTo(null);
            boolean isCancelled = ihm.showDialog(); // TODO à utiliser ?
        }
    }

    void deleteBalise() {
        if (_epreuveList.getSelectedIndex() != -1 && _epreuveList.getSelectedValue().getBalises().getSize() != 0 && _baliseList.getSelectedIndex() != -1) {
            Balise selectedValue = _baliseList.getSelectedValue();
            String message = L10n.getString("raid_panel.balise.delete.confirmation", selectedValue.getNumero());
            int reply = JOptionPane.showConfirmDialog(null,
                    message,
                    L10n.getString("confirmation_title"),
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
            if (reply == JOptionPane.YES_OPTION) {
                final JList<Epreuve> epreuveList = _desktopView.getListeEpreuves();
                epreuveList.getSelectedValue().getBalises().removeBalise(_baliseList.getSelectedValue());
                _baliseList.repaint();
                if (epreuveList.getSelectedValue().getBalises().getSize() == 0) {
                    _baliseList.setSelectedIndex(-1);
                } else {
                    _baliseList.setSelectedIndex(0);
                }
            }
            _raidPanelView.setBaliseCount(_epreuveList.getSelectedValue().getNbBalises());
            _raidPanelView.setBaliseTitle(BALISES_LABEL + _epreuveList.getSelectedValue().getTotalPointsEpreuve() + POINTS_LABEL);
        }
    }

    void addMultipleBalises() {
        Epreuve epreuve = _epreuveList.getSelectedValue();
        MultipleBaliseEditor ihm = new MultipleBaliseEditor(epreuve, _parent);
        ihm.setLocationRelativeTo(null);
        ihm.setVisible(true); // TODO ShowDialog()
        _baliseList.setListData(_epreuveList.getSelectedValue().getBalises().getBalises());
        _baliseList.setSelectedIndex(_epreuveList.getSelectedValue().getBalises().getSize() - 1);
        _raidPanelView.setBaliseCount(_epreuveList.getSelectedValue().getNbBalises());
        _raidPanelView.setBaliseTitle(BALISES_LABEL + _epreuveList.getSelectedValue().getTotalPointsEpreuve() + POINTS_LABEL);
    }

    void addEpreuve() {
        if (_etapeComboBox.getSelectedIndex() != -1) {
            EpreuveEditor ihm = new EpreuveEditor(
                    _desktopView,
                    new Epreuve(),
                    true);
            ihm.setLocationRelativeTo(null);
            ihm.showDialog();
        }
    }

    void editEpreuve() {
        if (_epreuveList.getSelectedIndex() != -1) {
            EpreuveEditor ihm = new EpreuveEditor(
                    _desktopView,
                    _epreuveList.getSelectedValue(),
                    false);
            ihm.setLocationRelativeTo(null);
            ihm.setVisible(true); // TODO ShowDialog()
        }
    }
}
