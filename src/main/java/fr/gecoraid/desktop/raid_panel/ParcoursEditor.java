package fr.gecoraid.desktop.raid_panel;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DeskTopPresenter;
import fr.gecoraid.desktop.DesktopView;
import fr.gecoraid.model.Parcours;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;

class ParcoursEditor extends JDialog {
    private static final long serialVersionUID = 1L;

    private final transient DeskTopPresenter _presenter;
    private final transient Parcours _parcours;
    private final boolean _creationFlag;
    private JTextField _parcoursNameTextField;
    private JLabel _errorLabel;
    private Parcours _result;

    ParcoursEditor( DesktopView parent,
                    DeskTopPresenter presenter,
                    Parcours parcours,
                    boolean creationFlag ) {
        super(parent);
        _presenter = presenter;
        _parcours = parcours;
        _creationFlag = creationFlag;
        _initializeUI();
        _parcoursNameTextField.setText(_parcours.getName());
    }

    Parcours showDialog() {
        setVisible(true);
        return _result;
    }

    private void _initializeUI() {
        if (_creationFlag) {
            setTitle(L10n.getString("parcours_editor.title1"));
        } else {
            setTitle(L10n.getString("parcours_editor.title2"));
        }
        setModal(true);
        _createContent();
        pack();
    }

    private void _createContent() {
        JPanel jContentPane = new JPanel(new BorderLayout());
        jContentPane.add(_createTitleLabel(), BorderLayout.NORTH);
        jContentPane.add(_createContentPanel(), BorderLayout.CENTER);
        jContentPane.add(_createButtonPanel(), BorderLayout.SOUTH);
        setContentPane(jContentPane);
    }

    private JPanel _createTitleLabel() {
        JPanel panelNom = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panelNom.add(new JLabel(L10n.getString("parcours_editor.name.label")));
        return panelNom;
    }

    private JPanel _createContentPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));

        JPanel panelTextField = new JPanel(new GridLayout(1, 1));
        _parcoursNameTextField = new JTextField();
        panelTextField.add(_parcoursNameTextField, BorderLayout.CENTER);
        panel.add(panelTextField);

        JPanel panelError = new JPanel(new FlowLayout(FlowLayout.LEFT));
        _errorLabel = new JLabel("");
        _errorLabel.setForeground(Color.red);
        _errorLabel.setPreferredSize(new Dimension(300, 60));
        panelError.add(_errorLabel);
        panel.add(panelError);

        return panel;
    }

    private JComponent _createButtonPanel() {
        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 5));
        buttonPanel.add(_createValidateButton());
        buttonPanel.add(_createCancelButton());
        return buttonPanel;
    }

    private JButton _createValidateButton() {
        final JButton button;
        if (_creationFlag) {
            button = new JButton(L10n.getString("parcours_editor.button.validate1"));
        } else {
            button = new JButton(L10n.getString("parcours_editor.button.validate2"));
        }
        button.addActionListener(event -> {
            if (_parcoursNameTextField.getText().trim().isEmpty()) {
                DesktopView.beep();
                _errorLabel.setText(L10n.getString("parcours_editor.error1"));
            } else if (_creationFlag) {
                if (_presenter.getRaid().existeParcours(_parcoursNameTextField.getText(), _parcours)) {
                    DesktopView.beep();
                    _errorLabel.setText(L10n.getString("parcours_editor.error2"));
                } else {
                    _parcours.setNom(_parcoursNameTextField.getText());
                    _result = _parcours;
                    dispose();
                }
            } else {
                if (_presenter.getRaid().existeParcoursAvecMemeCasse(_parcoursNameTextField.getText(), _parcours)) {
                    DesktopView.beep();
                    _errorLabel.setText(L10n.getString("parcours_editor.error2"));
                } else {
                    _parcours.setNom(_parcoursNameTextField.getText().trim());
                    _result = _parcours;
                    dispose();
                }
            }
        });
        getRootPane().setDefaultButton(button);
        return button;
    }

    private JButton _createCancelButton() {
        JButton button = new JButton(L10n.getString("cancel"));
        button.addActionListener(event -> {
            _result = null;
            dispose();
        });
        return button;
    }
}
