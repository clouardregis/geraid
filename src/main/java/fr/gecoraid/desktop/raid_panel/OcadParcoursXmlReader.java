package fr.gecoraid.desktop.raid_panel;

import fr.gecoraid.L10n;
import fr.gecoraid.model.GlobalSettings;
import fr.gecoraid.desktop.widget.ErrorPane;
import fr.gecoraid.model.Balise;
import fr.gecoraid.model.Epreuve;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class OcadParcoursXmlReader {
    private OcadParcoursXmlReader() {
    }

    // TODO Renommer en SCREAMING_SNAKE_CASE
    private static final String IOFVERSIONV2 = "IOFVersion";
    private static final String IOFVERSIONV3 = "iofVersion";
    private static final String VERSION = "version";
    private static final String COURSE = "Course";
    private static final String RACECOURSEDATA = "RaceCourseData";
    private static final String COURSENAME = "CourseName";
    private static final String NAME = "Name";
    private static final String COURSEVARIATION = "CourseVariation";
    private static final String COURSECONTROL = "CourseControl";
    private static final String CONTROLCODE = "ControlCode";
    private static final String CONTROL = "Control";
    private static final String ID = "Id";
    private static final String COURSEVARIATIONID = "CourseVariationId";

    private static Element _racine;

    // TODO Tests unitaires : récupérer un fichier OCAD
    public static void importer( GlobalSettings globalSettings, Epreuve epreuve, String fichier ) {
        Document document;
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setNamespaceAware(true);
            factory.setFeature("http://xml.org/sax/features/validation", false);
            factory.setFeature("http://apache.org/xml/features/nonvalidating/load-dtd-grammar", false);
            factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
            DocumentBuilder builder = factory.newDocumentBuilder();
            File settingsFile = new File(fichier);
            document = builder.parse(settingsFile);
            document.getDocumentElement().normalize();
            _racine = document.getDocumentElement();
            if (!_racine.getAttribute(IOFVERSIONV3).isEmpty()) {
                if (_racine.getAttribute(IOFVERSIONV3).substring(0, 1).compareTo("3") == 0) {
                    _recupereCourseV3(globalSettings, epreuve);
                }
            } else if (_racine.getElementsByTagName(IOFVERSIONV2).getLength() > 0) {
                if (((Element) _racine.getElementsByTagName(IOFVERSIONV2).item(0)).getAttribute(VERSION).substring(0, 1).compareTo("2") == 0) {
                    _recupereCourseV2(globalSettings, epreuve);
                }
            } else {
                ErrorPane.showMessageDialog(L10n.getString("ocad_reader.error1"));
            }
        } catch (IOException | ParserConfigurationException | SAXException e) {
            ErrorPane.showMessageDialog(L10n.getString("ocad_reader.error2", e.getClass().getName(), e.getMessage()));
        }
    }

    private static void _recupereCourseV2( GlobalSettings globalSettings, Epreuve epreuve ) {
        final NodeList courseElements = _racine.getElementsByTagName(COURSE);
        ArrayList<String> liste = new ArrayList<>();
        liste.add(L10n.getString("ocad_reader.all_controls"));
        if (courseElements.getLength() > 0) {
            for (int i = 0; i < courseElements.getLength(); i++) {
                Element course = (Element) courseElements.item(i);
                NodeList variations = course.getElementsByTagName(COURSEVARIATION);
                for (int j = 0; j < variations.getLength(); j++) {
                    Element variation = (Element) variations.item(j);
                    if (_getChildText(variation, NAME) != null) {
                        liste.add(_getChildText(course, COURSENAME) + " - " + _getChildText(variation, NAME));
                    } else {
                        liste.add(_getChildText(course, COURSENAME) + " - " + _getChildText(variation, COURSEVARIATIONID));
                    }
                }

            }
            String courseChoisie = (String) JOptionPane.showInputDialog(null, L10n.getString("ocad_reader.circuit_chooser.message"), L10n.getString("ocad_reader.circuit_chooser.title"), JOptionPane.INFORMATION_MESSAGE, null, liste.toArray(), liste.get(0));
            if (courseChoisie != null) {
                if (courseChoisie.compareTo(liste.get(0)) == 0) {
                    _recupereTousPostesV2(globalSettings, epreuve);
                    return;
                }
                for (int i = 0; i < courseElements.getLength(); i++) {
                    Element course = (Element) courseElements.item(i);
                    NodeList variations = course.getElementsByTagName(COURSEVARIATION);
                    for (int j = 0; j < variations.getLength(); j++) {
                        Element variation = (Element) variations.item(j);
                        if ((_getChildText(course, COURSENAME) + " - " + _getChildText(variation, NAME)).compareTo(courseChoisie) == 0 ||
                                (_getChildText(course, COURSENAME) + " - " + _getChildText(variation, COURSEVARIATIONID)).compareTo(courseChoisie) == 0) {
                            NodeList controls = variation.getElementsByTagName(COURSECONTROL);
                            for (int k = 0; k < controls.getLength(); k++) {
                                Element code = (Element) controls.item(k);
                                Balise balise = new Balise(Integer.parseInt(_getChildText(code, CONTROLCODE).trim()));
                                balise.setPoints(globalSettings.getPenaltyPointsDefault());
                                balise.setTemps(globalSettings.getPenaltyTimeDefault());
                                balise.setPointsPosteManquant(globalSettings.getMispunchPenaltyPoints());
                                balise.setTempsPosteManquant(globalSettings.getMispunchPenaltyTimeInS());
                                epreuve.getBalises().addBalise(balise);
                            }
                        }
                    }
                }
            }
        } else {
            ErrorPane.showMessageDialog(L10n.getString("ocad_reader.error3"));
        }
    }

    private static void _recupereTousPostesV2( GlobalSettings globalSettings, Epreuve epreuve ) {
        final NodeList posteElements = _racine.getElementsByTagName(CONTROL);
        if (posteElements.getLength() > 0) {
            for (int i = 0; i < posteElements.getLength(); i++) {
                Element poste = (Element) posteElements.item(i);
                Balise balise = new Balise(Integer.parseInt(_getChildText(poste, CONTROLCODE).trim()));
                balise.setPoints(globalSettings.getPenaltyPointsDefault());
                balise.setTemps(globalSettings.getPenaltyTimeDefault());
                balise.setPointsPosteManquant(globalSettings.getMispunchPenaltyPoints());
                balise.setTempsPosteManquant(globalSettings.getMispunchPenaltyTimeInS());
                epreuve.getBalises().addBalise(balise);
            }
        }
    }

    private static void _recupereCourseV3( GlobalSettings globalSettings, Epreuve epreuve ) {
        Element race = (Element) _racine.getElementsByTagNameNS(_racine.getNamespaceURI(), RACECOURSEDATA).item(0);
        NodeList courseElements = race.getElementsByTagNameNS(_racine.getNamespaceURI(), COURSE);
        String[] liste = new String[courseElements.getLength() + 1];
        liste[0] = L10n.getString("ocad_reader.all_controls");
        if (courseElements.getLength() > 1) {
            for (int i = 0; i < courseElements.getLength(); i++) {
                Element course = (Element) courseElements.item(i);
                liste[i + 1] = _getChildText(course, NAME, _racine.getNamespaceURI());

            }

            String courseChoisie = (String) JOptionPane.showInputDialog(null, L10n.getString("ocad_reader.circuit_chooser.message"), L10n.getString("ocad_reader.circuit_chooser.title"), JOptionPane.INFORMATION_MESSAGE, null, liste, liste[0]);
            if (courseChoisie != null) {
                if (courseChoisie.compareTo(liste[0]) == 0) {
                    _recupereTousPostesV3(globalSettings, epreuve);
                    return;
                }
                for (int i = 0; i < courseElements.getLength(); i++) {
                    Element course = (Element) courseElements.item(i);
                    if (_getChildText(course, NAME, _racine.getNamespaceURI()).compareTo(courseChoisie) == 0) {
                        NodeList controls = course.getElementsByTagNameNS(_racine.getNamespaceURI(), COURSECONTROL);
                        for (int j = 0; j < controls.getLength(); j++) {
                            Element code = (Element) controls.item(j);
                            if (code.getAttribute(L10n.getString("ocad_reader.type")).compareTo(CONTROL) == 0) {
                                Balise balise = new Balise(Integer.parseInt(_getChildText(code, CONTROL, _racine.getNamespaceURI()).trim()));
                                balise.setPoints(globalSettings.getPenaltyPointsDefault());
                                balise.setTemps(globalSettings.getPenaltyTimeDefault());
                                balise.setPointsPosteManquant(globalSettings.getMispunchPenaltyPoints());
                                balise.setTempsPosteManquant(globalSettings.getMispunchPenaltyTimeInS());
                                epreuve.getBalises().addBalise(balise);
                            }
                        }
                    }
                }
            }
        } else {
            ErrorPane.showMessageDialog(L10n.getString("ocad_reader.error3"));
        }
    }

    private static void _recupereTousPostesV3( GlobalSettings globalSettings, Epreuve epreuve ) {
        Node race = _racine.getElementsByTagNameNS(_racine.getNamespaceURI(), RACECOURSEDATA).item(0);
        NodeList posteElements = race.getOwnerDocument().getElementsByTagNameNS(_racine.getNamespaceURI(), CONTROL);
        if (posteElements.getLength() > 0) {
            for (int i = 0; i < posteElements.getLength(); i++) {
                Element poste = (Element) posteElements.item(i);
                if (_isInt(_getChildText(poste, ID, _racine.getNamespaceURI()).trim())) {
                    Balise balise = new Balise(Integer.parseInt(_getChildText(poste, ID, _racine.getNamespaceURI()).trim()));
                    balise.setPoints(globalSettings.getPenaltyPointsDefault());
                    balise.setTemps(globalSettings.getPenaltyTimeDefault());
                    balise.setPointsPosteManquant(globalSettings.getMispunchPenaltyPoints());
                    balise.setTempsPosteManquant(globalSettings.getMispunchPenaltyTimeInS());
                    epreuve.getBalises().addBalise(balise);
                }
            }
        }
    }

    private static boolean _isInt( String str ) {
        return (str.lastIndexOf("-") == 0 && !str.equals("-0")) ? str.replace("-", "").matches(
                "\\d+") : str.matches("\\d+");
    }


    private static String _getChildText( Element element, String namespaceURI, String tag ) {
        final Node node = element.getElementsByTagNameNS(namespaceURI, tag).item(0);
        Node firstChild = node.getFirstChild();
        if (firstChild == null) {
            return "";
        }
        return firstChild.getTextContent();
    }

    private static String _getChildText( Element element, String tag ) {
        final Node node = element.getElementsByTagName(tag).item(0);
        Node firstChild = node.getFirstChild();
        if (firstChild == null) {
            return "";
        }
        return firstChild.getTextContent();
    }
}
