package fr.gecoraid.desktop.raid_panel;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DesktopView;
import fr.gecoraid.model.Etape;
import fr.gecoraid.model.Parcours;
import fr.gecoraid.model.TypeDepart;
import fr.gecoraid.model.TypeLimite;
import fr.gecoraid.tools.TimeManager;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.Date;

class EtapeEditor extends JDialog {
    private static final long serialVersionUID = 1L;

    private final DesktopView _parent;

    // TODO séparer la vue du modèle
    private final transient Etape _etape;

    private final boolean _creationFlag;
    private JTextField _nameTextField;
    private JCheckBox _completedCheckBox;
    private JLabel _errorLabel;
    private JComboBox<TypeDepart> _typeComboBox;
    private JPanel _heureDepartPanel;
    private JSpinner _heureSpinner;
    private JSpinner _minuteSpinner;
    private JComboBox<TypeLimite> _typeLimiteCombBox;
    private JPanel _heureLimiteTempsPanel;
    private JSpinner _heureLimiteTempsSpinner;
    private JSpinner _minuteLimiteTempsSpinner;
    private JPanel _heureLimiteHorairePanel;
    private JSpinner _heureLimiteHoraireSpinner;
    private JSpinner _minuteLimiteHoraireSpinner;
    private JPanel _penalitePanel;
    private JSpinner _penalitePointsSpinner;
    private JSpinner _tempsSpinner;
    private JSpinner _penaliteTempsSpinner;
    private JPanel _gelPanel;
    private JCheckBox _gelCheckbox;

    // TODO mettre une visibilite packaqge
    // TODO Retourner false or true to the presenter meaning the action is validate or cancel
    // TODO In the presenter operates the modification rathter than here
    public EtapeEditor( DesktopView parent, Etape etape, boolean creationFlag ) {
        super(parent);
        _parent = parent;
        _etape = etape;
        _creationFlag = creationFlag;
        _initializeUI();
        initializeData();
    }

    public boolean showDialog() {
        setVisible(true);
        return true;
    }

    private void _initializeUI() {
        setModal(true);
        if (_creationFlag) {
            setTitle(L10n.getString("etape_editor.title1"));
        } else {
            setTitle(L10n.getString("etape_editor.title2"));
        }
        setContentPane(_createContentPane());
        pack();
    }

    private void initializeData() {
        _typeComboBox.setModel(new DefaultComboBoxModel<>(TypeDepart.values()));
        _typeComboBox.setSelectedItem(_etape.getType());
        switch (_etape.getType()) {
            case GROUPE:
                _heureSpinner.setValue(TimeManager.getHeures(_etape.getHeureDepart()));
                _minuteSpinner.setValue(TimeManager.getMinutes(_etape.getHeureDepart()));
                break;
            case BOITIER:
                _heureDepartPanel.setVisible(false);
                break;
            default:
                break;
        }
        _nameTextField.setText(_etape.getName());
        _completedCheckBox.setSelected(_etape.isFini());
        _typeLimiteCombBox.setModel(new DefaultComboBoxModel<>(TypeLimite.values()));
        _typeLimiteCombBox.setSelectedItem(_etape.getTypeLimite());
        _penalitePointsSpinner.setValue(_etape.getPenalite());
        _penaliteTempsSpinner.setValue(_etape.getPenaliteTemps());
        _tempsSpinner.setValue(_etape.getIntervalPenalite());
        switch (_etape.getTypeLimite()) {
            case AVECLIMITEHORAIRE:
                _heureLimiteHorairePanel.setVisible(true);
                _heureLimiteTempsPanel.setVisible(false);
                _gelPanel.setVisible(false);
                _heureLimiteHoraireSpinner.setValue(TimeManager.getHeures(_etape.getHeureLimite()));
                _minuteLimiteHoraireSpinner.setValue(TimeManager.getMinutes(_etape.getHeureLimite()));
                break;
            case AVECLIMITETEMPS:
                _heureLimiteHorairePanel.setVisible(false);
                _heureLimiteTempsPanel.setVisible(true);
                _gelPanel.setVisible(true);
                _heureLimiteTempsSpinner.setValue(TimeManager.getHeures(_etape.getTempsLimite()));
                _minuteLimiteTempsSpinner.setValue(TimeManager.getMinutes(_etape.getTempsLimite()));
                _gelCheckbox.setSelected(_etape.isGelDansLimiteTemps());
                break;
            case SANSLIMITE:
                _heureLimiteHorairePanel.setVisible(false);
                _heureLimiteTempsPanel.setVisible(false);
                _gelPanel.setVisible(false);
                break;
            default:
                break;
        }
    }


    private JPanel _createContentPane() {
        JPanel jContentPane = new JPanel(new BorderLayout());
        jContentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
        jContentPane.add(_createJPanel(), BorderLayout.NORTH);
        jContentPane.add(_createButtonPanel(), BorderLayout.SOUTH);
        return jContentPane;
    }

    private JPanel _createJPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(BorderFactory.createEmptyBorder(0, 10, 0, 10));

        JPanel labelPanel = new JPanel(new BorderLayout());
        labelPanel.add(new JLabel(L10n.getString("etape_editor.etape_name")), BorderLayout.WEST);
        labelPanel.add(_createJTextFieldNom(), BorderLayout.CENTER);
        panel.add(labelPanel);
        panel.add(_createJCheckBoxFini());
        panel.add(_createJPanelType());
        panel.add(_createJPanelHeureDepart());
        panel.add(_createJPanelTypeLimite());
        panel.add(_createJPanelHeureLimiteTemps());
        panel.add(_createHeureLimiteHorairePanel());
        panel.add(_createPenalitePanel());
        panel.add(_createGelPanel());
        panel.add(_getLabelErreur());
        return panel;
    }

    private JTextField _createJTextFieldNom() {
        _nameTextField = new JTextField();
        return _nameTextField;
    }

    private JComponent _getLabelErreur() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        _errorLabel = new JLabel("");
        _errorLabel.setForeground(Color.red);
        _errorLabel.setPreferredSize(new Dimension(340, 60));
        panel.add(_errorLabel);
        return panel;
    }

    private JComponent _createJCheckBoxFini() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        _completedCheckBox = new JCheckBox(L10n.getString("etape_editor.finished_checkbox"));
        panel.add(_completedCheckBox);
        return panel;
    }

    private JPanel _createJPanelType() {
        JPanel jPanelType = new JPanel(new FlowLayout(FlowLayout.LEFT));
        jPanelType.add(new JLabel(L10n.getString("etape_editor.start_type")));
        jPanelType.add(_createJComboBoxType());
        return jPanelType;
    }

    private JComboBox<TypeDepart> _createJComboBoxType() {
        _typeComboBox = new JComboBox<>();
        _typeComboBox.addActionListener(event -> {
            assert (_typeComboBox.getSelectedItem() != null);
            switch ((TypeDepart) _typeComboBox.getSelectedItem()) {
                case GROUPE:
                    _heureDepartPanel.setVisible(true);
                    break;
                case BOITIER:
                    _heureDepartPanel.setVisible(false);
                    break;
                default:
                    break;
            }
        });
        return _typeComboBox;
    }

    private JPanel _createJPanelHeureDepart() {
        _heureDepartPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        _heureDepartPanel.add(new JLabel(L10n.getString("etape_editor.start_time")));
        _heureDepartPanel.add(_createJSpinnerHeure());
        _heureDepartPanel.add(new JLabel(L10n.getString("hour_abbreviation")));
        _heureDepartPanel.add(_createJSpinnerMinute());
        _heureDepartPanel.add(new JLabel(L10n.getString("minute_abbreviation")));
        return _heureDepartPanel;
    }

    private JSpinner _createJSpinnerHeure() {
        _heureSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 48, 1));
        return _heureSpinner;
    }

    private JSpinner _createJSpinnerMinute() {
        _minuteSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 59, 1));
        return _minuteSpinner;
    }

    private JPanel _createJPanelTypeLimite() {
        JPanel jPanelTypeLimite = new JPanel(new FlowLayout(FlowLayout.LEFT));
        jPanelTypeLimite.add(new JLabel(L10n.getString("etape_editor.limit")));
        jPanelTypeLimite.add(_createJComboBoxTypeLimite());
        return jPanelTypeLimite;
    }

    private JComboBox<TypeLimite> _createJComboBoxTypeLimite() {
        _typeLimiteCombBox = new JComboBox<>();
        _typeLimiteCombBox.addActionListener(event -> {
            assert (_typeLimiteCombBox.getSelectedItem() != null);
            switch ((TypeLimite) _typeLimiteCombBox.getSelectedItem()) {
                case SANSLIMITE:
                    _heureLimiteHorairePanel.setVisible(false);
                    _heureLimiteTempsPanel.setVisible(false);
                    _penalitePanel.setVisible(false);
                    _gelPanel.setVisible(false);
                    break;
                case AVECLIMITEHORAIRE:
                    _heureLimiteHorairePanel.setVisible(true);
                    _heureLimiteTempsPanel.setVisible(false);
                    _penalitePanel.setVisible(true);
                    _gelPanel.setVisible(false);
                    break;
                case AVECLIMITETEMPS:
                    _heureLimiteHorairePanel.setVisible(false);
                    _heureLimiteTempsPanel.setVisible(true);
                    _penalitePanel.setVisible(true);
                    _gelPanel.setVisible(true);
                    break;

                default:
                    break;
            }
        });
        return _typeLimiteCombBox;
    }

    private JPanel _createJPanelHeureLimiteTemps() {
        _heureLimiteTempsPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        _heureLimiteTempsPanel.add(new JLabel(L10n.getString("etape_editor.limit_time")));
        _heureLimiteTempsPanel.add(_createJSpinnerHeureLimiteTemps());
        _heureLimiteTempsPanel.add(new JLabel(L10n.getString("hour_abbreviation")));
        _heureLimiteTempsPanel.add(_createJSpinnerMinuteLimiteTemps());
        _heureLimiteTempsPanel.add(new JLabel(L10n.getString("minute_abbreviation")));
        return _heureLimiteTempsPanel;
    }

    private JSpinner _createJSpinnerHeureLimiteTemps() {
        _heureLimiteTempsSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 23, 1));
        return _heureLimiteTempsSpinner;
    }

    private JSpinner _createJSpinnerMinuteLimiteTemps() {
        _minuteLimiteTempsSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 59, 1));
        return _minuteLimiteTempsSpinner;
    }

    private JPanel _createHeureLimiteHorairePanel() {
        _heureLimiteHorairePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        _heureLimiteHorairePanel.setVisible(false);
        _heureLimiteHorairePanel.add(new JLabel(L10n.getString("etape_editor.limit_hour")));
        _heureLimiteHorairePanel.add(_createHeureLimiteHoraireSpinner());
        _heureLimiteHorairePanel.add(new JLabel(L10n.getString("hour_abbreviation")));
        _heureLimiteHorairePanel.add(_createMinuteLimiteHoraireSpinner());
        _heureLimiteHorairePanel.add(new JLabel(L10n.getString("minute_abbreviation")));
        return _heureLimiteHorairePanel;
    }

    private JSpinner _createHeureLimiteHoraireSpinner() {
        _heureLimiteHoraireSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 48, 1));
        return _heureLimiteHoraireSpinner;
    }

    private JSpinner _createMinuteLimiteHoraireSpinner() {
        _minuteLimiteHoraireSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 59, 1));
        return _minuteLimiteHoraireSpinner;
    }

    private JPanel _createPenalitePanel() {
        _penalitePanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        _penalitePanel.add(new JLabel(L10n.getString("etape_editor.minus")));
        _penalitePanel.add(_createPointsSpinner());
        _penalitePanel.add(new JLabel(L10n.getString("etape_editor.points_and_plus")));
        _penalitePanel.add(_createPenaliteTempsSpinner());
        _penalitePanel.add(new JLabel(L10n.getString("etape_editor.mn_by")));
        _penalitePanel.add(_createTempsSpinner());
        _penalitePanel.add(new JLabel(L10n.getString("minute_abbreviation")));
        return _penalitePanel;
    }

    private JSpinner _createPointsSpinner() {
        _penalitePointsSpinner = new JSpinner(new SpinnerNumberModel(0, 0, 500, 10));
        return _penalitePointsSpinner;
    }

    private JSpinner _createTempsSpinner() {
        _tempsSpinner = new JSpinner(new SpinnerNumberModel(1, 1, 60, 1));
        return _tempsSpinner;
    }

    private JSpinner _createPenaliteTempsSpinner() {
        _penaliteTempsSpinner = new JSpinner();
        _penaliteTempsSpinner.setMinimumSize(new Dimension(47, 20));
        _penaliteTempsSpinner.setModel(new SpinnerNumberModel(0, 0, 60, 1));
        return _penaliteTempsSpinner;
    }

    private JPanel _createGelPanel() {
        _gelPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        _gelCheckbox = new JCheckBox(L10n.getString("etape_editor.freeze_checkbox"));
        _gelPanel.add(_gelCheckbox);
        return _gelPanel;
    }

    private JPanel _createButtonPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 5));
        panel.add(_createValidateButton());
        panel.add(_createCancelButton());
        return panel;
    }

    private JButton _createValidateButton() {
        JButton button;
        if (_creationFlag) {
            button = new JButton(L10n.getString("etape_editor.validate_button1"));
        } else {
            button = new JButton(L10n.getString("etape_editor.validate_button2"));
        }
        getRootPane().setDefaultButton(button);
        button.addActionListener(event -> {
            // TODO Mettre dans le presenter
            if (_nameTextField.getText().trim().isEmpty()) {
                DesktopView.beep();
                _errorLabel.setText(L10n.getString("etape_editor.error1"));
            } else if (_creationFlag) {
                _etape.setNom(_nameTextField.getText());
                _etape.setFini(_completedCheckBox.isSelected());
                _etape.setType((TypeDepart) _typeComboBox.getSelectedItem());
                if (_etape.getType().equals(TypeDepart.GROUPE)) {
                    _etape.setHeureDepart(new Date((Integer) _heureSpinner.getValue() * 3600000L +
                            (Integer) _minuteSpinner.getValue() * 60000L));
                }
                _etape.setTypeLimite((TypeLimite) _typeLimiteCombBox.getSelectedItem());
                if (_etape.getTypeLimite().equals(TypeLimite.AVECLIMITEHORAIRE)) {
                    _etape.setHeureLimite(new Date((Integer) _heureLimiteHoraireSpinner.getValue() * 3600000L +
                            (Integer) _minuteLimiteHoraireSpinner.getValue() * 60000L));
                }
                if (_etape.getTypeLimite().equals(TypeLimite.AVECLIMITETEMPS)) {
                    _etape.setTempsLimite(new Date((Integer) _heureLimiteTempsSpinner.getValue() * 3600000L +
                            (Integer) _minuteLimiteTempsSpinner.getValue() * 60000L));
                    _etape.setGelDansLimiteTemps(_gelCheckbox.isSelected());
                }
                if (!_etape.getTypeLimite().equals(TypeLimite.SANSLIMITE)) {
                    _etape.setPenalite((Integer) _penalitePointsSpinner.getValue());
                    _etape.setPenaliteTemps((Integer) _penaliteTempsSpinner.getValue());
                    _etape.setIntervalPenalite((Integer) _tempsSpinner.getValue());
                }
                final JComboBox<Parcours> combo = _parent.getComboBoxParcours();
                assert (combo.getSelectedItem() != null);
                if (((Parcours) combo.getSelectedItem()).existeEtape(_nameTextField.getText(), _etape)) {
                    DesktopView.beep();
                    _errorLabel.setText(L10n.getString("etape_editor.error2"));
                } else {
                    ((Parcours) combo.getSelectedItem()).getEtapes().addEtape(_etape);
                    _parent.getEtapeCombox().setModel(new DefaultComboBoxModel<>(((Parcours) combo.getSelectedItem()).getEtapes().getEtapes()));
                    _parent.getEtapeCombox().setSelectedIndex(((Parcours) combo.getSelectedItem()).getEtapes().getSize() - 1);
                    dispose();
                }
            } else {
                final JComboBox<Parcours> combo = _parent.getComboBoxParcours();
                assert (combo.getSelectedItem() != null);
                if (((Parcours) combo.getSelectedItem()).existeEtape(_nameTextField.getText(), _etape)) {
                    DesktopView.beep();
                    _errorLabel.setText(L10n.getString("etape_editor.error3"));
                } else {
                    _etape.setNom(_nameTextField.getText());
                    _etape.setFini(_completedCheckBox.isSelected());
                    _etape.setType((TypeDepart) _typeComboBox.getSelectedItem());
                    if (_etape.getType().equals(TypeDepart.GROUPE)) {
                        _etape.setHeureDepart(new Date((Integer) _heureSpinner.getValue() * 3600000L +
                                (Integer) _minuteSpinner.getValue() * 60000L));
                    }
                    _etape.setTypeLimite((TypeLimite) _typeLimiteCombBox.getSelectedItem());
                    if (_etape.getTypeLimite().equals(TypeLimite.AVECLIMITEHORAIRE)) {
                        _etape.setHeureLimite(new Date((Integer) _heureLimiteHoraireSpinner.getValue() * 3600000L +
                                (Integer) _minuteLimiteHoraireSpinner.getValue() * 60000L));
                    }
                    if (_etape.getTypeLimite().equals(TypeLimite.AVECLIMITETEMPS)) {
                        _etape.setTempsLimite(new Date((Integer) _heureLimiteTempsSpinner.getValue() * 3600000L +
                                (Integer) _minuteLimiteTempsSpinner.getValue() * 60000L));
                        _etape.setGelDansLimiteTemps(_gelCheckbox.isSelected());
                    }
                    if (!_etape.getTypeLimite().equals(TypeLimite.SANSLIMITE)) {
                        _etape.setPenalite((Integer) _penalitePointsSpinner.getValue());
                        _etape.setPenaliteTemps((Integer) _penaliteTempsSpinner.getValue());
                        _etape.setIntervalPenalite((Integer) _tempsSpinner.getValue());
                    }
                    _parent.getEtapeCombox().repaint();
                    dispose();
                }
            }
        });
        return button;
    }

    private JButton _createCancelButton() {
        JButton button = new JButton(L10n.getString("cancel"));
        button.addActionListener(event -> dispose());
        return button;
    }
}
