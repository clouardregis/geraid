package fr.gecoraid.desktop.raid_panel;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DeskTopPresenter;
import fr.gecoraid.desktop.DesktopView;
import fr.gecoraid.desktop.widget.IconButton;
import fr.gecoraid.model.Balise;
import fr.gecoraid.model.Epreuve;
import fr.gecoraid.model.Equipe;
import fr.gecoraid.model.Etape;
import fr.gecoraid.model.Parcours;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.border.TitledBorder;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class RaidPanelView extends JPanel {
    private static final String DELETE_IMAGE_NAME = "delete.png";
    private static final String ADD_IMAGE_NAME = "add.png";
    private static final String EDIT_IMAGE_NAME = "edit.png";

    private final RaidPanelPresenter _presenter;

    private JLabel _baliseTitleLabel;
    private JLabel _baliseCountLabel;

    public RaidPanelView( DesktopView parent,
                          DeskTopPresenter presenter,
                          JList<Equipe> equipeList,
                          JComboBox<Parcours> parcoursComboBox,
                          JList<Epreuve> epreuveList,
                          JComboBox<Etape> etapeComboBox,
                          JList<Balise> baliseList ) {
        _presenter = new RaidPanelPresenter(
                this,
                parent,
                presenter,
                equipeList,
                parcoursComboBox,
                epreuveList,
                etapeComboBox,
                baliseList);
        _initializeUI();
    }

    void setBaliseCount( int number ) {
        _baliseCountLabel.setText(String.valueOf(number));
    }

    void setBaliseTitle( String title ) {
        _baliseTitleLabel.setText(title);
    }

    void _initializeUI() {
        setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
        add(_createParcoursPanel());
        add(_createEtapePanel());
        add(_createEpreuvePanel());
        add(_createBalisePanel());
    }

    private JPanel _createParcoursPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.setBorder(new TitledBorder(null, L10n.getString("raid_panel.parcours.title"), TitledBorder.LEADING, TitledBorder.TOP));
        panel.add(_createParcoursComboBox());
        panel.add(_createAddParcoursButton());
        panel.add(_createEditParcoursButton());
        panel.add(_createRemoveParcoursButton());
        panel.add(_createDuplicateParcoursButton());
        return panel;
    }

    private JComboBox<Parcours> _createParcoursComboBox() {
        JComboBox<Parcours> _parcoursComboBox = _presenter.getParcoursComboBox();
        Dimension preferredSize = _parcoursComboBox.getPreferredSize();
        preferredSize.width = 250;
        _parcoursComboBox.setPreferredSize(preferredSize);
        _parcoursComboBox.addItemListener(event -> _presenter.selectParcours());
        return _parcoursComboBox;
    }

    private JButton _createAddParcoursButton() {
        JButton button = new IconButton(ADD_IMAGE_NAME);
        button.setToolTipText(L10n.getString("raid_panel.parcours.add.tooltip"));
        button.addActionListener(event -> _presenter.addParcours());
        return button;
    }

    private JButton _createEditParcoursButton() {
        JButton button = new IconButton(EDIT_IMAGE_NAME);
        button.setToolTipText(L10n.getString("raid_panel.parcours.edit.tooltip"));
        button.addActionListener(event -> _presenter.editParcours());
        return button;
    }

    private JButton _createRemoveParcoursButton() {
        JButton button = new IconButton(DELETE_IMAGE_NAME);
        button.setToolTipText(L10n.getString("raid_panel.parcours.delete.tooltip"));
        button.addActionListener(event -> _presenter.deleteParcours());
        return button;
    }

    private JButton _createDuplicateParcoursButton() {
        JButton button = new IconButton("copy.png");
        button.setToolTipText(L10n.getString("raid_panel.parcours.duplicate.tooltip"));
        button.addActionListener(event -> _presenter.duplicateParcours());
        return button;
    }

    private JPanel _createEtapePanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.setBorder(new TitledBorder(null, L10n.getString("raid_panel.etape.title"), TitledBorder.LEADING, TitledBorder.TOP));
        panel.add(_createEtapeComboBox());
        panel.add(_createAddEtapeButton());
        panel.add(_createEditEtapeButton());
        panel.add(_createRemoveEtapeButton());
        return panel;
    }

    private JComboBox<Etape> _createEtapeComboBox() {
        JComboBox<Etape> _etapeComboBox = _presenter.getEtapeComboBox();
        JComboBox<Parcours> _parcoursComboBox = _presenter.getParcoursComboBox();
        Dimension preferredSize = _parcoursComboBox.getPreferredSize();
        preferredSize.width = 250;
        _etapeComboBox.setPreferredSize(preferredSize);
        _etapeComboBox.addActionListener(event -> _presenter.selectEtape());
        return _etapeComboBox;
    }

    private JButton _createAddEtapeButton() {
        JButton button = new IconButton(ADD_IMAGE_NAME);
        button.setToolTipText(L10n.getString("raid_panel.etape.add.tooltip"));
        button.addActionListener(event -> _presenter.addEtape());
        return button;
    }

    private JButton _createEditEtapeButton() {
        JButton button = new IconButton(EDIT_IMAGE_NAME);
        button.setToolTipText(L10n.getString("raid_panel.etape.edit.tooltip"));
        button.addActionListener(event -> _presenter.editEtape());
        return button;
    }

    private JButton _createRemoveEtapeButton() {
        JButton button = new IconButton(DELETE_IMAGE_NAME);
        button.setToolTipText(L10n.getString("raid_panel.etape.delete.tooltip"));
        button.addActionListener(event -> _presenter.deleteEtape());
        return button;
    }

    private JPanel _createEpreuvePanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.setBorder(new TitledBorder(null, L10n.getString("raid_panel.epreuve.title"), TitledBorder.LEADING, TitledBorder.TOP));
        panel.add(_createEpreuveGoUpAndDownButtonsPanel());
        panel.add(_createEpreuveScrollPane());
        panel.add(_createEpreuveButtonPanel());
        return panel;
    }

    private JPanel _createEpreuveGoUpAndDownButtonsPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(_createUpButton());
        panel.add(_createDownButton());
        return panel;
    }

    private JButton _createUpButton() {
        JButton button = new IconButton("up.png");
        button.setToolTipText(L10n.getString("raid_panel.epreuve.move_up.tooltip"));
        button.addActionListener(event -> _presenter.moveUpEpreuve());
        return button;
    }

    private JButton _createDownButton() {
        JButton button = new IconButton("down.png");
        button.setToolTipText(L10n.getString("raid_panel.epreuve.move_down.tooltip"));
        button.addActionListener(event -> _presenter.moveDownEpreuve());
        return button;
    }

    private JScrollPane _createEpreuveScrollPane() {
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setPreferredSize(new Dimension(250, 80));
        scrollPane.setViewportView(_createEpreuveList());
        return scrollPane;
    }

    private JList<Epreuve> _createEpreuveList() {
        JList<Epreuve> _epreuveList = _presenter.getEpreuveList();
        _epreuveList.addListSelectionListener(event -> _presenter.selectEpreuve());
        _epreuveList.setVisibleRowCount(4);
        _epreuveList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        _epreuveList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked( MouseEvent event ) {
                if (event.getClickCount() == 2) {
                    _presenter.editEpreuve();
                }
            }
        });
        return _epreuveList;
    }

    private JPanel _createEpreuveButtonPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.add(_createAddEpreuveButton());
        panel.add(_createEditEpreuveButton());
        panel.add(_createRemoveEpreuveButton());
        panel.add(_createImportEpreuveButton());
        panel.add(_createExportEpreuveButton());
        return panel;
    }

    private JButton _createRemoveEpreuveButton() {
        JButton button = new IconButton(DELETE_IMAGE_NAME);
        button.setToolTipText(L10n.getString("raid_panel.epreuve.delete.tooltip"));
        button.addActionListener(event -> _presenter.deleteEpreuve());
        return button;
    }

    private JButton _createAddEpreuveButton() {
        JButton button = new IconButton(ADD_IMAGE_NAME);
        button.setToolTipText(L10n.getString("raid_panel.epreuve.add.tooltip"));
        button.addActionListener(event -> _presenter.addEpreuve());
        return button;
    }

    private JButton _createEditEpreuveButton() {
        IconButton editEpreuveButton = new IconButton(EDIT_IMAGE_NAME);
        editEpreuveButton.setToolTipText(L10n.getString("raid_panel.epreuve.edit.tooltip"));
        editEpreuveButton.addActionListener(event -> _presenter.editEpreuve());
        return editEpreuveButton;
    }

    private JButton _createImportEpreuveButton() {
        JButton button = new IconButton("import-csv.png");
        button.setToolTipText(L10n.getString("raid_panel.epreuve.import.tooltip"));
        button.addActionListener(event -> _presenter.importEpreuve());
        return button;
    }

    private JButton _createExportEpreuveButton() {
        JButton button = new IconButton("export-csv.png");
        button.setToolTipText(L10n.getString("raid_panel.epreuve.export.tooltip"));
        button.addActionListener(event -> _presenter.exportEpreuve());
        return button;
    }


    private JPanel _createBalisePanel() {
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBorder(new TitledBorder(null, L10n.getString("raid_panel.balise.title"), TitledBorder.LEADING, TitledBorder.TOP));
        panel.add(_createBaliseButtonsPanel(), BorderLayout.NORTH);
        panel.add(_createBaliseUpDownPanel(), BorderLayout.WEST);
        panel.add(_createBaliseScrollPane(), BorderLayout.CENTER);
        return panel;
    }

    private JPanel _createBaliseButtonsPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        _baliseTitleLabel = new JLabel(L10n.getString("raid_panel.balise.label"));
        _baliseCountLabel = new JLabel("0");
        panel.add(_baliseCountLabel);
        panel.add(_baliseTitleLabel);
        panel.add(_createAddBaliseButton());
        panel.add(_createAddMultipleBalisesButton());
        panel.add(_createEditBaliseButton());
        panel.add(_createRemoveBaliseButton());
        panel.add(_createOcadBaliseButton());
        return panel;
    }

    private JPanel _createBaliseUpDownPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(_createBaliseUpButton());
        panel.add(_createBaliseDownButton());
        return panel;
    }

    private JButton _createBaliseUpButton() {
        JButton button = new IconButton("up.png");
        button.setToolTipText(L10n.getString("raid_panel.balise.move_up.tooltip"));
        button.addActionListener(event -> _presenter.moveUpBalise());
        return button;
    }

    private JButton _createBaliseDownButton() {
        JButton button = new IconButton("down.png");
        button.setToolTipText(L10n.getString("raid_panel.balise.move_down.tooltip"));
        button.addActionListener(event -> _presenter.moveDownBalise());
        return button;
    }

    private JButton _createOcadBaliseButton() {
        JButton button = new IconButton("ocad.png");
        button.setToolTipText(L10n.getString("raid_panel.balise.import_ocad"));
        button.addActionListener(event -> _presenter.importEpreuveFromOcad());
        return button;
    }

    private JButton _createAddBaliseButton() {
        JButton button = new IconButton(ADD_IMAGE_NAME);
        button.setToolTipText(L10n.getString("raid_panel.balise.add.tooltip"));
        button.addActionListener(event -> _presenter.addBalise());
        return button;
    }

    private JButton _createEditBaliseButton() {
        JButton button = new IconButton(EDIT_IMAGE_NAME);
        button.setToolTipText(L10n.getString("raid_panel.balise.edit.tooltip"));
        button.addActionListener(event -> _presenter.editBalise());
        return button;
    }

    private JButton _createRemoveBaliseButton() {
        JButton button = new IconButton(DELETE_IMAGE_NAME);
        button.setToolTipText(L10n.getString("raid_panel.balise.delete.tooltip"));
        button.addActionListener(event -> _presenter.deleteBalise());
        return button;
    }

    private JScrollPane _createBaliseScrollPane() {
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setPreferredSize(new Dimension(200, 90));
        scrollPane.setViewportView(_createBaliseList());
        return scrollPane;
    }

    private JList<Balise> _createBaliseList() {
        JList<Balise> _baliseList = _presenter.getBaliseList();
        _baliseList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked( MouseEvent event ) {
                if (event.getClickCount() == 2) {
                    _presenter.editBalise();
                }
            }
        });
        return _baliseList;
    }

    private JButton _createAddMultipleBalisesButton() {
        JButton button = new IconButton("add-multiple.png");
        button.setToolTipText(L10n.getString("raid_panel.balise.add_multiple.tooltip"));
        button.addActionListener(event -> _presenter.addMultipleBalises());
        return button;
    }
}
