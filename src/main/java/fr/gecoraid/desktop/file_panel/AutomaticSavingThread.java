package fr.gecoraid.desktop.file_panel;

import javax.swing.SwingWorker;

class AutomaticSavingThread extends SwingWorker<Void, Void> {
    private Thread _currentThread = null;
    private final FilePanelPresenter _presenter;

    AutomaticSavingThread( FilePanelPresenter presenter ) {
        _presenter = presenter;
    }

    @Override
    protected Void doInBackground() throws Exception {
        _currentThread = Thread.currentThread();
        while (!_currentThread.isInterrupted()) {
            if (_presenter.backupFolderExists()) {
                _presenter.startBackup();
                _presenter.setBackUpStateOk();
            } else {
                _presenter.setBackupStateNotOk();
            }
            //noinspection BusyWait
            Thread.sleep(1000);
            _presenter.setBackupStateNormal();
            //noinspection BusyWait
            Thread.sleep(_presenter.getBackupFrequency() * 60000L);
        }
        return null;
    }

    void stop() {
        if (_currentThread != null) {
            _currentThread.interrupt();
        }
    }
}
