package fr.gecoraid.desktop.file_panel;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DeskTopPresenter;
import fr.gecoraid.desktop.DesktopView;
import fr.gecoraid.desktop.widget.IconButton;
import fr.gecoraid.tools.ExtensionFileFilter;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import java.awt.Dimension;
import java.io.File;

public class FilePanelView extends JPanel {
    private final transient FilePanelPresenter _presenter;

    private JButton _saveButton;
    private JButton _backupButton;
    private JSpinner _backupSpinner;
    private JButton _startBackupButton;

    public FilePanelView( DeskTopPresenter deskTopPresenter ) {
        _presenter = new FilePanelPresenter(this, deskTopPresenter);
        _initializeUI();
    }

    public void setSavingOkState() {
        //noinspection ConstantConditions
        _saveButton.setIcon(new ImageIcon(getClass().getClassLoader().getResource("fr/gecoraid/icon/success.png")));
    }

    public void setSavingNormalState() {
        //noinspection ConstantConditions
        _saveButton.setIcon(new ImageIcon(getClass().getClassLoader().getResource("fr/gecoraid/icon/save.png")));
    }

    void setBackupStateOk() {
        //noinspection ConstantConditions
        _backupButton.setIcon(new ImageIcon(DesktopView.class.getClassLoader().getResource("fr/gecoraid/icon/success.png")));
    }

    void setBackupStateNotOk() {
        //noinspection ConstantConditions
        _backupButton.setIcon(new ImageIcon(DesktopView.class.getClassLoader().getResource("fr/gecoraid/icon/error.png")));
    }

    void setBackupStateNormal() {
        //noinspection ConstantConditions
        _backupButton.setIcon(new ImageIcon(DesktopView.class.getClassLoader().getResource("fr/gecoraid/icon/backup.png")));
    }

    int getBackupFrequency() {
        return (int) _backupSpinner.getValue();
    }

    void startBackupButton() {
        //noinspection ConstantConditions
        _startBackupButton.setIcon(new ImageIcon(DesktopView.class.getClassLoader().getResource("fr/gecoraid/icon/stop-backup.png")));
        _startBackupButton.setToolTipText(L10n.getString("file_panel.backup.stop"));
    }

    void stopBackupButton() {
        //noinspection ConstantConditions
        _startBackupButton.setIcon(new ImageIcon(DesktopView.class.getClassLoader().getResource("fr/gecoraid/icon/start-backup.png")));
        _startBackupButton.setToolTipText(L10n.getString("file_panel.backup.start"));
    }

    private void _initializeUI() {
        setBorder(new TitledBorder(null, L10n.getString("file_panel.title"), TitledBorder.LEADING, TitledBorder.TOP));
        add(_createNewFileButton());
        add(_createOpenFileButton());
        add(_createSaveFileButton());
        add(_createBackupButton());
        add(_createExportToCsvButton());
        add(createSeparator());
        add(createFrequencyPanel());
        add(createStartBackupButton());
    }

    private JButton _createNewFileButton() {
        JButton button = new IconButton("new.png");
        button.setToolTipText(L10n.getString("file_panel.new"));
        button.addActionListener(event -> _presenter.createNewRaidFile());
        return button;
    }

    private JButton _createOpenFileButton() {
        JButton button = new IconButton("open.png");
        button.setToolTipText(L10n.getString("file_panel.open"));
        button.addActionListener(event -> _presenter.openRaidFile());
        return button;
    }

    private JButton _createSaveFileButton() {
        _saveButton = new IconButton("save.png");
        _saveButton.setToolTipText(L10n.getString("file_panel.save"));
        _saveButton.addActionListener(event -> _presenter.saveCurrentRaid());
        return _saveButton;
    }

    private JButton _createBackupButton() {
        _backupButton = new IconButton("backup.png");
        _backupButton.setToolTipText(L10n.getString("file_panel.backup"));
        _backupButton.addActionListener(event -> _presenter.startBackup());
        return _backupButton;
    }

    private JButton _createExportToCsvButton() {
        JButton button = new IconButton("export-csv.png");
        button.setToolTipText(L10n.getString("file_panel.export.tooltip"));
        button.addActionListener(event -> {
            JFileChooser chooser = new JFileChooser();
            ExtensionFileFilter filter = new ExtensionFileFilter("csv", L10n.getString("csv_file.description"));
            chooser.setFileFilter(filter);
            chooser.setCurrentDirectory(new File(_presenter.getRaidFolderPath()));
            int returnVal = chooser.showSaveDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                _presenter.exportToCsv(chooser.getSelectedFile().getAbsolutePath());
            }
        });
        return button;
    }

    private JSpinner _createBackupFrequencySpinner() {
        _backupSpinner = new JSpinner();
        _backupSpinner.setModel(new SpinnerNumberModel(1, 1, 59, 1));
        return _backupSpinner;
    }

    private static JSeparator createSeparator() {
        JSeparator separator = new JSeparator(SwingConstants.VERTICAL);
        separator.setPreferredSize(new Dimension(5, 50));
        return separator;
    }

    private JPanel createFrequencyPanel() {
        JPanel panel = new JPanel();
        panel.add(new JLabel(L10n.getString("file_panel.auto_save.label")));
        panel.add(_createBackupFrequencySpinner());
        panel.add(new JLabel(L10n.getString("file_panel.auto_save.unit")));
        return panel;
    }

    private JButton createStartBackupButton() {
        _startBackupButton = new IconButton("start-backup.png");
        _startBackupButton.setToolTipText(L10n.getString("file_panel.backup.tooltip"));
        _startBackupButton.addActionListener(action -> _presenter.startOrStopBackup());
        return _startBackupButton;
    }
}
