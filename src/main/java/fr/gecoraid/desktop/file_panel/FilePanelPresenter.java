package fr.gecoraid.desktop.file_panel;

import fr.gecoraid.desktop.DeskTopPresenter;
import fr.gecoraid.model.CsvEquipes;
import fr.gecoraid.tools.Utils;

class FilePanelPresenter {
    private final FilePanelView _filePanelView;
    private final DeskTopPresenter _parent;
    private AutomaticSavingThread _backupTask;

    FilePanelPresenter( FilePanelView filePanelView, DeskTopPresenter parent ) {
        _filePanelView = filePanelView;
        _parent = parent;
    }

    boolean backupFolderExists() {
        return _parent.backupFolderExists();
    }

    long getBackupFrequency() {
        return _filePanelView.getBackupFrequency();
    }

    void setBackUpStateOk() {
        _filePanelView.setBackupStateOk();
    }

    void setBackupStateNotOk() {
        _filePanelView.setBackupStateNotOk();
    }

    void setBackupStateNormal() {
        _filePanelView.setBackupStateNormal();
    }

    void startBackup() {
        _parent.backup();
    }

    void createNewRaidFile() {
        _parent.createNewRaid();
    }

    void openRaidFile() {
        _parent.openRaidFile();
    }

    void saveCurrentRaid() {
        _parent.saveCurrentRaid();
    }

    String getRaidFolderPath() {
        return _parent.getRaidFolderPath();
    }

    void exportToCsv( String absolutePath ) {
        String fileName = Utils.checkExtension(absolutePath, ".csv");
        CsvEquipes.exportAllTeams(_parent.getRaid(), fileName);
    }

    void startOrStopBackup() {
        if (_backupTask != null) {
            _backupTask.stop();
            _backupTask = null;
            _filePanelView.stopBackupButton();
        } else {
            _backupTask = new AutomaticSavingThread(this);
            _backupTask.execute();
            _filePanelView.startBackupButton();
        }
    }
}
