package fr.gecoraid.desktop;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Frame;
import java.util.concurrent.atomic.AtomicReference;

// TODO mettre une visibilite package
public class RecentFileDialog {
    private static JDialog _optionWindow;

    public static int showInputDialog( Frame parent,
                                       String title,
                                       String message,
                                       String[] data,
                                       String cancelLabel ) {
        AtomicReference<Integer> returnValue = new AtomicReference<>(-1);
        JPanel container = new JPanel(new BorderLayout());

        JLabel labelMessage = new JLabel(message);
        labelMessage.setBorder(new EmptyBorder(10, 10, 10, 10));
        labelMessage.setHorizontalAlignment(SwingConstants.CENTER);
        container.add(labelMessage, BorderLayout.NORTH);

        JList<String> dataList = new JList<>(data);
        dataList.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
        dataList.addListSelectionListener(event -> {
            if (!event.getValueIsAdjusting()) {
                returnValue.set(dataList.getSelectedIndex());
                _optionWindow.dispose();
            }
        });
        dataList.setPreferredSize(new Dimension(400, 200));
        JPanel panel = new JPanel();
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        panel.add(dataList);
        container.add(panel, BorderLayout.CENTER);

        JPanel buttonContainer = new JPanel(new FlowLayout());
        JButton cancelButton = new JButton(cancelLabel);
        cancelButton.addActionListener(event -> _optionWindow.dispose());
        buttonContainer.add(cancelButton);
        container.add(buttonContainer, BorderLayout.SOUTH);

        _optionWindow = new JDialog(parent, title, true);
        _optionWindow.setContentPane(container);
        _optionWindow.pack();
        _optionWindow.setLocationRelativeTo(parent);
        _optionWindow.setVisible(true);

        return returnValue.get();
    }
}