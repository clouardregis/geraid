package fr.gecoraid.desktop;

import fr.gecoraid.L10n;
import fr.gecoraid.model.Equipe;
import fr.gecoraid.model.Etape;
import fr.gecoraid.model.Parcours;
import fr.gecoraid.model.Partial;
import fr.gecoraid.model.ResultatPuce;
import fr.gecoraid.desktop.widget.IconButton;
import fr.gecoraid.tools.TimeManager;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.KeyEvent;
import java.util.Date;

public class ChipResultManagementDialog extends JDialog {
    private static final long serialVersionUID = 1L;

    private final transient DeskTopPresenter _presenter;

    // TODO séparer la vue du modèle
    private transient ResultatPuce _resultatCourant;
    private JComboBox<Parcours> _comboBoxParcours;
    private JComboBox<Etape> _comboBoxEtape;
    private JComboBox<Equipe> _comboBoxEquipe;
    private JPanel _panelCenter;
    private JList<Partial> _jListPostes;

    private JSpinner _jSpinnerHeureD;
    private JSpinner _jSpinnerMinuteD;
    private JSpinner _jSpinnerSecondeD;
    private JSpinner _jSpinnerHeureA;
    private JSpinner _jSpinnerMinuteA;
    private JSpinner _jSpinnerSecondeA;

    // TODO Trop grande classe -> diviser
    public ChipResultManagementDialog( DesktopView parent, DeskTopPresenter presenter ) {
        super(parent);
        _presenter = presenter; // TODO review code with MVP architecture
        _initializeUI();
        _panelCenter.setVisible(false);
        _initParcours();
    }

    public ChipResultManagementDialog( DeskTopPresenter presenter, Parcours parcours, Etape etape, Equipe equipe ) {
        _presenter = presenter; // TODO review code with MVP architecture
        _initializeUI();
        _panelCenter.setVisible(false);
        _initParcours();
        _comboBoxParcours.setSelectedItem(parcours);
        _comboBoxEtape.setSelectedItem(etape);
        _comboBoxEquipe.setSelectedItem(equipe);
    }

    public void showDialog() {
        setVisible(true);
    }

    ResultatPuce getResultatCourant() {
        return _resultatCourant;
    }

    private void _initParcours() {
        _comboBoxParcours.setModel(new DefaultComboBoxModel<>(_presenter.getRaid().getResultatsPuce().getParcours()));
        if (_comboBoxParcours.getItemCount() > 0) {
            _comboBoxParcours.setSelectedIndex(0);
            _comboBoxEtape.setVisible(true);
            _comboBoxEquipe.setVisible(true);
            _panelCenter.setVisible(true);
        } else {
            _comboBoxParcours.setSelectedIndex(-1);
            _comboBoxEtape.setVisible(false);
            _comboBoxEquipe.setVisible(false);
            _panelCenter.setVisible(false);
        }
    }

    private void _initializeUI() {
        setModal(true);
        // TODO L10n.getString("chip_result_management_dialog")
        setTitle("Gestion des résultats");
        setContentPane(_getContentPane());
        pack();
    }

    private JPanel _getContentPane() {
        JPanel jContentPane = new JPanel();
        jContentPane.setBorder(new EmptyBorder(10, 10, 10, 10));
        jContentPane.setLayout(new BorderLayout());
        jContentPane.add(_getPanelChoixResultat(), BorderLayout.NORTH);
        jContentPane.add(_getPanelResultat(), BorderLayout.CENTER);
        jContentPane.add(_getPanelInformation(), BorderLayout.SOUTH);
        return jContentPane;
    }

    private JPanel _getPanelChoixResultat() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        // TODO L10n.getString("chip_result_management_dialog")
        panel.setBorder(new TitledBorder(null, "Choix du résultat", TitledBorder.LEADING, TitledBorder.TOP));
        panel.add(_createJPanelParcours());
        panel.add(_createJPanelEtape());
        panel.add(_createJPanelEquipe());
        return panel;
    }

    private JPanel _createJPanelParcours() {
        // TODO L10n.getString("chip_result_management_dialog")
        JLabel label = new JLabel("Parcours :");
        label.setPreferredSize(new Dimension(70, 16));
        JPanel jPanelParcours = new JPanel(new FlowLayout(FlowLayout.LEFT));
        jPanelParcours.add(label);
        jPanelParcours.add(_createJComboBoxParcours());
        return jPanelParcours;
    }

    private JComboBox<Parcours> _createJComboBoxParcours() {
        _comboBoxParcours = new JComboBox<>();
        Dimension preferredSize = _comboBoxParcours.getPreferredSize();
        _comboBoxParcours.setPreferredSize(new Dimension(260, preferredSize.height));
        _comboBoxParcours.addActionListener(event -> {
            if (_comboBoxParcours.getSelectedIndex() != -1) {
                _comboBoxEtape.setModel(new DefaultComboBoxModel<>(_presenter.getRaid().getResultatsPuce().getEtapes((Parcours) _comboBoxParcours.getSelectedItem())));
                if (_comboBoxEtape.getItemCount() > 0) {
                    _comboBoxEtape.setSelectedIndex(0);
                } else {
                    _comboBoxEtape.setSelectedIndex(-1);
                }
            }
        });
        return _comboBoxParcours;
    }

    private JPanel _createJPanelEtape() {
        // TODO L10n.getString("chip_result_management_dialog")
        JLabel label = new JLabel("Étape :");
        label.setPreferredSize(new Dimension(70, 16));
        FlowLayout flowLayout2 = new FlowLayout();
        flowLayout2.setAlignment(FlowLayout.LEFT);
        JPanel jPanelEtape = new JPanel();
        jPanelEtape.setLayout(flowLayout2);
        jPanelEtape.add(label);
        jPanelEtape.add(_createJComboBoxEtape());
        return jPanelEtape;
    }

    private JComboBox<Etape> _createJComboBoxEtape() {
        _comboBoxEtape = new JComboBox<>();
        Dimension preferredSize = _comboBoxEtape.getPreferredSize();
        _comboBoxEtape.setPreferredSize(new Dimension(260, preferredSize.height));
        _comboBoxEtape.addActionListener(event -> {
            if (_comboBoxEtape.getSelectedIndex() != -1) {
                _comboBoxEquipe.setModel(new DefaultComboBoxModel<>(_presenter.getRaid().getResultatsPuce().getEquipes((Etape) _comboBoxEtape.getSelectedItem())));
                if (_comboBoxEquipe.getItemCount() > 0) {
                    _comboBoxEquipe.setSelectedIndex(0);
                } else {
                    _comboBoxEquipe.setSelectedIndex(-1);
                }
            }
        });
        return _comboBoxEtape;
    }

    private JPanel _createJPanelEquipe() {
        // TODO L10n.getString("chip_result_management_dialog")
        JLabel label = new JLabel("Equipe :");
        label.setPreferredSize(new Dimension(70, 16));
        FlowLayout flowLayout3 = new FlowLayout();
        flowLayout3.setAlignment(FlowLayout.LEFT);
        JPanel jPanelEquipe = new JPanel();
        jPanelEquipe.setLayout(flowLayout3);
        jPanelEquipe.add(label);
        jPanelEquipe.add(_createJComboBoxEquipe());
        return jPanelEquipe;
    }

    private JComboBox<Equipe> _createJComboBoxEquipe() {
        _comboBoxEquipe = new JComboBox<>();
        Dimension preferredSize = _comboBoxEquipe.getPreferredSize();
        _comboBoxEquipe.setPreferredSize(new Dimension(260, preferredSize.height));
        _comboBoxEquipe.addActionListener(event -> {
            if (_comboBoxEquipe.getSelectedIndex() != -1) {
                _panelCenter.setVisible(true);
                _resultatCourant = _presenter.getRaid().getResultatsPuce().getResultatPuce(
                        (Etape) _comboBoxEtape.getSelectedItem(),
                        (Equipe) _comboBoxEquipe.getSelectedItem());
                _jSpinnerHeureD.setValue(TimeManager.getHeures(_resultatCourant.getPuce().getStarttime()));
                _jSpinnerMinuteD.setValue(TimeManager.getMinutes(_resultatCourant.getPuce().getStarttime()));
                _jSpinnerSecondeD.setValue(TimeManager.getSecondes(_resultatCourant.getPuce().getStarttime()));
                _jSpinnerHeureA.setValue(TimeManager.getHeures(_resultatCourant.getPuce().getFinishtime()));
                _jSpinnerMinuteA.setValue(TimeManager.getMinutes(_resultatCourant.getPuce().getFinishtime()));
                _jSpinnerSecondeA.setValue(TimeManager.getSecondes(_resultatCourant.getPuce().getFinishtime()));
                _jListPostes.setListData(_resultatCourant.getPuce().getPartiels());
                _jListPostes.setSelectedIndex(0);
            } else {
                _panelCenter.setVisible(false);
            }
        });
        return _comboBoxEquipe;
    }

    private JPanel _getPanelResultat() {
        _panelCenter = new JPanel();
        // TODO L10n.getString("chip_result_management_dialog")
        _panelCenter.setBorder(new TitledBorder(null, "Résultat", TitledBorder.LEADING, TitledBorder.TOP));
        _panelCenter.setLayout(new BorderLayout());
        _panelCenter.add(_createJPaneltemps(), BorderLayout.NORTH);
        _panelCenter.add(_createJPanelPostes(), BorderLayout.CENTER);
        return _panelCenter;
    }

    private JPanel _createJPaneltemps() {
        JPanel panel = new JPanel();
        BoxLayout layout = new BoxLayout(panel, BoxLayout.Y_AXIS);
        panel.setLayout(layout);

        JPanel panel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        // TODO L10n.getString("chip_result_management_dialog")
        panel2.add(new JLabel("Supprimer ce résultat :"));
        panel2.add(_createJButtonSupprimerResultat());
        panel.add(panel2);

        JPanel panel3 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        // TODO L10n.getString("chip_result_management_dialog")
        panel3.add(new JLabel("Visualiser ce résultat :"));
        panel3.add(_createJButtonVisu());
        panel.add(panel3);

        panel.add(_createJPanelStart());
        panel.add(_createJPanelFinish());
        return panel;
    }

    private JPanel _createJPanelStart() {
        JPanel jPanelStart = new JPanel(new FlowLayout(FlowLayout.LEFT));
        // TODO L10n.getString("chip_result_management_dialog")
        jPanelStart.add(new JLabel("Départ :"));
        jPanelStart.add(_createJSpinnerHeureD());
        jPanelStart.add(new JLabel(L10n.getString("hour_abbreviation")));
        jPanelStart.add(_createJSpinnerMinuteD());
        jPanelStart.add(new JLabel(L10n.getString("minute_abbreviation")));
        jPanelStart.add(_createJSpinnerSecondeD());
        return jPanelStart;
    }

    private JPanel _createJPanelFinish() {
        JPanel jPanelFinish = new JPanel(new FlowLayout(FlowLayout.LEFT));
        // TODO L10n.getString("chip_result_management_dialog")
        jPanelFinish.add(new JLabel("Arrivée :"));
        jPanelFinish.add(_createJSpinnerHeureA());
        jPanelFinish.add(new JLabel(L10n.getString("hour_abbreviation")));
        jPanelFinish.add(_createJSpinnerMinuteA());
        jPanelFinish.add(new JLabel(L10n.getString("minute_abbreviation")));
        jPanelFinish.add(_createJSpinnerSecondeA());
        return jPanelFinish;
    }

    private JSpinner _createJSpinnerHeureD() {
        _jSpinnerHeureD = new JSpinner(new SpinnerNumberModel(0, 0, 23, 1));
        _jSpinnerHeureD.setPreferredSize(new Dimension(40, 20));
        _jSpinnerHeureD.addChangeListener(event -> {
            Date st = _resultatCourant.getPuce().getStarttime();
            st = TimeManager.setHeure(st, (Integer) _jSpinnerHeureD.getValue());
            _resultatCourant.getPuce().setStarttime(st);
        });
        return _jSpinnerHeureD;
    }

    private JSpinner _createJSpinnerMinuteD() {
        _jSpinnerMinuteD = new JSpinner(new SpinnerNumberModel(0, 0, 59, 1));
        _jSpinnerMinuteD.setPreferredSize(new Dimension(40, 20));
        _jSpinnerMinuteD.addChangeListener(event -> {
            Date st = _resultatCourant.getPuce().getStarttime();
            st = TimeManager.setMinute(st, (Integer) _jSpinnerMinuteD.getValue());
            _resultatCourant.getPuce().setStarttime(st);
        });
        return _jSpinnerMinuteD;
    }

    private JSpinner _createJSpinnerSecondeD() {
        _jSpinnerSecondeD = new JSpinner(new SpinnerNumberModel(0, 0, 59, 1));
        _jSpinnerSecondeD.setPreferredSize(new Dimension(40, 20));
        _jSpinnerSecondeD.addChangeListener(event -> {
            Date st = _resultatCourant.getPuce().getStarttime();
            st = TimeManager.setSeconde(st, (Integer) _jSpinnerSecondeD.getValue());
            _resultatCourant.getPuce().setStarttime(st);
        });
        return _jSpinnerSecondeD;
    }

    private JSpinner _createJSpinnerHeureA() {
        _jSpinnerHeureA = new JSpinner(new SpinnerNumberModel(0, 0, 47, 1));
        _jSpinnerHeureA.setPreferredSize(new Dimension(40, 20));
        _jSpinnerHeureA.addChangeListener(event -> {
            Date st = _resultatCourant.getPuce().getFinishtime();
            st = TimeManager.setHeure(st, (Integer) _jSpinnerHeureA.getValue());
            _resultatCourant.getPuce().setFinishtime(st);
        });
        return _jSpinnerHeureA;
    }

    private JSpinner _createJSpinnerMinuteA() {
        _jSpinnerMinuteA = new JSpinner(new SpinnerNumberModel(0, 0, 59, 1));
        _jSpinnerMinuteA.setPreferredSize(new Dimension(40, 20));
        _jSpinnerMinuteA.addChangeListener(event -> {
            Date st = _resultatCourant.getPuce().getFinishtime();
            st = TimeManager.setMinute(st, (Integer) _jSpinnerMinuteA.getValue());
            _resultatCourant.getPuce().setFinishtime(st);
        });
        return _jSpinnerMinuteA;
    }

    private JSpinner _createJSpinnerSecondeA() {
        _jSpinnerSecondeA = new JSpinner(new SpinnerNumberModel(0, 0, 59, 1));
        _jSpinnerSecondeA.setPreferredSize(new Dimension(40, 20));
        _jSpinnerSecondeA.addChangeListener(event -> {
            Date st = _resultatCourant.getPuce().getFinishtime();
            st = TimeManager.setSeconde(st, (Integer) _jSpinnerSecondeA.getValue());
            _resultatCourant.getPuce().setFinishtime(st);
        });
        return _jSpinnerSecondeA;
    }

    private JPanel _createJPanelPostes() {
        JPanel jPanelPostes = new JPanel(new BorderLayout());
        // TODO L10n.getString("chip_result_management_dialog")
        jPanelPostes.setBorder(new TitledBorder(null, "Postes", TitledBorder.LEADING, TitledBorder.TOP));
        jPanelPostes.add(_createJPanelButtons(), BorderLayout.NORTH);
        jPanelPostes.add(_createJScrollPane(), BorderLayout.CENTER);
        jPanelPostes.add(_createJPanelOrderButton(), BorderLayout.WEST);
        return jPanelPostes;
    }

    private JPanel _createJPanelButtons() {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        jPanel.add(_createJButtonAjouter());
        jPanel.add(_createJButtonEdit());
        jPanel.add(_createJButtonSupprimer());
        return jPanel;
    }

    private JButton _createJButtonAjouter() {
        JButton jButtonAjouter = new IconButton("add.png");
        // TODO L10n.getString("chip_result_management_dialog")
        jButtonAjouter.setToolTipText("Ajouter un poste");
        jButtonAjouter.addActionListener(event -> {
            IhmPartiel ihm = new IhmPartiel(ChipResultManagementDialog.this, new Partial(), true);
            ihm.setLocationRelativeTo(null);
            ihm.setVisible(true); // TODO ShowDialog()
            _jListPostes.setListData(_resultatCourant.getPuce().getPartiels());
            _jListPostes.setSelectedIndex(0);
        });
        return jButtonAjouter;
    }

    private JButton _createJButtonEdit() {
        JButton jButtonEdit = new IconButton("edit.png");
        // TODO L10n.getString("chip_result_management_dialog")
        jButtonEdit.setToolTipText("Modifier le poste");
        jButtonEdit.addActionListener(event -> {
            if (_jListPostes.getModel().getSize() > 0) {
                IhmPartiel ihm = new IhmPartiel(ChipResultManagementDialog.this, _jListPostes.getSelectedValue(), false);
                ihm.setLocationRelativeTo(null);
                ihm.setVisible(true); // TODO ShowDialog()
                _jListPostes.setListData(_resultatCourant.getPuce().getPartiels());
                _jListPostes.setSelectedIndex(0);
            }
        });
        return jButtonEdit;
    }

    private JButton _createJButtonSupprimer() {
        JButton jButtonSupprimer = new IconButton("delete.png");
        // TODO L10n.getString("chip_result_management_dialog")
        jButtonSupprimer.setToolTipText("Supprimer le poste");
        jButtonSupprimer.addActionListener(event -> {
            if (_jListPostes.getModel().getSize() > 0) {
                int reply = JOptionPane.showConfirmDialog(null,
                        // TODO L10n.getString("chip_result_management_dialog")
                        "Souhaitez-vous supprimer définitivement ce partiel\u00A0?", "Confirmation",
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (reply == JOptionPane.YES_OPTION) {
                    _resultatCourant.getPuce().removePartiel(_jListPostes.getSelectedValue());
                }
                _jListPostes.setListData(_resultatCourant.getPuce().getPartiels());
                _jListPostes.setSelectedIndex(0);
            }
        });
        return jButtonSupprimer;
    }

    private JPanel _createJPanelOrderButton() {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(new BoxLayout(jPanel, BoxLayout.Y_AXIS));
        jPanel.add(_createJButtonUp());
        jPanel.add(_createJButtonDown());
        return jPanel;
    }

    private JButton _createJButtonUp() {
        JButton jButtonUp = new IconButton("up.png");
        // TODO L10n.getString("chip_result_management_dialog")
        jButtonUp.setToolTipText("Monter le poste");
        jButtonUp.addActionListener(event -> {
            if (_jListPostes.getSelectedIndex() >= 0) {
                _resultatCourant.getPuce().upBalise(_jListPostes.getSelectedValue());
                _jListPostes.repaint();
                _jListPostes.setSelectedIndex(_jListPostes.getSelectedIndex() - 1);
            }
        });
        return jButtonUp;
    }

    private JButton _createJButtonDown() {
        JButton jButtonDown = new IconButton("down.png");
        jButtonDown.setMnemonic(KeyEvent.VK_UNDEFINED);
        // TODO L10n.getString("chip_result_management_dialog")
        jButtonDown.setToolTipText("Descendre le poste");
        jButtonDown.addActionListener(event -> {
            if (_jListPostes.getSelectedIndex() >= 0) {
                _resultatCourant.getPuce().downBalise(_jListPostes.getSelectedValue());
                _jListPostes.repaint();
                _jListPostes.setSelectedIndex(_jListPostes.getSelectedIndex() + 1);
            }
        });
        return jButtonDown;
    }

    private JComponent _getPanelInformation() {
        // TODO L10n.getString("chip_result_management_dialog")
        JLabel label = new JLabel("<html>Attention, les modifications effectuées dans cette fenêtre<br>sont immédiatement prises en compte.</html");
        label.setForeground(Color.red);
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.add(label);
        return panel;
    }

    private JButton _createJButtonSupprimerResultat() {
        JButton jButtonSupprimerResultat = new IconButton("delete.png");
        // TODO L10n.getString("chip_result_management_dialog.tooltip")
        jButtonSupprimerResultat.setToolTipText("Supprimer ce résultat");
        jButtonSupprimerResultat.addActionListener(event -> {
            int reply = JOptionPane.showConfirmDialog(null,
                    // TODO L10n.getString("chip_result_management_dialog")
                    "Souhaitez-vous supprimer définitivement ce résultat\u00A0?",
                    "Confirmation",
                    JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            if (reply == JOptionPane.YES_OPTION) {
                _presenter.getRaid().getResultatsPuce().removeResultatPuce(_resultatCourant);
                _initParcours();
            }
        });
        return jButtonSupprimerResultat;
    }

    private JScrollPane _createJScrollPane() {
        JScrollPane jScrollPane = new JScrollPane();
        jScrollPane.setViewportView(_createJListPostes());
        return jScrollPane;
    }

    private JList<Partial> _createJListPostes() {
        _jListPostes = new JList<>();
        _jListPostes.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        return _jListPostes;
    }

    private JButton _createJButtonVisu() {
        final JButton button = new IconButton("podium.png");
        button.setMnemonic(KeyEvent.VK_UNDEFINED);
        button.addActionListener(event -> _presenter.ihmResultatPuce(_resultatCourant, true, true));
        return button;
    }
}
