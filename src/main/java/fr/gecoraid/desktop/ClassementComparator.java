package fr.gecoraid.desktop;

import java.util.Comparator;

// TODO put in the convenient folder
// TODO mettre une visibilite package
public abstract class ClassementComparator implements Comparator<Object[]> {
// TODO Refactor
    @Override
    public int compare( Object[] arg0, Object[] arg1 ) {
        int temps = ((String) arg0[5]).compareTo((String) arg1[5]);
        Integer a = Integer.parseInt((String) arg0[6]);
        Integer b = Integer.parseInt((String) arg1[6]);
        int points = (a).compareTo(b);

        if (points < 0) {
            return 1;
        } else if (points > 0) {
            return -1;
        } else {
            return Integer.compare(temps, 0);
        }
    }
}
