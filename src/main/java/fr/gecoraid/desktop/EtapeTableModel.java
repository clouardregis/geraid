package fr.gecoraid.desktop;

import fr.gecoraid.model.Equipe;
import fr.gecoraid.model.Parcours;
import fr.gecoraid.model.ResultatEtape;

import javax.swing.JTable;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.print.PageFormat;
import java.awt.print.Printable;

// TODO mettre une visibilite package
public class EtapeTableModel extends DataTableModel implements Printable {
    private static final long serialVersionUID = -5974347188812805359L;
    private final transient Object[][] _data;
    private final transient ResultatEtape _resultatEtape;

    public EtapeTableModel( ResultatEtape re ) {
        super(re.getEntetes());
        _resultatEtape = re;
        _data = re.getData();
    }

    @Override
    public int getColumnCount() {
        return _columnNames.length;
    }

    @Override
    public int getRowCount() {
        return _data.length;
    }

    @Override
    public String getColumnName( int col ) {
        return _columnNames[col];
    }

    @Override
    public Object getValueAt( int row, int col ) {
        if (getRowCount() > 0) {
            if ((_data[row][col] + "").compareTo("0") == 0 || (_data[row][col] + "").compareTo("0:00:00") == 0) {
                return "";
            } else {
                return _data[row][col];
            }
        }
        return "";
    }

    @Override
    public Class<?> getColumnClass( int c ) {
        return getValueAt(0, c).getClass();
    }

    @Override
    public void setValueAt( Object value, int row, int col ) {
        _data[row][col] = value;
        fireTableCellUpdated(row, col);
    }

    @Override
    public String[] getDataAsCsv() {
        String[] retour = new String[getRowCount()];
        for (int r = 0; r < getRowCount(); r++) {
            StringBuilder ligne = new StringBuilder(getValueAt(r, 0).toString());
            for (int c = 1; c < getColumnCount(); c++) {
                ligne.append(";" + "\"").append(getValueAt(r, c)).append("\"");
            }
            retour[r] = ligne.toString();
        }

        return retour;
    }

    public String getHeaderAsHtml() {
        StringBuilder retour = new StringBuilder("<table><tr style='font-weight: bold;'>");
        for (int i = 0; i < 7; i++) {
            if (i != 2) {
                retour.append("<td>").append(_columnNames[i]).append("</td>");
            }
        }
        retour.append("</tr>");
        return retour.toString();
    }

    public String getDataAsHtml( JTable table ) {
        StringBuilder retour = new StringBuilder();
        for (int r = 0; r < getRowCount(); r++) {
            retour.append("<tr style='");
            if (r % 2 == 0) {
                retour.append("background-color: #FFFFE0;'");
            } else {
                retour.append("background-color: #E0FFFF;'");
            }
            retour.append(" align=center>");
            for (int c = 0; c < 7; c++) {
                if (c != 2) {
                    retour.append("<td>").append(getValueAt(table.convertRowIndexToModel(r), c)).append("</td>");
                }
            }
            retour.append("</tr>");
        }
        retour.append("</table>");
        if (getRowCount() < 25) {
            for (int e = getRowCount(); e < 25; e++) {
                retour.append("<br>");
            }
        }

        return retour.toString();
    }

    public String getDataAsHtml( JTable table, int index ) {
        StringBuilder retour = new StringBuilder();
        if (getRowCount() < index) {
            index = getRowCount();
        }
        for (int r = 0; r < index; r++) {
            retour.append("<tr style='");
            if (r % 2 == 0) {
                retour.append("background-color: #FFFFE0;'");
            } else {
                retour.append("background-color: #E0FFFF;'");
            }
            retour.append(" align=center>");
            for (int c = 0; c < 7; c++) {
                if (c != 2) {
                    if (c == 3) {
                        Parcours p = _resultatEtape.getGeRaid().getParcoursIdPuce((String) getValueAt(table.convertRowIndexToModel(r), 2));
                        Equipe equipe = p.getEquipes().getEquipeIdPuce((String) getValueAt(table.convertRowIndexToModel(r), 2));
                        retour.append("<td><b>").append(getValueAt(table.convertRowIndexToModel(r), c)).append("</b>");
                        for (int n = 0; n < equipe.getRaiders().getRaiders().size(); n++) {
                            retour.append("<br>").append(equipe.getRaiders().getRaiders().get(n).toString());
                        }
                        retour.append("</td>");
                    } else {
                        retour.append("<td>").append(getValueAt(table.convertRowIndexToModel(r), c)).append("</td>");
                    }
                }
            }
            retour.append("</tr>");
        }
        retour.append("</table>");

        return retour.toString();
    }

    @Override
    public int print( Graphics graphics, PageFormat pageFormat, int pageIndex ) {
        int retValue = Printable.NO_SUCH_PAGE;
        int size = 12;
        int marge = 10;
        int index = 0;
        if (pageIndex == 0) {
            graphics.setColor(Color.BLACK);
            index = index + size;
            graphics.setFont(new Font("Serif", Font.PLAIN, index));
            index = index + size;
            graphics.drawString(_resultatEtape.getGeRaid().getRaidName(), marge, index);
            index = index + size;
            // TODO L10n.getString()
            graphics.drawString("Parcours : " + _resultatEtape.getGeRaid().getParcours(_resultatEtape.getEtape()).getName(), marge, index);
            index = index + size;
            // TODO L10n.getString()
            graphics.drawString("Etape : " + _resultatEtape.getEtape().getName(), marge, index);
            index = index + size;
            // TODO L10n.getString()
            graphics.drawString("Catégorie : " + _resultatEtape.getCategorie().getLongName(), marge, index);
            index = index + size;
            index = index + size;

            for (Object[] datum : _data) {
                graphics.drawString(datum[0] + "  " + datum[3], marge, index);
                index = index + size;
            }
            // La page est valide
            retValue = Printable.PAGE_EXISTS;
        }
        return retValue;
    }
}
