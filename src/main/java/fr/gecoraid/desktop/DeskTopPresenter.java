package fr.gecoraid.desktop;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.raid_file.RaidFileXmlWriter;
import fr.gecoraid.desktop.setting_menu.GlobalSettingsDialog;
import fr.gecoraid.desktop.setting_menu.PlatformSettingsHandler;
import fr.gecoraid.desktop.setting_menu.RecentFileHandler;
import fr.gecoraid.desktop.sportident_panel.SportIdentCardPanel;
import fr.gecoraid.desktop.widget.ErrorPane;
import fr.gecoraid.model.Categorie;
import fr.gecoraid.model.Equipe;
import fr.gecoraid.model.GlobalSettings;
import fr.gecoraid.model.Parcours;
import fr.gecoraid.model.Raid;
import fr.gecoraid.model.ResultatPuce;
import fr.gecoraid.tools.ExtensionFileFilter;
import fr.gecoraid.tools.TimeManager;
import fr.gecoraid.tools.Utils;

import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import java.io.File;

import static fr.gecoraid.GeCoRaid.SOFTWARE_NAME;

// TODO mettre une visibilite package
public class DeskTopPresenter {
    private final DesktopView _desktopView;
    private final Raid _raid;
    private final GlobalSettings _globalSettings;
    private boolean _modified = false;
    private boolean _sportIdentReaderPuceEnCours = false;

    private final SportIdentReaderHandlerPuce _sportIdentHandlerPuce;
    private final SportIdentReaderHandler _sportIdentHandler;

    public DeskTopPresenter( DesktopView desktopView ) {
        _desktopView = desktopView;
        _globalSettings = new GlobalSettings();
        _raid = new Raid();
        _sportIdentHandler = new SportIdentReaderHandler(this);
        _sportIdentHandlerPuce = new SportIdentReaderHandlerPuce(_raid, this);
    }

    public Raid getRaid() {
        // TODO : Application de la règle de Déméter -> A supprimer à terme et passer par le presenteur lui-meme
        return _raid;
    }

    // TODO A supprimer
    public GlobalSettings getGlobalSettings() {
        return _globalSettings;
    }

    public String getRaidFolderPath() {
        return _globalSettings.getRaidFolderPath();
    }

    public JList<Equipe> getListEquipes() {
        return _desktopView.getListEquipes();
    }

    public JComboBox<Parcours> getComboBoxParcours() {
        return _desktopView.getComboBoxParcours();
    }

    public SportIdentReaderHandlerPuce getSportIdentHandlerPuce() {
        return _sportIdentHandlerPuce;
    }

    public boolean getSportIdentReaderPuceEnCours() {
        return _sportIdentReaderPuceEnCours;
    }

    public void setSportIdentReaderPuceEnCours( boolean value ) {
        _sportIdentReaderPuceEnCours = value;
    }


    public boolean isModified() {
        return _modified;
    }

    public void setModified( boolean value ) {
        _modified = value;
    }

    public void createNewRaid() {
        int choice = _desktopView.quitConfirmationDialog();
        if (choice == JOptionPane.YES_OPTION) {
            saveCurrentRaid();
        }
        if (choice != JOptionPane.CANCEL_OPTION) {
            _raid.clear();
            _raid.setCategories(_globalSettings.getCategoriesTemplate());
            _raid.setRaidName(L10n.getString("desktop_presenter.new_raid"));
            _desktopView.updateFrameTitle();
            _desktopView.updateComboxParcours();
            _desktopView.updateTable();
            setModified(false);
        }
    }

    public void openRaidFile() {
        int choice = _desktopView.quitConfirmationDialog();
        if (choice != JOptionPane.CANCEL_OPTION) {
            try {
                _desktopView.ouvrirFichierRaid();
            } catch (Exception ignored) {
                DesktopView.beep();
                ErrorPane.showMessageDialog(L10n.getString("desktop_presenter.error1"));
            }
        }
    }

    public void saveCurrentRaid() {
        if (_raid.getSavingFile().isEmpty()) {
            saveAsCurrentRaid();
        } else {
            File file = new File(_raid.getSavingFile());
            if (file.exists()) {
                TaskEnregistrer task = new TaskEnregistrer(_desktopView);
                task.execute();
                RaidFileXmlWriter.writeIntoRaidFile(_raid, _raid.getSavingFile());
                _modified = false;
            } else {
                DesktopView.beep();
                ErrorPane.showMessageDialog(L10n.getString("desktop_presenter.error2"));
            }
        }
    }

    public void saveAsCurrentRaid() {
        JFileChooser chooser = new JFileChooser();
        ExtensionFileFilter filter = new ExtensionFileFilter("grd", L10n.getString("grd_file.description"));
        chooser.setFileFilter(filter);
        chooser.setCurrentDirectory(new File(_globalSettings.getRaidFolderPath()));

        int returnVal = chooser.showSaveDialog(_desktopView);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            _raid.setSavingFile(Utils.checkExtension(chooser.getSelectedFile().getAbsolutePath(), ".grd"));
            RaidFileXmlWriter.writeIntoRaidFile(_raid, _raid.getSavingFile());
            _modified = false;
            System.err.println("ouiououoi");
            RecentFileHandler.addToRecentFiles(_raid.getSavingFile());
            _desktopView.updateFrameTitle();
        }
    }

    public void enregistrer() {
        String chemin = _globalSettings.getWorkingDirectoryPath() + "/" + _raid.getRaidName() + " " + TimeManager.getDateHeureMinuteSeconte() + ".grd";
        RaidFileXmlWriter.writeIntoRaidFile(getRaid(), chemin);
        _modified = false;
    }

    public void quit() {
        int choice = _desktopView.quitConfirmationDialog();
        if (choice == 0) {
            saveCurrentRaid();
        }
        if (choice < 2) {
            _desktopView.finish();
            System.exit(0);
        } else {
            _desktopView.cancelQuit();
        }
    }

    public boolean configureGlobalSettings() {
        GlobalSettingsDialog dialog = new GlobalSettingsDialog(_desktopView, this);
        dialog.setLocationRelativeTo(_desktopView);
        boolean isCancelled = dialog.showDialog();
        if (!isCancelled) {
            _desktopView.updateFrameTitle();
        }
        return isCancelled;
    }

    public void backup() {
        JFileChooser chooser = new JFileChooser(_globalSettings.getWorkingDirectoryPath());
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        int returnVal = chooser.showSaveDialog(_desktopView);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            _globalSettings.setWorkingDirectoryPath(chooser.getSelectedFile().toString());
            enregistrer();
        }
    }

    public void activateSportIdentButton( SportIdentCardPanel panel,
                                          boolean isSportIdentReaderEnabled,
                                          int hours,
                                          int minutes,
                                          String portCom ) {
        if (isSportIdentReaderEnabled) {
            _desktopView.setEnabledButtonReaderPuce(true);
            _sportIdentHandler.stop();
            panel.changeStateToStart();
        } else {
            if (portCom != null) {
                _sportIdentHandler.setZeroTimeInMilliseconds(hours, minutes);
                _sportIdentHandler.setPortName(portCom);
                if (_sportIdentHandler.start()) {
                    _desktopView.setEnabledButtonReaderPuce(false);
                    panel.changeStateToWaitForInitialization();
                } else {
                    panel.displayBadPortErrorMessage();
                }
            } else {
                panel.displayNoPortErrorMessage();
            }
        }
    }

    public void setZeroHour( int hours ) {
        _raid.getZeroTime().hours = hours;
    }

    public void setZeroMinute( int minutes ) {
        _raid.getZeroTime().minutes = minutes;
    }

    // TODO renommer
    public void ihmResultatPuce( ResultatPuce result, boolean equipeValide, boolean visu, boolean resultatReduit ) {
        IhmResultatPuce ihm = new IhmResultatPuce(
                _desktopView,
                this,
                result,
                equipeValide,
                visu,
                resultatReduit);

        ihm.setLocationRelativeTo(_desktopView);
        ihm.setVisible(true);// TODO ShowDialog()
    }

    public void ihmResultatPuce( ResultatPuce result, boolean equipeValide, boolean visu ) {
        ihmResultatPuce(result, equipeValide, visu, _globalSettings.isShortIndividualResultPrinting());
    }

    public void displayStationStatus( SportIdentStationState status ) {
        _desktopView.displayStationStatus(status);
    }

    public boolean backupFolderExists() {
        File dossier = new File(_globalSettings.getWorkingDirectoryPath());
        return dossier.exists();
    }

    public boolean createEquipe( Equipe equipe,
                                 String name,
                                 String bib,
                                 String chip,
                                 boolean isUnclassified,
                                 boolean isAbsentOrAbandon,
                                 Categorie category ) {
        equipe.setNom(name);
        equipe.setDossard(bib);
        equipe.setIdPuce(chip);
        equipe.setUnclassified(isUnclassified);
        equipe.setAbsent(isAbsentOrAbandon);
        equipe.setCategorie(category);
        if (!getRaid().existeIdPuce(chip, equipe)) {
            final JComboBox<Parcours> combo = _desktopView.getComboBoxParcours();
            getRaid().addEquipe((Parcours) combo.getSelectedItem(), equipe);
            assert (combo.getSelectedItem() != null);
            _desktopView.getListEquipes().setListData(((Parcours) combo.getSelectedItem()).getEquipes().getEquipes());
            _desktopView.getListEquipes().setSelectedValue(equipe, true);
            return true;
        } else {
            return false;
        }
    }

    public boolean modifyEquipe( Equipe equipe,
                                 String name,
                                 String bib,
                                 String chip,
                                 boolean unclassified,
                                 boolean absentOrAbandon,
                                 Categorie category ) {
        if (!getRaid().existeIdPuce(chip, equipe)) {
            equipe.setNom(name);
            equipe.setDossard(bib);
            equipe.setIdPuce(chip);
            equipe.setUnclassified(unclassified);
            equipe.setAbsent(absentOrAbandon);
            equipe.setCategorie(category);
            final JComboBox<Parcours> combo = _desktopView.getComboBoxParcours();
            assert (combo.getSelectedItem() != null);
            ((Parcours) combo.getSelectedItem()).getEquipes().trierEquipes(_desktopView.getTypeTriCourant());
            _desktopView.getListEquipes().repaint();
            _desktopView.getListEquipes().setSelectedValue(equipe, true);
            return (true);
        } else {
            return false;
        }
    }

    public boolean existChip( String chipNumber ) {
        return getRaid().existePuce(chipNumber);
    }

    public void refreshResulTable() {
        _desktopView.refreshResultTable();
    }

    public void updateEquipeList() {
        _desktopView.updateEquipeList();
    }

    void loadGlobalSettings() {
        PlatformSettingsHandler.loadGlobalSettings(_globalSettings);
        _desktopView.updateFrameTitle();
    }

    void displayRecentRaidFiles() {
        try {
            final String[] filesNames = RecentFileHandler.getRecentFilePaths();
            if (filesNames.length != 0) {
                final int index = RecentFileDialog.showInputDialog(_desktopView,
                        SOFTWARE_NAME,
                        L10n.getString("desktop_presenter.recent_raids"),
                        RecentFileHandler.getFileNamesFromFilePath(filesNames),
                        L10n.getString("desktop_presenter.new_raid_button"));
                if (index >= 0) {
                    _desktopView._openRaidFile(filesNames[index]);
                } else {
                    createNewRaid();
                }
            }
        } catch (Exception e) {
            DesktopView.beep();
            ErrorPane.showMessageDialog(L10n.getString("desktop_presenter.error3"));
        }
    }
}
