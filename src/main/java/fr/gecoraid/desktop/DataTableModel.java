package fr.gecoraid.desktop;

import javax.swing.table.AbstractTableModel;

public abstract class DataTableModel extends AbstractTableModel {
    protected final String[] _columnNames;

    public DataTableModel( String[] columnNames ) {
        _columnNames = columnNames;
    }

    public final String getHeaderAsCsv() {
        StringBuilder header = new StringBuilder();
        header.append(_columnNames[0]);
        for (int i = 1; i < _columnNames.length; i++) {
            header.append(";").append(_columnNames[i]);
        }
        return header.toString();
    }

    public abstract String[] getDataAsCsv();
}
