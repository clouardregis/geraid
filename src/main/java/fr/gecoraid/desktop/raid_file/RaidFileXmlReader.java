package fr.gecoraid.desktop.raid_file;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.widget.ErrorPane;
import fr.gecoraid.model.*;
import fr.gecoraid.tools.TimeManager;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;

import static fr.gecoraid.desktop.raid_file.RaidFileXmlTag.*;

public class RaidFileXmlReader {
    private RaidFileXmlReader() {
    }

    public static void readRaidFile( Raid raid, String raidFile ) {
        try {
            DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
            factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            File settingsFile = new File(raidFile);
            DocumentBuilder builder = factory.newDocumentBuilder();
            Document document = builder.parse(settingsFile);
            document.getDocumentElement().normalize();

            _readSettings(raid, document);
            _readCategories(raid, document);
            _readParcours(raid, document);
            _readResults(raid, document);
            _readPenalties(raid, document);
        } catch (IOException | SAXException | ParserConfigurationException e) {
            ErrorPane.showMessageDialog(L10n.getString("xml_raid.read.error1", e.getClass().getName(), e.getMessage()));
        }
    }

    private static void _readSettings( Raid geRaid, Document document ) {
        geRaid.setRaidName(_getChildText(document, PARCOURS_NOM));
        geRaid.setPageHeader(_getChildText(document, HEADER));
        geRaid.setPageFooter(_getChildText(document, FOOTER));
        if (document.getElementsByTagName(HZH) != null) { // UT
            geRaid.getZeroTime().hours = Integer.parseInt(_getChildText(document, HZH));
            geRaid.getZeroTime().minutes = Integer.parseInt(_getChildText(document, HZM));
        }
    }

    private static void _readCategories( Raid geRaid, Document document ) {
        Element root = (Element) document.getElementsByTagName(CATEGORIES).item(0);
        final NodeList categoryElements = root.getElementsByTagName(CATEGORIE);
        for (int i = 0; i < categoryElements.getLength(); i++) {
            Element element = (Element) categoryElements.item(i);
            Categorie category = new Categorie(_getChildText(element, NOMLONG), _getChildText(element, NOMCOURT));
            geRaid.addCategorie(category);
        }
    }

    private static void _readParcours( Raid geRaid, Document racine ) {
        Element root = (Element) racine.getElementsByTagName(PARCOURSS).item(0);
        final NodeList parcoursElements = root.getElementsByTagName(PARCOURS);
        for (int i = 0; i < parcoursElements.getLength(); i++) {
            Element parcoursElement = (Element) parcoursElements.item(i);
            Parcours parcours = new Parcours(_getChildText(parcoursElement, PARCOURS_NOM).trim());
            parcours.setEquipes(readEquipes(geRaid, parcoursElement));
            parcours.setEtapes(readEtapes(parcoursElement));
            geRaid.addParcours(parcours);
        }
    }

    private static void _readResults( Raid geRaid, Document document ) {
        Element root = (Element) document.getElementsByTagName(RESULTATS).item(0);
        final NodeList resultElements = root.getElementsByTagName(RESULTAT);
        for (int i = 0; i < resultElements.getLength(); i++) {
            Element resultElement = (Element) resultElements.item(i);
            ResultatPuce resultatPuce = new ResultatPuce();
            resultatPuce.setParcours(geRaid.getParcourss().getParcoursDeNom(_getChildText(resultElement, PARCOURS)));
            resultatPuce.setEtape(resultatPuce.getParcours().getEtape(_getChildText(resultElement, ETAPE)));
            resultatPuce.setEquipe(resultatPuce.getParcours().getEquipe(_getChildText(resultElement, EQUIPE)));

            Element puceElement = (Element) resultElement.getElementsByTagName(PUCE).item(0);
            Puce puce = resultatPuce.getPuce();
            puce.setErasetime(TimeManager.newDate(_getChildText(puceElement, ERASE)));
            puce.setControltime(TimeManager.newDate(_getChildText(puceElement, CONTROL)));
            puce.setStarttime(TimeManager.newDate(_getChildText(puceElement, START)));
            puce.setFinishtime(TimeManager.newDate(_getChildText(puceElement, FINISH)));
            puce.setReadtime(TimeManager.newDate(_getChildText(puceElement, READ)));

            Element partialRoot = (Element) puceElement.getElementsByTagName(PARTIELS).item(0);
            final NodeList partialElements = partialRoot.getElementsByTagName(PARTIEL);
            Partial[] ps = new Partial[partialElements.getLength()];
            for (int j = 0; j < partialElements.getLength(); j++) {
                Element partialElement = (Element) partialElements.item(j);
                Partial partial = new Partial();
                partial.setCode(Integer.parseInt(_getChildText(partialElement, CODE)));
                partial.setTime(TimeManager.newDate(_getChildText(partialElement, TIME)));
                ps[j] = partial;
            }
            puce.setPartiels(ps);
            if (resultatPuce.getEquipe() != null) {
                geRaid.addResultatPuce(resultatPuce);
            } else {
                ErrorPane.showMessageDialog(L10n.getString("xml_raid.read.error2", _getChildText(resultElement, EQUIPE)));
            }
        }
    }

    private static void _readPenalties( Raid geRaid, Document document ) {
        Element root = (Element) document.getElementsByTagName(PENALITES).item(0);
        final NodeList penaliteElements = root.getElementsByTagName(PENALITE);
        if (penaliteElements.getLength() == 0) {
            return;
        }
        for (int i = 0; i < penaliteElements.getLength(); i++) {
            Element penaliteElement = (Element) penaliteElements.item(i);
            Penalite penalite = new Penalite();
            penalite.setParcours(geRaid.getParcourss().getParcoursDeNom(_getChildText(penaliteElement, PARCOURS)));
            penalite.setEtape(penalite.getParcours().getEtape(_getChildText(penaliteElement, ETAPE)));
            penalite.setNom(_getChildText(penaliteElement, NOM));
            Element individualPenaltyRoot = (Element) penaliteElement.getElementsByTagName(PENINDVS).item(0);
            NodeList individualPenaltyElements = individualPenaltyRoot.getElementsByTagName(PENINDV);
            for (int j = 0; j < individualPenaltyElements.getLength(); j++) {
                Element penaliteIndividuelleElement = (Element) penaliteElements.item(j);
                PenaliteIndividuelle penaliteIndividuelle = new PenaliteIndividuelle();
                penaliteIndividuelle.setPuce(_getChildText(penaliteIndividuelleElement, PUCE));
                penaliteIndividuelle.setPoint(Integer.parseInt(_getChildText(penaliteIndividuelleElement, POINTS)));
                penaliteIndividuelle.setTemps(Integer.parseInt(_getChildText(penaliteIndividuelleElement, TEMPS)));
                penalite.getPenalites().add(penaliteIndividuelle);
            }
            geRaid.getPenalites().getPenalites().add(penalite);
        }
    }

    private static Equipes readEquipes( Raid geRaid, Element parcours ) {
        final Element root = (Element) parcours.getElementsByTagName(EQUIPES).item(0);
        final NodeList equipeElements = root.getElementsByTagName(EQUIPE);
        Equipes equipes = new Equipes();
        for (int i = 0; i < equipeElements.getLength(); i++) {
            Element equipeElement = (Element) equipeElements.item(i);
            Equipe equipe = new Equipe(geRaid);
            equipe.setNom(_getChildText(equipeElement, TEAM_LAST_NAME));
            equipe.setDossard(_getChildText(equipeElement, DOSSARD));
            equipe.setIdPuce(_getChildText(equipeElement, IDPUCE));
            int unclassified = Integer.parseInt(_getChildText(equipeElement, NC));
            equipe.setUnclassified(unclassified == 1);
            if (equipeElement.getElementsByTagName(ABS).getLength() > 0) {
                int abs = Integer.parseInt(_getChildText(equipeElement, ABS));
                equipe.setAbsent(abs == 1);
            }
            equipe.setCategorie(geRaid.getCategories().getCategorie(_getChildText(equipeElement, CATEGORIE)));
            equipe.setRaiders(readRaiders(equipeElement));
            equipes.addEquipe(equipe);
        }
        return equipes;
    }

    private static Raiders readRaiders( Element equipe ) {
        final Element raiderRoot = (Element) equipe.getElementsByTagName(RAIDERS).item(0);
        final NodeList raiderElements = raiderRoot.getElementsByTagName(RAIDER);
        Raiders raiders = new Raiders();
        for (int i = 0; i < raiderElements.getLength(); i++) {
            Element raiderElement = (Element) raiderElements.item(i);
            Raider raider = new Raider();
            raider.setLastname(_getChildText(raiderElement, TEAM_LAST_NAME));
            raider.setFirstname(_getChildText(raiderElement, TEAM_FIRST_NAME));
            raiders.addRaider(raider);
        }
        return raiders;
    }

    private static Epreuves recupereEpreuves( Element p ) {
        final NodeList epreuveElements = p.getElementsByTagName(EPREUVE);
        Epreuves epreuves = new Epreuves();
        for (int i = 0; i < epreuveElements.getLength(); i++) {
            Element epreuveElement = (Element) epreuveElements.item(i);
            Epreuve epreuve = new Epreuve();
            epreuve.setNom(_getChildText(epreuveElement, ET_NOM));
            epreuve.setTypeLimite(TypeLimite.valueOf(_getChildText(epreuveElement, TYPEL)));
            if (epreuve.getTypeLimite().equals(TypeLimite.AVECLIMITEHORAIRE)) {
                epreuve.setHeureLimite(TimeManager.newDate(_getChildText(epreuveElement, LIMITE)));
            }
            if (epreuve.getTypeLimite().equals(TypeLimite.AVECLIMITETEMPS)) {
                epreuve.setTempsLimite(TimeManager.newDate(_getChildText(epreuveElement, LIMITE)));
            }
            if (!epreuve.getTypeLimite().equals(TypeLimite.SANSLIMITE)) {
                epreuve.setPenalite(Integer.parseInt(_getChildText(epreuveElement, POINTS)));
                epreuve.setPenaliteTemps(Integer.parseInt(_getChildText(epreuveElement, MN)));
                epreuve.setIntervalPenalite(Integer.parseInt(_getChildText(epreuveElement, TEMPS)));
            }
            if (epreuveElement.getElementsByTagName(MULTI).getLength() > 0) {
                epreuve.setMultiplicateurTemps(Integer.parseInt(_getChildText(epreuveElement, MULTI)));
            }
            if (epreuveElement.getElementsByTagName(LIGNE).getLength() > 0) {
                int line = Integer.parseInt(_getChildText(epreuveElement, LIGNE));
                epreuve.setCourseEnLigne((line) == 1);
                int db1 = Integer.parseInt(_getChildText(epreuveElement, DEBUTB1));
                epreuve.setDebutEpreuve((db1) == 1);
                int fdb = Integer.parseInt(_getChildText(epreuveElement, FINB));
                epreuve.setFinEpreuve((fdb) == 1);
                if (epreuveElement.getElementsByTagName(LIGNE).getLength() > 0) {
                    if (epreuveElement.getElementsByTagName(CHRONO).getLength() > 0) {
                        int c = Integer.parseInt(_getChildText(epreuveElement, CHRONO));
                        epreuve.setChronometree((c) == 1);
                    } else {
                        epreuve.setChronometree(false);
                    }
                }
                if (epreuveElement.getElementsByTagName(HORSCHRONO).getLength() > 0) {
                    int c = Integer.parseInt(_getChildText(epreuveElement, HORSCHRONO));
                    epreuve.setHorsChrono((c) == 1);
                } else {
                    epreuve.setHorsChrono(false);
                }
            } else {
                epreuve.setCourseEnLigne(false);
                epreuve.setDebutEpreuve(false);
                epreuve.setFinEpreuve(false);
                epreuve.setChronometree(false);
            }
            if (epreuveElement.getElementsByTagName(AVANTEPREUVESUIVANTE).getLength() > 0) {
                int as = Integer.parseInt(_getChildText(epreuveElement, AVANTEPREUVESUIVANTE));
                epreuve.setAvantEpreuveSuivante(as == 1);
            }
            if (epreuveElement.getElementsByTagName(APRESEPREUVEPRECEDENTE).getLength() > 0) {
                int as = Integer.parseInt(_getChildText(epreuveElement, APRESEPREUVEPRECEDENTE));
                epreuve.setApresEpreuvePrecedente(as == 1);
            }
            if (epreuveElement.getElementsByTagName(EXCLUSIVE_WITH_NEXT).getLength() > 0) {
                int exclusive = Integer.parseInt(_getChildText(epreuveElement, EXCLUSIVE_WITH_NEXT));
                epreuve.setExclusiveWithNextEpreuve(exclusive == 1);
            }
            epreuve.setBalises(recupereBalises(epreuveElement));
            epreuves.getEpreuves().add(epreuve);
        }
        return epreuves;
    }

    private static Etapes readEtapes( Element parcours ) {
        final NodeList etapeElements = parcours.getElementsByTagName(ETAPE);
        Etapes etapes = new Etapes();
        for (int i = 0; i < etapeElements.getLength(); i++) {
            Element etapeElement = (Element) etapeElements.item(i);
            Etape etape = new Etape();
            etape.setNom(_getChildText(etapeElement, ET_NOM));
            int complete = Integer.parseInt(_getChildText(etapeElement, FIN));
            etape.setFini(complete == 1);
            if (etapeElement.getElementsByTagName(GEL).getLength() > 0) {
                int gel = Integer.parseInt(_getChildText(etapeElement, GEL));
                etape.setGelDansLimiteTemps(gel == 1);
            }
            etape.setType(TypeDepart.valueOf(_getChildText(etapeElement, TYPE)));
            if (etape.getType().equals(TypeDepart.GROUPE)) {
                etape.setHeureDepart(TimeManager.newDate(_getChildText(etapeElement, HEURE)));
            }
            etape.setTypeLimite(TypeLimite.valueOf(_getChildText(etapeElement, TYPEL)));
            if (etape.getTypeLimite().equals(TypeLimite.AVECLIMITEHORAIRE)) {
                etape.setHeureLimite(TimeManager.newDate(_getChildText(etapeElement, LIMITE)));
            }
            if (etape.getTypeLimite().equals(TypeLimite.AVECLIMITETEMPS)) {
                etape.setTempsLimite(TimeManager.newDate(_getChildText(etapeElement, LIMITE)));
            }
            if (!etape.getTypeLimite().equals(TypeLimite.SANSLIMITE)) {
                etape.setPenalite(Integer.parseInt(_getChildText(etapeElement, POINTS)));
                etape.setPenaliteTemps(Integer.parseInt(_getChildText(etapeElement, MN)));
                etape.setIntervalPenalite(Integer.parseInt(_getChildText(etapeElement, TEMPS)));
            }
            etape.setEpreuves(recupereEpreuves(etapeElement));
            etapes.getEtapes().add(etape);
        }
        return etapes;
    }

    private static Balises recupereBalises( Element p ) {
        final NodeList baliseElements = p.getElementsByTagName(BALISE);
        Balises balises = new Balises();
        for (int i = 0; i < baliseElements.getLength(); i++) {
            Element b = (Element) baliseElements.item(i);
            Balise balise = new Balise();
            balise.setNumero(Integer.parseInt(_getChildText(b, NUMERO)));
            balise.setPoints(Integer.parseInt(_getChildText(b, POINTS)));
            balise.setTemps(Integer.parseInt(_getChildText(b, TEMPS)));
            balise.setPointsPosteManquant(Integer.parseInt(_getChildText(b, POINTS_PM)));
            balise.setTempsPosteManquant(Integer.parseInt(_getChildText(b, TEMPS_PM)));
            int chronometer = Integer.parseInt(_getChildText(b, CHRONO));
            switch (chronometer) {
                case 0:
                    balise.setArreterChrono(false);
                    balise.setDemarerChrono(false);
                    break;
                case 1:
                    balise.setArreterChrono(true);
                    balise.setDemarerChrono(false);
                    break;
                case 2:
                    balise.setArreterChrono(false);
                    balise.setDemarerChrono(true);
                    break;
                default:
                    break;
            }
            balises.getBalises().add(balise);
        }
        return balises;
    }

    private static String _getChildText( Document root, String tag ) {
        final Node node = root.getElementsByTagName(tag).item(0);
        Node firstChild = node.getFirstChild();
        if (firstChild == null) {
            return "";
        }
        return firstChild.getTextContent();
    }

    private static String _getChildText( Element element, String tag ) {
        final Node node = element.getElementsByTagName(tag).item(0);
        Node firstChild = node.getFirstChild();
        if (firstChild == null) {
            return "";
        }
        return firstChild.getTextContent();
    }
}
