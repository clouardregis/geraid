package fr.gecoraid.desktop.raid_file;

import fr.gecoraid.L10n;
import fr.gecoraid.SessionLauncher;
import fr.gecoraid.desktop.widget.ErrorPane;
import fr.gecoraid.model.Balises;
import fr.gecoraid.model.Epreuves;
import fr.gecoraid.model.Equipe;
import fr.gecoraid.model.Equipes;
import fr.gecoraid.model.Etapes;
import fr.gecoraid.model.Raid;
import fr.gecoraid.model.Raider;
import fr.gecoraid.model.Raiders;
import fr.gecoraid.model.TypeDepart;
import fr.gecoraid.model.TypeLimite;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileWriter;
import java.io.IOException;

import static fr.gecoraid.desktop.raid_file.RaidFileXmlTag.*;

public class RaidFileXmlWriter {
    private RaidFileXmlWriter() {
    }

    public static void writeIntoRaidFile( Raid raid, String raidFile ) {
        try {
            Document document = buildDocumentFromRaid(raid);
            _saveInFile(raidFile, document);
        } catch (IOException | TransformerException | ParserConfigurationException e) {
            ErrorPane.showMessageDialog(L10n.getString("xml_raid.write.error1", e.getClass().getName(), e.getMessage()));
        }
    }

    static Document buildDocumentFromRaid( Raid raid ) throws ParserConfigurationException {
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document document = builder.newDocument();
        Element racine = document.createElement(GERAID);
        document.appendChild(racine);
        _writeSettings(raid, document, racine);
        _writeCategories(raid, document, racine);
        _writeParcours(raid, document, racine);
        _writeResults(raid, document, racine);
        _writePenalties(raid, document, racine);
        return document;
    }

    private static void _writeSettings( Raid raid, Document document, Element racine ) {
        Element version = document.createElement(VERSION);
        version.appendChild(document.createTextNode(SessionLauncher.getVersion()));
        racine.appendChild(version);

        Element nomRaid = document.createElement(PARCOURS_NOM);
        nomRaid.appendChild(document.createTextNode(raid.getRaidName()));
        racine.appendChild(nomRaid);

        Element raidHeader = document.createElement(HEADER);
        raidHeader.appendChild(document.createTextNode(raid.getPageHeader()));
        racine.appendChild(raidHeader);

        Element raidFooter = document.createElement(FOOTER);
        raidFooter.appendChild(document.createTextNode(raid.getPageFooter()));
        racine.appendChild(raidFooter);

        Element zeroHourH = document.createElement(HZH);
        zeroHourH.appendChild(document.createTextNode(raid.getZeroTime().hours + ""));
        racine.appendChild(zeroHourH);
        Element zeroHourM = document.createElement(HZM);
        zeroHourM.appendChild(document.createTextNode(raid.getZeroTime().minutes + ""));
        racine.appendChild(zeroHourM);
    }

    private static void _writeCategories( Raid raid, Document document, Element racine ) {
        Element categoryElements = document.createElement(CATEGORIES);
        for (int i = 0; i < raid.getCategories().getSize(); i++) {
            Element categoryElement = document.createElement(CATEGORIE);
            Element longName = document.createElement(NOMLONG);
            longName.appendChild(document.createTextNode(raid.getCategories().getCategories().get(i).getLongName()));
            categoryElement.appendChild(longName);
            Element shortName = document.createElement(NOMCOURT);
            shortName.appendChild(document.createTextNode(raid.getCategories().getCategories().get(i).getShortName()));
            categoryElement.appendChild(shortName);
            categoryElements.appendChild(categoryElement);
        }
        racine.appendChild(categoryElements);
    }

    private static void _writeParcours( Raid raid, Document document, Element racine ) {
        Element pars = document.createElement(PARCOURSS);
        for (int i = 0; i < raid.getParcourss().getSize(); i++) {
            Element p = document.createElement(PARCOURS);

            Element nom = document.createElement(PARCOURS_NOM);
            nom.appendChild(document.createTextNode(raid.getParcourss().getNomAIndex(i)));
            p.appendChild(nom);

            Element eqs = document.createElement(EQUIPES);
            Equipes equipes = raid.getParcourss().getParcourss().get(i).getEquipes();
            for (int j = 0; j < equipes.getSize(); j++) {
                Element equipeElement = document.createElement(EQUIPE);
                Equipe equipe = equipes.getEquipes().get(j);
                Element name = document.createElement(TEAM_LAST_NAME);
                name.appendChild(document.createTextNode(equipe.getNom()));
                equipeElement.appendChild(name);
                Element eDos = document.createElement(DOSSARD);
                eDos.appendChild(document.createTextNode(equipe.getDossard()));
                equipeElement.appendChild(eDos);
                Element eId = document.createElement(IDPUCE);
                eId.appendChild(document.createTextNode(equipe.getIdPuce()));
                equipeElement.appendChild(eId);
                Element nc = document.createElement(NC);
                nc.appendChild(document.createTextNode(equipe.getNonClasseeAsInteger()));
                equipeElement.appendChild(nc);
                Element abs = document.createElement(ABS);
                abs.appendChild(document.createTextNode(equipe.getAbsenceAsInteger()));
                equipeElement.appendChild(abs);
                Element eCat = document.createElement(CATEGORIE);
                eCat.appendChild(document.createTextNode(equipe.getCategorie().getShortName()));
                equipeElement.appendChild(eCat);

                // Raiders
                Raiders rds = equipe.getRaiders();
                Element rs = document.createElement(RAIDERS);
                for (int k = 0; k < rds.getSize(); k++) {
                    Raider rd = rds.getRaiders().get(k);
                    Element r = document.createElement(RAIDER);

                    Element raiderLastname = document.createElement(TEAM_LAST_NAME);
                    raiderLastname.appendChild(document.createTextNode(rd.getNom()));
                    r.appendChild(raiderLastname);
                    Element raiderFirstname = document.createElement(TEAM_FIRST_NAME);
                    raiderFirstname.appendChild(document.createTextNode(rd.getPrenom()));
                    r.appendChild(raiderFirstname);

                    rs.appendChild(r);
                }
                equipeElement.appendChild(rs);
                eqs.appendChild(equipeElement);
            }
            p.appendChild(eqs);

            Element ets = document.createElement(ETAPES);
            Etapes etapes = raid.getParcourss().getParcourss().get(i).getEtapes();
            for (int l = 0; l < etapes.getSize(); l++) {
                Element e = document.createElement(ETAPE);
                Element etapeName = document.createElement(ET_NOM);
                etapeName.appendChild(document.createTextNode(etapes.getEtapes().get(l).getName()));
                e.appendChild(etapeName);
                Element etapeCompleted = document.createElement(FIN);
                etapeCompleted.appendChild(document.createTextNode(etapes.getEtapes().get(l).getFini()));
                e.appendChild(etapeCompleted);
                Element eGel = document.createElement(GEL);
                eGel.appendChild(document.createTextNode(etapes.getEtapes().get(l).getGelDansLimiteTemps()));
                e.appendChild(eGel);
                Element etapeType = document.createElement(TYPE);
                etapeType.appendChild(document.createTextNode(etapes.getEtapes().get(l).getType().name()));
                e.appendChild(etapeType);
                Element etapeHour = document.createElement(HEURE);
                if (etapes.getEtapes().get(l).getType().equals(TypeDepart.GROUPE)) {
                    etapeHour.appendChild(document.createTextNode(etapes.getEtapes().get(l).getHeureDepart().getTime() + ""));
                }
                e.appendChild(etapeHour);
                Element etapeTypeL = document.createElement(TYPEL);
                etapeTypeL.appendChild(document.createTextNode(etapes.getEtapes().get(l).getTypeLimite().name()));
                e.appendChild(etapeTypeL);
                Element etapeLimit = document.createElement(LIMITE);
                if (etapes.getEtapes().get(l).getTypeLimite().equals(TypeLimite.AVECLIMITEHORAIRE)) {
                    etapeLimit.appendChild(document.createTextNode(etapes.getEtapes().get(l).getHeureLimite().getTime() + ""));
                } else if (etapes.getEtapes().get(l).getTypeLimite().equals(TypeLimite.AVECLIMITETEMPS)) {
                    etapeLimit.appendChild(document.createTextNode(etapes.getEtapes().get(l).getTempsLimite().getTime() + ""));
                } else if (etapes.getEtapes().get(l).getTypeLimite().equals(TypeLimite.SANSLIMITE)) {
                    etapeLimit.appendChild(document.createTextNode(0 + ""));
                }
                e.appendChild(etapeLimit);
                Element ePoints = document.createElement(POINTS);
                ePoints.appendChild(document.createTextNode(etapes.getEtapes().get(l).getPenalite() + ""));
                e.appendChild(ePoints);
                Element eMn = document.createElement(MN);
                eMn.appendChild(document.createTextNode(etapes.getEtapes().get(l).getPenaliteTemps() + ""));
                e.appendChild(eMn);
                Element eTemps = document.createElement(TEMPS);
                eTemps.appendChild(document.createTextNode(etapes.getEtapes().get(l).getIntervalPenalite() + ""));
                e.appendChild(eTemps);

                Element eps = document.createElement(EPREUVES);
                Epreuves epreuves = etapes.getEtapes().get(l).getEpreuves();
                for (int z = 0; z < epreuves.getSize(); z++) {
                    Element ep = document.createElement(EPREUVE);
                    Element epreuveName = document.createElement(ET_NOM);
                    epreuveName.appendChild(document.createTextNode(epreuves.getEpreuves().get(z).getName()));
                    ep.appendChild(epreuveName);
                    Element multi = document.createElement(MULTI);
                    multi.appendChild(document.createTextNode(epreuves.getEpreuves().get(z).getMultiplicateurTemps() + ""));
                    ep.appendChild(multi);
                    Element epreuveTypeL = document.createElement(TYPEL);
                    epreuveTypeL.appendChild(document.createTextNode(epreuves.getEpreuves().get(z).getTypeLimite().name()));
                    ep.appendChild(epreuveTypeL);
                    Element epreuveLimit = document.createElement(LIMITE);
                    if (epreuves.getEpreuves().get(z).getTypeLimite().equals(TypeLimite.AVECLIMITEHORAIRE)) {
                        epreuveLimit.appendChild(document.createTextNode(epreuves.getEpreuves().get(z).getHeureLimite().getTime() + ""));
                    } else if (epreuves.getEpreuves().get(z).getTypeLimite().equals(TypeLimite.AVECLIMITETEMPS)) {
                        epreuveLimit.appendChild(document.createTextNode(epreuves.getEpreuves().get(z).getTempsLimite().getTime() + ""));
                    } else if (epreuves.getEpreuves().get(z).getTypeLimite().equals(TypeLimite.SANSLIMITE)) {
                        epreuveLimit.appendChild(document.createTextNode(0 + ""));
                    }
                    ep.appendChild(epreuveLimit);
                    Element li = document.createElement(LIGNE);
                    li.appendChild(document.createTextNode(epreuves.getEpreuves().get(z).getLigne()));
                    ep.appendChild(li);
                    Element db1 = document.createElement(DEBUTB1);
                    db1.appendChild(document.createTextNode(epreuves.getEpreuves().get(z).getDebutEpreuve()));
                    ep.appendChild(db1);
                    Element fdb = document.createElement(FINB);
                    fdb.appendChild(document.createTextNode(epreuves.getEpreuves().get(z).getFinEpreuve()));
                    ep.appendChild(fdb);
                    Element ch = document.createElement(CHRONO);
                    ch.appendChild(document.createTextNode(epreuves.getEpreuves().get(z).getChrono()));
                    ep.appendChild(ch);
                    Element HC = document.createElement(HORSCHRONO);
                    HC.appendChild(document.createTextNode(epreuves.getEpreuves().get(z).getHorsChrono()));
                    ep.appendChild(HC);
                    Element epPoints = document.createElement(POINTS);
                    epPoints.appendChild(document.createTextNode(epreuves.getEpreuves().get(z).getPenalite() + ""));
                    ep.appendChild(epPoints);
                    Element epMn = document.createElement(MN);
                    epMn.appendChild(document.createTextNode(epreuves.getEpreuves().get(z).getPenaliteTemps() + ""));
                    ep.appendChild(epMn);
                    Element epTemps = document.createElement(TEMPS);
                    epTemps.appendChild(document.createTextNode(epreuves.getEpreuves().get(z).getIntervalPenalite() + ""));
                    ep.appendChild(epTemps);
                    Element aep = document.createElement(APRESEPREUVEPRECEDENTE);
                    aep.appendChild(document.createTextNode(epreuves.getEpreuves().get(z).getApresEpreuvePrecedente()));
                    ep.appendChild(aep);
                    Element aes = document.createElement(AVANTEPREUVESUIVANTE);
                    aes.appendChild(document.createTextNode(epreuves.getEpreuves().get(z).getAvantEpreuveSuivante()));
                    ep.appendChild(aes);
                    Element exclusive = document.createElement(EXCLUSIVE_WITH_NEXT);
                    exclusive.appendChild(document.createTextNode(epreuves.getEpreuves().get(z).getExclusiveWithNextEpreuve()));
                    ep.appendChild(exclusive);

                    Element bs = document.createElement(BALISES);
                    Balises balises = epreuves.getEpreuves().get(z).getBalises();
                    for (int m = 0; m < balises.getSize(); m++) {
                        Element balise = document.createElement(BALISE);
                        Element baliseNumber = document.createElement(NUMERO);
                        baliseNumber.appendChild(document.createTextNode(balises.getBalises().get(m).getNumero() + ""));
                        balise.appendChild(baliseNumber);

                        Element balisePoints = document.createElement(POINTS);
                        balisePoints.appendChild(document.createTextNode(balises.getBalises().get(m).getPoints() + ""));
                        balise.appendChild(balisePoints);
                        Element bTemps = document.createElement(TEMPS);
                        bTemps.appendChild(document.createTextNode(balises.getBalises().get(m).getTemps() + ""));
                        balise.appendChild(bTemps);

                        Element baliseMpPoints = document.createElement(POINTS_PM);
                        baliseMpPoints.appendChild(document.createTextNode(balises.getBalises().get(m).getPointsPosteManquant() + ""));
                        balise.appendChild(baliseMpPoints);
                        Element bTempsPm = document.createElement(TEMPS_PM);
                        bTempsPm.appendChild(document.createTextNode(balises.getBalises().get(m).getTempsPosteManquant() + ""));
                        balise.appendChild(bTempsPm);

                        Element baliseChronometer = document.createElement(CHRONO);
                        if (balises.getBalises().get(m).isArreterChrono()) {
                            baliseChronometer.appendChild(document.createTextNode("1"));
                        } else if (balises.getBalises().get(m).isDemarerChrono()) {
                            baliseChronometer.appendChild(document.createTextNode("2"));
                        } else {
                            baliseChronometer.appendChild(document.createTextNode("0"));
                        }
                        balise.appendChild(baliseChronometer);
                        bs.appendChild(balise);
                    }
                    ep.appendChild(bs);
                    eps.appendChild(ep);
                }
                e.appendChild(eps);
                ets.appendChild(e);
            }
            p.appendChild(ets);
            pars.appendChild(p);
        }
        racine.appendChild(pars);
    }

    private static void _writeResults( Raid raid, Document document, Element racine ) {
        Element rs = document.createElement(RESULTATS);
        for (int i = 0; i < raid.getResultatsPuce().getSize(); i++) {
            Element r = document.createElement(RESULTAT);
            Element pc = document.createElement(PARCOURS);
            pc.appendChild(document.createTextNode(raid.getResultatsPuce().getResultatsPuce().get(i).getParcours().getName()));
            r.appendChild(pc);
            Element et = document.createElement(ETAPE);
            et.appendChild(document.createTextNode(raid.getResultatsPuce().getResultatsPuce().get(i).getEtape().getName()));
            r.appendChild(et);
            Element eq = document.createElement(EQUIPE);
            eq.appendChild(document.createTextNode(raid.getResultatsPuce().getResultatsPuce().get(i).getEquipe().getIdPuce()));
            r.appendChild(eq);
            Element pu = document.createElement(PUCE);
            Element era = document.createElement(ERASE);
            era.appendChild(document.createTextNode(raid.getResultatsPuce().getResultatsPuce().get(i).getPuce().getErasetime().getTime() + ""));
            pu.appendChild(era);
            Element cnt = document.createElement(CONTROL);
            cnt.appendChild(document.createTextNode(raid.getResultatsPuce().getResultatsPuce().get(i).getPuce().getControltime().getTime() + ""));
            pu.appendChild(cnt);
            Element str = document.createElement(START);
            str.appendChild(document.createTextNode(raid.getResultatsPuce().getResultatsPuce().get(i).getPuce().getStarttime().getTime() + ""));
            pu.appendChild(str);
            Element fns = document.createElement(FINISH);
            fns.appendChild(document.createTextNode(raid.getResultatsPuce().getResultatsPuce().get(i).getPuce().getFinishtime().getTime() + ""));
            pu.appendChild(fns);
            Element red = document.createElement(READ);
            red.appendChild(document.createTextNode(raid.getResultatsPuce().getResultatsPuce().get(i).getPuce().getReadtime().getTime() + ""));
            pu.appendChild(red);
            Element parts = document.createElement(PARTIELS);
            for (int j = 0; j < raid.getResultatsPuce().getResultatsPuce().get(i).getPuce().getPartiels().length; j++) {
                Element part = document.createElement(PARTIEL);

                Element cd = document.createElement(CODE);
                cd.appendChild(document.createTextNode(raid.getResultatsPuce().getResultatsPuce().get(i).getPuce().getPartiels()[j].getCode() + ""));
                part.appendChild(cd);
                Element tm = document.createElement(TIME);
                tm.appendChild(document.createTextNode(raid.getResultatsPuce().getResultatsPuce().get(i).getPuce().getPartiels()[j].getTime().getTime() + ""));
                part.appendChild(tm);
                parts.appendChild(part);
            }
            pu.appendChild(parts);
            r.appendChild(pu);
            rs.appendChild(r);
        }
        racine.appendChild(rs);
    }

    private static void _writePenalties( Raid raid, Document document, Element racine ) {
        Element pns = document.createElement(PENALITES);
        for (int i = 0; i < raid.getPenalites().getPenalites().size(); i++) {
            Element pn = document.createElement(PENALITE);
            Element nm = document.createElement(NOM);
            nm.appendChild(document.createTextNode(raid.getPenalites().getPenalites().get(i).getNom()));
            pn.appendChild(nm);
            Element pc = document.createElement(PARCOURS);
            pc.appendChild(document.createTextNode(raid.getPenalites().getPenalites().get(i).getParcours().getName()));
            pn.appendChild(pc);
            Element et = document.createElement(ETAPE);
            et.appendChild(document.createTextNode(raid.getPenalites().getPenalites().get(i).getEtape().getName()));
            pn.appendChild(et);
            Element pis = document.createElement(PENINDVS);
            for (int j = 0; j < raid.getPenalites().getPenalites().get(i).getPenalites().size(); j++) {
                Element pi = document.createElement(PENINDV);
                Element pu = document.createElement(PUCE);
                pu.appendChild(document.createTextNode(raid.getPenalites().getPenalites().get(i).getPenalites().get(j).getPuce()));
                pi.appendChild(pu);
                Element pt = document.createElement(POINTS);
                pt.appendChild(document.createTextNode(raid.getPenalites().getPenalites().get(i).getPenalites().get(j).getPoint() + ""));
                pi.appendChild(pt);
                Element tp = document.createElement(TEMPS);
                tp.appendChild(document.createTextNode(raid.getPenalites().getPenalites().get(i).getPenalites().get(j).getTemps() + ""));
                pi.appendChild(tp);
                pis.appendChild(pi);
            }
            pn.appendChild(pis);
            pns.appendChild(pn);
        }
        racine.appendChild(pns);
    }

    private static void _saveInFile( String raidFile, Document document ) throws IOException, TransformerException {
        DOMSource source = new DOMSource(document);
        FileWriter writer = new FileWriter(raidFile);
        StreamResult result = new StreamResult(writer);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
        transformerFactory.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
        transformerFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);

        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        transformer.transform(source, result);
    }
}
