package fr.gecoraid.desktop;

import fr.gecoraid.desktop.file_menu.FileMenu;
import fr.gecoraid.desktop.help_menu.HelpMenu;
import fr.gecoraid.desktop.raid_menu.RaidMenu;
import fr.gecoraid.desktop.setting_menu.SettingMenu;

import javax.swing.JMenuBar;

class DesktopMenu extends JMenuBar {
    private final transient DeskTopPresenter _presenter;
    private final transient DesktopView _desktopView;

    DesktopMenu( DesktopView desktopView, DeskTopPresenter presenter ) {
        _presenter = presenter;
        _desktopView = desktopView;
        _createMenu();
    }

    private void _createMenu() {
        add(new FileMenu(_presenter));
        add(new RaidMenu(_desktopView, _presenter));
        add(new SettingMenu(_presenter));
        add(new HelpMenu());
    }
}
