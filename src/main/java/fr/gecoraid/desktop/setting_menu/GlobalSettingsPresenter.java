package fr.gecoraid.desktop.setting_menu;

import fr.gecoraid.desktop.DeskTopPresenter;
import fr.gecoraid.model.Categories;
import fr.gecoraid.model.GlobalSettings;

// TODO mettre une visibilite package
class GlobalSettingsPresenter {
    private final GlobalSettings _globalSettings;
    private final  DeskTopPresenter _parent;

    GlobalSettingsPresenter( DeskTopPresenter presenter ) {
        _parent = presenter;
        _globalSettings = presenter.getGlobalSettings();
    }

    GlobalSettings getGlobalSettings() {
        return _globalSettings;
    }

    String getClubName() {
        return _globalSettings.getClubName();
    }

     void setClubName( String name ) {
        _globalSettings.setClubName(name.trim());
    }

     String getRaidDirectoryPath() {
        return _globalSettings.getRaidFolderPath();
    }

     void setRaidDirectoryPath( String folder ) {
        _globalSettings.setRaidDirectoryPath(folder);
    }

     String getWorkingDirectoryPath() {
        return _globalSettings.getWorkingDirectoryPath();
    }

     void setWorkingDirectoryPath( String folder ) {
        _globalSettings.setWorkingDirectoryPath(folder);
    }

     boolean isResultAutoScrolling() {
        return _globalSettings.isResultAutoScrollingEnabled();
    }

     void setResultAutoScrolling( boolean value ) {
        _globalSettings.setResultAutoScrolling(value);
    }

     Object getAutoScrollingSpeedInS() {
        return _globalSettings.getAutoScrollingSpeedInS();
    }

     void setAutoScrollingSpeedInS( int speed ) {
        _globalSettings.setAutoScrollingSpeedInS(speed);
    }

     Categories getCategoriesTemplate() {
        return _globalSettings.getCategoriesTemplate();
    }

     void setCategoriesTemplate( Categories categories ) {
        _globalSettings.setCategoriesTemplate(categories);
    }

     boolean isShortIndividualResultPrinting() {
        return _globalSettings.isShortIndividualResultPrinting();
    }

     void setShortIndividualResultPrinting( boolean value ) {
        _globalSettings.setShortIndividualResultPrinting(value);
    }

     boolean isShortGlobalResultPrinting() {
        return _globalSettings.isShortGlobalResultPrinting();
    }

     void setShortGlobalResultPrinting( boolean value ) {
        _globalSettings.setShortGlobalResultPrinting(value);
    }

     void saveGlobalSettings() {
        PlatformSettingsHandler.saveGlobalSettings(_globalSettings);
    }
}
