package fr.gecoraid.desktop.setting_menu;

import org.jetbrains.annotations.VisibleForTesting;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class RecentFileHandler {
    private static final String RECENT_FILES_FILENAME = "recent_projects";
    private static final int RECENT_FILES_SIZE = 10;

    public static String[] getRecentFilePaths() {
        return _getRecentFilePaths(PlatformSettingsHandler.getGlobalSettingsDirectory(), RECENT_FILES_FILENAME);
    }

    public static String[] getFileNamesFromFilePath( String[] filePaths ) {
        String[] filenames = new String[filePaths.length];
        for (int i = 0; i < filePaths.length; i++) {
            final File file = new File(filePaths[i]);
            final String filename = removeExtension(file.getName());
            filenames[i] = filename;
        }
        return filenames;
    }

    public static void addToRecentFiles( String filename ) {
        String directory = PlatformSettingsHandler.getGlobalSettingsDirectory();
        _addToRecentFiles(filename, directory, RECENT_FILES_FILENAME);
    }

    @VisibleForTesting
    public static String[] _getRecentFilePaths( String directory, String recentFilesFilename ) {
        String path = directory + File.separator + recentFilesFilename;
        final ArrayList<String> files = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            String file;
            while ((file = reader.readLine()) != null) {
                if (!file.isEmpty()) {
                    files.add(file);
                }
            }
        } catch (IOException ignored) {
        }
        return files.toArray(new String[0]);
    }

    @VisibleForTesting
    public static String removeExtension( String filename ) {
        int index = filename.lastIndexOf('.');
        return filename.substring(0, index);
    }

    @VisibleForTesting
    public static void _addToRecentFiles( String filename, String directory, String recentFilesFilename ) {
        final ArrayList<String> files = new ArrayList<>();
        String path = directory + File.separator + recentFilesFilename;
        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            String file;
            while ((file = reader.readLine()) != null) {
                if (!file.isEmpty()) {
                    files.add(file);
                }
            }
        } catch (IOException ignored) {
        }
        if (files.size() > RECENT_FILES_SIZE - 1) {
            files.remove(RECENT_FILES_SIZE - 1);
        }
        if (!files.contains(filename)) {
            files.add(0, filename);
            try (Writer writer = new BufferedWriter(
                    new OutputStreamWriter(Files.newOutputStream(Paths.get(path)), StandardCharsets.UTF_8))) {
                for (String file : files) {
                    writer.write(file + "\n");
                }
            } catch (IOException ignored) {
            }
        }    }

    @VisibleForTesting
    public static void _removeFromRecentFiles( String filename, String directory, String recentFilesFilename ) {
        // TODO Ajouter un menu contextuel (bouton droite) dans la liste pour supprimer un fichier (ou utiliser la touche delete)
        final ArrayList<String> files = new ArrayList<>();
        String path = directory + File.separator + recentFilesFilename;

        try (BufferedReader reader = new BufferedReader(new FileReader(path))) {
            String file;
            while ((file = reader.readLine()) != null) {
                if (!file.isEmpty()) {
                    files.add(file);
                }
            }
        } catch (IOException ignored) {
        }
        for (int i = 0; i < files.size(); i++) {
            if (filename.equals(files.get(i))) {
                files.remove(i);
                break;
            }
        }
        try (Writer writer = new BufferedWriter(
                new OutputStreamWriter(Files.newOutputStream(Paths.get(path)), StandardCharsets.UTF_8))) {
            for (String file : files) {
                writer.write(file + "\n");
            }
        } catch (IOException ignored) {
        }
    }
}
