package fr.gecoraid.desktop.setting_menu;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.widget.ErrorPane;
import fr.gecoraid.model.Categorie;
import fr.gecoraid.model.GlobalSettings;
import org.jetbrains.annotations.VisibleForTesting;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.swing.filechooser.FileSystemView;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;

class GlobalSettingsFile {
    private static final String SETTING_FILE_NAME = "config.xml";
    private static final String CONFIG = "config";
    private static final String CLUB_NAME = "nom";
    private static final String CATEGORIES = "categories";
    private static final String CATEGORY = "categorie";
    private static final String LONG_NAME = "nomLong";
    private static final String SHORT_NAME = "nomCourt";
    private static final String RAID_FOLDER = "dossier";
    private static final String SAVE_FOLDER = "dossiers";
    private static final String TICKET = "ticket";
    private static final String PRINTING = "print";
    private static final String POINTS = "points";
    private static final String TIME = "temps";
    private static final String MISPUNCH_PENALTY_POINTS = "pointsPm";
    private static final String MISPUNCH_PENALTY_TIME = "tempsPm";
    private static final String SCROLLING_SPEED = "vitesse";
    private static final String AUTO_SCROLLING = "defilement";

    static void load( GlobalSettings globalSettings, String settingsDirectory ) {
        final String settingAbsoluteFileName = _getSettingAbsoluteFileName(settingsDirectory);
        final File defaultDirectory = FileSystemView.getFileSystemView().getDefaultDirectory();
        try {
            _readGlobalSettingFromXmlFile(globalSettings, settingAbsoluteFileName, defaultDirectory.getPath());
        } catch (IOException | ParserConfigurationException | SAXException e) {
            ErrorPane.showMessageDialog(L10n.getString("settings_handle.error1") + " " + e.getClass().getName() + ", " + e.getMessage());
        }
    }

    static void save( GlobalSettings globalSettings, String settingsDirectory ) {
        final String settingAbsoluteFileName = _getSettingAbsoluteFileName(settingsDirectory);
        try {
            final Document document = _saveGlobalSettingsToXml(globalSettings);
            DOMSource source = new DOMSource(document);
            Transformer transformer = TransformerFactory.newInstance().newTransformer();
            StreamResult result = new StreamResult(new File(settingAbsoluteFileName));
            transformer.transform(source, result);
        } catch (ParserConfigurationException | TransformerException e) {
            throw new RuntimeException(e);
        }
    }

    @VisibleForTesting
    static Document _saveGlobalSettingsToXml( GlobalSettings globalSettings ) throws ParserConfigurationException {
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        Document document = builder.newDocument();
        Element root = document.createElement(CONFIG);
        document.appendChild(root);

        Element clubName = document.createElement(CLUB_NAME);
        clubName.appendChild(document.createTextNode(globalSettings.getClubName()));
        root.appendChild(clubName);

        Element raidFolder = document.createElement(RAID_FOLDER);
        raidFolder.appendChild(document.createTextNode(globalSettings.getRaidFolderPath()));
        root.appendChild(raidFolder);

        Element saveFolder = document.createElement(SAVE_FOLDER);
        saveFolder.appendChild(document.createTextNode(globalSettings.getWorkingDirectoryPath()));
        root.appendChild(saveFolder);

        Element ticket = document.createElement(TICKET);
        ticket.appendChild(document.createTextNode(globalSettings.getIsShortIndividualResultPrintingAsInt()));
        root.appendChild(ticket);

        Element printing = document.createElement(PRINTING);
        printing.appendChild(document.createTextNode(globalSettings.getIsShortGlobalResultPrintingAsInt()));
        root.appendChild(printing);

        Element autoScrolling = document.createElement(AUTO_SCROLLING);
        autoScrolling.appendChild(document.createTextNode(globalSettings.getResultAutoScrollingAsInt()));
        root.appendChild(autoScrolling);

        Element scrollingSpeed = document.createElement(SCROLLING_SPEED);
        scrollingSpeed.appendChild(document.createTextNode(globalSettings.getAutoScrollingSpeedInS() + ""));
        root.appendChild(scrollingSpeed);

        Element categories = document.createElement(CATEGORIES);
        for (int i = 0; i < globalSettings.getCategoriesTemplate().getSize(); i++) {
            final Element category = document.createElement(CATEGORY);
            final Element longName = document.createElement(LONG_NAME);
            longName.appendChild(document.createTextNode(globalSettings.getCategoriesTemplate().getCategories().get(i).getLongName()));
            category.appendChild(longName);
            final Element shortName = document.createElement(SHORT_NAME);
            shortName.appendChild(document.createTextNode(globalSettings.getCategoriesTemplate().getCategories().get(i).getShortName()));
            category.appendChild(shortName);
            categories.appendChild(category);
        }

        Element points = document.createElement(POINTS);
        points.appendChild(document.createTextNode(globalSettings.getPenaltyPointsDefault() + ""));
        root.appendChild(points);

        Element time = document.createElement(TIME);
        time.appendChild(document.createTextNode(globalSettings.getPenaltyTimeDefault() + ""));
        root.appendChild(time);

        Element mispunchPenaltyPoints = document.createElement(MISPUNCH_PENALTY_POINTS);
        mispunchPenaltyPoints.appendChild(document.createTextNode(globalSettings.getMispunchPenaltyPoints() + ""));
        root.appendChild(mispunchPenaltyPoints);

        Element mispunchPenaltyTime = document.createElement(MISPUNCH_PENALTY_TIME);
        mispunchPenaltyTime.appendChild(document.createTextNode(globalSettings.getMispunchPenaltyTimeInS() + ""));
        root.appendChild(mispunchPenaltyTime);
        root.appendChild(categories);
        return document;
    }

    @VisibleForTesting
    static void _readGlobalSettingFromXmlFile( GlobalSettings globalSettings, String settingsFilePath, String defaultDirectoryPath ) throws IOException, ParserConfigurationException, SAXException {
        DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
        File settingsFile = new File(settingsFilePath);
        Document root = builder.parse(settingsFile);
        root.getDocumentElement().normalize();
        final Node clubName = root.getElementsByTagName(CLUB_NAME).item(0);
        globalSettings.setClubName(clubName.getFirstChild().getTextContent().trim());

        final Node raidFolder = root.getElementsByTagName(RAID_FOLDER).item(0);
        final File file = new File(raidFolder.getFirstChild().getTextContent().trim());
        if (file.exists()) {
            globalSettings.setRaidDirectoryPath(raidFolder.getFirstChild().getTextContent().trim());
        } else {
            globalSettings.setRaidDirectoryPath(defaultDirectoryPath);
        }

        final Node saveFolder = root.getElementsByTagName(SAVE_FOLDER).item(0);
        File files = new File(saveFolder.getFirstChild().getTextContent().trim());
        if (files.exists()) {
            globalSettings.setWorkingDirectoryPath(saveFolder.getFirstChild().getTextContent().trim());
        } else {
            globalSettings.setRaidDirectoryPath(defaultDirectoryPath);
        }

        final Node ticket = root.getElementsByTagName(TICKET).item(0);
        if (ticket != null) {
            final int value = Integer.parseInt(ticket.getFirstChild().getTextContent().trim());
            globalSettings.setShortIndividualResultPrinting(value == 1);
        }

        final Node printing = root.getElementsByTagName(PRINTING).item(0);
        if (printing != null) {
            final int bool = Integer.parseInt(printing.getFirstChild().getTextContent().trim());
            globalSettings.setShortGlobalResultPrinting(bool == 1);
        }

        final Node autoScrolling = root.getElementsByTagName(AUTO_SCROLLING).item(0);
        if (autoScrolling != null) {
            int bool = Integer.parseInt(autoScrolling.getFirstChild().getTextContent());
            globalSettings.setResultAutoScrolling(bool == 1);
        }

        final Node scrollingSpeed = root.getElementsByTagName(SCROLLING_SPEED).item(0);
        if (scrollingSpeed != null) {
            globalSettings.setAutoScrollingSpeedInS(Integer.parseInt(scrollingSpeed.getFirstChild().getTextContent()));
        }
        _getAllCategories(globalSettings, root);

        final Node points = root.getElementsByTagName(POINTS).item(0);
        if (points != null) {
            globalSettings.setPenaltyPointsDefault(Integer.parseInt(points.getFirstChild().getTextContent()));
            final Node time = root.getElementsByTagName(TIME).item(0);
            globalSettings.setPenaltyTimeDefault(Integer.parseInt(time.getFirstChild().getTextContent()));
            final Node mispunchPenaltyPoints = root.getElementsByTagName(MISPUNCH_PENALTY_POINTS).item(0);
            globalSettings.setMispunchPenaltyPoints(Integer.parseInt(mispunchPenaltyPoints.getFirstChild().getTextContent()));
            final Node mispunchPenaltyTime = root.getElementsByTagName(MISPUNCH_PENALTY_TIME).item(0);
            globalSettings.setMispunchPenaltyTimeInS(Integer.parseInt(mispunchPenaltyTime.getFirstChild().getTextContent()));
        }
    }

    private static void _getAllCategories( GlobalSettings globalSettings, Document root ) {
        final NodeList categories = root.getElementsByTagName(CATEGORY);
        for (int i = 0; i < categories.getLength(); i++) {
            Node item = categories.item(i);
            if (item.getNodeType() == Node.ELEMENT_NODE) {
                Element element = (Element) item;
                String longName = element.getElementsByTagName(LONG_NAME).item(0).getTextContent();
                String shortName = element.getElementsByTagName(SHORT_NAME).item(0).getTextContent();
                Categorie category = new Categorie(
                        longName.trim(),
                        shortName.trim());
                globalSettings.getCategoriesTemplate().addCategorie(category);
            }
        }
    }

    private static String _getSettingAbsoluteFileName( String settingsDirectory ) {
        return settingsDirectory + File.separator + SETTING_FILE_NAME;
    }
}
