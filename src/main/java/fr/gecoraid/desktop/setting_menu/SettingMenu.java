package fr.gecoraid.desktop.setting_menu;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DeskTopPresenter;

import javax.swing.JMenu;
import javax.swing.JMenuItem;

public class SettingMenu extends JMenu {
    private final transient DeskTopPresenter _presenter;

    public SettingMenu( DeskTopPresenter presenter ) {
        super(L10n.getString("desktop.menu.settings.title"));
        _presenter = presenter;
        _initializeUI();
    }

    private void _initializeUI() {
        add(_createMenuItemGlobalSettings());
    }

    private JMenuItem _createMenuItemGlobalSettings() {
        JMenuItem menuItem = new JMenuItem(L10n.getString("desktop.menu.settings.global"));
        menuItem.addActionListener(event -> _presenter.configureGlobalSettings());
        return menuItem;
    }
}
