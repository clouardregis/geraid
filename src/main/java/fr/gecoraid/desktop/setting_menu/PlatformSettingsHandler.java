package fr.gecoraid.desktop.setting_menu;

import fr.gecoraid.model.GlobalSettings;
import org.jetbrains.annotations.VisibleForTesting;

import java.io.File;
import java.io.IOException;

public class PlatformSettingsHandler {
    private static final String GLOBAL_SETTINGS_DIRNAME = "gecoraid";

    public static void loadGlobalSettings( GlobalSettings globalSettings ) {
        final String directoryName = getGlobalSettingsDirectory();
        GlobalSettingsFile.load(globalSettings, directoryName);
    }

    public static void saveGlobalSettings( GlobalSettings globalSettings ) {
        GlobalSettingsFile.save(globalSettings, getGlobalSettingsDirectory());
    }

    public static void createGlobalSettingsDirectoryIfNeeded() throws IOException {
        final String directoryName = getGlobalSettingsDirectory();
        _createGlobalSettingsDirectoryIfNeeded(directoryName);
    }

    public static boolean isExistGlobalSettingsDirectory() {
        final String directoryName = getGlobalSettingsDirectory();
        final File directory = new File(directoryName);
        return directory.exists();
    }

    public static boolean platformIsMacOs() {
        return System.getProperty("os.name").startsWith("Mac");
    }

    public static boolean platformIsWindows() {
        return System.getProperty("os.name").startsWith("Windows");
    }

    public static boolean platformIsLinux() {
        return System.getProperty("os.name").startsWith("Linux");
    }

    public static String getGlobalSettingsDirectory() {
        final String homeDirectory = System.getProperty("user.home");
        File directory;
        if (platformIsMacOs()) {
            directory = new File(homeDirectory + "/Library/Application Support/" + GLOBAL_SETTINGS_DIRNAME);
        } else if (platformIsWindows()) {
            directory = new File(System.getenv("APPDATA") + File.separator + GLOBAL_SETTINGS_DIRNAME);
        } else if (platformIsLinux()) {
            directory = new File(homeDirectory + "/." + GLOBAL_SETTINGS_DIRNAME);
        } else {
            directory = new File(homeDirectory + "/." + GLOBAL_SETTINGS_DIRNAME);
        }
        return directory.getAbsolutePath();
    }

    @VisibleForTesting
    public static void _createGlobalSettingsDirectoryIfNeeded( String directoryName ) throws IOException {
        final File directory = new File(directoryName);
        if (!directory.exists()) {
            if (!directory.mkdirs()) {
                throw new IOException();
            }
        }
    }
}
