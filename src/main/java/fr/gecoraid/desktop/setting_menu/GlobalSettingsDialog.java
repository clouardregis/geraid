package fr.gecoraid.desktop.setting_menu;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DeskTopPresenter;
import fr.gecoraid.desktop.DesktopView;
import fr.gecoraid.desktop.widget.IconButton;
import fr.gecoraid.desktop.CategoryEditorDialog;
import fr.gecoraid.desktop.CategoryTableModel;
import fr.gecoraid.desktop.raid_panel.BaliseEditor;
import fr.gecoraid.model.Balise;
import fr.gecoraid.model.Categorie;
import fr.gecoraid.model.Categories;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SpinnerNumberModel;
import javax.swing.WindowConstants;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

public class GlobalSettingsDialog extends JDialog {
    private static final long serialVersionUID = 101L;

    private final DesktopView _parent;
    private final transient GlobalSettingsPresenter _presenter;

    private final transient Categories _categories = new Categories();
    private JTextField _clubNameTextField;
    private JTextField _raidFolderTextField;
    private JTable _categoryTable;
    private JButton _editCategoryButton;
    private JButton _removeButton;
    private JTextField _backupFolderTextField;
    private JCheckBox _individualResultReducedPrintCheckbox;
    private JCheckBox _globalResultReducedPrintCheckbox;
    private JCheckBox _automaticScrollingCheckboxCheckbox;
    private JSpinner _scrollingSpeedSpinner;
    private JButton _okButton;
    private boolean _isCancelled;

    public GlobalSettingsDialog( DesktopView parent, DeskTopPresenter presenter ) {
        super(parent);
        _parent = parent;
        _presenter = new GlobalSettingsPresenter(presenter);
        _initializeUI();
        _initializeData();
    }

    public boolean showDialog() {
        setVisible(true);
        return _isCancelled;
    }

    private void _initializeData() {
        _clubNameTextField.setText(_presenter.getClubName());
        _raidFolderTextField.setText(_presenter.getRaidDirectoryPath());
        _backupFolderTextField.setText(_presenter.getWorkingDirectoryPath());
        _individualResultReducedPrintCheckbox.setSelected(_presenter.isShortIndividualResultPrinting());
        _globalResultReducedPrintCheckbox.setSelected(_presenter.isShortGlobalResultPrinting());
        _automaticScrollingCheckboxCheckbox.setSelected(_presenter.isResultAutoScrolling());
        _scrollingSpeedSpinner.setValue(_presenter.getAutoScrollingSpeedInS());
        Categories categories = _presenter.getCategoriesTemplate();
        for (int i = 0; i < categories.getSize(); i++) {
            _categories.addCategorie(categories.getCategories().get(i));
        }
        _categoryTable.setModel(new CategoryTableModel(_categories));
    }

    private void _initializeUI() {
        setTitle(L10n.getString("global_settings.title"));
        setModal(true);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        JPanel contentPane = new JPanel(new BorderLayout());
        contentPane.add(_createContentPanel(), BorderLayout.CENTER);
        contentPane.add(_createButtonPanel(), BorderLayout.SOUTH);
        setContentPane(contentPane);
        pack();
    }

    private JPanel _createContentPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        panel.add(_createFolderPanel());
        panel.add(_createHtmlPanel());
        panel.add(_createPrintPanel());
        panel.add(_createCategoryPanel());
        return panel;
    }

    private JPanel _createFolderPanel() {
        JPanel panelDossiers = new JPanel();
        panelDossiers.setLayout(new BoxLayout(panelDossiers, BoxLayout.Y_AXIS));

        JPanel panel1 = new JPanel(new BorderLayout());
        panel1.add(new JLabel(L10n.getString("global_settings.club_name")), BorderLayout.WEST);
        _clubNameTextField = new JTextField(0);
        _clubNameTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void changedUpdate( DocumentEvent event ) {
            }

            @Override
            public void removeUpdate( DocumentEvent event ) {
                _checkMinimalSettings();
            }

            @Override
            public void insertUpdate( DocumentEvent event ) {
                _checkMinimalSettings();
            }
        });
        panel1.add(_clubNameTextField, BorderLayout.CENTER);
        panelDossiers.add(panel1);

        JPanel panel2 = new JPanel(new BorderLayout());
        panel2.add(new JLabel(L10n.getString("global_settings.raid_folder")), BorderLayout.WEST);
        _raidFolderTextField = new JTextField();
        _raidFolderTextField.setEditable(false);
        panel2.add(_raidFolderTextField, BorderLayout.CENTER);
        panel2.add(_createRaidFolderBrowser(), BorderLayout.EAST);
        panelDossiers.add(panel2);

        JPanel panel3 = new JPanel(new BorderLayout());
        panel3.add(new JLabel(L10n.getString("global_settings.data_folder")), BorderLayout.WEST);
        _backupFolderTextField = new JTextField();
        _backupFolderTextField.setEditable(false);
        panel3.add(_backupFolderTextField, BorderLayout.CENTER);
        panel3.add(_createButtonBackupFolderBrowser(), BorderLayout.EAST);
        panelDossiers.add(panel3);
        panelDossiers.add(_createCheckpointSettingButton());
        return panelDossiers;
    }

    private void _checkMinimalSettings() {
        _okButton.setEnabled(_isValidDirectory(_backupFolderTextField.getText())
                && _isValidDirectory(_raidFolderTextField.getText()));
    }

    private boolean _isValidDirectory( String dirname ) {
        if (dirname == null || dirname.isEmpty()) {
            return false;
        }
        File file = new File(dirname);
        return file.exists() && file.canWrite();
    }

    private JButton _createRaidFolderBrowser() {
        JButton jButtonDossier = new JButton("...");
        jButtonDossier.addActionListener(event -> {
            // TODO Mettre dans un presenter
            JFileChooser chooser = new JFileChooser(_raidFolderTextField.getText());
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int returnVal = chooser.showOpenDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                _raidFolderTextField.setText(chooser.getSelectedFile().toString());
            }
            _checkMinimalSettings();
        });
        return jButtonDossier;
    }

    private JPanel _createCategoryPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(new TitledBorder(null, L10n.getString("global_settings.category.title"), TitledBorder.LEADING, TitledBorder.TOP));
        panel.add(_createCategoryButtonPanel());
        panel.add(_createCategoryList());
        return panel;
    }

    private JPanel _createCategoryButtonPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.add(_createAddButton());
        panel.add(_createEditButton());
        panel.add(_createRemoveButton());
        return panel;
    }

    private JScrollPane _createCategoryList() {
        _categoryTable = new JTable(null);
        _categoryTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        _categoryTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked( MouseEvent event ) {
                if (event.getClickCount() >= 2) {
                    // TODO Ajoute le double clic -> lancer l'éditeur d'equipe
                    // TODO Recupérer dans la fonction "_createEditButton()", la fonction qui lancer l'editeur
                } else {
                    // TODO Pourquoi _updateCategoriesTable ici
                    _updateButtonState();
                }
            }
        });


        _categoryTable.getTableHeader().setReorderingAllowed(false);
        JScrollPane scrollPane = new JScrollPane(null);
        scrollPane.setPreferredSize(new Dimension(500, 125));
        scrollPane.setViewportView(_categoryTable);
        return scrollPane;
    }

    private JButton _createAddButton() {
        JButton button = new IconButton("add.png");
        button.setToolTipText(L10n.getString("global_settings.category.add"));
        button.addActionListener(event -> {
            // TODO Mettre dans un presenter
            CategoryEditorDialog editor = new CategoryEditorDialog(this,
                    new Categorie("", ""),
                    _categories,
                    ( Categories c ) -> {
                        _categoryTable.setModel(new CategoryTableModel(c));
                        int lastRow = _categoryTable.getRowCount() - 1;
                        _categoryTable.setRowSelectionInterval(lastRow, lastRow);
                        _updateButtonState();
                        _checkMinimalSettings();
                    },
                    true);
            editor.setLocationRelativeTo(this);
            editor.setVisible(true); // TODO ShowDialog()
        });
        return button;
    }

    private JButton _createEditButton() {
        _editCategoryButton = new IconButton("edit.png");
        _editCategoryButton.setEnabled(false);
        _editCategoryButton.setToolTipText(L10n.getString("global_settings.category.modify"));
        _editCategoryButton.addActionListener(event -> {
            // TODO Mettre dans un presenter pour pouvoir etre appelée a un autre endroit
            // TODO faire une classe GlobalSettingPresenter
            Categorie selectedCategory = _categories.getCategorie((String) _categoryTable.getModel().getValueAt(_categoryTable.getSelectedRow(), 1));
            CategoryEditorDialog view = new CategoryEditorDialog(this,
                    selectedCategory,
                    _categories,
                    ( Categories c ) -> {
                        _categoryTable.setModel(new CategoryTableModel(c));
                        _checkMinimalSettings();
                    },
                    false);
            view.setLocationRelativeTo(this);
            view.showDialog(); //TODO utilisation du resultat de showDialog
        });
        return _editCategoryButton;
    }

    private JButton _createRemoveButton() {
        _removeButton = new IconButton("delete.png");
        _removeButton.setEnabled(false);
        _removeButton.setToolTipText(L10n.getString("global_settings.category.delete"));
        _removeButton.addActionListener(event -> {
            // TODO Mettre dans un presenter
            if (_categoryTable.getSelectedRow() >= 0) {
                Object category = _categoryTable.getModel().getValueAt(_categoryTable.getSelectedRow(), 0);
                _categories.removeCategorie((String) category);
                _categoryTable.setModel(new CategoryTableModel(_categories));
                _updateButtonState();
                _checkMinimalSettings();
            }
        });
        return _removeButton;
    }

    private void _updateButtonState() {
        if (_categoryTable.getSelectedRowCount() > 0) {
            _editCategoryButton.setEnabled(true);
            _removeButton.setEnabled(true);
        } else {
            _editCategoryButton.setEnabled(false);
            _removeButton.setEnabled(false);
        }
    }

    private JButton _createButtonBackupFolderBrowser() {
        JButton button = new JButton("...");
        button.addActionListener(action -> {
            JFileChooser chooser = new JFileChooser(_backupFolderTextField.getText());
            chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int returnVal = chooser.showOpenDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                _backupFolderTextField.setText(chooser.getSelectedFile().toString());
            }
            _checkMinimalSettings();
        });
        return button;
    }

    private JButton _createCheckpointSettingButton() {
        JButton button = new IconButton("balise.png");
        button.setText(L10n.getString("global_settings.beacon.button"));
        button.setContentAreaFilled(true);
        button.setBorderPainted(true);
        button.setPreferredSize(null);
        button.addActionListener(event -> {
            // TODO Mettre dans un presenter
            Balise balise = new Balise();
            BaliseEditor view = new BaliseEditor(_parent, _presenter.getGlobalSettings(), balise, true, true);
            view.setLocationRelativeTo(this);
            boolean isCancelled = view.showDialog();
            if (!isCancelled) {
                _checkMinimalSettings();
            }
        });
        return button;
    }

    private JPanel _createPrintPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(new TitledBorder(null, L10n.getString("global_settings.print.title"), TitledBorder.LEADING, TitledBorder.TOP));

        JPanel panel1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel1.add(_createIndividualResultReducedPrintCheckbox());
        panel.add(panel1);

        JPanel panel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel2.add(_createGlobalResultReducedPrintCheckbox());
        panel.add(panel2);
        return panel;
    }

    private JCheckBox _createIndividualResultReducedPrintCheckbox() {
        _individualResultReducedPrintCheckbox = new JCheckBox(L10n.getString("global_settings.print.individual"));
        _individualResultReducedPrintCheckbox.addActionListener(event -> _checkMinimalSettings());
        return _individualResultReducedPrintCheckbox;
    }

    private JCheckBox _createGlobalResultReducedPrintCheckbox() {
        _globalResultReducedPrintCheckbox = new JCheckBox(L10n.getString("global_settings.print.global"));
        _globalResultReducedPrintCheckbox.addActionListener(event -> _checkMinimalSettings());
        return _globalResultReducedPrintCheckbox;
    }

    private JPanel _createHtmlPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(new TitledBorder(null, L10n.getString("global_settings.html_scrolling.title"), TitledBorder.LEADING, TitledBorder.TOP));

        JPanel panel1 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel1.add(_createAutomaticScrollingCheckbox());
        panel.add(panel1);

        JPanel panel2 = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel2.add(new JLabel(L10n.getString("global_settings.html_scrolling.spinner.label")));
        panel2.add(_createScrollingSpeedSpinner());
        panel2.add(new JLabel(L10n.getString("global_settings.club_name.spinner.seconds")));
        panel.add(panel2);
        return panel;
    }

    private JCheckBox _createAutomaticScrollingCheckbox() {
        _automaticScrollingCheckboxCheckbox = new JCheckBox(L10n.getString("global_settings.html_scrolling.checkbox"));
        _automaticScrollingCheckboxCheckbox.addActionListener(event -> _checkMinimalSettings());
        return _automaticScrollingCheckboxCheckbox;
    }

    private JSpinner _createScrollingSpeedSpinner() {
        _scrollingSpeedSpinner = new JSpinner(new SpinnerNumberModel(20, 10, 60, 10));
        _scrollingSpeedSpinner.addChangeListener(event -> _checkMinimalSettings());

        return _scrollingSpeedSpinner;
    }

    private JPanel _createButtonPanel() {
        FlowLayout flowLayout = new FlowLayout(FlowLayout.CENTER);
        flowLayout.setHgap(50);
        JPanel panel = new JPanel(flowLayout);
        panel.add(_createValidateButton());
        panel.add(_createCancelButton());
        return panel;
    }

    private JButton _createValidateButton() {
        _okButton = new JButton(L10n.getString("validate"));
        _okButton.setEnabled(false);
        _okButton.addActionListener(event -> {
            // TODO Mettre dans un presenter
            _presenter.setClubName(_clubNameTextField.getText().trim());
            _presenter.setRaidDirectoryPath(_raidFolderTextField.getText().trim());
            _presenter.setWorkingDirectoryPath(_backupFolderTextField.getText().trim());
            _presenter.setShortIndividualResultPrinting(_individualResultReducedPrintCheckbox.isSelected());
            _presenter.setShortGlobalResultPrinting(_globalResultReducedPrintCheckbox.isSelected());
            _presenter.setResultAutoScrolling(_automaticScrollingCheckboxCheckbox.isSelected());
            _presenter.setAutoScrollingSpeedInS((int) _scrollingSpeedSpinner.getValue());
            _presenter.setCategoriesTemplate(_categories);
            _presenter.saveGlobalSettings();
            _isCancelled = false;
            dispose();
        });
        getRootPane().setDefaultButton(_okButton);
        return _okButton;
    }

    private JButton _createCancelButton() {
        JButton button = new JButton(L10n.getString("cancel"));
        button.addActionListener(event -> {
            _isCancelled = true;
            dispose();
        });
        return button;
    }
}