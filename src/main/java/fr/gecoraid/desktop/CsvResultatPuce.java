package fr.gecoraid.desktop;

import fr.gecoraid.desktop.widget.ErrorPane;
import fr.gecoraid.model.Partial;
import fr.gecoraid.model.Raid;
import fr.gecoraid.model.ResultatPuce;
import fr.gecoraid.model.ResultatsPuce;
import fr.gecoraid.tools.TimeManager;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.ParseException;

// TODO mettre une visibilite package
public class CsvResultatPuce {
    // TODO clean code
    public static void exportToFile( ResultatsPuce rps, String fichier ) {
        File chemin = new File(fichier);
        try (Writer monFichier = new OutputStreamWriter(Files.newOutputStream(chemin.toPath()), StandardCharsets.UTF_8)) {
            // TODO L10n.getString()
            String tampon = "Parcours;Etape;Dossard;Équipe;Catégorie;Puce;Départ;Arrivée";
            monFichier.write(tampon, 0, tampon.length());
            monFichier.write(System.lineSeparator());
            for (int i = 0; i < rps.getSize(); i++) {
                tampon = rps.getResultatsPuce().get(i).toCSV();
                monFichier.write(tampon, 0, tampon.length());
                monFichier.write(System.lineSeparator());
            }
        } catch (IOException e) {
            // TODO L10n.getString()
            // TODO beep
            ErrorPane.showMessageDialog("Erreur d'écriture du fichier d'export : " + e.getClass().getName() + ", " + e.getMessage());
        }
    }

    public static void importFromFile( Raid g, String fichier ) {
        File chemin = new File(fichier);
        String chaine;
        String[] tampon;

        try {
            if (!chemin.exists()) {
                chemin.createNewFile();
            }
            BufferedReader monFichier = new BufferedReader(new FileReader(chemin));
            g.getResultatsPuce().clearResultats();
            monFichier.readLine();
            while ((chaine = monFichier.readLine()) != null) {
                tampon = chaine.trim().split(";");
                ResultatPuce rp = new ResultatPuce();
                rp.setParcours(g.getParcourss().getParcoursDeNom(tampon[0]));
                rp.setEtape(g.getParcourss().getParcoursDeNom(tampon[0]).getEtape(tampon[1]));
                rp.setEquipe(g.getParcourss().getParcoursDeNom(tampon[0]).getEquipeIdPuce(tampon[5]));
                rp.getPuce().setIdPuce(tampon[5]);
                try {
                    rp.getPuce().setStarttime(TimeManager.parse(tampon[6], TimeManager.FORMATTER));
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                try {
                    rp.getPuce().setFinishtime(TimeManager.parse(tampon[7], TimeManager.FORMATTER));
                } catch (ParseException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                }
                Partial[] partiels = new Partial[(tampon.length - 8) / 2];
                for (int i = 0; i < (tampon.length - 8) / 2; i++) {
                    Partial partiel = new Partial();
                    partiel.setCode(Integer.parseInt(tampon[8 + i * 2]));
                    try {
                        partiel.setTime(TimeManager.parse(tampon[9 + i * 2], TimeManager.FORMATTER));
                    } catch (ParseException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                    partiels[i] = partiel;
                }
                rp.getPuce().setPartiels(partiels);
                g.addResultatPuce(rp);
            }
            monFichier.close();
        } catch (IOException e) {
            // TODO L10n.getString()
            // TODO beep
            ErrorPane.showMessageDialog("Erreur d'écriture du fichier d'import : " + e.getClass().getName() + ", " + e.getMessage());
        }
    }
}
