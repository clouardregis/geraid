package fr.gecoraid.desktop.sportident_panel;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DeskTopPresenter;
import fr.gecoraid.desktop.widget.IconButton;
import fr.gecoraid.desktop.widget.ErrorPane;
import fr.gecoraid.model.ZeroTime;

import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.border.TitledBorder;
import java.awt.Dimension;
import java.awt.FlowLayout;

public class SportIdentCardPanel extends JPanel {
    private final transient DeskTopPresenter _presenter;

    private JButton _sportIdentReaderButton;
    private JComboBox<String> _communicationPortsCombBox;
    private JSpinner _zeroHourSpinner;
    private JSpinner _zeroMinuteSpinner;
    private boolean _sportIdentReaderEnabled = false;

    public SportIdentCardPanel( DeskTopPresenter presenter ) {
        _presenter = presenter;
        _createContent();
        _updateCommunicationPortList();
    }

    public void changeStateToStart() {
        _sportIdentReaderButton.setIcon(_getIcon("play.png"));
        _sportIdentReaderEnabled = false;
        _zeroHourSpinner.setEnabled(true);
        _zeroMinuteSpinner.setEnabled(true);
    }

    public void changeStateToStop() {
        _sportIdentReaderButton.setIcon(_getIcon("stop.png"));
    }

    public void changeStateToWaitForInitialization() {
        _sportIdentReaderButton.setIcon(_getIcon("wait.png"));
        _sportIdentReaderEnabled = true;
        _zeroHourSpinner.setEnabled(false);
        _zeroMinuteSpinner.setEnabled(false);
    }

    public String getSelectedPort() {
        return (String) _communicationPortsCombBox.getSelectedItem();
    }

    public void activateSportIdentButton() {
        _presenter.activateSportIdentButton(this,
                _sportIdentReaderEnabled,
                (int) _zeroHourSpinner.getValue(),
                (int) _zeroMinuteSpinner.getValue(),
                (String) _communicationPortsCombBox.getSelectedItem());
    }

    public void setTime( ZeroTime timeZero ) {
        _zeroHourSpinner.setValue(timeZero.hours);
        _zeroMinuteSpinner.setValue(timeZero.minutes);
    }

    public void setSportIdentReaderButtonEnabled( boolean b ) {
        _sportIdentReaderButton.setEnabled(b);
    }

    public boolean isSportIdentReaderEnabled() {
        return _sportIdentReaderEnabled;
    }

    public void displayNoPortErrorMessage() {
        ErrorPane.showMessageDialog(L10n.getString("sportident_panel.error1"));
    }

    public void displayBadPortErrorMessage() {
        ErrorPane.showMessageDialog( L10n.getString("sportident_panel.error2"));
    }

    private void _createContent() {
        setLayout(new FlowLayout(FlowLayout.LEFT));
        setBorder(new TitledBorder(null, L10n.getString("sportident_panel.subtitle1"), TitledBorder.LEADING, TitledBorder.TOP));
        add(_createHourZeroPanel());
        add(_createSportIdentReaderPanel());
    }

    private JPanel _createHourZeroPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.setBorder(new TitledBorder(null, L10n.getString("sportident_panel.subtitle2"), TitledBorder.LEADING, TitledBorder.TOP));
        panel.add(_createZeroHourSpinner());
        panel.add(new JLabel(L10n.getString("hour_abbreviation")));
        panel.add(_createZeroMinuteSpinner());
        panel.add(new JLabel(L10n.getString("minute_abbreviation")));
        return panel;
    }

    private JPanel _createSportIdentReaderPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.setBorder(new TitledBorder(null, L10n.getString("sportident_panel.title"), TitledBorder.LEADING, TitledBorder.TOP));
        panel.add(_createCommunicationPortsComboBox());
        panel.add(_createReloadCommunicationPortsButton());
        panel.add(_createSportIdentReaderButton());
        return panel;
    }

    private JButton _createReloadCommunicationPortsButton() {
        JButton button = new IconButton("reload.png");
        button.setToolTipText(L10n.getString("sportident_panel.reset"));
        button.addActionListener(event -> _updateCommunicationPortList());
        return button;
    }

    private JSpinner _createZeroHourSpinner() {
        _zeroHourSpinner = new JSpinner();
        _zeroHourSpinner.setModel(new SpinnerNumberModel(0, 0, 23, 1));
        _zeroHourSpinner.addChangeListener(event -> _presenter.setZeroHour((int) _zeroHourSpinner.getValue()));
        return _zeroHourSpinner;
    }

    private JSpinner _createZeroMinuteSpinner() {
        _zeroMinuteSpinner = new JSpinner();
        _zeroMinuteSpinner.setModel(new SpinnerNumberModel(0, 0, 59, 1));
        _zeroMinuteSpinner.addChangeListener(event -> _presenter.setZeroMinute((int) _zeroMinuteSpinner.getValue()));
        return _zeroMinuteSpinner;
    }

    private JComboBox<String> _createCommunicationPortsComboBox() {
        _communicationPortsCombBox = new JComboBox<>();
        Dimension preferredSize = _communicationPortsCombBox.getPreferredSize();
        preferredSize.width = 150;
        _communicationPortsCombBox.setPreferredSize(preferredSize);
        return _communicationPortsCombBox;
    }

    private JButton _createSportIdentReaderButton() {
        _sportIdentReaderButton = new IconButton("play.png");
        _sportIdentReaderButton.setToolTipText(L10n.getString("sportident_panel.reader_button"));
        _sportIdentReaderButton.addActionListener(event -> activateSportIdentButton());
        return _sportIdentReaderButton;
    }

    private ImageIcon _getIcon( String iconName ) {
        //noinspection ConstantConditions
        return new ImageIcon(getClass().getClassLoader().getResource("fr/gecoraid/icon/" + iconName));
    }

    private void _updateCommunicationPortList() {
        _communicationPortsCombBox.setModel(new DefaultComboBoxModel<>(CommunicationRail.getAvailablePortsRail()));
    }
}
