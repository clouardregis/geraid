package fr.gecoraid.desktop.sportident_panel;

import com.fazecast.jSerialComm.SerialPort;

class CommunicationRail {
    private CommunicationRail() {
    }

    static String[] getAvailablePortsRail() {
        final SerialPort[] serialPorts = SerialPort.getCommPorts();
        final String[] portRailList = new String[serialPorts.length];
        for (int i = 0; i < portRailList.length; i++) {
            portRailList[i] = serialPorts[i].getSystemPortName();
        }
        return portRailList;
    }
}
