package fr.gecoraid.desktop;

import fr.gecoraid.model.Etape;
import fr.gecoraid.model.Parcours;
import fr.gecoraid.model.Raid;

import javax.swing.table.AbstractTableModel;

// TODO mettre une visibilite package
public class PenaliteTableModel extends AbstractTableModel {
    // TODO code review
    private static final long serialVersionUID = -5974347188812805359L;
    private final String[] _columnNames;
    private final transient Object[][] _data;

    public PenaliteTableModel( Raid gr, Parcours p, Etape e ) {
        _columnNames = gr.getEntetes(e);
        _data = gr.getData(p, e);
    }

    @Override
    public int getColumnCount() {
        return _columnNames.length;
    }

    @Override
    public int getRowCount() {
        return _data.length;
    }

    @Override
    public String getColumnName( int col ) {
        return _columnNames[col];
    }

    @Override
    public Object getValueAt( int row, int col ) {
        return _data[row][col];
    }

    @Override
    public Class<?> getColumnClass( int c ) {
        return getValueAt(0, c).getClass();
    }

    @Override
    public void setValueAt( Object value, int row, int col ) {
        _data[row][col] = value;
        fireTableCellUpdated(row, col);
    }

    public String getEntetesCSV() {
        StringBuilder retour = new StringBuilder();
        retour.append(_columnNames[0]);
        for (int i = 1; i < _columnNames.length; i++) {
            retour.append(";").append(_columnNames[i]);
        }

        return retour.toString();
    }

    public String[] getDataCSV() {
        String[] retour = new String[getRowCount()];
        for (int r = 0; r < getRowCount(); r++) {
            StringBuilder ligne = new StringBuilder(getValueAt(r, 0).toString());
            for (int c = 1; c < getColumnCount(); c++) {
                ligne.append(";").append(getValueAt(r, c));
            }
            retour[r] = ligne.toString();
        }

        return retour;
    }

    public String getEntetesEquipesCSV() {
        StringBuilder retour = new StringBuilder();
        retour.append(_columnNames[0]);
        for (int i = 1; i < 3; i++) {
            retour.append(";").append(_columnNames[i]);
        }
        // TODO L10n.getString()
        retour.append(";Points");
        // TODO L10n.getString()
        retour.append(";Temps (hh:mm:ss)");
        return retour.toString();
    }

    public String[] getDataEquipesCSV() {
        String[] retour = new String[getRowCount()];
        for (int r = 0; r < getRowCount(); r++) {
            StringBuilder ligne = new StringBuilder(getValueAt(r, 0).toString());
            for (int c = 1; c < 3; c++) {
                ligne.append(";").append(getValueAt(r, c));
            }
            retour[r] = ligne.toString();
        }
        return retour;
    }
}
