package fr.gecoraid.desktop;

import fr.gecoraid.L10n;
import fr.gecoraid.model.Categories;

import javax.swing.table.AbstractTableModel;

// TODO mettre une visibilite package
public class CategoryTableModel extends AbstractTableModel {
    private static final long serialVersionUID = -5974347188812805359L;

    private final String[] _categoryNames = new String[2];
    private final transient String[][] _data;

    public CategoryTableModel( Categories categories ) {
        _categoryNames[0] = L10n.getString("settings.category.long_name");
        _categoryNames[1] = L10n.getString("settings.category.short_name");
        _data = categories.getData();
    }

    @Override
    public int getColumnCount() {
        return _categoryNames.length;
    }

    @Override
    public int getRowCount() {
        return _data.length;
    }

    @Override
    public String getColumnName( int col ) {
        return _categoryNames[col];
    }

    @Override
    public Object getValueAt( int row, int col ) {
        return _data[row][col];
    }

    @Override
    public Class<?> getColumnClass( int c ) {
        return getValueAt(0, c).getClass();
    }

    @Override
    public void setValueAt( Object value, int row, int col ) {
        _data[row][col] = (String) value;
        fireTableCellUpdated(row, col);
    }
}
