package fr.gecoraid.desktop.help_menu;

import fr.gecoraid.L10n;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;
import javax.swing.KeyStroke;
import java.awt.event.KeyEvent;

public class HelpMenu extends JMenu {
    private final transient HelpPresenter _presenter = new HelpPresenter();

    public HelpMenu() {
        super("?");
        add(_createMenuItemAide());
        add( new JSeparator());
        add(_createMenuItemAbout());
    }

    private JMenuItem _createMenuItemAide() {
        final JMenuItem menuItem = new JMenuItem(L10n.getString("help_menu.help.item"));
        menuItem.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F1, 0));
        menuItem.addActionListener(event -> _presenter.displayHelp());
        return menuItem;
    }

    private JMenuItem _createMenuItemAbout() {
        final JMenuItem menuItem = new JMenuItem(L10n.getString("help_menu.about.item"));
        menuItem.addActionListener(event -> _presenter.displayAbout());
        return menuItem;
    }
}
