package fr.gecoraid.desktop.help_menu;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.widget.ErrorPane;

import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.net.URL;

class ManualView {
    private ManualView() {
    }

    public static void displayHelp() {
        if (Desktop.isDesktopSupported()) {
            try {
                URL url = ManualView.class.getClassLoader().getResource("fr/gecoraid/doc/manual.pdf");
                assert url != null;
                Desktop.getDesktop().open(new File(url.getFile()));
            } catch (IOException | RuntimeException e) {
                ErrorPane.showMessageDialog(L10n.getString("help_view.error.text", e.getClass().getName()));
            }
        }
    }
}
