package fr.gecoraid.desktop.help_menu;

import fr.gecoraid.L10n;
import fr.gecoraid.SessionLauncher;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.WindowConstants;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import javax.swing.border.EtchedBorder;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Desktop;
import java.awt.FlowLayout;
import java.awt.KeyboardFocusManager;
import java.awt.Point;
import java.awt.SystemColor;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.beans.PropertyChangeEvent;
import java.beans.VetoableChangeListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

class AboutView extends JDialog {
    private static final String DEVELOPER_NAMES = "Thierry PORRET<br>Régis CLOUARD<br>Martin FEAUX DE LA CROIX";

    private JLabel _titleLabel1;
    private JLabel _titleLabel2;
    private JLabel _textLabel;
    private JLabel _licenseLabel;

    private JLabel _projectUrlLabel;

    public AboutView() {
        _initializeUI();
        _titleLabel1.setText(L10n.getString("about_view.title1"));
        _titleLabel2.setText(L10n.getString("about_view.title2"));
        String version = L10n.getString("about_view.text1", SessionLauncher.getVersion());
        String developers = L10n.getString("about_view.text2", DEVELOPER_NAMES);
        _textLabel.setText(version + developers);
        _licenseLabel.setText(L10n.getString("about_view.license"));
        _projectUrlLabel.setText(L10n.getString("about_view.notice"));
        addDismissibleBehaviour(this);
        pack();
    }

    public void showDialog() {
        setVisible(true);
    }

    private static void addDismissibleBehaviour( AboutView aboutView ) {
        KeyboardFocusManager.getCurrentKeyboardFocusManager()
                .addVetoableChangeListener("focusedWindow", new VetoableChangeListener() {
                    private boolean gained = false;

                    @Override
                    public void vetoableChange( PropertyChangeEvent event ) {
                        if (event.getNewValue() == aboutView) {
                            gained = true;
                        }
                        if (gained && event.getNewValue() != aboutView) {
                            aboutView.dispose();
                        }
                    }
                });
    }

    private void _initializeUI() {
        setUndecorated(true);
        setModalityType(ModalityType.MODELESS);
        setTitle(L10n.getString("help_menu.about.item"));
        setResizable(false);
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setContentPane(_createBackgroundPanel());
    }

    private JPanel _createBackgroundPanel() {
        JPanel panel = new JPanel(new BorderLayout());
        Border blackLine = BorderFactory.createLineBorder(Color.black);
        Border emptyBorder = BorderFactory.createEmptyBorder(20, 20, 20, 20);
        Border compoundBorder = BorderFactory.createCompoundBorder(blackLine, emptyBorder);
        panel.setBorder(compoundBorder);
        panel.setOpaque(false);
        panel.setLocation(new Point(0, 16));
        panel.add(_createTitlePanel(), BorderLayout.NORTH);
        panel.add(_createTextPanel(), BorderLayout.CENTER);
        panel.add(_createLogoPanel(), BorderLayout.WEST);
        panel.add(_createLicensePanel(), BorderLayout.SOUTH);
        return panel;
    }

    private JPanel _createLogoPanel() {
        JLabel label = new JLabel();
        //noinspection ConstantConditions
        label.setIcon(new ImageIcon(getClass().getClassLoader().getResource("fr/gecoraid/icon/logo128.png")));
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.setOpaque(false);
        panel.add(label);
        return panel;
    }

    private JPanel _createTextPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 0));
        panel.setOpaque(true);
        panel.setBackground(fr.gecoraid.NimbusTheme.BUTTER_1);
        panel.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
        _textLabel = new JLabel();
        _textLabel.setBackground(SystemColor.control);
        panel.add(_textLabel);
        return panel;
    }

    private JPanel _createLicensePanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(new EmptyBorder(10, 0, 10, 0));
        panel.setOpaque(false);
        _licenseLabel = new JLabel();
        _licenseLabel.setHorizontalAlignment(SwingConstants.CENTER);
        panel.add(_licenseLabel);

        _projectUrlLabel = new JLabel();
        _projectUrlLabel.setHorizontalAlignment(SwingConstants.CENTER);
        _projectUrlLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));
        _projectUrlLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked( MouseEvent event ) {
                try {
                    Desktop.getDesktop().browse(new URI("https://gitlab.com/clouardregis/gecoraid"));
                } catch (URISyntaxException | IOException ignored) {
                }
            }
        });
        panel.add(_projectUrlLabel);
        return panel;
    }

    private JPanel _createTitlePanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        _titleLabel1 = new JLabel();
        _titleLabel1.setHorizontalAlignment(SwingConstants.CENTER);
        panel.add(_titleLabel1);
        _titleLabel2 = new JLabel();
        _titleLabel2.setHorizontalAlignment(SwingConstants.CENTER);
        panel.add(_titleLabel2);
        return panel;
    }
}