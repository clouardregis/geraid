package fr.gecoraid.desktop.help_menu;

class HelpPresenter {

    public void displayHelp() {
        ManualView.displayHelp();
    }

    public void displayAbout() {
        final AboutView dialog = new AboutView();
        dialog.setLocationRelativeTo(null);
        dialog.showDialog();
    }
}
