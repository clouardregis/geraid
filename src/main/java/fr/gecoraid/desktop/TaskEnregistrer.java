package fr.gecoraid.desktop;

import javax.swing.SwingWorker;

// TODO clean code
// TODO put in the convenient folder
public class TaskEnregistrer extends SwingWorker<Void, Void> {
    private final DesktopView _ihm;
    private Thread _currentThread = null;

    public TaskEnregistrer( DesktopView ihm ) {
        _ihm = ihm;
    }

    @Override
    protected Void doInBackground() throws Exception {
        _currentThread = Thread.currentThread();
        _ihm.setEnregistrementOk();
        Thread.sleep(1000);
        _ihm.setEnregistrementNormal();
        stop();
        return null;
    }

    public void stop() {
        if (_currentThread != null) {
            _currentThread.interrupt();
        }
    }
}
