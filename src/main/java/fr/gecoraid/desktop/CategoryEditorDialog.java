package fr.gecoraid.desktop;

import fr.gecoraid.L10n;
import fr.gecoraid.model.Categorie;
import fr.gecoraid.model.Categories;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.border.TitledBorder;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.util.function.Consumer;

// TODO mettre une visibilite package
public class CategoryEditorDialog extends JDialog {
    private static final long serialVersionUID = 1L;

    private final transient Categorie _category;
    private final transient Categories _categories;
    private final transient Consumer<Categories> _updateCallback;
    private final boolean _isCreation;
    private JTextField _longNameTextField;
    private JTextField _shortNameTextField;
    private JLabel _errorLabel;

    public CategoryEditorDialog( JDialog parent,
                                 Categorie category,
                                 Categories categories,
                                 Consumer<Categories> updateCallback,
                                 boolean isCreation ) {
        super(parent);
        _category = category;
        _categories = categories;
        _isCreation = isCreation;
        _updateCallback = updateCallback;
        _initializeUI();
        if (_isCreation) {
            setTitle(L10n.getString("category_editor.title1"));
        } else {
            setTitle(L10n.getString("category_editor.title2"));
        }
        _longNameTextField.setText(_category.getLongName());
        _shortNameTextField.setText(_category.getShortName());
    }

    public void showDialog() {
        setVisible(true);
    }

    private void _initializeUI() {
        setModal(true);
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(_createContentPanel(), BorderLayout.NORTH);
        panel.add(_createButtonPanel(), BorderLayout.SOUTH);
        setContentPane(panel);
        pack();
    }

    private JPanel _createContentPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP));

        JPanel panel1 = new JPanel(new GridBagLayout());

        final GridBagConstraints constraints1 = new GridBagConstraints();
        constraints1.gridx = 0;
        constraints1.gridy = 0;
        panel1.add(new JLabel(L10n.getString("settings.category.long_name")), constraints1);

        final GridBagConstraints constraints2 = new GridBagConstraints();
        constraints2.gridx = 1;
        constraints2.gridy = 0;
        constraints2.fill = GridBagConstraints.HORIZONTAL;
        constraints2.gridwidth = GridBagConstraints.REMAINDER;
        constraints2.weightx = 1;
        panel1.add(_createLongNameTextField(), constraints2);

        final GridBagConstraints constraints3 = new GridBagConstraints();
        constraints3.gridx = 0;
        constraints3.gridy = 1;
        panel1.add(new JLabel(L10n.getString("settings.category.short_name")), constraints3);

        GridBagConstraints constraints4 = new GridBagConstraints();
        constraints4.gridx = 1;
        constraints4.gridy = 1;
        constraints4.weightx = 1;
        constraints4.fill = GridBagConstraints.HORIZONTAL;
        constraints4.gridwidth = GridBagConstraints.REMAINDER;
        panel1.add(_createShortNameTextField(), constraints4);

        panel.add(panel1);

        JPanel panel2 = new JPanel(new BorderLayout());
        _errorLabel = new JLabel("");
        _errorLabel.setForeground(Color.red);
        _errorLabel.setPreferredSize(new Dimension(280, 20));
        panel2.add(_errorLabel, BorderLayout.CENTER);

        GridBagConstraints constraints5 = new GridBagConstraints();
        constraints5.gridx = 0;
        constraints5.gridy = 2;
        constraints5.weightx = 0.5;
        constraints5.fill = GridBagConstraints.HORIZONTAL;
        constraints5.gridwidth = GridBagConstraints.REMAINDER;
        panel.add(panel2, constraints5);

        return panel;
    }

    private JTextField _createLongNameTextField() {
        _longNameTextField = new JTextField();
        return _longNameTextField;
    }

    private JTextField _createShortNameTextField() {
        _shortNameTextField = new JTextField();
        return _shortNameTextField;
    }

    private JPanel _createButtonPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 5));
        panel.add(_createOkButton());
        panel.add(_createCancelButton());
        return panel;
    }

    private JButton _createOkButton() {
        JButton buttonOk;
        if (_isCreation) {
            buttonOk = new JButton(L10n.getString("category_editor.create"));
        } else {
            buttonOk = new JButton(L10n.getString("category_editor.modify"));
        }
        buttonOk.addActionListener(event -> {
            // TODO Mettre dans le presenter -> _result
            if (_longNameTextField.getText().trim().isEmpty() || _shortNameTextField.getText().trim().isEmpty()) {
                _errorLabel.setText(L10n.getString("category_editor.error1"));
            } else if (_categories.existeCategorie(_longNameTextField.getText().trim(), _shortNameTextField.getText().trim())) {
                _errorLabel.setText(L10n.getString("category_editor.error2"));
            } else {
                _category.setNomLong(_longNameTextField.getText());
                _category.setNomCourt(_shortNameTextField.getText());
                if (_isCreation) {
                    _categories.addCategorie(_category);
                }
                _updateCallback.accept(_categories);
                dispose();
            }
        });
        getRootPane().setDefaultButton(buttonOk);
        return buttonOk;
    }

    private JButton _createCancelButton() {
        JButton button = new JButton(L10n.getString("cancel"));
        button.addActionListener(event -> dispose());
        return button;
    }
}
