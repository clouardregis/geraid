package fr.gecoraid.desktop;

import fr.gecoraid.L10n;
import fr.gecoraid.model.Partial;
import fr.gecoraid.tools.TimeManager;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.util.Date;

class IhmPartiel extends JDialog {
    private static final long serialVersionUID = 1L;

    private final ChipResultManagementDialog _parent;

    // TODO séparer la vue du modèle
    private final transient Partial _partiel;

    private final boolean _creation;
    private JSpinner _spinnerHeure;
    private JSpinner _spinnerMinute;
    private JSpinner _spinnerSeconde;
    private JSpinner _spinnerCode;

    IhmPartiel( ChipResultManagementDialog parent, Partial p, boolean creation ) {
        // TODO code review
        super(parent);
        _parent = parent;
        _partiel = p;
        _creation = creation;
        _initializeUI();
        // TODO L10n.getString("result_panel.cvs_writer.error")
        if (_creation) {
            setTitle("Création d'un poste");
        } else {
            setTitle("Modification d'un poste");
        }
        _spinnerCode.setValue(_partiel.getCode());
        _spinnerHeure.setValue(TimeManager.getHeures(_partiel.getTime()));
        _spinnerMinute.setValue(TimeManager.getMinutes(_partiel.getTime()));
        _spinnerSeconde.setValue(TimeManager.getSecondes(_partiel.getTime()));
    }

    private void _initializeUI() {
        setModal(true);
        setResizable(false);
        // TODO L10n.getString()
        setTitle("Partiel");
        setContentPane(_createJContentPane());
        pack();
    }

    private JPanel _createJContentPane() {
        JPanel jContentPane = new JPanel();
        jContentPane.setLayout(new BorderLayout());
        jContentPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        jContentPane.add(_createJPanel(), BorderLayout.CENTER);
        jContentPane.add(_createButtonPanel(), BorderLayout.SOUTH);
        return jContentPane;
    }

    private JPanel _createJPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        JPanel jPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        // TODO L10n.getString()
        jPanel.add(new JLabel("Code :"));
        jPanel.add(_createJSpinnerCode());
        panel.add(jPanel);
        panel.add(_createJPanelHeureDepart());
        return panel;
    }

    private JPanel _createJPanelHeureDepart() {
        JPanel jPanelHeureDepart = new JPanel(new FlowLayout(FlowLayout.LEFT));
        jPanelHeureDepart.setPreferredSize(new Dimension(250, 35));
        // TODO L10n.getString()
        jPanelHeureDepart.add(new JLabel("Heure  : "));
        jPanelHeureDepart.add(_createJSpinnerHeure());
        jPanelHeureDepart.add(new JLabel(L10n.getString("hour_abbreviation")));
        jPanelHeureDepart.add(_createJSpinnerMinute());
        jPanelHeureDepart.add(new JLabel("M"));
        jPanelHeureDepart.add(_createJSpinnerSeconde());
        jPanelHeureDepart.add(new JLabel(L10n.getString("second_abbreviation")));
        return jPanelHeureDepart;
    }

    private JSpinner _createJSpinnerHeure() {
        _spinnerHeure = new JSpinner(new SpinnerNumberModel(0, 0, 47, 1));
        return _spinnerHeure;
    }

    private JSpinner _createJSpinnerMinute() {
        _spinnerMinute = new JSpinner(new SpinnerNumberModel(0, 0, 59, 1));
        return _spinnerMinute;
    }

    private JSpinner _createJSpinnerSeconde() {
        _spinnerSeconde = new JSpinner(new SpinnerNumberModel(0, 0, 59, 1));
        return _spinnerSeconde;
    }

    private JSpinner _createJSpinnerCode() {
        _spinnerCode = new JSpinner(new SpinnerNumberModel(31, 31, 500, 1));
        return _spinnerCode;
    }

    private JPanel _createButtonPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 5));
        panel.add(_createOkButton());
        panel.add(_createCancelButton());
        return panel;
    }

    private JButton _createOkButton() {
        JButton button;
        // TODO L10n.getString()
        if (_creation) {
            button = new JButton("Créer le poste");
        } else {
            button = new JButton("Modifier le poste");
        }
        getRootPane().setDefaultButton(button);
        button.addActionListener(event -> {
            if (_creation) {
                _partiel.setCode((int) _spinnerCode.getValue());
                _partiel.setTime(new Date((int) _spinnerHeure.getValue() * 3600000L +
                        (int) _spinnerMinute.getValue() * 60000L +
                        (int) _spinnerSeconde.getValue() * 1000L));
                Partial[] partiels = new Partial[_parent.getResultatCourant().getPuce().getPartiels().length + 1];
                System.arraycopy(_parent.getResultatCourant().getPuce().getPartiels(), 0,
                        partiels, 0, _parent.getResultatCourant().getPuce().getPartiels().length);
                partiels[_parent.getResultatCourant().getPuce().getPartiels().length] = _partiel;
                _parent.getResultatCourant().getPuce().setPartiels(partiels);
            } else {
                _partiel.setCode((int) _spinnerCode.getValue());
                _partiel.setTime(new Date((int) _spinnerHeure.getValue() * 3600000L +
                        (int) _spinnerMinute.getValue() * 60000L +
                        (int) _spinnerSeconde.getValue() * 1000L));
            }
            dispose();
        });
        return button;
    }

    private JButton _createCancelButton() {
        JButton button = new JButton(L10n.getString("cancel"));
        button.addActionListener(event -> dispose());
        return button;
    }
}
