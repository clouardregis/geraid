package fr.gecoraid.desktop.equipe_panel;

import fr.gecoraid.desktop.DesktopView;
import fr.gecoraid.model.Equipe;
import fr.gecoraid.model.Parcours;

import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.ListCellRenderer;
import java.awt.Color;
import java.awt.Component;
import java.awt.Font;

import static fr.gecoraid.NimbusTheme.ALUMINIUM_5;
import static fr.gecoraid.NimbusTheme.BUTTER_3;

class EquipeListCellRenderer extends JLabel implements ListCellRenderer<Equipe> {
    private static final long serialVersionUID = -6294326879574699200L;
    private final DesktopView _parent;

    public EquipeListCellRenderer( DesktopView parent ) {
        setOpaque(true);
        _parent = parent;
    }

    @Override
    public Component getListCellRendererComponent( JList<? extends Equipe> list,
                                                   Equipe equipe,
                                                   int index,
                                                   boolean isSelected,
                                                   boolean cellHasFocus ) {
        if (isSelected) {
            setForeground(_nimbusToColor(list.getSelectionForeground()));
            setBackground(_nimbusToColor(list.getSelectionBackground()));
        } else {
            setBackground(_nimbusToColor(list.getBackground()));
            setForeground(_nimbusToColor(list.getForeground()));
        }

        setText(equipe.toString());
        if (equipe.isAbsent()) {
            setFont(getFont().deriveFont(Font.ITALIC));
            if (isSelected) {
                setForeground(BUTTER_3);
            } else {
                setForeground(ALUMINIUM_5);
            }
        } else if (equipe.isArrived((Parcours) _parent.getComboBoxParcours().getSelectedItem(), _parent.getSelectedEtape())) {
            setFont(list.getFont());
        } else {
            setFont(getFont().deriveFont(Font.BOLD));
        }
        setEnabled(list.isEnabled());

        return this;
    }

    private Color _nimbusToColor( Color color ) {
        return new Color(color.getRed(), color.getGreen(), color.getBlue());
    }
}
