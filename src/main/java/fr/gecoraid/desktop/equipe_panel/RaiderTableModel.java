package fr.gecoraid.desktop.equipe_panel;

import fr.gecoraid.L10n;
import fr.gecoraid.model.Raiders;

import javax.swing.table.AbstractTableModel;

class RaiderTableModel extends AbstractTableModel {
    private static final long serialVersionUID = -5974347188812805359L;

    private final String[] _columnNames = new String[2];
    private final transient Object[][] _data;

    public RaiderTableModel( Raiders raiders ) {
        _columnNames[0] = L10n.getString("lastname");
        _columnNames[1] = L10n.getString("firstname");
        _data = raiders.getData();
    }

    @Override
    public int getColumnCount() {
        return _columnNames.length;
    }

    @Override
    public int getRowCount() {
        return _data.length;
    }

    @Override
    public String getColumnName( int col ) {
        return _columnNames[col];
    }

    @Override
    public Object getValueAt( int row, int col ) {
        return _data[row][col];
    }

    @Override
    public Class<?> getColumnClass( int c ) {
        return getValueAt(0, c).getClass();
    }

    @Override
    public void setValueAt( Object value, int row, int col ) {
        _data[row][col] = value;
        fireTableCellUpdated(row, col);
    }
}
