package fr.gecoraid.desktop.equipe_panel;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DeskTopPresenter;
import fr.gecoraid.desktop.DesktopView;
import fr.gecoraid.desktop.widget.IconButton;
import fr.gecoraid.model.Equipe;
import fr.gecoraid.tools.ExtensionFileFilter;
import fr.gecoraid.tools.Utils;

import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.TitledBorder;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;

public class EquipePanelView extends JPanel {
    private final transient EquipePanelPresenter _presenter;
    private final JList<Equipe> _equipeList;
    private JButton _sportIdentChipReaderButton;
    private JLabel _teamCounterLabel;

    public EquipePanelView( DesktopView parent, DeskTopPresenter presenter, JList<Equipe> equipeList ) {
        super(new BorderLayout());
        _equipeList = equipeList;
        _presenter = new EquipePanelPresenter(this, presenter, parent, equipeList);
        setBorder(new TitledBorder(null, L10n.getString("equipe_panel.title"), TitledBorder.LEADING, TitledBorder.TOP));
        setMinimumSize(new Dimension(450, 100));
        setPreferredSize(new Dimension(360, 100));
        add(_createEquipePanel(), BorderLayout.NORTH);
        add(_createTeamScrollPane(parent, equipeList), BorderLayout.CENTER);
    }

    public void setEnabledChipReaderButton( boolean value ) {
        _sportIdentChipReaderButton.setEnabled(value);
    }

    public void readSportIdentCard() {
        _presenter.readSportIdentCard();
    }

    public void changeCardReaderIcon( String iconFilename ) {
        //noinspection ConstantConditions
        ImageIcon imageIcon = new ImageIcon(getClass().getClassLoader().getResource("fr/gecoraid/icon/" + iconFilename));
        _sportIdentChipReaderButton.setIcon(imageIcon);
    }

    public void updateTeamTotal( int number ) {
        if (number > 1) {
            _teamCounterLabel.setText(String.format(L10n.getString("equipe_panel.equipe_count_plural", number)));
        } else {
            _teamCounterLabel.setText(String.format(L10n.getString("equipe_panel.equipe_count_singular", number)));
        }
    }

    private JPanel _createEquipePanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.add(_createTeamCounterPanel());
        panel.add(_createAddTeamButton());
        panel.add(_createEditTeamButton());
        panel.add(_createDeleteTeamButton());
        panel.add(_createEquipeMoveButton());
        panel.add(_createNumberBibsButton());
        panel.add(_createSortTeamButton());
        panel.add(_createImportTeamsButton());
        panel.add(_createExportTeamsButton());
        panel.add(_createSportIdentChipReaderButton());
        return panel;
    }

    private JButton _createImportTeamsButton() {
        JButton button = new IconButton("import-csv.png");
        button.setToolTipText(L10n.getString("equipe_panel.import_csv.tooltip"));
        button.addActionListener(event -> {
            JFileChooser chooser = new JFileChooser();
            chooser.setFileFilter(new ExtensionFileFilter("csv", L10n.getString("csv_file.description")));
            chooser.setCurrentDirectory(new File(_presenter.getRaidFolderPath()));
            int returnVal = chooser.showOpenDialog(null);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                String filename = chooser.getSelectedFile().getAbsolutePath();
                _presenter.importTeamListFromFile(filename);
            }
        });
        return button;
    }

    private JButton _createExportTeamsButton() {
        JButton button = new IconButton("export-csv.png");
        button.setToolTipText(L10n.getString("equipe_panel.export_csv.tooltip"));
        button.addActionListener(event -> {
            if (_equipeList.getModel().getSize() > 0) {
                JFileChooser chooser = new JFileChooser();
                chooser.setFileFilter(new ExtensionFileFilter("csv", L10n.getString("csv_file.description")));
                chooser.setCurrentDirectory(new File(_presenter.getRaidFolderPath()));
                int returnVal = chooser.showSaveDialog(null);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    String filename = Utils.checkExtension(chooser.getSelectedFile().getAbsolutePath(), ".csv");
                    _presenter.exportTeamListToFile(filename);
                }
            }
        });
        return button;
    }

    private JButton _createSportIdentChipReaderButton() {
        _sportIdentChipReaderButton = new IconButton("play.png");
        _sportIdentChipReaderButton.setToolTipText(L10n.getString("equipe_panel.chip.tooltip"));
        _sportIdentChipReaderButton.addActionListener(event -> _presenter.readSportIdentCard());
        return _sportIdentChipReaderButton;
    }

    private JButton _createNumberBibsButton() {
        JButton button = new IconButton("bib.png");
        button.setToolTipText(L10n.getString("equipe_panel.bib_numbering.tooltip"));
        button.addActionListener(event -> {
            if (_equipeList.getModel().getSize() > 0) {
                _presenter.numberAllBibs();
            }
        });
        return button;
    }

    private JButton _createSortTeamButton() {
        JButton button = new IconButton("sorting.png");
        button.addActionListener(event -> {
            if (_equipeList.getModel().getSize() > 0) {
                _presenter.sortTeams();
            }
        });
        button.setToolTipText(L10n.getString("equipe_panel.sort.tooltip"));
        return button;
    }

    private JButton _createEquipeMoveButton() {
        JButton button = new IconButton("move.png");
        button.setToolTipText(L10n.getString("equipe_panel.move.tooltip"));
        button.addActionListener(event -> {
            Equipe equipe = _equipeList.getSelectedValue();
            _presenter.moveTeamToAnotherParcours(equipe);
        });
        return button;
    }

    private JComponent _createTeamCounterPanel() {
        JPanel panel = new JPanel();
        panel.setBorder(BorderFactory.createLoweredBevelBorder());
        _teamCounterLabel = new JLabel();
        updateTeamTotal(0);
        panel.add(_teamCounterLabel);
        Dimension preferredSize = panel.getPreferredSize();
        preferredSize.width = 100;
        panel.setPreferredSize(preferredSize);
        return panel;
    }

    private JButton _createAddTeamButton() {
        JButton button = new IconButton("add.png");
        button.setToolTipText(L10n.getString("equipe_panel.add.tooltip"));
        button.addActionListener(event -> _presenter.addNewTeam());
        return button;
    }

    private JButton _createEditTeamButton() {
        JButton editTeamButton = new IconButton("edit.png");
        editTeamButton.setToolTipText(L10n.getString("equipe_panel.modify.tooltip"));
        editTeamButton.addActionListener(event -> {
            if (_equipeList.getSelectedIndex() != -1) {
                _presenter.editCurrentSelectedTeam(_equipeList.getSelectedValue());
            }
        });
        return editTeamButton;
    }

    private JButton _createDeleteTeamButton() {
        JButton button = new IconButton("delete.png");
        button.setToolTipText(L10n.getString("equipe_panel.delete.tooltip"));
        button.addActionListener(event -> {
            if (_equipeList.getModel().getSize() > 0 && _equipeList.getSelectedIndex() > -1) {
                Equipe equipe = _equipeList.getSelectedValue();
                _presenter.deleteCurrentSelectedTeam(equipe);
            }
        });
        return button;
    }

    private JScrollPane _createTeamScrollPane( DesktopView parent, JList<Equipe> equipeList ) {
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_AS_NEEDED);
        scrollPane.setViewportView(_createTeamList(parent, equipeList));
        return scrollPane;
    }

    private JList<Equipe> _createTeamList( DesktopView parent, JList<Equipe> equipeList ) {
        equipeList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked( MouseEvent event ) {
                if (event.getClickCount() > 1) {
                    Equipe equipe = _equipeList.getSelectedValue();
                    _presenter.displaySportIdentCardResults(equipe);
                }
            }
        });
        equipeList.setMinimumSize(new Dimension(450, 0));
        equipeList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        equipeList.setCellRenderer(new EquipeListCellRenderer(parent));
        return equipeList;
    }
}
