package fr.gecoraid.desktop.equipe_panel;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DeskTopPresenter;
import fr.gecoraid.desktop.DesktopView;
import fr.gecoraid.desktop.widget.IconButton;
import fr.gecoraid.model.Categorie;
import fr.gecoraid.model.Equipe;
import fr.gecoraid.model.Raider;
import fr.gecoraid.model.Raiders;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

class EquipeEditor extends JDialog {
    private static final long serialVersionUID = 1L;

    private final transient DeskTopPresenter _presenter;

    private final transient Equipe _equipe;
    private final boolean _isCreation;
    private JTable _raidersTable;
    private JTextField _nameTextField;
    private JTextField _bibTextField;
    private JTextField _chipTextField;
    private JComboBox<Categorie> _categoryComboBox;
    private JLabel _errorLabel;
    private JButton _editButton;
    private JButton _removeButton;
    private JCheckBox _unclassifiedCheckBox;
    private JCheckBox _absentOrAbandonCheckbox;

    public EquipeEditor( DeskTopPresenter presenter,
                         Equipe equipe,
                         boolean isCreation ) {
        _presenter = presenter;
        _equipe = equipe;
        _isCreation = isCreation;
        _initializeUI();
        if (_isCreation) {
            setTitle(L10n.getString("equipe_editor.title1"));
        } else {
            setTitle(L10n.getString("equipe_editor.title2"));
        }
        _nameTextField.setText(_equipe.getNom());
        _bibTextField.setText(_equipe.getDossard());
        _chipTextField.setText(_equipe.getIdPuce());
        _unclassifiedCheckBox.setSelected(_equipe.isNonClassee());
        _absentOrAbandonCheckbox.setSelected(_equipe.isAbsent());
        _categoryComboBox.setModel(new DefaultComboBoxModel<>(_presenter.getRaid().getCategories().getCategories()));
        if (_equipe.getCategorie() != null) {
            _categoryComboBox.setSelectedItem(_equipe.getCategorie());
        } else if (_categoryComboBox.getItemCount() > 0) {
            _categoryComboBox.setSelectedIndex(0);
        }
        _raidersTable.setModel(new RaiderTableModel(_equipe.getRaiders()));
    }

    public void showDialog() {
        setVisible(true);
    }

    private Raiders getEquipeRaiders() {
        return _equipe.getRaiders();
    }

    private void setTableRaiderModel( RaiderTableModel raiderTableModel ) {
        _raidersTable.setModel(raiderTableModel);
    }

    private void _onSelectRaidersTable() {
        if (_raidersTable.getSelectedRowCount() > 0) {
            _editButton.setEnabled(true);
            _removeButton.setEnabled(true);
        } else {
            _editButton.setEnabled(false);
            _removeButton.setEnabled(false);
        }
    }

    private void _initializeUI() {
        setModal(true);
        JPanel pane = new JPanel(new BorderLayout());
        pane.add(_createContentPanel(), BorderLayout.NORTH);
        pane.add(_createButtonPanel(), BorderLayout.SOUTH);
        setContentPane(pane);
        pack();
    }

    private JPanel _createContentPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        panel.add(_createIdentificationPanel());
        panel.add(_createEquipeStatePanel());

        JPanel categoryPanel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        categoryPanel.add(new JLabel(L10n.getString("equipe_editor.category")));
        categoryPanel.add(_createCategoryComboBox());
        panel.add(categoryPanel);

        panel.add(_createRaidersPanel());

        JPanel panelMessage = new JPanel(new BorderLayout());
        _errorLabel = new JLabel("");
        _errorLabel.setForeground(Color.red);
        _errorLabel.setPreferredSize(new Dimension(340, 55));
        panelMessage.add(_errorLabel, SwingConstants.CENTER);
        panel.add(panelMessage);
        return panel;
    }

    private JTextField _createNomTextField() {
        _nameTextField = new JTextField();
        return _nameTextField;
    }

    private JTextField _createDossardTextField() {
        _bibTextField = new JTextField();
        return _bibTextField;
    }

    private JTextField _createPuceTextField() {
        _chipTextField = new JTextField();
        return _chipTextField;
    }

    private JComboBox<Categorie> _createCategoryComboBox() {
        _categoryComboBox = new JComboBox<>();
        return _categoryComboBox;
    }

    private JPanel _createIdentificationPanel() {
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints c;

        c = new GridBagConstraints();
        c.gridy = 0;
        c.gridx = 0;
        c.anchor = GridBagConstraints.LINE_END;
        c.weightx = 0;
        c.insets = new Insets(0, 0, 0, 8);
        panel.add(new JLabel(L10n.getString("equipe_editor.name")), c);

        c = new GridBagConstraints();
        c.gridy = 0;
        c.gridx = 1;
        c.anchor = GridBagConstraints.LINE_START;
        c.weightx = 1;
        c.gridwidth = 3;
        c.fill = GridBagConstraints.HORIZONTAL;
        panel.add(_createNomTextField(), c);

        c = new GridBagConstraints();
        c.gridy = 1;
        c.gridx = 0;
        c.anchor = GridBagConstraints.LINE_END;
        c.weightx = 0;
        c.insets = new Insets(0, 0, 0, 8);
        panel.add(new JLabel(L10n.getString("equipe_editor.bib_number")), c);

        c = new GridBagConstraints();
        c.gridy = 1;
        c.gridx = 1;
        c.anchor = GridBagConstraints.LINE_START;
        c.weightx = 1;
        c.gridwidth = 3;
        c.fill = GridBagConstraints.HORIZONTAL;
        panel.add(_createDossardTextField(), c);

        c = new GridBagConstraints();
        c.gridy = 2;
        c.gridx = 0;
        c.anchor = GridBagConstraints.LINE_END;
        c.weightx = 0;
        c.insets = new Insets(0, 0, 0, 8);
        panel.add(new JLabel(L10n.getString("equipe_editor.chip_number")), c);

        c = new GridBagConstraints();
        c.gridy = 2;
        c.gridx = 1;
        c.anchor = GridBagConstraints.LINE_START;
        c.weightx = 1;
        c.gridwidth = 3;
        c.fill = GridBagConstraints.HORIZONTAL;
        panel.add(_createPuceTextField(), c);

        return panel;
    }

    private JPanel _createEquipeStatePanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.add(_createUnclassifiedCheckBox());
        panel.add(_createAbsentOrAbandonCheckbox());
        return panel;
    }

    private JCheckBox _createUnclassifiedCheckBox() {
        _unclassifiedCheckBox = new JCheckBox(L10n.getString("equipe_editor.unclassified.checkbox"));
        return _unclassifiedCheckBox;
    }

    private JPanel _createRaidersPanel() {
        JPanel panel = new JPanel();
        panel.setBorder(new TitledBorder(null, L10n.getString("equipe_editor.raiders.title"), TitledBorder.LEADING, TitledBorder.TOP));
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(_createRaiderButtonPanel());
        panel.add(_createRaidersScrollPane());
        return panel;
    }

    private JPanel _createRaiderButtonPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.add(_createAddButton());
        panel.add(_createEditButton());
        panel.add(_createRemoveButton());
        return panel;
    }

    private JButton _createAddButton() {
        JButton button = new IconButton("add.png");
        button.setToolTipText(L10n.getString("equipe_editor.add.button"));
        button.addActionListener(event -> {
            RaiderEditor editor = new RaiderEditor(new Raider(), true);
            editor.setLocationRelativeTo(this);
            Raider raider = editor.showDialog();
            if (raider != null) {
                Raiders raiders = getEquipeRaiders();
                raiders.addRaider(raider);
                setTableRaiderModel(new RaiderTableModel(raiders));
            }
        });
        return button;
    }

    private JButton _createEditButton() {
        _editButton = new IconButton("edit.png");
        _editButton.setToolTipText(L10n.getString("equipe_editor.edit.button"));
        _editButton.setEnabled(false);
        _editButton.addActionListener(event -> {
            Raider selectedRaider = _equipe.getRaiders().getRaider(
                    (String) _raidersTable.getValueAt(_raidersTable.getSelectedRow(), 0),
                    (String) _raidersTable.getValueAt(_raidersTable.getSelectedRow(), 1));
            RaiderEditor editor = new RaiderEditor(selectedRaider, false);
            editor.setLocationRelativeTo(this);
            Raider raider = editor.showDialog();
            if (raider != null) {
                Raiders raiders = getEquipeRaiders();
                setTableRaiderModel(new RaiderTableModel(raiders));
            }
        });
        return _editButton;
    }

    private JButton _createRemoveButton() {
        _removeButton = new IconButton("delete.png");
        _removeButton.setToolTipText(L10n.getString("equipe_editor.remove.button"));
        _removeButton.setEnabled(false);
        _removeButton.addActionListener(event -> {
            _equipe.getRaiders().removeRaider(
                    (String) _raidersTable.getValueAt(_raidersTable.getSelectedRow(), 0),
                    (String) _raidersTable.getValueAt(_raidersTable.getSelectedRow(), 1));
            _raidersTable.setModel(new RaiderTableModel(_equipe.getRaiders()));
            _onSelectRaidersTable();
        });
        return _removeButton;
    }

    private JScrollPane _createRaidersScrollPane() {
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setPreferredSize(new Dimension(200, 80));
        scrollPane.setViewportView(_createRaidersTable());
        return scrollPane;
    }

    private JTable _createRaidersTable() {
        _raidersTable = new JTable();
        _raidersTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        _raidersTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked( MouseEvent event ) {
                _onSelectRaidersTable();
            }
        });
        _raidersTable.getTableHeader().setReorderingAllowed(false);
        return _raidersTable;
    }

    private JCheckBox _createAbsentOrAbandonCheckbox() {
        _absentOrAbandonCheckbox = new JCheckBox(L10n.getString("equipe_editor.absent.checkbox"));
        return _absentOrAbandonCheckbox;
    }

    private JPanel _createButtonPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 5));
        if (_isCreation) {
            panel.add(_createValidateCreationButton());
        } else {
            panel.add(_createValidateEditionButton());
        }
        panel.add(_createCancelButton());
        return panel;
    }

    private JButton _createValidateCreationButton() {
        JButton button = new JButton(L10n.getString("equipe_editor.validate.button1"));
        getRootPane().setDefaultButton(button);
        button.addActionListener(event -> {
            if (_nameTextField.getText().trim().isEmpty()) {
                DesktopView.beep();
                _errorLabel.setText(L10n.getString("equipe_editor.error1"));
            } else {
                if (_presenter.createEquipe(
                        _equipe,
                        _nameTextField.getText(),
                        _bibTextField.getText(),
                        _chipTextField.getText(),
                        _unclassifiedCheckBox.isSelected(),
                        _absentOrAbandonCheckbox.isSelected(),
                        (Categorie) _categoryComboBox.getSelectedItem())) {
                    dispose();
                } else {
                    DesktopView.beep();
                    _errorLabel.setText(L10n.getString("equipe_editor.error2"));
                }
            }
        });
        return button;
    }

    private JButton _createValidateEditionButton() {
        JButton button = new JButton(L10n.getString("equipe_editor.validate.button2"));
        getRootPane().setDefaultButton(button);
        button.addActionListener(event -> {
            if (_nameTextField.getText().trim().isEmpty()) {
                DesktopView.beep();
                _errorLabel.setText(L10n.getString("equipe_editor.error1"));
            } else {
                if (_presenter.modifyEquipe(
                        _equipe,
                        _nameTextField.getText(),
                        _bibTextField.getText(),
                        _chipTextField.getText(),
                        _unclassifiedCheckBox.isSelected(),
                        _absentOrAbandonCheckbox.isSelected(),
                        (Categorie) _categoryComboBox.getSelectedItem()
                )) {
                    dispose();
                } else {
                    DesktopView.beep();
                    _errorLabel.setText(L10n.getString("equipe_editor.error3"));
                }
            }
        });
        return button;
    }

    private JButton _createCancelButton() {
        JButton button = new JButton(L10n.getString("cancel"));
        button.addActionListener(event -> dispose());
        return button;
    }
}
