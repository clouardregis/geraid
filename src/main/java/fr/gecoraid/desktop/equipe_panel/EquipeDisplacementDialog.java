package fr.gecoraid.desktop.equipe_panel;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DeskTopPresenter;
import fr.gecoraid.desktop.DesktopView;
import fr.gecoraid.model.Equipe;
import fr.gecoraid.model.Parcours;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.WindowConstants;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

class EquipeDisplacementDialog extends JDialog {
    private static final long serialVersionUID = 1L;

    private JLabel _errorLabel;
    private JComboBox<Parcours> _comboBox;
    private transient Parcours _destinationParcours;

    public EquipeDisplacementDialog( DeskTopPresenter presenter, Equipe equipe ) {
        _initializeUI();
        _comboBox.setModel(new DefaultComboBoxModel<>(presenter.getRaid().getParcourss().getParcourss()));
        if (presenter.getRaid().existeResultatPuce(equipe)) {
            DesktopView.beep();
            _errorLabel.setText(L10n.getString("equipe_displacement_dialog.error1"));
        }
    }

    public Parcours showDialog() {
        setVisible(true);
        return _destinationParcours;
    }

    private void _initializeUI() {
        setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        setModal(true);
        setTitle(L10n.getString("equipe_displacement_dialog.title"));
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        panel.add(_createContentPanel(), BorderLayout.NORTH);
        _errorLabel = new JLabel("");
        _errorLabel.setPreferredSize(new Dimension(215, 55));
        _errorLabel.setForeground(Color.red);
        panel.add(_errorLabel, BorderLayout.CENTER);
        panel.add(_createButtonPanel(), BorderLayout.SOUTH);
        setContentPane(panel);
        pack();
    }

    private JComponent _createContentPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        JPanel titlePanel = new JPanel(new FlowLayout());
        titlePanel.add(new JLabel(L10n.getString("equipe_displacement_dialog.label")));
        panel.add(titlePanel);
        JPanel equipeListPanel = new JPanel();
        equipeListPanel.add(_getEquipeComboBox());
        panel.add(equipeListPanel);
        return panel;
    }

    private JComboBox<Parcours> _getEquipeComboBox() {
        _comboBox = new JComboBox<>();
        return _comboBox;
    }

    private JPanel _createButtonPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 5));
        panel.add(_createValidateButton());
        panel.add(_createCancelButton());
        return panel;
    }

    private JButton _createValidateButton() {
        JButton button = new JButton(L10n.getString("equipe_displacement_dialog.validate_button"));
        button.addActionListener(event -> {
            _destinationParcours = (Parcours) _comboBox.getSelectedItem();
            dispose();
        });
        getRootPane().setDefaultButton(button);
        return button;
    }

    private JButton _createCancelButton() {
        JButton button = new JButton(L10n.getString("cancel"));
        button.addActionListener(event -> {
            _destinationParcours = null;
            dispose();
        });
        return button;
    }
}
