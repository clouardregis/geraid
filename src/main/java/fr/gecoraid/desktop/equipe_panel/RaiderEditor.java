package fr.gecoraid.desktop.equipe_panel;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DesktopView;
import fr.gecoraid.model.Raider;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;

class RaiderEditor extends JDialog {
    private static final long serialVersionUID = 1L;

    private final transient Raider _raider;
    private final boolean _creationFlag;
    private JTextField _lastnameTextField;
    private JTextField _firstnameTextField;
    private JLabel _errorLabel;
    private transient Raider _result;

    public RaiderEditor( Raider raider, boolean creationFlag ) {
        super();
        _raider = raider;
        _creationFlag = creationFlag;
        _initializeUI();
        if (_creationFlag) {
            setTitle(L10n.getString("raid_dialog.title1"));
        } else {
            setTitle(L10n.getString("raid_dialog.title2"));
        }
        _lastnameTextField.setText(_raider.getNom());
        _firstnameTextField.setText(_raider.getPrenom());
    }

    public Raider showDialog() {
        setVisible(true);
        return _result;
    }

    private void _initializeUI() {
        setModal(true);
        JPanel contentPane = new JPanel(new BorderLayout());
        contentPane.add(_createContentPanel(), BorderLayout.CENTER);
        contentPane.add(_createButtonPanel(), BorderLayout.SOUTH);
        setContentPane(contentPane);
        pack();
    }

    private JPanel _createContentPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        panel.add(_createIdentificationPanel());
        panel.add(_createErrorLabel());
        return panel;
    }

    private JLabel _createErrorLabel() {
        _errorLabel = new JLabel("");
        _errorLabel.setForeground(Color.red);
        _errorLabel.setPreferredSize(new Dimension(300, 25));
        _errorLabel.setHorizontalAlignment(SwingConstants.RIGHT);
        return _errorLabel;
    }

    private JPanel _createIdentificationPanel() {
        JPanel panel = new JPanel(new GridBagLayout());
        GridBagConstraints c;

        c = new GridBagConstraints();
        c.gridy = 0;
        c.gridx = 0;
        c.anchor = GridBagConstraints.LINE_END;
        c.weightx = 0;
        c.insets = new Insets(0, 0, 0, 8);
        panel.add(new JLabel(L10n.getString("lastname")), c);

        c = new GridBagConstraints();
        c.gridy = 0;
        c.gridx = 1;
        c.anchor = GridBagConstraints.LINE_START;
        c.weightx = 1;
        c.gridwidth = 3;
        c.fill = GridBagConstraints.HORIZONTAL;
        panel.add(_createLastnameTextField(), c);

        c = new GridBagConstraints();
        c.gridy = 1;
        c.gridx = 0;
        c.anchor = GridBagConstraints.LINE_END;
        c.weightx = 0;
        c.insets = new Insets(0, 0, 0, 8);
        panel.add(new JLabel(L10n.getString("firstname")), c);

        c = new GridBagConstraints();
        c.gridy = 1;
        c.gridx = 1;
        c.anchor = GridBagConstraints.LINE_START;
        c.weightx = 1;
        c.gridwidth = 3;
        c.fill = GridBagConstraints.HORIZONTAL;
        panel.add(_createFirstnameTextField(), c);
        return panel;
    }

    private JTextField _createLastnameTextField() {
        _lastnameTextField = new JTextField();
        return _lastnameTextField;
    }

    private JTextField _createFirstnameTextField() {
        _firstnameTextField = new JTextField();
        return _firstnameTextField;
    }

    private JPanel _createButtonPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 5));
        panel.add(_createValidateButton());
        panel.add(_createCancelButton());
        return panel;
    }

    private JButton _createValidateButton() {
        JButton button;
        if (_creationFlag) {
            button = new JButton(L10n.getString("raid_dialog.button.label1"));
        } else {
            button = new JButton(L10n.getString("raid_dialog.button.label2"));
        }
        button.addActionListener(event -> {
            if (_lastnameTextField.getText().trim().isEmpty() || _firstnameTextField.getText().trim().isEmpty()) {
                DesktopView.beep();
                _errorLabel.setText(L10n.getString("raid_dialog.error"));
            } else {
                _raider.setLastname(_lastnameTextField.getText());
                _raider.setFirstname(_firstnameTextField.getText());
                _result = _raider;
                dispose();
            }
        });
        getRootPane().setDefaultButton(button);
        return button;
    }

    private JButton _createCancelButton() {
        JButton button = new JButton(L10n.getString("cancel"));
        button.addActionListener(event -> {
            _result = null;
            dispose();
        });
        return button;
    }
}
