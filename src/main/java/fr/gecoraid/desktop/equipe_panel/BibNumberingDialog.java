package fr.gecoraid.desktop.equipe_panel;

import fr.gecoraid.L10n;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JTextField;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridLayout;

class BibNumberingDialog extends JDialog {
    private static final long serialVersionUID = 1L;

    private JSpinner _bibNumberSpinner;
    private JTextField _prefixTextField;
    private JTextField _suffixTextField;
    private transient Object[] _returnValues;

    public BibNumberingDialog() {
        _initializeUI();
    }

    public Object[] showDialog() {
        setVisible(true);
        return _returnValues;
    }

    private void _initializeUI() {
        setModal(true);
        setTitle(L10n.getString("bib_dialog.title"));
        JPanel contentPane = new JPanel(new BorderLayout());
        contentPane.add(_createContentPanel(), BorderLayout.NORTH);
        contentPane.add(_createButtonPanel(), BorderLayout.SOUTH);
        setContentPane(contentPane);
        pack();
    }

    private JPanel _createContentPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

        JPanel panel3 = new JPanel(new GridLayout(0, 2, 3, 3));
        JLabel label1 = new JLabel(L10n.getString("bib_dialog.prefix"));
        label1.setHorizontalAlignment(SwingConstants.RIGHT);
        panel3.add(label1);
        _prefixTextField = new JTextField(8);
        panel3.add(_prefixTextField);
        JLabel label2 = new JLabel(L10n.getString("bib_dialog.start_number"));
        label2.setHorizontalAlignment(SwingConstants.RIGHT);
        panel3.add(label2);
        _bibNumberSpinner = new JSpinner(new SpinnerNumberModel(1, 1, 10000, 1));
        panel3.add(_bibNumberSpinner);
        JLabel label3 = new JLabel(L10n.getString("bib_dialog.suffix"));
        label3.setHorizontalAlignment(SwingConstants.RIGHT);
        panel3.add(label3);
        _suffixTextField = new JTextField(8);
        panel3.add(_suffixTextField);
        panel.add(panel3);
        return panel;
    }

    private JPanel _createButtonPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 5));
        panel.add(_createValidateButton());
        panel.add(_createCancelButton());
        return panel;
    }

    private JButton _createValidateButton() {
        JButton button = new JButton(L10n.getString("bib_dialog.validate_button"));
        button.addActionListener(event -> {
            final String prefix = _prefixTextField.getText().trim();
            final int bibNumber = (int) _bibNumberSpinner.getValue();
            final String suffix = _suffixTextField.getText().trim();
            _returnValues = new Object[]{prefix, bibNumber, suffix};
            dispose();
        });
        getRootPane().setDefaultButton(button);
        return button;
    }

    private JButton _createCancelButton() {
        JButton button = new JButton(L10n.getString("cancel"));
        button.addActionListener(event -> {
            _returnValues = null;
            dispose();
        });
        return button;
    }
}
