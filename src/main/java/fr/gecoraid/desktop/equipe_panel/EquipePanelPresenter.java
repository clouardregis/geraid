package fr.gecoraid.desktop.equipe_panel;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DeskTopPresenter;
import fr.gecoraid.desktop.DesktopView;
import fr.gecoraid.desktop.ChipResultManagementDialog;
import fr.gecoraid.desktop.widget.ErrorPane;
import fr.gecoraid.model.CsvEquipes;
import fr.gecoraid.model.Equipe;
import fr.gecoraid.model.Equipes;
import fr.gecoraid.model.Parcours;
import fr.gecoraid.model.TypeTri;

import javax.swing.JList;
import javax.swing.JOptionPane;

class EquipePanelPresenter {
    private final DeskTopPresenter _deskTopPresenter;
    private final DesktopView _desktopView;
    private final EquipePanelView _equipePanelView;

    private final JList<Equipe> _equipeList;

    public EquipePanelPresenter( EquipePanelView equipePanelView,
                                 DeskTopPresenter deskTopPresenter,
                                 DesktopView desktopView,
                                 JList<Equipe> equipeList ) {
        _equipePanelView = equipePanelView;
        _deskTopPresenter = deskTopPresenter;
        _desktopView = desktopView;
        _equipeList = equipeList;
    }

    public String getRaidFolderPath() {
        return _deskTopPresenter.getRaidFolderPath();
    }

    public void importTeamListFromFile( String filename ) {
        CsvEquipes.importer(_deskTopPresenter.getRaid(), _getSelectedParcours(), filename);
        assert (_getSelectedParcours() != null);
        _equipeList.setListData(_getSelectedParcours().getEquipes().getEquipes());
        _equipeList.repaint();
        if (_equipeList.getModel().getSize() > 0) {
            _equipeList.setSelectedIndex(0);
        }
    }

    public void exportTeamListToFile( String filename ) {
        CsvEquipes.exportTeams(_getSelectedParcours().getEquipes(), filename);
    }

    public void numberAllBibs() {
        Equipes equipes = _getSelectedParcours().getEquipes();
        BibNumberingDialog dialog = new BibNumberingDialog();
        dialog.setLocationRelativeTo(null);
        Object[] returnedValues = dialog.showDialog();
        if (returnedValues != null) {
            equipes.numeroterEquipes(
                    (String) returnedValues[0],
                    (Integer) returnedValues[1],
                    (String) returnedValues[2]);
            _equipeList.repaint();
        }
    }

    public void addNewTeam() {
        if (_getSelectedParcours() != null) {
            EquipeEditor editor = new EquipeEditor(_deskTopPresenter,
                    new Equipe(_deskTopPresenter.getRaid()),
                    true);
            editor.setLocationRelativeTo(null);
            editor.showDialog();
            _equipePanelView.updateTeamTotal(_getSelectedParcours().getNombreEquipes());
        }
    }

    public void editCurrentSelectedTeam( Equipe equipe ) {
        EquipeEditor editor = new EquipeEditor(_deskTopPresenter,
                equipe,
                false);
        editor.setLocationRelativeTo(null);
        editor.showDialog();
    }

    public void deleteCurrentSelectedTeam( Equipe equipe ) {
        if (_getSelectedParcours() != null) {
            int reply = JOptionPane.showConfirmDialog(null,
                    L10n.getString("equipe_panel.delete.message", equipe.toString()),
                    L10n.getString("equipe_panel.delete.acknowledgment"),
                    JOptionPane.YES_NO_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
            if (reply == JOptionPane.YES_OPTION) {
                _deskTopPresenter.getRaid().removeEquipe(_getSelectedParcours().getEquipes(), equipe);
                _equipeList.repaint();
                if (_equipeList.getModel().getSize() == 0) {
                    _equipeList.setSelectedIndex(-1);
                } else {
                    _equipeList.setSelectedIndex(0);
                }
            }
            _equipePanelView.updateTeamTotal(_getSelectedParcours().getNombreEquipes());
        }
    }

    public void readSportIdentCard() {
        if (!_deskTopPresenter.getSportIdentReaderPuceEnCours()) {
            String port = _desktopView.getSelectedPort();
            if (port != null) {
                _deskTopPresenter.getSportIdentHandlerPuce().setPortName(port);
                if (_deskTopPresenter.getSportIdentHandlerPuce().start()) {
                    _equipePanelView.changeCardReaderIcon("wait.png");
                    _deskTopPresenter.setSportIdentReaderPuceEnCours(true);
                    _desktopView.setButtonSiReaderEnabled(false);
                } else {
                    ErrorPane.showMessageDialog(L10n.getString("equipe_panel.chip.error1"));
                }
            } else {
                ErrorPane.showMessageDialog(L10n.getString("equipe_panel.chip.error2"));
            }
        } else {
            _deskTopPresenter.getSportIdentHandlerPuce().stop();
            _equipePanelView.changeCardReaderIcon("play.png");
            _deskTopPresenter.setSportIdentReaderPuceEnCours(false);
            _desktopView.setButtonSiReaderEnabled(true);
        }
    }

    public void sortTeams() {
        TypeTri sortType = (TypeTri) JOptionPane.showInputDialog(null,
                L10n.getString("equipe_panel.sort.title"),
                L10n.getString("equipe_panel.sort.message"),
                JOptionPane.PLAIN_MESSAGE,
                null,
                TypeTri.values(),
                TypeTri.values()[0]);
        if (sortType != null) {
            _desktopView.setTypeTriCourant(sortType);
            _getSelectedParcours().getEquipes().trierEquipes(sortType);
            _equipeList.repaint();
        }
    }

    public void moveTeamToAnotherParcours( Equipe equipe ) {
        if (_equipeList.getSelectedIndex() > -1) {
            EquipeDisplacementDialog view = new EquipeDisplacementDialog(_deskTopPresenter, equipe);
            view.setLocationRelativeTo(null);
            Parcours targetParcours = view.showDialog();
            Parcours sourceParcours = _getSelectedParcours();
            if (targetParcours != null && !targetParcours.equals(sourceParcours)) {
                targetParcours.getEquipes().addEquipe(equipe);
                _deskTopPresenter.getRaid().removeResultatPuce(equipe);
                sourceParcours.getEquipes().removeEquipe(equipe);
                _equipeList.setListData(_getSelectedParcours().getEquipes().getEquipes());
                if (sourceParcours.getEquipes().getSize() > 0) {
                    _equipeList.setSelectedIndex(0);
                } else {
                    _equipeList.setSelectedIndex(-1);
                }
                _equipePanelView.updateTeamTotal(_getSelectedParcours().getNombreEquipes());
            }
        }
    }

    public void displaySportIdentCardResults( Equipe equipe ) {
        if (_deskTopPresenter.getRaid().getResultatsPuce().getSize() > 0) {
            ChipResultManagementDialog dialog = new ChipResultManagementDialog(
                    _deskTopPresenter,
                    _getSelectedParcours(),
                    _desktopView.getSelectedEtape(),
                    equipe);
            dialog.setLocationRelativeTo(null);
            dialog.showDialog();
        }
    }

    private Parcours _getSelectedParcours() {
        return _desktopView.getSelectedParcours();
    }
}
