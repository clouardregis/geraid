package fr.gecoraid.desktop.raid_menu;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DeskTopPresenter;
import fr.gecoraid.desktop.widget.IconButton;
import fr.gecoraid.desktop.PenaliteTableModel;
import fr.gecoraid.model.Etape;
import fr.gecoraid.model.Parcours;
import fr.gecoraid.model.Penalite;
import fr.gecoraid.model.PenaliteIndividuelle;
import fr.gecoraid.model.Raid;
import fr.gecoraid.tools.ExtensionFileFilter;
import fr.gecoraid.tools.Utils;

import javax.swing.BoxLayout;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableColumnModel;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

class PenaltiesManagementDialog extends JDialog {
    private static final long serialVersionUID = 6593087731537480601L;

    private final DeskTopPresenter _presenter;
    private final RaidMenuPresenter _raidMenuPresenter;

    private JTable _table;
    private JComboBox<Parcours> _parcoursComboBox;
    private JComboBox<Etape> _etapesComboBox;

    PenaltiesManagementDialog( RaidMenuPresenter raidMenuPresenter, DeskTopPresenter presenter ) {
        _presenter = presenter;
        _raidMenuPresenter = raidMenuPresenter;
        _initializeUI();
        _initialiseData();
    }

    void showDialog() {
        setVisible(true);
    }

    private void _initializeUI() {
        setModal(true);
        setIconImage(Toolkit.getDefaultToolkit().getImage(PenaltiesManagementDialog.class.getClassLoader().getResource("fr/gecoraid/icon/logo32.png")));
        setTitle(L10n.getString("penalty_management_dialog.title"));
        setMinimumSize(new Dimension(700, 400));
        JPanel contentPane = new JPanel();
        contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.Y_AXIS));
        contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
        contentPane.add(_createGlobalPanel());
        contentPane.add(_createIndividualPanel());
        contentPane.add(_createTeamListPanel());
        setContentPane(contentPane);
        pack();
    }

    private JPanel _createGlobalPanel() {
        final JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.setBorder(new TitledBorder(null, L10n.getString("penalty_management_dialog.global_panel.title"), TitledBorder.LEADING, TitledBorder.TOP));

        panel.add(new JLabel(L10n.getString("penalty_management_dialog.global_panel.label1")));
        panel.add(_createParcoursComboBox());

        panel.add(new JLabel(L10n.getString("penalty_management_dialog.global_panel.label2")));
        panel.add(_createEtapesComboBox());

        panel.add(_createAddPenaltyButton());
        panel.add(_createEditPenaltyButton());
        panel.add(_createDeletePenaltyButton());

        panel.add(_createImportPenaltyButton());
        panel.add(_createExportPenaltyButton());
        panel.add(_createExportTeamButton());

        return panel;
    }

    private JButton _createDeletePenaltyButton() {
        final JButton button = new IconButton("delete.png");
        button.setToolTipText(L10n.getString("penalty_management_dialog.global_panel.button_delete"));
        button.addActionListener(event -> {
            if (_table.getSelectedColumn() > 2) {
                final int index = _getSelectedColumnIndex();
                final Penalite penalite = _presenter.getRaid().getPenalite((Etape) _etapesComboBox.getSelectedItem(), index);
                final int anwser = JOptionPane.showConfirmDialog(null,
                        L10n.getString("penalty_management_dialog.global_panel.delete.message", penalite.getNom()),
                        L10n.getString("penalty_management_dialog.global_panel.delete.title"),
                        JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
                if (anwser == JOptionPane.YES_OPTION) {
                    _presenter.getRaid().getPenalites().getPenalites().remove(penalite);
                    _refreshTable();
                }
            }
        });
        return button;
    }

    private JButton _createEditPenaltyButton() {
        final JButton button = new IconButton("edit.png");
        button.setToolTipText(L10n.getString("penalty_management_dialog.global_panel.button_edit"));
        button.addActionListener(event -> {
            if (_table.getSelectedColumn() > 2) {
                final int index = _getSelectedColumnIndex();
                final Penalite penalty = _presenter.getRaid().getPenalite((Etape) _etapesComboBox.getSelectedItem(), index);
                final PenaltyEditor editor = new PenaltyEditor(_raidMenuPresenter, penalty, false);
                editor.setLocationRelativeTo(null);
                if (editor.showDialog()) {
                    _refreshTable();
                }
            }
        });
        return button;
    }

    private JButton _createExportTeamButton() {
        final JButton button = new IconButton("export-csv.png");
        button.setToolTipText(L10n.getString("penalty_management_dialog.global_panel.tooltip_export_team"));
        button.addActionListener(event -> {
            final JFileChooser chooser = new JFileChooser(_presenter.getRaidFolderPath());
            final ExtensionFileFilter filter = new ExtensionFileFilter("csv", L10n.getString("csv_file.description"));
            chooser.setFileFilter(filter);
            int returnValue = chooser.showSaveDialog(PenaltiesManagementDialog.this);
            if (returnValue == JFileChooser.APPROVE_OPTION) {
                final String filename = Utils.checkExtension(chooser.getSelectedFile().getAbsolutePath(), ".csv");
                CsvPenaltyHelper.exportTeamsWithPenalties((PenaliteTableModel) _table.getModel(), filename);
            }
        });
        return button;
    }

    private JButton _createAddPenaltyButton() {
        final JButton button = new IconButton("add.png");
        button.setToolTipText(L10n.getString("penalty_management_dialog.global_panel.button_add"));
        button.addActionListener(event -> {
            if (_etapesComboBox.getSelectedIndex() > -1) {
                final Penalite penalty = new Penalite();
                penalty.setParcours((Parcours) _parcoursComboBox.getSelectedItem());
                penalty.setEtape((Etape) _etapesComboBox.getSelectedItem());
                final PenaltyEditor editor = new PenaltyEditor(_raidMenuPresenter, penalty, true);
                editor.setLocationRelativeTo(null);
                if (editor.showDialog()) {
                    _refreshTable();
                }
            }
        });
        return button;
    }

    private JButton _createExportPenaltyButton() {
        final JButton btnExport = new IconButton("export-csv.png");
        btnExport.setToolTipText(L10n.getString("penalty_management_dialog.global_panel.tooltip_export_penalty"));
        btnExport.addActionListener(event -> {
            final JFileChooser chooser = new JFileChooser(_presenter.getRaidFolderPath());
            final ExtensionFileFilter filter = new ExtensionFileFilter("csv", L10n.getString("csv_file.description"));
            chooser.setFileFilter(filter);
            int returnVal = chooser.showSaveDialog(PenaltiesManagementDialog.this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                String filename = Utils.checkExtension(chooser.getSelectedFile().getAbsolutePath(), ".csv");
                CsvPenaltyHelper.exportPenalties((PenaliteTableModel) _table.getModel(), filename);
            }
        });
        return btnExport;
    }

    private JButton _createImportPenaltyButton() {
        final JButton button = new IconButton("import-csv.png");
        button.setToolTipText(L10n.getString("penalty_management_dialog.global_panel.tooltip_import_penalty"));
        button.addActionListener(event -> {
            if (_etapesComboBox.getSelectedIndex() > -1) {
                final JFileChooser chooser = new JFileChooser(_presenter.getRaidFolderPath());
                final ExtensionFileFilter filter = new ExtensionFileFilter("csv", L10n.getString("csv_file.description"));
                chooser.setFileFilter(filter);
                final int returnVal = chooser.showOpenDialog(PenaltiesManagementDialog.this);
                if (returnVal == JFileChooser.APPROVE_OPTION) {
                    final String filename = chooser.getSelectedFile().getAbsolutePath();
                    CsvPenaltyHelper.importAsCsv(_raidMenuPresenter, (Parcours) _parcoursComboBox.getSelectedItem(), (Etape) _etapesComboBox.getSelectedItem(), filename);
                    _refreshTable();
                }
            }
        });
        return button;
    }

    private JComboBox<Etape> _createEtapesComboBox() {
        _etapesComboBox = new JComboBox<>();
        _etapesComboBox.addItemListener(event -> _refreshTable());
        return _etapesComboBox;
    }

    private JComboBox<Parcours> _createParcoursComboBox() {
        _parcoursComboBox = new JComboBox<>();
        _parcoursComboBox.addItemListener(event -> {
            if (_parcoursComboBox.getSelectedIndex() != -1) {
                assert (_parcoursComboBox.getSelectedItem() != null);
                _etapesComboBox.setModel(new DefaultComboBoxModel<>(((Parcours) _parcoursComboBox.getSelectedItem()).getEtapes().getEtapes()));
                _etapesComboBox.repaint();
                if (_etapesComboBox.getItemCount() > 0) {
                    _etapesComboBox.setSelectedIndex(0);
                } else {
                    _etapesComboBox.setSelectedIndex(-1);
                }
            } else {
                _etapesComboBox.removeAllItems();
                _etapesComboBox.repaint();
                _etapesComboBox.setSelectedIndex(-1);
            }
            _refreshTable();
        });
        return _parcoursComboBox;
    }

    private JPanel _createIndividualPanel() {
        final JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.setBorder(new TitledBorder(null, L10n.getString("penalty_management_dialog.individual_panel.title"), TitledBorder.LEADING, TitledBorder.TOP));
        final JButton button = new IconButton("edit.png");
        button.setToolTipText(L10n.getString("penalty_management_dialog.individual_panel.edit.tooltip"));
        button.addActionListener(event -> _openIndividualPenaltyEditor());
        panel.add(button);
        return panel;
    }

    private JPanel _createTeamListPanel() {
        final JPanel panel = new JPanel();
        panel.setLayout(new BorderLayout());
        final JScrollPane scrollPane = new JScrollPane();
        _table = new JTable();
        _table.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked( MouseEvent event ) {
                if (event.getClickCount() == 2 && event.getButton() == MouseEvent.BUTTON1) {
                    _openIndividualPenaltyEditor();
                }
            }
        });
        _table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        _table.setRowSelectionAllowed(false);
        _table.setCellSelectionEnabled(true);
        scrollPane.setViewportView(_table);
        panel.add(scrollPane, BorderLayout.CENTER);
        return panel;
    }

    private void _openIndividualPenaltyEditor() {
        if (_table.getSelectedColumn() > 2 && ((String) _table.getValueAt(_table.getSelectedRow(), 2)).compareTo("") != 0) {
            boolean isACreation = ((String) _table.getValueAt(_table.getSelectedRow(), _table.getSelectedColumn())).isEmpty();
            final IndividualPenaltyEditor editor = new IndividualPenaltyEditor(_getSelecedIndividualPenalties(), isACreation);
            editor.setLocationRelativeTo(null);
            final List<PenaliteIndividuelle> individualPenalties = editor.showDialog();
            if (individualPenalties != null) {
                _addIndividualPenalties(individualPenalties);
                _refreshTable();
            }
        }
    }

    private void _initialiseData() {
        if (_presenter.getRaid().getParcourss().getSize() > 0) {
            _parcoursComboBox.setModel(new DefaultComboBoxModel<>(_presenter.getRaid().getParcourss().getParcourss()));
            assert (_parcoursComboBox.getSelectedItem() != null);
            _etapesComboBox.setModel(new DefaultComboBoxModel<>(((Parcours) _parcoursComboBox.getSelectedItem()).getEtapes().getEtapes()));
            _etapesComboBox.repaint();
            if (_etapesComboBox.getItemCount() > 0) {
                _etapesComboBox.setSelectedIndex(0);
            } else {
                _etapesComboBox.setSelectedIndex(-1);
            }
            _refreshTable();
        }
    }

    private void _addIndividualPenalties( List<PenaliteIndividuelle> individualPenalties ) {
        final Raid raid = _presenter.getRaid();
        for (final PenaliteIndividuelle p : individualPenalties) {
            raid.getPenalite((Etape) _etapesComboBox.getSelectedItem(), _getSelectedColumnIndex()).addPenaliteIndividuelle(p);
        }
    }

    private void _refreshTable() {
        _table.setModel(new PenaliteTableModel(_presenter.getRaid(), (Parcours) _parcoursComboBox.getSelectedItem(), (Etape) _etapesComboBox.getSelectedItem()));
        TableColumnModel tableModel = _table.getColumnModel();
        for (int i = 0; i < tableModel.getColumnCount(); i++) {
            tableModel.getColumn(i).setPreferredWidth(60);
        }
        tableModel.getColumn(0).setPreferredWidth(50);
        tableModel.getColumn(1).setPreferredWidth(150);
        tableModel.getColumn(2).setPreferredWidth(60);
    }

    private int _getSelectedColumnIndex() {
        int index = _table.getSelectedColumn() - 3;
        if (index % 2 == 1) {
            index = (index - 1) / 2;
        } else {
            index = index / 2;
        }
        return index;
    }

    private PenaliteIndividuelle[] _getSelecedIndividualPenalties() {
        final PenaliteIndividuelle[] selected = new PenaliteIndividuelle[_table.getSelectedRowCount()];
        final int[] selectedRowIndexes = _table.getSelectedRows();
        for (int i = 0; i < selectedRowIndexes.length; i++) {
            final PenaliteIndividuelle penalty = new PenaliteIndividuelle();
            penalty.setPuce((String) _table.getValueAt(selectedRowIndexes[i], 2));
            PenaliteIndividuelle selectedPenalty = _presenter.getRaid().getPenaliteIndividuelle((Etape) _etapesComboBox.getSelectedItem(), _getSelectedColumnIndex(), (String) _table.getValueAt(selectedRowIndexes[i], 2));
            if (selectedPenalty != null) {
                penalty.setPoint(selectedPenalty.getPoint());
                penalty.setTemps(selectedPenalty.getTemps());
            }
            selected[i] = penalty;
        }
        return selected;
    }
}
