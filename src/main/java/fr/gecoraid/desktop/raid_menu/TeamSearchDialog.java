package fr.gecoraid.desktop.raid_menu;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DeskTopPresenter;
import fr.gecoraid.model.Equipe;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

class TeamSearchDialog extends JDialog {
    private static final long serialVersionUID = 1L;
    private final transient DeskTopPresenter _presenter;

    private JTextField _nameField;
    private JList<Equipe> _teamList;
    private Equipe _result;

    TeamSearchDialog( DeskTopPresenter presenter ) {
        super();
        _presenter = presenter;
        _initializeUI();
        _teamList.setListData(_presenter.getRaid().getToutesLesEquipes().getEquipes());
    }

    Equipe showDialog() {
        setVisible(true);
        return _result;
    }

    private void _initializeUI() {
        setModal(true);
        setTitle(L10n.getString("team_search_dialog.title"));
        setContentPane(_createJContentPane());
        pack();
    }

    private JPanel _createJContentPane() {
        JPanel jContentPane = new JPanel(new BorderLayout());
        jContentPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        jContentPane.add(_createJPanelRecherche(), BorderLayout.NORTH);
        jContentPane.add(_getScrollPane(), BorderLayout.CENTER);
        jContentPane.add(_createButtonPanel(), BorderLayout.SOUTH);
        return jContentPane;
    }

    private JTextField _createNameTextField() {
        _nameField = new JTextField();
        _nameField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased( KeyEvent event ) {
                _teamList.setListData(_presenter.getRaid().getToutesLesEquipes(_nameField.getText().toUpperCase()).getEquipes());
                _teamList.repaint();
            }
        });
        return _nameField;
    }

    private JPanel _createJPanelRecherche() {
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(new JLabel(L10n.getString("team_search_dialog.label")), BorderLayout.WEST);
        panel.add(_createNameTextField(), BorderLayout.CENTER);
        return panel;
    }

    private JComponent _getScrollPane() {
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setPreferredSize(new Dimension(340, 250));
        scrollPane.setViewportView(_getTeamList());
        return scrollPane;
    }

    private JList<Equipe> _getTeamList() {
        _teamList = new JList<>();
        _teamList.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked( MouseEvent event ) {
                if (event.getClickCount() == 2) {
                    _selectTeam();
                }
            }
        });
        return _teamList;
    }

    private JPanel _createButtonPanel() {
        JPanel jPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 5));
        jPanel.add(_createValidateButton());
        jPanel.add(_createCancelButton());
        return jPanel;
    }

    private JButton _createValidateButton() {
        JButton buttonOk = new JButton(L10n.getString("team_search_dialog.button"));
        buttonOk.addActionListener(event -> _selectTeam());
        getRootPane().setDefaultButton(buttonOk);
        return buttonOk;
    }

    private void _selectTeam() {
        if (_teamList.getSelectedIndex() > -1) {
            _result = _teamList.getSelectedValue();
            dispose();
        }
    }

    private JButton _createCancelButton() {
        JButton button = new JButton(L10n.getString("cancel"));
        button.setHorizontalTextPosition(SwingConstants.CENTER);
        button.addActionListener(event -> {
            _result = null;
            dispose();
        });
        return button;
    }
}
