package fr.gecoraid.desktop.raid_menu;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.widget.ErrorPane;

import javax.swing.JOptionPane;
import java.awt.Desktop;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

class ParcoursHtmlHelper {
    private ParcoursHtmlHelper() {
    }

    static void save( String htmlCode, String htmlFileName ) {
        File path = new File(htmlFileName);
        try (Writer writer = new OutputStreamWriter(Files.newOutputStream(path.toPath()), StandardCharsets.UTF_8)) {
            writer.write(htmlCode, 0, htmlCode.length());
            writer.write(System.lineSeparator());
            if (Desktop.isDesktopSupported()) {
                final Desktop desktop = Desktop.getDesktop();
                if (desktop.isSupported(Desktop.Action.BROWSE)) {
                    try {
                        desktop.browse(path.toURI());
                        JOptionPane.showMessageDialog(null,
                                L10n.getString("parcours_html_helper.message"),
                                L10n.getString("information"),
                                JOptionPane.INFORMATION_MESSAGE);
                    } catch (IOException e) {
                        ErrorPane.showMessageDialog(L10n.getString("parcours_html_helper.error2", e.getClass().getName()));
                    }
                }
            } else {
                ErrorPane.showMessageDialog(L10n.getString("parcours_html_helper.error3", path.getAbsolutePath()));
            }
        } catch (IOException e) {
            ErrorPane.showMessageDialog(L10n.getString("parcours_html_helper.error1", e.getClass().getName(), e.getMessage()));
        }
    }
}