package fr.gecoraid.desktop.raid_menu;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DesktopView;
import fr.gecoraid.desktop.PenaliteTableModel;
import fr.gecoraid.desktop.widget.ErrorPane;
import fr.gecoraid.model.Etape;
import fr.gecoraid.model.Parcours;
import fr.gecoraid.model.Penalite;
import fr.gecoraid.model.PenaliteIndividuelle;
import fr.gecoraid.tools.Utils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

class CsvPenaltyHelper {
    private CsvPenaltyHelper() {
    }

    static void exportTeamsWithPenalties( PenaliteTableModel model, String filename ) {
        final File file = new File(filename);
        try (Writer writer = new OutputStreamWriter(Files.newOutputStream(file.toPath()), StandardCharsets.UTF_8)) {
            final String contents = model.getEntetesEquipesCSV();
            writer.write(contents, 0, contents.length());
            writer.write(System.lineSeparator());
            final String[] lines = model.getDataEquipesCSV();
            for (final String line : lines) {
                writer.write(line, 0, line.length());
                writer.write(System.lineSeparator());
            }
        } catch (IOException e) {
            DesktopView.beep();
            ErrorPane.showMessageDialog(L10n.getString("csv_penalty_helper.error1", e.getClass().getName(), e.getMessage()));
        }
    }

    static void exportPenalties( PenaliteTableModel model, String filename ) {
        final File file = new File(filename);
        try (Writer writer = new OutputStreamWriter(Files.newOutputStream(file.toPath()), StandardCharsets.UTF_8)) {
            final String contents = model.getEntetesCSV();
            writer.write(contents, 0, contents.length());
            writer.write(System.lineSeparator());
            final String[] lines = model.getDataCSV();
            for (String line : lines) {
                writer.write(line, 0, line.length());
                writer.write(System.lineSeparator());
            }
        } catch (IOException e) {
            DesktopView.beep();
            ErrorPane.showMessageDialog(L10n.getString("csv_penalty_helper.error2", e.getClass().getName(), e.getMessage()));
        }
    }

    static void importAsCsv( RaidMenuPresenter presenter, Parcours parcours, Etape etape, String filename ) {
        try {
            final File file = new File(filename);
            if (!file.exists()) {
                if (!file.createNewFile()) {
                    throw new IOException("Cannot create file");
                }
            }
            final BufferedReader fileReader = new BufferedReader(new FileReader(file));
            fileReader.readLine();
            final Penalite penalty = new Penalite();
            penalty.setNom(Utils.getNom(filename));
            penalty.setParcours(parcours);
            penalty.setEtape(etape);
            String line;
            while ((line = fileReader.readLine()) != null) {
                final String[] text = line.trim().split(";");
                if (text.length > 3) {
                    PenaliteIndividuelle individualPenalty = new PenaliteIndividuelle();
                    individualPenalty.setPuce(text[2]);
                    if (Utils.estUnEntier(text[3])) {
                        individualPenalty.setPoint(Integer.parseInt(text[3]));
                    }
                    if (text.length > 4) {
                        individualPenalty.setTemps(Utils.parseInt(text[4]));
                    }
                    penalty.getPenalites().add(individualPenalty);
                }
            }
            presenter.addPenalty(penalty);
            fileReader.close();
        } catch (IOException io) {
            DesktopView.beep();
            ErrorPane.showMessageDialog(L10n.getString("csv_penalty_helper.error3", io.getClass().getName(), io.getMessage()));
        }
    }
}
