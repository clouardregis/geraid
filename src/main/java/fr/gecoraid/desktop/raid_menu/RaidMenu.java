package fr.gecoraid.desktop.raid_menu;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DeskTopPresenter;
import fr.gecoraid.desktop.DesktopView;

import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JSeparator;

public class RaidMenu extends JMenu {
    private final transient RaidMenuPresenter _presenter;

    public RaidMenu( DesktopView desktopView, DeskTopPresenter presenter ) {
        super(L10n.getString("desktop.menu.raid.title"));
        _presenter = new RaidMenuPresenter(presenter, desktopView);
        _initializeUI();
    }

    private void _initializeUI() {
        add(_createMenuItemDisplayParcoursInBrowser());
        add(_createMenuSearch());
        add(_createMenuItemPenalty());
        add(_createMenuSportIdentResults());
        add(new JSeparator());
        add(_createMenuItemRaidConfiguration());
    }

    private JMenuItem _createMenuItemDisplayParcoursInBrowser() {
        final JMenuItem menuItem = new JMenuItem(L10n.getString("desktop.menu.raid.display_parcours"));
        menuItem.addActionListener(action -> _presenter.displayParcoursInBrowser());
        return menuItem;
    }

    private JMenu _createMenuSearch() {
        final JMenu menu = new JMenu(L10n.getString("desktop.menu.raid.search"));
        menu.add(_createMenuItemSearchTeamByChip());
        menu.add(_createMenuItemSearchTeamByName());
        return menu;
    }

    private JMenuItem _createMenuItemSearchTeamByChip() {
        final JMenuItem menuItem = new JMenuItem(L10n.getString("desktop.menu.raid.search.chip"));
        menuItem.addActionListener(event -> _presenter.searchTeamByChip());
        return menuItem;
    }

    private JMenuItem _createMenuItemSearchTeamByName() {
        final JMenuItem menuItem = new JMenuItem(L10n.getString("desktop.menu.raid.search.team"));
        menuItem.addActionListener(event -> _presenter.searchTeamByName());
        return menuItem;
    }

    private JMenuItem _createMenuItemPenalty() {
        final JMenuItem menuItem = new JMenuItem(L10n.getString("desktop.menu.raid.penalty"));
        menuItem.addActionListener(event -> _presenter.managePenalties());
        return menuItem;
    }

    private JMenu _createMenuSportIdentResults() {
        final JMenu menu = new JMenu(L10n.getString("desktop.menu.raid.sportident"));
        menu.add(_createMenuItemManageResults());
        menu.add(_createMenuItemExportResultToCsvFile());
        menu.add(_createMenuImportResultFromCsvFile());
        return menu;
    }

    private JMenuItem _createMenuItemManageResults() {
        final JMenuItem menuItem = new JMenuItem(L10n.getString("desktop.menu.sportindent.manage"));
        menuItem.addActionListener(event -> _presenter.manageChipResults());
        return menuItem;
    }

    private JMenuItem _createMenuItemExportResultToCsvFile() {
        final JMenuItem menuItem = new JMenuItem(L10n.getString("desktop.menu.sportindent.export"));
        menuItem.addActionListener(event -> _presenter.exportResultToCsvFile());
        return menuItem;
    }

    private JMenuItem _createMenuImportResultFromCsvFile() {
        final JMenuItem menuItem = new JMenuItem(L10n.getString("desktop.menu.sportindent.import"));
        menuItem.addActionListener(action -> _presenter.importResultFromCsvFile());
        return menuItem;
    }

    private JMenuItem _createMenuItemRaidConfiguration() {
        final JMenuItem menuItem = new JMenuItem(L10n.getString("desktop.menu.raid.settings"));
        menuItem.addActionListener(event -> _presenter.configureRaid());
        return menuItem;
    }
}
