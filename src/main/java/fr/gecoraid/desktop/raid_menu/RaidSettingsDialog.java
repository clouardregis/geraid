package fr.gecoraid.desktop.raid_menu;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.CategoryEditorDialog;
import fr.gecoraid.desktop.CategoryTableModel;
import fr.gecoraid.desktop.DesktopView;
import fr.gecoraid.desktop.widget.IconButton;
import fr.gecoraid.model.Categorie;
import fr.gecoraid.model.Categories;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

class RaidSettingsDialog extends JDialog {
    private static final long serialVersionUID = 102L;

    private final RaidMenuPresenter _presenter;
    private final transient Categories _categories = new Categories();

    private JTextField _headerTextField;
    private JTextField _footerTextField;
    private JTable _categoryTable;
    private JTextField _raidNameTextField;
    private JButton _editButton;
    private JButton _removeButton;
    private JButton _validateButton;
    private boolean _result;

    RaidSettingsDialog( DesktopView parent, RaidMenuPresenter presenter ) {
        super(parent);
        _presenter = presenter;
        _initializeUI();
        _initializeData();
    }

    boolean showDialog() {
        setVisible(true);
        return _result;
    }

    private void _initializeData() {
        _raidNameTextField.setText(_presenter.getRaidName());
        _raidNameTextField.setCaretPosition(0);
        Categories categories = _presenter.getCategories();
        for (int i = 0; i < categories.getSize(); i++) {
            _categories.addCategorie(categories.getCategories().get(i));
        }
        _categoryTable.setModel(new CategoryTableModel(_categories));
        _headerTextField.setText(_presenter.getPageHeader());
        _headerTextField.setCaretPosition(0);
        _footerTextField.setText(_presenter.getPageFooter());
        _footerTextField.setCaretPosition(0);
        _validateButton.setEnabled(false);
    }

    private void _updateCategoriesTable() {
        if (_categoryTable.getSelectedRowCount() > 0) {
            _editButton.setEnabled(true);
            _removeButton.setEnabled(true);
        } else {
            _editButton.setEnabled(false);
            _removeButton.setEnabled(false);
        }
    }

    private void _initializeUI() {
        setSize(400, 500);
        setModal(true);
        setTitle(L10n.getString("raid_settings.title"));
        JPanel pane = new JPanel(new BorderLayout());
        pane.add(_createContentPanel(), BorderLayout.CENTER);
        pane.add(_createButtonPanel(), BorderLayout.SOUTH);
        setContentPane(pane);
        pack();
    }

    private JPanel _createContentPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(new EmptyBorder(10, 10, 10, 10));
        panel.add(_createRaidNamePanel());
        panel.add(_createRaidCategoriesPanel());
        panel.add(_createPrintPanel());
        return panel;
    }

    private JPanel _createRaidNamePanel() {
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(new JLabel(L10n.getString("raid_settings.name")), BorderLayout.WEST);
        _raidNameTextField = new JTextField();
        _raidNameTextField.getDocument().addDocumentListener(new DocumentListener() {
            public void changedUpdate( DocumentEvent event ) {
            }

            public void removeUpdate( DocumentEvent event ) {
                setModified();
            }

            public void insertUpdate( DocumentEvent event ) {
                setModified();
            }
        });
        panel.add(_raidNameTextField, BorderLayout.CENTER);
        panel.setBorder(new EmptyBorder(5, 5, 5, 5));
        return panel;
    }

    private JPanel _createRaidCategoriesPanel() {
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(new TitledBorder(null, L10n.getString("raid_settings.category.title"), TitledBorder.LEADING, TitledBorder.TOP));
        panel.add(_createCategoryButtonPanel());
        panel.add(_createCategoryList());
        return panel;
    }

    private JPanel _createCategoryButtonPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.add(_createAddButton());
        panel.add(_createEditButton());
        panel.add(_createRemoveButton());
        return panel;
    }

    private JScrollPane _createCategoryList() {
        _categoryTable = new JTable();
        _categoryTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        _categoryTable.setSize(new Dimension(292, 120));
        _categoryTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked( MouseEvent event ) {
                _updateCategoriesTable();
            }
        });
        _categoryTable.getTableHeader().setReorderingAllowed(false);
        JScrollPane scrollPane = new JScrollPane();
        scrollPane.setPreferredSize(new Dimension(295, 125));
        scrollPane.setViewportView(_categoryTable);
        return scrollPane;
    }

    private JButton _createAddButton() {
        final JButton button = new IconButton("add.png");
        button.setToolTipText(L10n.getString("raid_settings.category.add"));
        button.addActionListener(event -> {
            CategoryEditorDialog editor = new CategoryEditorDialog(this,
                    new Categorie("", ""),
                    _categories,
                    ( Categories c ) -> {
                        _categoryTable.setModel(new CategoryTableModel(c));
                        int lastRow = _categoryTable.getRowCount() - 1;
                        _categoryTable.setRowSelectionInterval(lastRow, lastRow);
                        _updateCategoriesTable();
                        setModified();
                    },
                    true);
            editor.setLocationRelativeTo(this);
            editor.showDialog();
        });
        return button;
    }

    private JButton _createEditButton() {
        _editButton = new IconButton("edit.png");
        _editButton.setEnabled(false);
        _editButton.setToolTipText(L10n.getString("raid_settings.category.modify"));
        _editButton.addActionListener(event -> {
            Categorie category = _categories.getCategorie((String) _categoryTable.getModel().getValueAt(_categoryTable.getSelectedRow(), 1));
            CategoryEditorDialog view = new CategoryEditorDialog(this, category,
                    _categories,
                    ( Categories c ) -> {
                        _categoryTable.setModel(new CategoryTableModel(c));
                        setModified();
                    },
                    false);
            view.setLocationRelativeTo(this);
            view.showDialog();
        });
        return _editButton;
    }

    private JButton _createRemoveButton() {
        _removeButton = new IconButton("delete.png");
        _removeButton.setEnabled(false);
        _removeButton.setToolTipText(L10n.getString("raid_settings.category.delete"));
        _removeButton.addActionListener(event -> {
            if (_categoryTable.getSelectedRow() >= 0) {
                Object category = _categoryTable.getModel().getValueAt(_categoryTable.getSelectedRow(), 0);
                _categories.removeCategorie((String) category);
                _categoryTable.setModel(new CategoryTableModel(_categories));
                setModified();
                _updateCategoriesTable();
            }
        });
        return _removeButton;
    }

    private JPanel _createPrintPanel() {
        JPanel panel = new JPanel(new GridBagLayout());
        panel.setBorder(new TitledBorder(null, L10n.getString("raid_settings.print.title"), TitledBorder.LEADING, TitledBorder.TOP));
        GridBagConstraints c;

        c = new GridBagConstraints();
        c.gridy = 0;
        c.gridx = 0;
        c.anchor = GridBagConstraints.LINE_END;
        c.weightx = 0;
        panel.add(new JLabel(L10n.getString("raid_settings.print.header")), c);
        c = new GridBagConstraints();
        c.gridy = 0;
        c.gridx = 1;
        c.anchor = GridBagConstraints.LINE_START;
        c.weightx = 1;
        c.gridwidth = 3;
        c.fill = GridBagConstraints.HORIZONTAL;
        panel.add(_createHeaderTexField(), c);

        c = new GridBagConstraints();
        c.gridy = 1;
        c.gridx = 0;
        c.anchor = GridBagConstraints.LINE_END;
        c.weightx = 0;
        panel.add(new JLabel(L10n.getString("raid_settings.print.footer")), c);
        c = new GridBagConstraints();
        c.gridy = 1;
        c.gridx = 1;
        c.anchor = GridBagConstraints.LINE_START;
        c.weightx = 1;
        c.gridwidth = 3;
        c.fill = GridBagConstraints.HORIZONTAL;
        panel.add(_createFooterTextField(), c);
        return panel;
    }

    private JTextField _createHeaderTexField() {
        _headerTextField = new JTextField();
        _headerTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void changedUpdate( DocumentEvent event ) {
            }

            @Override
            public void removeUpdate( DocumentEvent event ) {
                setModified();
            }

            @Override
            public void insertUpdate( DocumentEvent event ) {
                setModified();
            }
        });
        return _headerTextField;
    }

    private JTextField _createFooterTextField() {
        _footerTextField = new JTextField();
        _footerTextField.getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void changedUpdate( DocumentEvent event ) {
            }

            @Override
            public void removeUpdate( DocumentEvent event ) {
                setModified();
            }

            @Override
            public void insertUpdate( DocumentEvent event ) {
                setModified();
            }
        });
        return _footerTextField;
    }

    private JPanel _createButtonPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 50, 5));
        panel.add(_createValidateButton());
        panel.add(_createCancelButton());
        return panel;
    }

    private JButton _createValidateButton() {
        _validateButton = new JButton(L10n.getString("validate"));
        _validateButton.addActionListener(event -> {
            _presenter.setRaidName(_raidNameTextField.getText().trim());
            _presenter.setPageHeader(_headerTextField.getText().trim());
            _presenter.setPageFooter(_footerTextField.getText().trim());
            _presenter.setCategories(_categories);
            _result = true;
            dispose();
        });
        getRootPane().setDefaultButton(_validateButton);
        return _validateButton;
    }

    private JButton _createCancelButton() {
        JButton button = new JButton(L10n.getString("cancel"));
        button.addActionListener(event -> {
            _result = false;
            dispose();
        });
        return button;
    }

    private void setModified() {
        _validateButton.setEnabled(true);
    }
}
