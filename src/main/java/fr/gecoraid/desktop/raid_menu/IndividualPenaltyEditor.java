package fr.gecoraid.desktop.raid_menu;

import fr.gecoraid.L10n;
import fr.gecoraid.model.PenaliteIndividuelle;

import javax.swing.BorderFactory;
import javax.swing.BoxLayout;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.SpinnerNumberModel;
import javax.swing.SwingConstants;
import javax.swing.border.TitledBorder;
import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.util.ArrayList;
import java.util.List;

class IndividualPenaltyEditor extends JDialog {
    private static final long serialVersionUID = 1L;

    private final transient PenaliteIndividuelle[] _penalties;
    private final boolean _creation;

    private JRadioButton _bonusRadioButton;
    private JRadioButton _malusRadioButton;
    private JSpinner _spinnerPoints;
    private JSpinner _hourSpinner;
    private JSpinner _minuteSpinner;
    private JSpinner _secondSpinner;
    private List<PenaliteIndividuelle> _result;

    IndividualPenaltyEditor( PenaliteIndividuelle[] penalties, boolean creation ) {
        _penalties = penalties;
        _creation = creation;
        _initializeUI();
        _initPoints();
        _initTemps();
    }

    public List<PenaliteIndividuelle> showDialog() {
        setVisible(true);
        return _result;
    }

    private void _initializeUI() {
        setTitle(L10n.getString("individual_penalty_editor.title"));
        setModal(true);
        final JPanel contentPane = new JPanel(new BorderLayout());
        contentPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        contentPane.add(_createContentPanel(), BorderLayout.CENTER);
        contentPane.add(_createButtonPanel(), BorderLayout.SOUTH);
        setContentPane(contentPane);
        pack();
    }

    private JPanel _createContentPanel() {
        final JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(_createTypePanel());
        panel.add(_createPointPanel());
        panel.add(_createTimePanel());
        return panel;
    }

    private JPanel _createButtonPanel() {
        final JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 5));
        panel.add(_createValidateButton());
        panel.add(_createCancelButton());
        return panel;
    }

    private void _initPoints() {
        if (_penalties[0].getPoint() == 0 && _penalties[0].getTemps() < 0) {
            _bonusRadioButton.setSelected(true);
        }
        if (_penalties[0].getPoint() > 0 && _penalties[0].getTemps() <= 0) {
            _bonusRadioButton.setSelected(true);
        }
        _spinnerPoints.setValue(Math.abs(_penalties[0].getPoint()));
    }

    private void _initTemps() {
        final int hours = (int) Math.floor(Math.abs(_penalties[0].getTemps()) / 3600D);
        final int minutes = (int) Math.floor((Math.abs(_penalties[0].getTemps()) - hours * 3600D) / 60D);
        final int seconds = Math.abs(_penalties[0].getTemps()) - hours * 3600 - minutes * 60;
        _hourSpinner.setValue(hours);
        _minuteSpinner.setValue(minutes);
        _secondSpinner.setValue(seconds);
    }

    private JButton _createValidateButton() {
        final JButton button;
        if (_creation) {
            button = new JButton(L10n.getString("individual_penalty_editor.button1"));
        } else {
            button = new JButton(L10n.getString("individual_penalty_editor.button2"));
        }
        getRootPane().setDefaultButton(button);
        button.addActionListener(event -> {
            List<PenaliteIndividuelle> penalties = new ArrayList<>();
            for (PenaliteIndividuelle penalty : _penalties) {
                if (_bonusRadioButton.isSelected()) {
                    penalty.setPoint((int) _spinnerPoints.getValue());
                    if (((int) _hourSpinner.getValue() + (int) _minuteSpinner.getValue() + (int) _secondSpinner.getValue()) != 0) {
                        penalty.setTemps(((int) (_hourSpinner.getValue()) * 3600 + (int) (_minuteSpinner.getValue()) * 60 + (int) (_secondSpinner.getValue())) * (-1));
                    } else {
                        penalty.setTemps(0);
                    }
                } else {
                    if ((int) _spinnerPoints.getValue() != 0) {
                        penalty.setPoint((int) _spinnerPoints.getValue() * (-1));
                    } else {
                        penalty.setPoint(0);
                    }
                    penalty.setTemps(((int) (_hourSpinner.getValue()) * 3600 + (int) (_minuteSpinner.getValue()) * 60 + (int) (_secondSpinner.getValue())));
                }
                penalties.add(penalty);
            }
            _result = penalties;
            dispose();
        });
        return button;
    }

    private JPanel _createTypePanel() {
        final JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.setBorder(new TitledBorder(null, L10n.getString("individual_penalty_editor.form.title1"), TitledBorder.LEADING, TitledBorder.TOP));
        panel.add(_createBonusRadioButton());
        panel.add(_createMalusRadioButton());
        ButtonGroup bg = new ButtonGroup();
        bg.add(_bonusRadioButton);
        bg.add(_malusRadioButton);
        return panel;
    }

    private JRadioButton _createBonusRadioButton() {
        _bonusRadioButton = new JRadioButton(L10n.getString("individual_penalty_editor.form.bonus"));
        return _bonusRadioButton;
    }

    private JRadioButton _createMalusRadioButton() {
        _malusRadioButton = new JRadioButton(L10n.getString("individual_penalty_editor.form.penalty"));
        _malusRadioButton.setSelected(true);
        return _malusRadioButton;
    }

    private JPanel _createPointPanel() {
        final JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.setBorder(new TitledBorder(null, L10n.getString("individual_penalty_editor.form.title2"), TitledBorder.LEADING, TitledBorder.TOP));
        panel.add(_createPointSpinner());
        panel.add(new JLabel(L10n.getString("individual_penalty_editor.form.points")));
        return panel;
    }

    private JSpinner _createPointSpinner() {
        _spinnerPoints = new JSpinner();
        _spinnerPoints.setModel(new SpinnerNumberModel(0, 0, 100, 10));
        return _spinnerPoints;
    }

    private JPanel _createTimePanel() {
        final JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        panel.setBorder(new TitledBorder(null, L10n.getString("individual_penalty_editor.form.title3"), TitledBorder.LEADING, TitledBorder.TOP));
        panel.add(_createHourSpinner());
        panel.add(new JLabel(L10n.getString("hour_abbreviation")));
        panel.add(_createMinuteSpinner());
        panel.add(new JLabel(L10n.getString("minute_abbreviation")));
        panel.add(_createSecondSpinner());
        panel.add(new JLabel(L10n.getString("second_abbreviation")));
        return panel;
    }

    private JSpinner _createHourSpinner() {
        _hourSpinner = new JSpinner();
        _hourSpinner.setModel(new SpinnerNumberModel(0, 0, 23, 1));
        return _hourSpinner;
    }

    private JSpinner _createMinuteSpinner() {
        _minuteSpinner = new JSpinner();
        _minuteSpinner.setModel(new SpinnerNumberModel(0, 0, 59, 1));
        return _minuteSpinner;
    }

    private JSpinner _createSecondSpinner() {
        _secondSpinner = new JSpinner();
        _secondSpinner.setModel(new SpinnerNumberModel(0, 0, 59, 1));
        return _secondSpinner;
    }

    private JButton _createCancelButton() {
        final JButton button = new JButton(L10n.getString("cancel"));
        button.setHorizontalTextPosition(SwingConstants.CENTER);
        button.addActionListener(event -> {
            _result = null;
            dispose();
        });
        return button;
    }
}
