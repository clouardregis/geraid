package fr.gecoraid.desktop.raid_menu;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DeskTopPresenter;
import fr.gecoraid.desktop.DesktopView;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

class ChipSearchDialog extends JDialog {
    private static final long serialVersionUID = 1L;

    private final transient DeskTopPresenter _presenter;

    private JTextField _numberField;
    private JLabel _errorLabel;
    private String _result;

    ChipSearchDialog( DeskTopPresenter presenter ) {
        _presenter = presenter;
        _initializeUI();
    }

    String showDialog() {
        setVisible(true);
        return _result;
    }

    private void _initializeUI() {
        setModal(true);
        setTitle(L10n.getString("chip_search_dialog.title"));
        JPanel panel = new JPanel(new BorderLayout());
        panel.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        panel.add(_createSearchPanel(), BorderLayout.NORTH);
        panel.add(_getErrorLabel(), BorderLayout.CENTER);
        panel.add(_createButtonPanel(), BorderLayout.SOUTH);
        setContentPane(panel);
        pack();
    }

    private JPanel _createSearchPanel() {
        JPanel panel = new JPanel(new BorderLayout());
        panel.add(new JLabel(L10n.getString("chip_search_dialog.number")), BorderLayout.WEST);
        panel.add(_createTextFieldNumber(), BorderLayout.CENTER);
        return panel;
    }

    private JComponent _getErrorLabel() {
        _errorLabel = new JLabel("");
        _errorLabel.setForeground(Color.red);
        _errorLabel.setPreferredSize(new Dimension(300, 30));
        return _errorLabel;
    }

    private JTextField _createTextFieldNumber() {
        _numberField = new JTextField();
        return _numberField;
    }

    private JPanel _createButtonPanel() {
        JPanel jPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 5));
        jPanel.add(_createValidateButton());
        jPanel.add(_createCancelButton());
        return jPanel;
    }

    private JButton _createValidateButton() {
        JButton button = new JButton(L10n.getString("chip_search_dialog.button"));
        button.addActionListener(event -> {
            final String chipNumber = _numberField.getText().trim();
            if (chipNumber.isEmpty()) {
                DesktopView.beep();
                _errorLabel.setText(L10n.getString("chip_search_dialog.error1"));
            } else if (_presenter.existChip(chipNumber)) {
                _result = chipNumber;
                dispose();
            } else {
                DesktopView.beep();
                _errorLabel.setText(L10n.getString("chip_search_dialog.error2"));
            }
        });
        getRootPane().setDefaultButton(button);
        return button;
    }

    private JButton _createCancelButton() {
        JButton button = new JButton(L10n.getString("cancel"));
        button.setHorizontalTextPosition(SwingConstants.CENTER);
        button.addActionListener(event -> {
            _result = null;
            dispose();
        });
        return button;
    }
}
