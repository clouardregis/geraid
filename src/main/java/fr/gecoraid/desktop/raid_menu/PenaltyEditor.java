package fr.gecoraid.desktop.raid_menu;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DesktopView;
import fr.gecoraid.model.Penalite;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.FlowLayout;

class PenaltyEditor extends JDialog {
    private static final long serialVersionUID = 1L;

    private final RaidMenuPresenter _presenter;
    private final transient Penalite _penalite;

    private final boolean _creation;
    private JTextField _textFieldNom;
    private JLabel _labelMessage;
    private boolean _result;

    PenaltyEditor( RaidMenuPresenter presenter, Penalite penalty, boolean creation ) {
        _presenter = presenter;
        _penalite = penalty;
        _creation = creation;
        _initializeUI();
        if (_creation) {
            setTitle(L10n.getString("penalty_editor.title1"));
        } else {
            setTitle(L10n.getString("penalty_editor.title2"));
        }
        _textFieldNom.setText(_penalite.getNom());
    }

    public boolean showDialog() {
        setVisible(true);
        return _result;
    }

    private void _initializeUI() {
        setModal(true);
        setResizable(false);
        JPanel jContentPane = new JPanel(new BorderLayout());
        jContentPane.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        jContentPane.add(_createJContentPane(), BorderLayout.NORTH);
        jContentPane.add(_getLabelMessage(), BorderLayout.CENTER);
        jContentPane.add(_createButtonPanel(), BorderLayout.SOUTH);
        setContentPane(jContentPane);
        pack();
    }

    private JPanel _createJContentPane() {
        JPanel jContentPane = new JPanel();
        JLabel jLabel = new JLabel();
        jLabel.setText(L10n.getString("penalty_editor.label"));
        jContentPane.add(jLabel);
        jContentPane.add(_createJTextFieldNom());
        return jContentPane;
    }

    private JComponent _getLabelMessage() {
        _labelMessage = new JLabel();
        _labelMessage.setText("");
        _labelMessage.setForeground(Color.red);
        _labelMessage.setPreferredSize(new Dimension(215, 55));
        return _labelMessage;
    }

    private JPanel _createButtonPanel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.CENTER, 10, 5));
        panel.add(_createValidateButton());
        panel.add(_createCancelButton());
        return panel;
    }

    private JTextField _createJTextFieldNom() {
        _textFieldNom = new JTextField(20);
        return _textFieldNom;
    }

    private JButton _createValidateButton() {
        final JButton buttonOk;
        if (_creation) {
            buttonOk = new JButton(L10n.getString("penalty_editor.button1"));
        } else {
            buttonOk = new JButton(L10n.getString("penalty_editor.button2"));
        }
        getRootPane().setDefaultButton(buttonOk);
        buttonOk.addActionListener(event -> {
            if (_textFieldNom.getText().trim().isEmpty()) {
                DesktopView.beep();
                _labelMessage.setText(L10n.getString("penalty_editor.error"));
            } else if (_creation) {
                _penalite.setNom(_textFieldNom.getText());
                _presenter.addPenalty(_penalite);
                _result = true;
                dispose();
            } else {
                _penalite.setNom(_textFieldNom.getText());
                _result = true;
                dispose();
            }
        });
        return buttonOk;
    }

    private JButton _createCancelButton() {
        JButton button = new JButton(L10n.getString("cancel"));
        button.setHorizontalTextPosition(SwingConstants.CENTER);
        button.addActionListener(event -> {
            _result = false;
            dispose();
        });
        return button;
    }
}
