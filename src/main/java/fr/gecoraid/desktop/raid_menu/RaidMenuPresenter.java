package fr.gecoraid.desktop.raid_menu;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.ChipResultManagementDialog;
import fr.gecoraid.desktop.CsvResultatPuce;
import fr.gecoraid.desktop.DeskTopPresenter;
import fr.gecoraid.desktop.DesktopView;
import fr.gecoraid.model.Categories;
import fr.gecoraid.model.Equipe;
import fr.gecoraid.model.Parcours;
import fr.gecoraid.model.Penalite;
import fr.gecoraid.tools.ExtensionFileFilter;
import fr.gecoraid.tools.Utils;

import javax.swing.JComboBox;
import javax.swing.JFileChooser;

class RaidMenuPresenter {
    final DeskTopPresenter _parent;
    final DesktopView _desktopView;

    RaidMenuPresenter( DeskTopPresenter parent, DesktopView desktopView ) {
        _parent = parent;
        _desktopView = desktopView;
    }

    void addPenalty( Penalite penalty ) {
        _parent.getRaid().getPenalites().getPenalites().add(penalty);
    }

    void displayParcoursInBrowser() {
        if (_parent.getRaid().getParcourss().getSize() > 0) {
            ParcoursHtmlHelper.save(_parent.getRaid().exportParcoursAsHTML(), "parcours.html");
        }
    }

    void searchTeamByChip() {
        final ChipSearchDialog dialog = new ChipSearchDialog(_parent);
        dialog.setLocationRelativeTo(_desktopView);
        final String chipNumber = dialog.showDialog();
        if (chipNumber != null) {
            final Parcours parcours = _parent.getRaid().getParcoursIdPuce(chipNumber);
            final JComboBox<Parcours> combo = _desktopView.getComboBoxParcours();
            combo.setSelectedItem(parcours);
            _desktopView.getListEquipes().setSelectedValue(parcours.getEquipes().getEquipeIdPuce(chipNumber), true);
        }
    }

    void searchTeamByName() {
        final TeamSearchDialog dialog = new TeamSearchDialog(_parent);
        dialog.setLocationRelativeTo(_desktopView);
        final Equipe team = dialog.showDialog();
        if (team != null) {
            final Parcours parcours = _parent.getRaid().getParcourss().getParcours(team);
            final JComboBox<Parcours> combo = _parent.getComboBoxParcours();
            combo.setSelectedItem(parcours);
            _parent.getListEquipes().setSelectedValue(team, true);
        }
    }

    void managePenalties() {
        PenaltiesManagementDialog dialog = new PenaltiesManagementDialog(this, _parent);
        dialog.setLocationRelativeTo(_desktopView);
        dialog.showDialog();
    }

    void configureRaid() {
        final RaidSettingsDialog dialog = new RaidSettingsDialog(_desktopView, this);
        dialog.setLocationRelativeTo(_desktopView);
        if (dialog.showDialog()) {
            _desktopView.updateListEquipe();
            _desktopView.updateFrameTitle();
        }
    }

    void manageChipResults() {
        if (_parent.getRaid().getResultatsPuce().getSize() > 0) {
            final ChipResultManagementDialog dialog = new ChipResultManagementDialog(_desktopView, _parent);
            dialog.setLocationRelativeTo(_desktopView);
            dialog.showDialog();
        }
    }

    void exportResultToCsvFile() {
        final JFileChooser chooser = new JFileChooser(_parent.getRaidFolderPath());
        final ExtensionFileFilter filter = new ExtensionFileFilter("csv", L10n.getString("csv_file.description"));
        chooser.setFileFilter(filter);
        final int returnValue = chooser.showSaveDialog(_desktopView);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            final String filename = Utils.checkExtension(chooser.getSelectedFile().getAbsolutePath(), ".csv");
            CsvResultatPuce.exportToFile(_parent.getRaid().getResultatsPuce(), filename);
        }
    }

    void importResultFromCsvFile() {
        final JFileChooser chooser = new JFileChooser(_parent.getRaidFolderPath());
        final ExtensionFileFilter filter = new ExtensionFileFilter("csv", L10n.getString("csv_file.description"));
        chooser.setFileFilter(filter);
        final int returnValue = chooser.showOpenDialog(_desktopView);
        if (returnValue == JFileChooser.APPROVE_OPTION) {
            final String filename = chooser.getSelectedFile().getAbsolutePath();
            CsvResultatPuce.importFromFile(_parent.getRaid(), filename);
            _desktopView.refreshResultTable();
        }
    }

    String getRaidName() {
        return _parent.getRaid().getRaidName();
    }

    Categories getCategories() {
        return _parent.getRaid().getCategories();
    }

    String getPageHeader() {
        return _parent.getRaid().getPageHeader();
    }

    String getPageFooter() {
        return _parent.getRaid().getPageFooter();
    }

    void setRaidName( String name ) {
        _parent.getRaid().setRaidName(name);
    }

    void setPageHeader( String text ) {
        _parent.getRaid().setPageHeader(text);
    }

    void setPageFooter( String text ) {
        _parent.getRaid().setPageFooter(text);
    }

    void setCategories( Categories categories ) {
        _parent.getRaid().setCategories(categories);
    }
}
