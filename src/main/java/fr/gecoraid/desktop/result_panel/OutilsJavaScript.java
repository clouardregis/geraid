package fr.gecoraid.desktop.result_panel;

class OutilsJavaScript {
    private OutilsJavaScript() {
    }

    // TODO clean code
    // TODO put in the convenient folder
    static String getJavaScriptDefilementParPage() {
        return "<script language='JavaScript'>" + "function lancerScroll() {" +
                "if ((document.body.scrollTop + document.body.clientHeight) >= document.body.scrollHeight - 3) {" +
                "document.body.scrollTop = 0;" +
                "window.location.reload();" +
                "}" +
                "var top = document.body.scrollTop;" +
                "scrollAuto(top);" +
                "}" +
                "function scrollAuto(arg) {" +
                "window.scrollBy(0,20);" +
                "if (document.body.scrollTop < (arg + document.body.clientHeight - document.body.clientHeight/5)) {" +
                "setTimeout(scrollAuto,100, arg);" +
                "}" +
                "}" +
                "</script>";
    }

    static String getJavaScriptLancementDefilementParPage( int seconde ) {
        return "<script type='text/javascript'>var myVar = setInterval(lancerScroll," + seconde + ")</script>";
    }

    // TODO not used !
//    static String getJavaScriptDefilementEnContinu( int milliSecondes ) {
//        return "<script language='JavaScript'>"
//                + "function scrollAuto(){"
//                + "var index = " + milliSecondes + ";"
//                + " window.scrollBy(0,2);"
//                + "if (document.body.scrollTop < 150){"
//                + "index = index*3;"
//                + "}"
//                + "if ((document.body.scrollTop + document.body.clientHeight) >= document.body.scrollHeight - 3){"
//                + "document.body.scrollTop = 0;"
//                + "window.location.reload();"
//                + "}"
//                + "if ((document.body.scrollTop + document.body.clientHeight) > document.body.scrollHeight - 150){"
//                + "index = index*3;"
//                + "}"
//                + "setTimeout('scrollAuto()',index);"
//                + "}"
//                + "</script>";
//    }

    // TODO not used !
//    static String getJavaScriptLancementDefilementEnContinu() {
//        return "<script type='text/javascript'>scrollAuto();</script>";
//    }
}
