package fr.gecoraid.desktop.result_panel;

import javax.swing.SwingWorker;

class AutomaticHtmlExportTask extends SwingWorker<Void, Void> {
    private final ResultPresenter _presenter;
    private Thread _currentThread = null;

    AutomaticHtmlExportTask( ResultPresenter resultPresenter ) {
        _presenter = resultPresenter;
    }

    @SuppressWarnings("BusyWait")
    @Override
    protected Void doInBackground() throws Exception {
        _currentThread = Thread.currentThread();
        while (!_currentThread.isInterrupted()) {
            if (_presenter.existWorkingDirectory()) {
                _presenter.exportResultToHtmlFile();
                _presenter.setExportStatusSuccess();
            } else {
                _presenter.setExportStatusFailure();
            }
            Thread.sleep(1000L);
            _presenter.setExportStatusNormal();
            Thread.sleep(_presenter.getExportAutoPeriodInMn() * 60_000L);
        }
        return null;
    }

    void stop() {
        if (_currentThread != null) {
            _currentThread.interrupt();
        }
    }
}
