package fr.gecoraid.desktop.result_panel;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DeskTopPresenter;
import fr.gecoraid.desktop.DesktopView;
import fr.gecoraid.desktop.EpreuveTableModel;
import fr.gecoraid.desktop.EtapeTableModel;
import fr.gecoraid.desktop.ParcoursTableModel;
import fr.gecoraid.desktop.widget.ErrorPane;
import fr.gecoraid.desktop.widget.IconButton;
import fr.gecoraid.model.Categorie;
import fr.gecoraid.model.Epreuve;
import fr.gecoraid.model.Etape;
import fr.gecoraid.model.IVisualisation;
import fr.gecoraid.model.Parcours;
import fr.gecoraid.model.ResultatEtape;
import fr.gecoraid.model.ResultatParcours;
import fr.gecoraid.model.TypeVisualisation;
import fr.gecoraid.model.TypeVisualisationEpreuve;
import fr.gecoraid.model.TypeVisualisationParcours;
import fr.gecoraid.tools.ExtensionFileFilter;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumnModel;
import javax.swing.text.Document;
import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.print.PrinterException;
import java.io.File;
import java.io.IOException;

public class ResultView extends JPanel {
    private final DesktopView _desktopView;
    private final transient ResultPresenter _presenter;

    // TODO Passer par le presenter
    private final JComboBox<Categorie> _categoriesComboBox;
    private final JComboBox<Etape> _etapesComboBox;
    private final JComboBox<Parcours> _parcoursComboBox;
    private final JList<Epreuve> _epreuvesList;

    private final ButtonGroup _buttonGroup = new ButtonGroup();
    private final JPopupMenu _popupMenu;
    private JTable _resultTable;
    private JComboBox<IVisualisation> _typeVisualisationComboBox;
    private JRadioButton _epreuveRadioButton;
    private JEditorPane _globalResultsEditorPane;
    private JRadioButton _parcoursResultsRadioButton;
    private JSpinner _podiumSpinner;
    private JButton _exportHtmlButton;
    private JSpinner _exportAutoSpinner;
    private JRadioButton _etapeRadioButton;
    private JRadioButton _parcoursRadioButton;
    private JCheckBox _puceParEquipeCheckBox;
    private JLabel _courseStateLabel;

    public ResultView( DesktopView parent,
                       DeskTopPresenter desktopPresenter,
                       JComboBox<Parcours> parcoursComboBox, // TODO passer par le presentateur ?
                       JList<Epreuve> epreuvesList,
                       JComboBox<Etape> etapesComboBox,
                       JComboBox<Categorie> categoriesComboBox ) {
        super(new BorderLayout());
        _desktopView = parent;
        _presenter = new ResultPresenter(desktopPresenter, this);
        _parcoursComboBox = parcoursComboBox;
        _epreuvesList = epreuvesList;
        _etapesComboBox = etapesComboBox;
        _categoriesComboBox = categoriesComboBox;
        _popupMenu = _createPopupMenu();
        _initializeUI();
    }

    public int getSpinnerExportAutoValueInMn() {
        return (int) _exportAutoSpinner.getValue();
    }

    public void setExportSuccess() {
        //noinspection ConstantConditions
        _exportHtmlButton.setIcon(new ImageIcon(DesktopView.class.getClassLoader().getResource("fr/gecoraid/icon/success.png")));
    }

    public void setExportFailure() {
        //noinspection ConstantConditions
        _exportHtmlButton.setIcon(new ImageIcon(DesktopView.class.getClassLoader().getResource("fr/gecoraid/icon/error.png")));
    }

    public void setExportNormal() {
        //noinspection ConstantConditions
        _exportHtmlButton.setIcon(new ImageIcon(DesktopView.class.getClassLoader().getResource("fr/gecoraid/icon/html.png")));
    }

    public String exportResultToHtmlFile() {
        if (_parcoursComboBox.getSelectedItem() != null
                && _categoriesComboBox.getSelectedItem() != null
                && _etapesComboBox.getSelectedItem() != null) {
            return _presenter.exportResultsAsHtml(
                    _getVisualizationType(),
                    ((Parcours) _parcoursComboBox.getSelectedItem()).getName(),
                    ((Categorie) _categoriesComboBox.getSelectedItem()).getShortName(),
                    ((Etape) _etapesComboBox.getSelectedItem()).getName(),
                    _epreuvesList.getSelectedValue().getName(),
                    _resultTable);
        } else {
            return null;
        }
    }

    public void refreshResultTable() {
        // TODO Refactor code -> brain method
        if (_parcoursComboBox.getSelectedIndex() >= 0) {
            if (_epreuvesList.getSelectedIndex() >= 0) {
                if (_typeVisualisationComboBox.getSelectedIndex() < 0) {
                    _presenter.fillCategoriesComboBox();
                }
                if (_isRadiobuttonEtapeSelected()) {
                    _resultTable.setModel(new EtapeTableModel(new ResultatEtape(_presenter.getRaid(),
                            (Etape) _etapesComboBox.getSelectedItem(),
                            (Categorie) _categoriesComboBox.getSelectedItem(),
                            (TypeVisualisation) _typeVisualisationComboBox.getSelectedItem(),
                            _isPuceParEquipeCheckBoxSelected())));
                    TableColumnModel tcm = _resultTable.getColumnModel();
                    for (int i = 0; i < tcm.getColumnCount() - 1; i++) {
                        tcm.getColumn(i).setPreferredWidth(55);
                    }
                    tcm.getColumn(0).setPreferredWidth(30);
                    tcm.getColumn(3).setPreferredWidth(300);
                    tcm.getColumn(4).setPreferredWidth(100);
                    if (_presenter.getRaid().isAfficherNomsEquipiers()) {
                        assert (_parcoursComboBox.getSelectedItem() != null);
                        _resultTable.setRowHeight(20 * (((Parcours) _parcoursComboBox.getSelectedItem()).getNombreMaximumEquipiers() + 1));
                    } else {
                        _resultTable.setRowHeight(20);
                    }
                } else if (_isParcoursRadioButtonSelected()) {
                    _resultTable.setModel(new ParcoursTableModel(
                            new ResultatParcours(
                                    _presenter.getRaid(),
                                    (Parcours) _parcoursComboBox.getSelectedItem(),
                                    (Categorie) _categoriesComboBox.getSelectedItem(),
                                    (TypeVisualisationParcours) _typeVisualisationComboBox.getSelectedItem(),
                                    _isPuceParEquipeCheckBoxSelected())));
                    TableColumnModel tcm = _resultTable.getColumnModel();
                    for (int i = 0; i < tcm.getColumnCount() - 1; i++) {
                        tcm.getColumn(i).setPreferredWidth(55);
                    }
                    tcm.getColumn(0).setPreferredWidth(30);
                    tcm.getColumn(3).setPreferredWidth(300);
                    tcm.getColumn(4).setPreferredWidth(100);
                    if (_presenter.getRaid().isAfficherNomsEquipiers()) {
                        assert (_parcoursComboBox.getSelectedItem() != null);
                        _resultTable.setRowHeight(20 * (((Parcours) _parcoursComboBox.getSelectedItem()).getNombreMaximumEquipiers() + 1));
                    } else {
                        _resultTable.setRowHeight(20);
                    }
                } else {
                    _resultTable.setModel(new EpreuveTableModel(new ResultatEtape(_presenter.getRaid(),
                            (Etape) _etapesComboBox.getSelectedItem(),
                            (Categorie) _categoriesComboBox.getSelectedItem(),
                            TypeVisualisation.SIMPLE,
                            _isPuceParEquipeCheckBoxSelected()),
                            _epreuvesList.getSelectedValue(),
                            (TypeVisualisationEpreuve) _typeVisualisationComboBox.getSelectedItem()));
                    TableColumnModel tcm = _resultTable.getColumnModel();
                    for (int i = 0; i < tcm.getColumnCount() - 1; i++) {
                        tcm.getColumn(i).setPreferredWidth(55);
                    }
                    tcm.getColumn(0).setPreferredWidth(30);
                    tcm.getColumn(3).setPreferredWidth(300);
                    tcm.getColumn(4).setPreferredWidth(100);
                    if (_presenter.getRaid().isAfficherNomsEquipiers()) {
                        assert (_parcoursComboBox.getSelectedItem() != null);
                        _resultTable.setRowHeight(20 * (((Parcours) _parcoursComboBox.getSelectedItem()).getNombreMaximumEquipiers() + 1));
                    } else {
                        _resultTable.setRowHeight(20);
                    }
                }
            }
            if (_isRadiobuttonEtapeSelected()) {
                assert (_parcoursComboBox.getSelectedItem() != null);
                int nbEquipesEnCourse = ((Parcours) _parcoursComboBox.getSelectedItem()).getNombreEquipesPresentes() - _resultTable.getRowCount();
                if (nbEquipesEnCourse <= 0) {
                    setTextInCourseStateLabel(L10n.getString("result_view.detailed_result.status.message1"));
                } else if (nbEquipesEnCourse == 1) {
                    setTextInCourseStateLabel(L10n.getString("result_view.detailed_result.status.message2"));
                } else {
                    setTextInCourseStateLabel(L10n.getString("result_view.detailed_result.status.message3", nbEquipesEnCourse));
                }
            } else {
                setTextInCourseStateLabel(null);
            }
        } else {
            _resultTable.removeAll();
        }
    }

    public void updateTable() {
        _resultTable.setModel(new EtapeTableModel(
                new ResultatEtape(_presenter.getRaid(),
                        (Etape) _etapesComboBox.getSelectedItem(),
                        (Categorie) _categoriesComboBox.getSelectedItem(),
                        (TypeVisualisation) _typeVisualisationComboBox.getSelectedItem(),
                        _isPuceParEquipeCheckBoxSelected())));
    }

    public void fillCategoryComboBox() {
        if (_categoriesComboBox.getActionListeners().length > 0) {
            _categoriesComboBox.removeActionListener(_categoriesComboBox.getActionListeners()[0]);
            _typeVisualisationComboBox.removeActionListener(_typeVisualisationComboBox.getActionListeners()[0]);
        }
        _categoriesComboBox.removeAllItems();
        _typeVisualisationComboBox.removeAllItems();
        _categoriesComboBox.addItem(new Categorie(L10n.getString("result_view.detailed_result.categorie.label"), L10n.getString("result_view.detailed_result.categorie.label")));
        for (int i = 0; i < _presenter.getRaid().getCategories().getSize(); i++) {
            _categoriesComboBox.addItem(_presenter.getRaid().getCategories().getCategories().get(i));
        }
        _categoriesComboBox.setSelectedIndex(0);
        if (_isRadiobuttonEtapeSelected()) {
            _typeVisualisationComboBox.setModel(new DefaultComboBoxModel<>(TypeVisualisation.values()));
        } else if (_isParcoursRadioButtonSelected()) {
            _typeVisualisationComboBox.setModel(new DefaultComboBoxModel<>(TypeVisualisationParcours.values()));
        } else {
            _typeVisualisationComboBox.setModel(new DefaultComboBoxModel<>(TypeVisualisationEpreuve.values()));
        }
        _typeVisualisationComboBox.addActionListener(event -> _desktopView.refreshResultTable());
        _categoriesComboBox.addActionListener(event -> _desktopView.refreshResultTable());
    }

    private boolean _isRadiobuttonEtapeSelected() {
        return _etapeRadioButton.isSelected();
    }

    private boolean _isParcoursRadioButtonSelected() {
        return _parcoursRadioButton.isSelected();
    }

    private boolean _isPuceParEquipeCheckBoxSelected() {
        return _puceParEquipeCheckBox.isSelected();
    }

    private void setTextInCourseStateLabel( String text ) {
        if (text == null) {
            text = " ";
        }
        _courseStateLabel.setText(text);
    }

    private JPopupMenu _createPopupMenu() {
        final JPopupMenu popupMenu = new JPopupMenu();
        popupMenu.add(getReducedMenuItem());
        popupMenu.add(getCompleteMenuItem());
        popupMenu.add(getSPORTIdentResultMenuItem());
        return popupMenu;
    }

    private JMenuItem getReducedMenuItem() {
        final JMenuItem menuItem = new JMenuItem(L10n.getString("result_view.popup_menu_label1"));
        menuItem.addActionListener(event -> {
            _popupMenu.setVisible(false);
            final int row = _resultTable.getSelectedRow();
            _presenter.displayReduceResults(
                    (Parcours) _parcoursComboBox.getSelectedItem(),
                    (Etape) _etapesComboBox.getSelectedItem(),
                    (String) _resultTable.getValueAt(row, 2)
            );
        });
        return menuItem;
    }

    private JMenuItem getCompleteMenuItem() {
        final JMenuItem menuItem = new JMenuItem(L10n.getString("result_view.popup_menu_label2"));
        menuItem.setEnabled(true);
        menuItem.addActionListener(event -> {
            _popupMenu.setVisible(false);
            final int row = _resultTable.getSelectedRow();
            _presenter.displayCompleteResults(
                    (Parcours) _parcoursComboBox.getSelectedItem(),
                    (Etape) _etapesComboBox.getSelectedItem(),
                    (String) _resultTable.getValueAt(row, 2));
        });
        return menuItem;
    }

    private JMenuItem getSPORTIdentResultMenuItem() {
        final JMenuItem menuItem = new JMenuItem(L10n.getString("result_view.popup_menu_label3"));
        menuItem.addActionListener(event -> {
            _popupMenu.setVisible(false);
            final int row = _resultTable.getSelectedRow();
            _presenter.displaySPORTIndentResults((Parcours) _parcoursComboBox.getSelectedItem(),
                    (Etape) _etapesComboBox.getSelectedItem(),
                    _resultTable.getValueAt(row, 2).toString());
        });
        return menuItem;
    }

    private void _initializeUI() {
        final JTabbedPane tabbedPane = new JTabbedPane(SwingConstants.TOP);
        tabbedPane.addTab(L10n.getString("result_view.tab1.title2"), null, _createDetailedResultsPanel());
        tabbedPane.addTab(L10n.getString("result_view.tab2.title1"), null, _createGlobalResultsPanel());
        add(tabbedPane);
    }

    private JComponent _createDetailedResultsPanel() {
        final JPanel panel = new JPanel(new BorderLayout());
        panel.add(_createDetailedResultsControlPanel(), BorderLayout.NORTH);
        panel.add(_createDetailedResultsDataPanel(), BorderLayout.CENTER);
        return panel;
    }

    private JPanel _createDetailedResultsControlPanel() {
        final JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.add(_createCourseStateLabel());
        panel.add(_createDetailedResultsControlSubPanel());
        return panel;
    }

    private JPanel _createDetailedResultsControlSubPanel() {
        final FlowLayout layout = new FlowLayout(FlowLayout.LEADING);
        layout.setAlignOnBaseline(true);
        final JPanel panel = new JPanel(layout);
        panel.add(_createTypeToolbar());
        panel.add(_createFilterToolbar());
        panel.add(_createVisualizationToolbar());
        panel.add(_createToolsToolbar());
        panel.add(_createHTMLToolbar());
        return panel;
    }

    private JComponent _createDetailedResultsDataPanel() {
        final JTable table = _createDetailedResultsTable();
        table.setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
        final JScrollPane scrollPane = new JScrollPane(table);
        scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        return scrollPane;
    }

    private JTable _createDetailedResultsTable() {
        _resultTable = new JTable() {
            @Override
            public boolean getScrollableTracksViewportWidth() {
                return super.getScrollableTracksViewportWidth() && getPreferredSize().width < getParent().getWidth();
            }
        };
        _resultTable.setDefaultRenderer(String.class, new MultiLineCellRenderer());
        _resultTable.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
        _resultTable.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        _resultTable.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked( MouseEvent event ) {
                _popupMenu.setVisible(false);
                _resultTable.changeSelection(_resultTable.rowAtPoint(event.getPoint()), 0, false, false);
                if (event.getButton() == MouseEvent.BUTTON3) {
                    _popupMenu.setLocation(event.getXOnScreen(), event.getYOnScreen());
                    _popupMenu.setVisible(true);
                }
            }
        });
        _resultTable.setAutoCreateRowSorter(true);
        _resultTable.getTableHeader().setReorderingAllowed(false);
        return _resultTable;
    }

    private JPanel _createCourseStateLabel() {
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        _courseStateLabel = new JLabel(" ");
        panel.add(_courseStateLabel);
        return panel;
    }

    private JRadioButton _createEpreuveRadioButton() {
        _epreuveRadioButton = new JRadioButton(L10n.getString("result_view.detailed_result.type.choice1"));
        _epreuveRadioButton.addActionListener(event -> _presenter.fillCategoriesComboBox());
        return _epreuveRadioButton;
    }

    private JRadioButton _createEtapeRadioButton() {
        _etapeRadioButton = new JRadioButton(L10n.getString("result_view.detailed_result.type.choice2"));
        _etapeRadioButton.addActionListener(event -> _presenter.fillCategoriesComboBox());
        _etapeRadioButton.setSelected(true);
        return _etapeRadioButton;
    }

    private JRadioButton _createParcoursButton() {
        _parcoursRadioButton = new JRadioButton(L10n.getString("result_view.detailed_result.type.choice3"));
        _parcoursRadioButton.addActionListener(event -> _presenter.fillCategoriesComboBox());
        return _parcoursRadioButton;
    }

    private JPanel _createTypeToolbar() {
        final JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(new TitledBorder(null, L10n.getString("result_view.detailed_result.type.title"), TitledBorder.LEADING, TitledBorder.TOP));
        panel.add(_createEpreuveRadioButton());
        panel.add(_createEtapeRadioButton());
        panel.add(_createParcoursButton());

        final ButtonGroup group = new ButtonGroup();
        group.add(_epreuveRadioButton);
        group.add(_etapeRadioButton);
        group.add(_parcoursRadioButton);

        final Dimension preferredSize = panel.getPreferredSize();
        preferredSize.height = 90;
        panel.setPreferredSize(preferredSize);
        return panel;
    }

    private JPanel _createFilterToolbar() {
        final JPanel panel = new JPanel();
        panel.setLayout(new GridLayout(2, 2));
        panel.setBorder(new TitledBorder(null, L10n.getString("result_view.detailed_result.filter.title"), TitledBorder.LEADING, TitledBorder.TOP));
        panel.add(new JLabel(L10n.getString("result_view.detailed_result.filter.label1")));
        panel.add(_createCategorieComboBox());
        panel.add(new JLabel(L10n.getString("result_view.detailed_result.filter.label2")));
        panel.add(_createVisualizationTypeComboBox());
        final Dimension preferredSize = panel.getPreferredSize();
        preferredSize.height = 90;
        panel.setPreferredSize(preferredSize);
        return panel;
    }

    private JPanel _createVisualizationToolbar() {
        final JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
        panel.setBorder(new TitledBorder(null, L10n.getString("result_view.detailed_result.visualization.title"), TitledBorder.LEADING, TitledBorder.TOP));
        panel.add(_createPuceParEquipeCheckbox());
        panel.add(_createDisplayParticipantsNamesCheckbox());
        final Dimension preferredSize = panel.getPreferredSize();
        preferredSize.height = 90;
        panel.setPreferredSize(preferredSize);
        return panel;
    }

    private JComboBox<Categorie> _createCategorieComboBox() {
        _categoriesComboBox.setSelectedIndex(-1);
        final Dimension preferredSize = _categoriesComboBox.getPreferredSize();
        preferredSize.width = 100;
        _categoriesComboBox.setPreferredSize(preferredSize);
        return _categoriesComboBox;
    }

    private JComboBox<IVisualisation> _createVisualizationTypeComboBox() {
        _typeVisualisationComboBox = new JComboBox<>();
        final Dimension preferredSize = _typeVisualisationComboBox.getPreferredSize();
        preferredSize.width = 100;
        _typeVisualisationComboBox.setPreferredSize(preferredSize);
        return _typeVisualisationComboBox;
    }

    private JPanel _createToolsToolbar() {
        final JPanel panel = new JPanel();
        panel.setBorder(new TitledBorder(null, L10n.getString("result_view.detailed_result.tools.title"), TitledBorder.LEADING, TitledBorder.TOP));
        panel.add(_createReloadButton());
        panel.add(_createPrintButton());
        panel.add(_createExportButton());
        final Dimension preferredSize = panel.getPreferredSize();
        preferredSize.height = 90;
        panel.setPreferredSize(preferredSize);
        return panel;
    }

    private JButton _createReloadButton() {
        final JButton button = new IconButton("reload.png");
        button.setToolTipText(L10n.getString("result_view.detailed_result.tools.button1"));
        button.addActionListener(event -> _desktopView.refreshResultTable());
        return button;
    }

    private JButton _createPrintButton() {
        final JButton button = new IconButton("printer.png");
        button.setToolTipText(L10n.getString("result_view.detailed_result.tools.button2"));
        button.addActionListener(event -> {
            try {
                if (_parcoursComboBox.getSelectedItem() != null
                        && _categoriesComboBox.getSelectedItem() != null
                        && _etapesComboBox.getSelectedItem() != null) {
                    _presenter.printResults(
                            _resultTable,
                            _getVisualizationType(),
                            (Parcours) _parcoursComboBox.getSelectedItem(),
                            (Categorie) _categoriesComboBox.getSelectedItem(),
                            (Etape) _etapesComboBox.getSelectedItem(),
                            _epreuvesList.getSelectedValue());
                }
            } catch (PrinterException ex) {
                ErrorPane.showMessageDialog(L10n.getString("result_view.detailed_result.tools.print.error", ex.getMessage()));
            }
        });
        return button;
    }

    private JButton _createExportButton() {
        final JButton button = new IconButton("export-csv.png");
        button.setToolTipText(L10n.getString("result_view.detailed_result.tools.button3"));
        button.addActionListener(event -> {
            final JFileChooser chooser = new JFileChooser();
            ExtensionFileFilter filter = new ExtensionFileFilter("csv", L10n.getString("csv_file.description"));
            chooser.setFileFilter(filter);
            chooser.setCurrentDirectory(new File(_presenter.getRaidFolderPath()));
            int returnVal = chooser.showSaveDialog(this);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                _presenter.exportResultAsCSV(
                        chooser.getSelectedFile().getAbsolutePath(),
                        _resultTable,
                        _getVisualizationType());
            }
        });
        return button;
    }

    private JPanel _createHTMLToolbar() {
        final JPanel panel = new JPanel();
        panel.setBorder(new TitledBorder(null, L10n.getString("result_view.detailed_result.export.title"), TitledBorder.LEADING, TitledBorder.TOP));
        panel.add(_createButtonExportResultAsHTML());

        final JSeparator separator1 = new JSeparator(SwingConstants.VERTICAL);
        separator1.setPreferredSize(new Dimension(5, 50));
        panel.add(separator1);

        panel.add(_createExportAutoSpinner());
        panel.add(new JLabel(L10n.getString("minute_abbreviation")));
        panel.add(_createExportAutoButton());

        final JSeparator separator2 = new JSeparator(SwingConstants.VERTICAL);
        separator2.setPreferredSize(new Dimension(5, 50));
        panel.add(separator2);

        panel.add(_createPodiumSpinner());
        panel.add(new JLabel(L10n.getString("result_view.detailed_result.export.places")));
        panel.add(_createPodiumButton());
        return panel;
    }

    private JSpinner _createExportAutoSpinner() {
        _exportAutoSpinner = new JSpinner();
        _exportAutoSpinner.setModel(new SpinnerNumberModel(1, 1, 59, 1));
        return _exportAutoSpinner;
    }

    private JButton _createExportAutoButton() {
        final JButton button = new IconButton("play.png");
        button.addActionListener(event -> {
            if (_presenter.runAutomaticExportResultsAsHtml()) {
                //noinspection ConstantConditions
                button.setIcon(new ImageIcon(DesktopView.class.getClassLoader().getResource("fr/gecoraid/icon/play.png")));
                button.setToolTipText(L10n.getString("result_view.detailed_result.export.tooltip2"));
            } else {
                //noinspection ConstantConditions
                button.setIcon(new ImageIcon(DesktopView.class.getClassLoader().getResource("fr/gecoraid/icon/pause.png")));
                button.setToolTipText(L10n.getString("result_view.detailed_result.export.tooltip5"));
            }
        });
        button.setToolTipText(L10n.getString("result_view.detailed_result.export.tooltip4"));
        button.setSize(new Dimension(30, 30));
        return button;
    }

    private JCheckBox _createPuceParEquipeCheckbox() {
        _puceParEquipeCheckBox = new JCheckBox(L10n.getString("result_view.detailed_result.visualization.checkbox1"));
        _puceParEquipeCheckBox.addActionListener(event -> _desktopView.refreshResultTable());
        return _puceParEquipeCheckBox;
    }

    private JCheckBox _createDisplayParticipantsNamesCheckbox() {
        final JCheckBox checkbox = new JCheckBox(L10n.getString("result_view.detailed_result.visualization.checkbox2"));
        checkbox.addActionListener(event -> _presenter.displayEquipeMateNames(checkbox.isSelected()));
        return checkbox;
    }

    private JSpinner _createPodiumSpinner() {
        _podiumSpinner = new JSpinner();
        _podiumSpinner.setModel(new SpinnerNumberModel(3, 1, null, 1));
        return _podiumSpinner;
    }

    private JButton _createButtonExportResultAsHTML() {
        _exportHtmlButton = new IconButton("html.png");
        _exportHtmlButton.setToolTipText(L10n.getString("result_view.detailed_result.export.tooltip1"));
        _exportHtmlButton.addActionListener(event -> {
            if (_epreuvesList.getSelectedIndex() > -1) {
                String filePath = exportResultToHtmlFile();
                if (filePath != null) {
                    new SnackBar(L10n.getString("result_view.detailed_result.export.success", filePath));
                } else {
                    ErrorPane.showMessageDialog(L10n.getString("result_view.detailed_result.export.error"));
                }
            }
        });
        return _exportHtmlButton;
    }

    private JButton _createPodiumButton() {
        final JButton button = new IconButton("podium.png");
        button.setToolTipText(L10n.getString("result_view.detailed_result.export.tooltip3"));
        button.addActionListener(event -> {
            if (_parcoursComboBox.getSelectedItem() != null
                    && _categoriesComboBox.getSelectedItem() != null
                    && _etapesComboBox.getSelectedItem() != null) {
                String filePath = _presenter.exportPodiumAsHtml(_getVisualizationType(),
                        (Parcours) _parcoursComboBox.getSelectedItem(),
                        (Categorie) _categoriesComboBox.getSelectedItem(),
                        (Etape) _etapesComboBox.getSelectedItem(),
                        _epreuvesList.getSelectedValue().getName(),
                        _resultTable,
                        (int) _podiumSpinner.getValue());
                if (filePath != null) {
                    new SnackBar(L10n.getString("result_view.detailed_result.export.success", filePath));
                } else {
                    ErrorPane.showMessageDialog(L10n.getString("result_view.detailed_result.export.error"));
                }
            }
        });
        return button;
    }

    private VisualizationType _getVisualizationType() {
        if (_etapeRadioButton.isSelected()) {
            return VisualizationType.ETAPE;
        } else if (_parcoursRadioButton.isSelected()) {
            return VisualizationType.PARCOURS;
        } else {
            return VisualizationType.EPREUVE;
        }
    }

    private JPanel _createGlobalResultsPanel() {
        final JPanel panel = new JPanel(new BorderLayout(0, 0));
        panel.add(_createGlobalResultsControlPanel(), BorderLayout.NORTH);
        panel.add(_createGlobalResultsDataPanel(), BorderLayout.CENTER);
        return panel;
    }

    private JPanel _createGlobalResultsControlPanel() {
        final JPanel panel = new JPanel();
        panel.add(_createParcoursResultsRadioButton());
        panel.add(_createEtapeResultsRadioButton());
        panel.add(_createRefreshResultsButton());
        return panel;
    }

    private JRadioButton _createParcoursResultsRadioButton() {
        _parcoursResultsRadioButton = new JRadioButton(L10n.getString("result_view.global_result.radio_button1"));
        _parcoursResultsRadioButton.setSelected(true);
        _buttonGroup.add(_parcoursResultsRadioButton);
        return _parcoursResultsRadioButton;
    }

    private JRadioButton _createEtapeResultsRadioButton() {
        final JRadioButton button = new JRadioButton(L10n.getString("result_view.global_result.radio_button2"));
        _buttonGroup.add(button);
        return button;
    }

    private JButton _createRefreshResultsButton() {
        final JButton button = new IconButton("reload.png");
        button.setToolTipText(L10n.getString("result_view.global_result.reload_button"));
        button.addActionListener(event -> {
            final String url = _presenter.refreshResults(_parcoursResultsRadioButton.isSelected());
            if (url != null) {
                try {
                    Document document = _globalResultsEditorPane.getDocument();
                    document.putProperty(Document.StreamDescriptionProperty, null);
                    _globalResultsEditorPane.setPage(url);
                } catch (IOException ignored) {
                    // OK
                }
            }
        });
        return button;
    }

    private JScrollPane _createGlobalResultsDataPanel() {
        final JScrollPane scrollPane = new JScrollPane();
        scrollPane.setViewportView(_createGlobalResultsHTMLViewer());
        return scrollPane;
    }

    private JEditorPane _createGlobalResultsHTMLViewer() {
        _globalResultsEditorPane = new JEditorPane();
        _globalResultsEditorPane.setEditable(false);
        _globalResultsEditorPane.setContentType("text/html");
        return _globalResultsEditorPane;
    }

    static class MultiLineCellRenderer extends JTextArea implements TableCellRenderer {
        private static final long serialVersionUID = -5699535840057997829L;

        MultiLineCellRenderer() {
            setLineWrap(true);
            setWrapStyleWord(true);
            setOpaque(true);
        }

        @Override
        public Component getTableCellRendererComponent( JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column ) {
            if (isSelected) {
                setForeground(table.getSelectionForeground());
                setBackground(table.getSelectionBackground());
            } else {
                setForeground(table.getForeground());
                setBackground(table.getBackground());
            }
            setFont(table.getFont());
            if (hasFocus) {
                setBorder(UIManager.getBorder("Table.focusCellHighlightBorder"));
                if (table.isCellEditable(row, column)) {
                    setForeground(UIManager.getColor("Table.focusCellForeground"));
                    setBackground(UIManager.getColor("Table.focusCellBackground"));
                }
            } else {
                setBorder(new EmptyBorder(1, 2, 1, 2));
            }
            setText((value == null) ? "" : value.toString());
            return this;
        }
    }
}
