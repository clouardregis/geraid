package fr.gecoraid.desktop.result_panel;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.EpreuveTableModel;
import fr.gecoraid.desktop.EtapeTableModel;
import fr.gecoraid.desktop.ParcoursTableModel;
import fr.gecoraid.desktop.widget.ErrorPane;
import fr.gecoraid.model.Etape;
import fr.gecoraid.model.Parcours;
import fr.gecoraid.model.Raid;
import fr.gecoraid.model.ResultatEtape;
import fr.gecoraid.model.ResultatParcours;
import fr.gecoraid.model.TypeVisualisation;
import fr.gecoraid.model.TypeVisualisationParcours;

import javax.swing.JTable;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.text.MessageFormat;

class HtmlWriter {
    private HtmlWriter() {
    }

    static String exportResults( String outputDirectory,
                                 String raidName,
                                 VisualizationType visualizationType,
                                 String parcoursName,
                                 String categorieName,
                                 String etapeName,
                                 String epreuveName,
                                 JTable resultTable,
                                 boolean withAutoScrollingScript,
                                 int autoScrollingPauseDurationInS ) {
        final File file = new File(outputDirectory);
        if (!file.exists()) {
            return null;
        }
        final StringBuilder absoluteFilename = new StringBuilder(outputDirectory).append(File.separator).append(raidName).append("_").append(parcoursName);
        final StringBuilder header = new StringBuilder();
        header.append("<h1>Résultats provisoires ").append(raidName).append("</h1>");
        header.append("<h2>Parcours : ").append(parcoursName).append("</h2>");
        switch (visualizationType) {
            case PARCOURS:
                absoluteFilename.append("_").append(categorieName).append(".html");
                _exportParcours(absoluteFilename.toString(),
                        (ParcoursTableModel) resultTable.getModel(),
                        resultTable,
                        header.toString(),
                        withAutoScrollingScript,
                        autoScrollingPauseDurationInS);
                break;
            case ETAPE:
                absoluteFilename.append("_").append(etapeName).append("_")
                        .append(categorieName).append(".html");
                header.append("<h2>Étape : ").append(etapeName).append("</h2>");
                _exportEtape(absoluteFilename.toString(),
                        (EtapeTableModel) resultTable.getModel(),
                        resultTable,
                        header.toString(),
                        withAutoScrollingScript,
                        autoScrollingPauseDurationInS);
                break;
            case EPREUVE:
                absoluteFilename.append("_").append(etapeName);
                header.append("<h2>Étape : ").append(etapeName).append("</h2>");
                absoluteFilename.append("_").append(epreuveName);
                header.append("<h2>Épreuve : ").append(epreuveName).append("</h2>");
                absoluteFilename.append("_").append(categorieName);
                absoluteFilename.append(".html");
                _exportEpreuve(absoluteFilename.toString(),
                        (EpreuveTableModel) resultTable.getModel(),
                        resultTable,
                        header.toString(),
                        withAutoScrollingScript,
                        autoScrollingPauseDurationInS);
                break;
        }
        return absoluteFilename.toString();
    }

    static String exportPodium( String outputDirectory,
                                String raidName,
                                String parcoursName,
                                String categorieName,
                                String etapeName,
                                String epreuveName,
                                JTable resultTable,
                                int podiumValue,
                                VisualizationType visualizationType,
                                boolean withAutoScrollingScript,
                                int autoScrollingPauseDurationInS ) {
        final File file = new File(outputDirectory);
        if (!file.exists()) {
            return null;
        }
        final StringBuilder outputFileName = new StringBuilder(outputDirectory);
        outputFileName.append(File.separator).append(raidName).append("_Podium_");
        outputFileName.append(parcoursName);

        final StringBuilder header = new StringBuilder();
        header.append("<h1>Podium provisoire ").append(raidName).append("</h1>")
                .append("<h2>Parcours : ").append(parcoursName).append("</h2>");
        switch (visualizationType) {
            case PARCOURS:
                outputFileName.append("_").append(categorieName).append(".html");
                _generateParcoursPodium(outputFileName.toString(),
                        (ParcoursTableModel) resultTable.getModel(),
                        resultTable,
                        podiumValue,
                        header.toString(),
                        withAutoScrollingScript,
                        autoScrollingPauseDurationInS);
                break;
            case ETAPE:
                outputFileName.append("_").append(etapeName).append("_").append(categorieName).append(".html");
                header.append("<h2>Étape : ").append(etapeName).append("</h2>");
                _generateEtapePodium(outputFileName.toString(),
                        (EtapeTableModel) resultTable.getModel(),
                        resultTable,
                        podiumValue,
                        header.toString(),
                        withAutoScrollingScript,
                        autoScrollingPauseDurationInS);
                break;
            case EPREUVE:
                outputFileName.append("_").append(etapeName).append("_")
                        .append(epreuveName).append("_")
                        .append(categorieName).append(".html");
                header.append("<h2>Étape : ").append(etapeName).append("</h2>")
                        .append("<h2>Épreuve : ").append(epreuveName).append("</h2>");
                _generateEpreuvePodium(outputFileName.toString(),
                        (EpreuveTableModel) resultTable.getModel(),
                        resultTable,
                        podiumValue,
                        header.toString(),
                        withAutoScrollingScript,
                        autoScrollingPauseDurationInS);
                break;
        }
        return outputFileName.toString();
    }

    static String refreshResults( String outputDirectoryPath,
                                  Raid raid,
                                  boolean isParcoursResultSelected,
                                  boolean withAutoScrollingScript,
                                  int autoScrollingPauseDurationInS ) {

        final String outputFilename = outputDirectoryPath + File.separator + raid.getRaidName() + ".html";
        if (isParcoursResultSelected) {
            _exportParcours(outputFilename, raid, withAutoScrollingScript, autoScrollingPauseDurationInS);
        } else {
            _generateParcoursByEtape(outputFilename, raid, withAutoScrollingScript, autoScrollingPauseDurationInS);
        }
        return outputFilename;
    }

    static void printResults( String raidName,
                              JTable resultTable,
                              VisualizationType visualizationType,
                              String parcoursName,
                              String categorieName,
                              String etapeName,
                              String epreuveName,
                              boolean isShortGlobalResultPrinting ) throws PrinterException {
        if (isShortGlobalResultPrinting) {
            final PrinterJob job = PrinterJob.getPrinterJob();
            switch (visualizationType) {
                case EPREUVE:
                    job.setPrintable((EpreuveTableModel) resultTable.getModel());
                    break;
                case ETAPE:
                    job.setPrintable((EtapeTableModel) resultTable.getModel());
                    break;
                case PARCOURS:
                    job.setPrintable((ParcoursTableModel) resultTable.getModel());
                    break;
            }
            if (job.printDialog()) {
                job.print();
            }
        } else {
            final StringBuilder header = new StringBuilder(raidName);
            header.append(" - ").append(parcoursName);
            if (visualizationType == VisualizationType.ETAPE || visualizationType == VisualizationType.EPREUVE) {
                assert (etapeName != null);
                header.append(" - ").append(etapeName);
            }
            if (visualizationType == VisualizationType.EPREUVE) {
                header.append(" - ").append(epreuveName);
            }
            header.append(" - : ").append(categorieName);
            final MessageFormat headerFormat = new MessageFormat(header.toString());
            resultTable.print(JTable.PrintMode.FIT_WIDTH, headerFormat, null);
        }
    }

    private static void _generateParcoursByEtape( String absoluteFilename,
                                                  Raid geRaid,
                                                  boolean withAutoScrollingScript,
                                                  int autoScrollingPauseDurationInS ) {
        final File file = new File(absoluteFilename);
        try (Writer writer = new OutputStreamWriter(Files.newOutputStream(file.toPath()), StandardCharsets.UTF_8)) {
            final StringBuilder code = _generateHeaderCode(withAutoScrollingScript, absoluteFilename);
            code.append("<h1>Résultat provisoire ").append(geRaid.getRaidName()).append("</h1>");
            for (int i = 0; i < geRaid.getParcourss().getSize(); i++) {
                final Parcours parcours = geRaid.getParcourss().getParcourss().get(i);
                code.append("<h2>Parcours : ").append(parcours.getName()).append("</h2>");
                for (int j = 0; j < parcours.getEtapes().getSize(); j++) {
                    final Etape etape = parcours.getEtapes().getEtapes().get(j);
                    code.append("<h3>Étape : ").append(etape.getName()).append("</h3>");
                    final ResultatEtape etapeResult = new ResultatEtape(geRaid,
                            etape,
                            null,
                            TypeVisualisation.SIMPLE,
                            true);
                    code.append(etapeResult.getHeaderAsHtml());
                    code.append(etapeResult.getDataAsHtml());
                }
            }
            if (withAutoScrollingScript) {
                code.append(OutilsJavaScript.getJavaScriptLancementDefilementParPage(autoScrollingPauseDurationInS));
            }
            code.append("<br><br><br></body></html>");
            writer.write(code.toString());
        } catch (IOException e) {
            ErrorPane.showMessageDialog(L10n.getString("result_panel.html_writer.error", e.getClass().getName(), e.getMessage()));
        }
    }

    private static void _exportParcours( String absoluteFilename, Raid geRaid,
                                         boolean withAutoScrollingScript,
                                         int autoScrollingPauseDurationInS ) {
        final File file = new File(absoluteFilename);
        try (Writer writer = new OutputStreamWriter(Files.newOutputStream(file.toPath()), StandardCharsets.UTF_8)) {
            final StringBuilder code = _generateHeaderCode(withAutoScrollingScript, absoluteFilename);
            code.append("<h1>Résultat provisoire ").append(geRaid.getRaidName()).append("</h1>");
            for (int i = 0; i < geRaid.getParcourss().getSize(); i++) {
                final Parcours parcours = geRaid.getParcourss().getParcourss().get(i);
                code.append("<h2>Parcours : ").append(parcours.getName()).append("</h2>");
                final ResultatParcours parcoursResults = new ResultatParcours(geRaid,
                        parcours,
                        null,
                        TypeVisualisationParcours.AVEC_ETAPE,
                        true);
                code.append(parcoursResults.getHeaderAsHtml());
                code.append(parcoursResults.getDataAsHtml());
            }
            if (withAutoScrollingScript) {
                code.append(OutilsJavaScript.getJavaScriptLancementDefilementParPage(autoScrollingPauseDurationInS));
            }
            code.append("<br><br><br></body></html>");
            writer.write(code.toString());
        } catch (IOException e) {
            ErrorPane.showMessageDialog(L10n.getString("result_panel.html_writer.error", e.getClass().getName(), e.getMessage()));
        }
    }

    private static void _exportParcours( String absoluteFilename,
                                         ParcoursTableModel parcoursTableModel,
                                         JTable resultTable,
                                         String header,
                                         boolean withAutoScrollingScript,
                                         int autoScrollingPauseDurationInS ) {
        // TODO unit test
        final File file = new File(absoluteFilename);
        try (final Writer writer = new OutputStreamWriter(Files.newOutputStream(file.toPath()), StandardCharsets.UTF_8)) {
            final StringBuilder code = _generateHeaderCode(withAutoScrollingScript, absoluteFilename);
            code.append(header);
            code.append(parcoursTableModel.getHeaderAsHtml());
            code.append(parcoursTableModel.getDataAsHtml(resultTable));
            // TODO il est ajoute deux fois ? (voir headersource au-dessus)
            if (withAutoScrollingScript) {
                code.append(OutilsJavaScript.getJavaScriptLancementDefilementParPage(autoScrollingPauseDurationInS));
            }
            code.append("<br><br><br></body></html>");
            writer.write(code.toString());
        } catch (IOException e) {
            ErrorPane.showMessageDialog(L10n.getString("result_panel.html_writer.error", e.getClass().getName(), e.getMessage()));
        }
    }

    private static void _exportEtape( String absoluteFilename,
                                      EtapeTableModel etapeTableModel,
                                      JTable resultTable,
                                      String header,
                                      boolean withAutoScrollingScript,
                                      int autoScrollingPauseDurationInS ) {
        final File file = new File(absoluteFilename);
        try (Writer writer = new OutputStreamWriter(Files.newOutputStream(file.toPath()), StandardCharsets.UTF_8)) {
            final StringBuilder code = _generateHeaderCode(withAutoScrollingScript, absoluteFilename);
            code.append(header);
            code.append(etapeTableModel.getHeaderAsHtml());
            code.append(etapeTableModel.getDataAsHtml(resultTable));
            if (withAutoScrollingScript) {
                code.append(OutilsJavaScript.getJavaScriptLancementDefilementParPage(autoScrollingPauseDurationInS));
            }
            code.append("<br><br><br></body></html>");
            writer.write(code.toString());
            writer.write(System.lineSeparator());
        } catch (IOException e) {
            ErrorPane.showMessageDialog(L10n.getString("result_panel.html_writer.error", e.getClass().getName(), e.getMessage()));
        }
    }

    private static void _exportEpreuve( String absoluteFilename,
                                        EpreuveTableModel epreuveTableModel,
                                        JTable resultTable,
                                        String header,
                                        boolean withAutoScrollingScript,
                                        int autoScrollingPauseDurationInS ) {
        final File file = new File(absoluteFilename);
        try (Writer writer = new OutputStreamWriter(Files.newOutputStream(file.toPath()), StandardCharsets.UTF_8)) {
            final StringBuilder code = _generateHeaderCode(withAutoScrollingScript, absoluteFilename);
            code.append(header);
            code.append(epreuveTableModel.getHeaderAsHtml());
            code.append(epreuveTableModel.getDataAsHtml(resultTable));
            if (withAutoScrollingScript) {
                code.append(OutilsJavaScript.getJavaScriptLancementDefilementParPage(autoScrollingPauseDurationInS));
            }
            code.append("<br><br><br></body></html>");
            writer.write(code.toString());
            writer.write(System.lineSeparator());
        } catch (IOException e) {
            ErrorPane.showMessageDialog(L10n.getString("result_panel.html_writer.error", e.getClass().getName(), e.getMessage()));
        }
    }

    private static StringBuilder _generateHeaderCode( boolean withAutoScrollingScript, String filePath ) {
        // TODO tester -> ut
        final StringBuilder code = new StringBuilder("<!doctype html><html lang=\"fr-FR\"><head><meta charset=\"UTF-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">");
        final File file = new File(filePath);
        code.append("<title>").append(file.getName()).append("</title>");
        if (withAutoScrollingScript) {
            code.append(OutilsJavaScript.getJavaScriptDefilementParPage());
        }
        code.append("</head><body>");
        return code;
    }

    private static void _generateParcoursPodium( String absoluteFilename,
                                                 ParcoursTableModel parcoursTableModel,
                                                 JTable resultTable,
                                                 int index,
                                                 String header,
                                                 boolean withAutoScrollingScript,
                                                 int autoScrollingPauseDurationInS ) {
        final File file = new File(absoluteFilename);
        try (Writer writer = new OutputStreamWriter(Files.newOutputStream(file.toPath()), StandardCharsets.UTF_8)) {
            final StringBuilder code = _generateHeaderCode(withAutoScrollingScript, absoluteFilename);
            code.append(header);
            code.append(parcoursTableModel.getHeaderAsHtml());
            code.append(parcoursTableModel.getDataAsHtml(resultTable, index));
            if (withAutoScrollingScript) {
                code.append(OutilsJavaScript.getJavaScriptLancementDefilementParPage(autoScrollingPauseDurationInS));
            }
            code.append("<br><br><br></body></html>");
            writer.write(code.toString());
            writer.write(System.lineSeparator());
        } catch (IOException e) {
            ErrorPane.showMessageDialog(L10n.getString("result_panel.html_writer.error", e.getClass().getName(), e.getMessage()));
        }
    }

    private static void _generateEtapePodium( String absoluteFilename,
                                              EtapeTableModel etapeTableModel,
                                              JTable resultTable,
                                              int index,
                                              String header,
                                              boolean withAutoScrollingScript,
                                              int autoScrollingPauseDurationInS ) {
        final File file = new File(absoluteFilename);
        try (Writer writer = new OutputStreamWriter(Files.newOutputStream(file.toPath()), StandardCharsets.UTF_8)) {
            final StringBuilder code = _generateHeaderCode(withAutoScrollingScript, absoluteFilename);
            code.append(header);
            code.append(etapeTableModel.getHeaderAsHtml());
            code.append(etapeTableModel.getDataAsHtml(resultTable, index));
            if (withAutoScrollingScript) {
                code.append(OutilsJavaScript.getJavaScriptLancementDefilementParPage(autoScrollingPauseDurationInS));
            }
            code.append("<br><br><br></body></html>");
            writer.write(code.toString());
            writer.write(System.lineSeparator());
        } catch (IOException e) {
            ErrorPane.showMessageDialog(L10n.getString("result_panel.html_writer.error", e.getClass().getName(), e.getMessage()));
        }
    }

    private static void _generateEpreuvePodium( String absoluteFilename,
                                                EpreuveTableModel epreuveTableModel,
                                                JTable resultTable,
                                                int index,
                                                String header,
                                                boolean withAutoScrollingScript,
                                                int autoScrollingPauseDurationInS ) {
        final File file = new File(absoluteFilename);
        try (Writer writer = new OutputStreamWriter(Files.newOutputStream(file.toPath()), StandardCharsets.UTF_8)) {
            final StringBuilder code = _generateHeaderCode(withAutoScrollingScript, absoluteFilename);
            code.append(header);
            code.append(epreuveTableModel.getHeaderAsHtml());
            code.append(epreuveTableModel.getDataAsHtml(resultTable, index));
            if (withAutoScrollingScript) {
                code.append(OutilsJavaScript.getJavaScriptLancementDefilementParPage(autoScrollingPauseDurationInS));
            }
            code.append("<br><br><br></body></html>");
            writer.write(code.toString());
            writer.write(System.lineSeparator());
        } catch (IOException e) {
            ErrorPane.showMessageDialog(L10n.getString("result_panel.html_writer.error", e.getClass().getName(), e.getMessage()));
        }
    }
}
