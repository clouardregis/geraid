package fr.gecoraid.desktop.result_panel;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.DataTableModel;
import fr.gecoraid.desktop.DesktopView;
import fr.gecoraid.desktop.widget.ErrorPane;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

class CsvWriter {
    private CsvWriter() {
    }

    static void export( DataTableModel resultTableModel, String filename ) {
        final File file = new File(filename);
        try (Writer writer = new OutputStreamWriter(Files.newOutputStream(file.toPath()), StandardCharsets.UTF_8)) {
            final String csvText = resultTableModel.getHeaderAsCsv();
            writer.write(csvText);
            writer.write(System.lineSeparator());
            for (String row : resultTableModel.getDataAsCsv()) {
                writer.write(row);
                writer.write(System.lineSeparator());
            }
        } catch (IOException e) {
            DesktopView.beep();
            ErrorPane.showMessageDialog(L10n.getString("result_panel.cvs_writer.error", e.getClass().getName(), e.getMessage()));
        }
    }
}
