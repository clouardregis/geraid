package fr.gecoraid.desktop.result_panel;

import javax.swing.AbstractAction;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.WindowConstants;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;

class SnackBar extends JDialog {
    private static final long serialVersionUID = -982836036007008898L;
    private final String _message;

    SnackBar( String message ) {
        _message = message;
        _initComponents();
    }

    private void _initComponents() {
        final Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
        final Insets screenInsets = Toolkit.getDefaultToolkit().getScreenInsets(getGraphicsConfiguration());
        setLocation(0, screenSize.height - screenInsets.bottom - 80);
        setSize(screenSize.width, 80);
        setLayout(null);
        setUndecorated(true);
        setLayout(new GridBagLayout());

        final GridBagConstraints constraints = new GridBagConstraints();
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.weightx = 1.0f;
        constraints.weighty = 1.0f;
        constraints.insets = new Insets(5, 5, 5, 5);
        constraints.fill = GridBagConstraints.BOTH;
        final JLabel messageLabel = new JLabel(_message);
        final ImageIcon image = new ImageIcon(Toolkit.getDefaultToolkit().getImage(SnackBar.class.getClassLoader().getResource("fr/gecoraid/icon/logo32.png")));
        messageLabel.setIcon(image);
        messageLabel.setOpaque(false);
        add(messageLabel, constraints);

        constraints.gridx++;
        constraints.weightx = 0f;
        constraints.weighty = 0f;
        constraints.fill = GridBagConstraints.NONE;
        constraints.anchor = GridBagConstraints.NORTH;
        final JButton closeButton = new JButton(new AbstractAction("x") {
            @Override
            public void actionPerformed( final ActionEvent event ) {
                dispose();
            }
        });
        closeButton.setMargin(new Insets(1, 4, 1, 4));
        closeButton.setFocusable(false);
        add(closeButton, constraints);

        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setVisible(true);
        setAlwaysOnTop(true);

        new Thread(() -> {
            try {
                Thread.sleep(10 * 1_000L);
                dispose();
            } catch (InterruptedException ignored) {
                Thread.currentThread().interrupt();
            }
        }).start();
    }
}
