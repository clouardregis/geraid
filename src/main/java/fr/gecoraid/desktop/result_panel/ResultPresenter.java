package fr.gecoraid.desktop.result_panel;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.ChipResultManagementDialog;
import fr.gecoraid.desktop.DeskTopPresenter;
import fr.gecoraid.desktop.EpreuveTableModel;
import fr.gecoraid.desktop.EtapeTableModel;
import fr.gecoraid.desktop.ParcoursTableModel;
import fr.gecoraid.model.Categorie;
import fr.gecoraid.model.Epreuve;
import fr.gecoraid.model.Etape;
import fr.gecoraid.model.Parcours;
import fr.gecoraid.model.Raid;
import fr.gecoraid.model.ResultatPuce;
import fr.gecoraid.tools.Utils;

import javax.swing.JTable;
import java.awt.print.PrinterException;
import java.io.File;

class ResultPresenter {
    private final DeskTopPresenter _parentPresenter;
    private AutomaticHtmlExportTask _automaticHtmlExportTask;
    private final ResultView _view;

    ResultPresenter( DeskTopPresenter presenter, ResultView view ) {
        _parentPresenter = presenter;
        _view = view;
    }

    void exportResultToHtmlFile() {
        _view.exportResultToHtmlFile();
    }

    boolean existWorkingDirectory() {
        File dossier = new File(_parentPresenter.getGlobalSettings().getWorkingDirectoryPath());
        return dossier.exists();
    }

    void setExportStatusFailure() {
        _view.setExportFailure();
    }

    void setExportStatusNormal() {
        _view.setExportNormal();
    }

    void setExportStatusSuccess() {
        _view.setExportSuccess();
    }

    void displayEquipeMateNames( boolean selected ) {
        _parentPresenter.getRaid().setAfficherNomsEquipiers(selected);
        _parentPresenter.refreshResulTable();
    }

    long getExportAutoPeriodInMn() {
        return _view.getSpinnerExportAutoValueInMn();
    }

    void fillCategoriesComboBox() {
        _view.fillCategoryComboBox();
        _parentPresenter.refreshResulTable();
    }

    boolean runAutomaticExportResultsAsHtml() {
        if (_automaticHtmlExportTask != null) {
            _automaticHtmlExportTask.stop();
            _automaticHtmlExportTask = null;
            return false;
        } else {
            _automaticHtmlExportTask = new AutomaticHtmlExportTask(this);
            _automaticHtmlExportTask.execute();
            return true;
        }
    }

    void displaySPORTIndentResults( Parcours parcours, Etape etape, String puce ) {
        if (getRaid().getResultatsPuce().getSize() > 0) {
            ChipResultManagementDialog dialog = new ChipResultManagementDialog(
                    _parentPresenter,
                    parcours,
                    etape,
                    getRaid().getEquipePuce(puce));
            dialog.setLocationRelativeTo(_view);
            dialog.setVisible(true);// TODO ShowDialog()
        }
    }

    void displayReduceResults( Parcours parcours, Etape etape, String chip ) {
        ResultatPuce chipResult = _parentPresenter.getRaid().getResultatsPuce().getResultatPuce(parcours, etape, chip);
        if (chipResult != null) {
            _parentPresenter.ihmResultatPuce(chipResult, true, true, true);
        }
    }

    void displayCompleteResults( Parcours parcours, Etape etape, String chip ) {
        ResultatPuce chipResult = _parentPresenter.getRaid().getResultatsPuce().getResultatPuce(parcours, etape, chip);
        if (chipResult != null) {
            _parentPresenter.ihmResultatPuce(chipResult, true, true, false);
        }
    }

    String getRaidFolderPath() {
        return _parentPresenter.getGlobalSettings().getRaidFolderPath();
    }

    Raid getRaid() {
        return _parentPresenter.getRaid();
    }

    // TODO refactor
    String exportPodiumAsHtml( VisualizationType visualizationType,
                               Parcours parcours,
                               Categorie categorie,
                               Etape etape,
                               String epreuveName,
                               JTable table,
                               int podiumValue ) {
        return HtmlWriter.exportPodium(
                _parentPresenter.getGlobalSettings().getWorkingDirectoryPath(), getRaid().getRaidName(),
                parcours.getName(), categorie.getShortName(), etape.getName(), epreuveName, table, podiumValue, visualizationType, _parentPresenter.getGlobalSettings().isResultAutoScrollingEnabled(),
                _parentPresenter.getGlobalSettings().getAutoScrollingPauseDurationInS());
    }

    String exportResultsAsHtml( VisualizationType visualizationType,
                                String parcoursName,
                                String categorieName,
                                String etapeName,
                                String epreuveName,
                                JTable resultTable ) {
        return HtmlWriter.exportResults(_parentPresenter.getGlobalSettings().getWorkingDirectoryPath(),
                getRaid().getRaidName(),
                visualizationType,
                parcoursName,
                categorieName,
                etapeName,
                epreuveName,
                resultTable,
                _parentPresenter.getGlobalSettings().isResultAutoScrollingEnabled(),
                _parentPresenter.getGlobalSettings().getAutoScrollingPauseDurationInS());
    }

    String refreshResults( boolean isParcoursResultSelected ) {
        String outputFilename = HtmlWriter.refreshResults(
                _parentPresenter.getGlobalSettings().getWorkingDirectoryPath(),
                getRaid(),
                isParcoursResultSelected,
                _parentPresenter.getGlobalSettings().isResultAutoScrollingEnabled(),
                _parentPresenter.getGlobalSettings().getAutoScrollingPauseDurationInS());
        new SnackBar(L10n.getString("result_view.refresh.button.message", outputFilename));
        return "file:///" + outputFilename;
    }

    void printResults( JTable table,
                       VisualizationType visualizationType,
                       Parcours parcours,
                       Categorie categorie,
                       Etape etape,
                       Epreuve epreuve ) throws PrinterException {
        HtmlWriter.printResults(
                getRaid().getRaidName(),
                table,
                visualizationType,
                parcours.getName(),
                categorie.getShortName(),
                etape.getName(),
                epreuve.getName(),
                _parentPresenter.getGlobalSettings().isShortGlobalResultPrinting());
    }

    void exportResultAsCSV( String filePath,
                            JTable table, // TODO passer tableModel directement ?
                            VisualizationType visualizationType ) {
        // TODO Mettre tout cela dans une classe du modèle
        final String fichier = Utils.checkExtension(filePath, ".csv");
        switch (visualizationType) {
            case ETAPE:
                CsvWriter.export((EtapeTableModel) table.getModel(), fichier);
                break;
            case PARCOURS:
                CsvWriter.export((ParcoursTableModel) table.getModel(), fichier);
                break;
            case EPREUVE:
                CsvWriter.export((EpreuveTableModel) table.getModel(), fichier);
                break;
        }
    }
}