package fr.gecoraid.desktop;

import fr.gecoraid.L10n;
import fr.gecoraid.SessionLauncher;
import fr.gecoraid.desktop.equipe_panel.EquipePanelView;
import fr.gecoraid.desktop.file_panel.FilePanelView;
import fr.gecoraid.desktop.raid_file.RaidFileXmlReader;
import fr.gecoraid.desktop.raid_panel.RaidPanelView;
import fr.gecoraid.desktop.result_panel.ResultView;
import fr.gecoraid.desktop.sportident_panel.SportIdentCardPanel;
import fr.gecoraid.desktop.widget.ErrorPane;
import fr.gecoraid.model.Balise;
import fr.gecoraid.model.Categorie;
import fr.gecoraid.model.Epreuve;
import fr.gecoraid.model.Equipe;
import fr.gecoraid.model.Etape;
import fr.gecoraid.model.Parcours;
import fr.gecoraid.model.TypeTri;
import fr.gecoraid.tools.ExtensionFileFilter;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JComboBox;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingUtilities;
import javax.swing.WindowConstants;
import java.awt.BorderLayout;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintStream;

import static fr.gecoraid.GeCoRaid.SOFTWARE_NAME;

// TODO mettre une visibilite package
public class DesktopView extends JFrame {
    private static final long serialVersionUID = 1L;

    private final transient DeskTopPresenter _presenter;
    private String _titre = "GeCoRaid ";

    private TypeTri _typeTriCourant = TypeTri.DOSSARD;
    private transient PrintStream _printStreamLog;

    // TODO Graphisme -> séparer la partie construction du raid de la partie execution du raid (2 onglets) ?
    // TODO Lancer un bip en cas d'erreurs
    private EquipePanelView _equipePanelView;
    private FilePanelView _filePanelView;

    private final JList<Epreuve> _jListEpreuves = new JList<>();
    private final JComboBox<Parcours> _comboBoxParcours = new JComboBox<>();
    private final JList<Equipe> _listEquipes = new JList<>();
    private final JList<Balise> _listBalises = new JList<>();
    private final JComboBox<Etape> _comboBoxEtapes = new JComboBox<>();
    private ResultView _panelResultats;
    private SportIdentCardPanel _sportIdentPanel;
    private final JComboBox<Categorie> _comboBoxCategorie = new JComboBox<>();

    public DesktopView() {
        _presenter = new DeskTopPresenter(this);
        _initializeUI();
        _titre = _titre + SessionLauncher.getVersion();
        _comboBoxParcours.setModel(new DefaultComboBoxModel<>(_presenter.getRaid().getParcourss().getParcourss()));
        pack();
    }

    public DeskTopPresenter getPresenter() {
        return _presenter;
    }

    public void setEnregistrementOk() {
        _filePanelView.setSavingOkState();
    }

    public void setEnregistrementNormal() {
        _filePanelView.setSavingNormalState();
    }

    public Parcours getSelectedParcours() {
        return (Parcours) _comboBoxParcours.getSelectedItem();
    }


    public JList<Epreuve> getListeEpreuves() {
        return _jListEpreuves;
    }

    public JComboBox<Parcours> getComboBoxParcours() {
        return _comboBoxParcours;
    }

    public JList<Equipe> getListEquipes() {
        return _listEquipes;
    }

    public Etape getSelectedEtape() {
        return (Etape) _comboBoxEtapes.getSelectedItem();
    }

    public JComboBox<Etape> getEtapeCombox() {
        return _comboBoxEtapes;
    }

    public JList<Balise> getListeBalises() {
        return _listBalises;
    }

    public TypeTri getTypeTriCourant() {
        return _typeTriCourant;
    }

    public void setTypeTriCourant( TypeTri value ) {
        _typeTriCourant = value;
    }

    public String getSelectedPort() {
        // TODO demander au presenter
        return _sportIdentPanel.getSelectedPort();
    }

    public void initialise() {
        _initialiseLog();
        _presenter.loadGlobalSettings();
        _presenter.displayRecentRaidFiles();
    }

    public void displayStationStatus( SportIdentStationState status ) {
        if (_sportIdentPanel.isSportIdentReaderEnabled()) {
            if (status == SportIdentStationState.READY) {
                _sportIdentPanel.changeStateToStop();
            } else if (status == SportIdentStationState.FAILED) {
                // TODO L10n.getString()
                ErrorPane.showMessageDialog("Impossible de se connecter à la station maître.\nSoit la station n'est pas branchée\nsoit elle n'a pas encore eu le temps de s'initialiser.");
                // TODO a revoir en utilisant _sportIdentPanel.changeStateToStart() ???
                SwingUtilities.invokeLater(() -> _sportIdentPanel.activateSportIdentButton());
            }
        } else if (_presenter.getSportIdentReaderPuceEnCours()) {
            if (status == SportIdentStationState.READY) {
                _equipePanelView.changeCardReaderIcon("stop.png");
            } else if (status == SportIdentStationState.FAILED) {
                // TODO L10n.getString()
                ErrorPane.showMessageDialog("Impossible de se connecter à la station maître.\nSoit la station n'est pas branchée\nsoit elle n'a pas encore eu le temps de s'initialiser.");
                // TODO a revoir en utilisant le presenter
                SwingUtilities.invokeLater(() -> _equipePanelView.readSportIdentCard());
            }
        }
    }

    public void updateEquipeList() {
        assert (_comboBoxParcours.getSelectedItem() != null);
        _listEquipes.setListData(((Parcours) _comboBoxParcours.getSelectedItem()).getEquipes().getEquipes());
        if (_listEquipes.getModel().getSize() > -1) {
            _listEquipes.setSelectedIndex(0);
        }
    }

    public static void beep() {
        Toolkit.getDefaultToolkit().beep();
    }

    public void updateTable() {
        _panelResultats.updateTable();
    }

    private void _initialiseLog() {
        // TODO mettre dans le dossier var/log/.gecoraid ? Garder ?
        File fileLog = new File("GeRaidLog.txt");
        try {
            _printStreamLog = new PrintStream(fileLog);
            System.setOut(_printStreamLog);
        } catch (FileNotFoundException e) {
            //  Nothing to do
        }
    }

    private void _initializeUI() {
        setSize(1069, 727);
        setMinimumSize(new Dimension(820, 400));
        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getClassLoader().getResource("fr/gecoraid/icon/logo24.png")));
        setJMenuBar(new DesktopMenu(this, _presenter));
        setContentPane(getJContentPane());
        setTitle(SOFTWARE_NAME);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing( WindowEvent event ) {
                _presenter.quit();
            }
        });
    }

    public int quitConfirmationDialog() {
        if (_presenter.isModified()) {
            return JOptionPane.showConfirmDialog(null,
                    // TODO L10n.getString()
                    "Souhaitez-vous enregistrer le raid\u00A0?",
                    // TODO L10n.getString()
                    "Enregistrer",
                    JOptionPane.YES_NO_CANCEL_OPTION,
                    JOptionPane.QUESTION_MESSAGE);
        } else {
            return JOptionPane.NO_OPTION;
        }
    }

    public void ouvrirFichierRaid() {
        // TODO FAire une valeur de retour pour gérer le cancel
        JFileChooser chooser = new JFileChooser();
        ExtensionFileFilter filter = new ExtensionFileFilter("grd", L10n.getString("grd_file.description"));
        chooser.setFileFilter(filter);
        chooser.setCurrentDirectory(new File(_presenter.getRaidFolderPath()));
        int returnVal = chooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            _openRaidFile(chooser.getSelectedFile().getAbsolutePath());
            _presenter.setModified(true);
        } else if (returnVal != JFileChooser.CANCEL_OPTION) {
            // TODO regrouper avec presenter.creerNouveauFichierRaid
            // TODO Faire une méthode commune resetRaid();
            _presenter.getRaid().clear();
            _presenter.getRaid().setCategories(_presenter.getGlobalSettings().getCategoriesTemplate());
            // TODO L10n.getString()
            _presenter.getRaid().setRaidName("Nouveau Raid");
            updateFrameTitle();
            updateComboxParcours();
            updateTable();
            _comboBoxParcours.repaint();
            _comboBoxParcours.setSelectedIndex(-1);
            _presenter.setModified(false);
        }
    }

    protected void _openRaidFile( String filename ) {
        _presenter.getRaid().clear();
        _presenter.getRaid().setSavingFile(filename);
        RaidFileXmlReader.readRaidFile(_presenter.getRaid(), _presenter.getRaid().getSavingFile());
        updateFrameTitle();
        _panelResultats.fillCategoryComboBox();
        if (_presenter.getRaid().getParcourss().getSize() > 0) {
            _comboBoxParcours.setSelectedIndex(0);
        } else {
            _comboBoxParcours.setSelectedIndex(-1);
        }
        refreshResultTable();
    }

    private JScrollPane getJScrollPane() {
        JScrollPane jScrollPane = new JScrollPane();
        jScrollPane.setPreferredSize(new Dimension(0, 0));
        jScrollPane.setViewportView(getJPanel());
        return jScrollPane;
    }

    private JPanel getJPanelNord() {
        FlowLayout flowLayout = new FlowLayout();
        flowLayout.setAlignment(FlowLayout.LEFT);
        JPanel jPanelNord = new JPanel();
        jPanelNord.setLayout(flowLayout);
        jPanelNord.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        jPanelNord.add(getJPanelParcours(), null);
        return jPanelNord;
    }

    private JPanel getJPanel() {
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        mainPanel.setVisible(true); // TODO ?
        mainPanel.setSize(new Dimension(0, 0));
        mainPanel.setPreferredSize(new Dimension(0, 0));
        mainPanel.add(getJPanelNord(), BorderLayout.NORTH);
        mainPanel.add(getSplitPane(), BorderLayout.CENTER);
        return mainPanel;
    }

    private JSplitPane _createRaidTabbedPane() {
        _equipePanelView = new EquipePanelView(this,
                _presenter,
                _listEquipes);
        RaidPanelView raidPanelView = new RaidPanelView(this,
                _presenter,
                _listEquipes,
                _comboBoxParcours,
                _jListEpreuves,
                _comboBoxEtapes,
                _listBalises);
        JSplitPane pane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT,
                _equipePanelView,
                raidPanelView);
        pane.setPreferredSize(new Dimension(800, 300));
        pane.setOneTouchExpandable(true);
        pane.setContinuousLayout(true);
        pane.setDividerSize(10);
        pane.setDividerLocation(540);
        return pane;
    }


    private JPanel getJPanelParcours() {
        FlowLayout flowLayout6 = new FlowLayout();
        flowLayout6.setAlignment(FlowLayout.LEFT);
        JPanel jPanelParcours = new JPanel();
        jPanelParcours.setLayout(flowLayout6);
        _filePanelView = new FilePanelView(_presenter);
        jPanelParcours.add(_filePanelView);
        _sportIdentPanel = new SportIdentCardPanel(_presenter);
        jPanelParcours.add(_sportIdentPanel);
        return jPanelParcours;
    }

    private JPanel getJContentPane() {
        JPanel jContentPane = new JPanel(new BorderLayout());
        jContentPane.add(getJScrollPane(), BorderLayout.CENTER);
        return jContentPane;
    }

    public void updateFrameTitle() {
        setTitle(_titre
                + " - "
                + _presenter.getGlobalSettings().getClubName()
                + " - "
                + _presenter.getRaid().getRaidName());
    }

    private JSplitPane getSplitPane() {
        JSplitPane splitPane = new JSplitPane(JSplitPane.VERTICAL_SPLIT, _createRaidTabbedPane(), _createResultTabbedPane());
        splitPane.setOneTouchExpandable(true);
        splitPane.setContinuousLayout(true);
        splitPane.setOrientation(JSplitPane.VERTICAL_SPLIT);
        splitPane.setDividerSize(10);
        splitPane.setDividerLocation(400);
        return splitPane;
    }

    public void refreshResultTable() {
        _sportIdentPanel.setTime(_presenter.getRaid().getZeroTime());
        _panelResultats.refreshResultTable();
    }

    public void setButtonSiReaderEnabled( boolean b ) {
        _sportIdentPanel.setSportIdentReaderButtonEnabled(b);
    }

    public EquipePanelView getEquipePanel() {
        return _equipePanelView;
    }

    private JPanel _createResultTabbedPane() {
        _panelResultats = new ResultView(this,
                _presenter,
                _comboBoxParcours,
                _jListEpreuves,
                _comboBoxEtapes,
                _comboBoxCategorie);
        return _panelResultats;
    }

    public void finish() {
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        _printStreamLog.close();
        dispose();
    }

    public void cancelQuit() {
        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
    }

    public void updateListEquipe() {
        _listEquipes.repaint();
    }

    public void updateComboxParcours() {
        _comboBoxParcours.setSelectedIndex(-1);
        _comboBoxParcours.repaint();
    }

    public void setEnabledButtonReaderPuce( boolean b ) {
        _equipePanelView.setEnabledChipReaderButton(b);
    }
}
