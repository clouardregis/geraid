package fr.gecoraid;

import javax.swing.UIManager;
import java.awt.Color;

public class NimbusTheme {
    private NimbusTheme() {
    }

    public static final Color GREEN_1 = new Color(56, 135, 128);
    public static final Color GREEN_2 = new Color(167, 190, 157);

    public static final Color BUTTER_1 = new Color(255, 236, 82);
    public static final Color BUTTER_3 = new Color(237, 212, 0);

    public static final Color ALUMINIUM_1 = new Color(242, 246, 242);

    public static final Color ALUMINIUM_5 = new Color(85, 87, 83);

    public static void loadTheme() {
        UIManager.put("nimbusBase", GREEN_1);
        UIManager.put("nimbusBlueGrey", GREEN_2);
        UIManager.put("control", ALUMINIUM_1);
        UIManager.put("nimbusSelection", GREEN_1);
        UIManager.put("nimbusSelectionBackground", GREEN_1);
        UIManager.put("nimbusSelectedText", Color.WHITE);
        UIManager.put("nimbusLightBackground", BUTTER_1);

        UIManager.put("MenuItem:MenuItemAccelerator.textForeground", Color.gray);
    }
}
