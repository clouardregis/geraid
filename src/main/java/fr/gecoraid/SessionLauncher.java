package fr.gecoraid;

import fr.gecoraid.desktop.DeskTopPresenter;
import fr.gecoraid.desktop.DesktopView;
import fr.gecoraid.desktop.setting_menu.PlatformSettingsHandler;
import fr.gecoraid.desktop.widget.ErrorPane;
import fr.gecoraid.model.Raid;

import javax.swing.ImageIcon;
import javax.swing.WindowConstants;
import java.awt.Frame;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;

public class SessionLauncher {
    private SessionLauncher() {
    }

    public static String getVersion() {
        try {
            final InputStream inputStream = Raid.class.getResourceAsStream("/fr/gecoraid/version.txt");
            assert inputStream != null;
            try (BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream))) {
                return reader.readLine();
            }
        } catch (IOException e) {
            return "0.0.0";
        }
    }

    static void init() {
        final DesktopView desktop = new DesktopView();
        desktop.setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        desktop.setExtendedState(Frame.MAXIMIZED_BOTH);

        _configureGlobalSettingsIfNeeded(desktop);
        desktop.setVisible(true);
        desktop.initialise();

        //noinspection ConstantConditions
        desktop.setIconImages(Arrays.asList(
                (new ImageIcon(desktop.getClass().getClassLoader().getResource("fr/gecoraid/icon/logo24.png"))).getImage(),
                (new ImageIcon(desktop.getClass().getClassLoader().getResource("fr/gecoraid/icon/logo32.png"))).getImage(),
                (new ImageIcon(desktop.getClass().getClassLoader().getResource("fr/gecoraid/icon/logo64.png"))).getImage(),
                (new ImageIcon(desktop.getClass().getClassLoader().getResource("fr/gecoraid/icon/logo128.png"))).getImage()
        ));
    }

    private static void _configureGlobalSettingsIfNeeded( DesktopView desktop ) {
        final DeskTopPresenter desktopPresenter = desktop.getPresenter();
        if (!PlatformSettingsHandler.isExistGlobalSettingsDirectory()) {
            try {
                PlatformSettingsHandler.createGlobalSettingsDirectoryIfNeeded();
                boolean cancelled = desktopPresenter.configureGlobalSettings();
                if (cancelled) {
                    System.exit(0);
                }
            } catch (IOException e) {
                final String directory = PlatformSettingsHandler.getGlobalSettingsDirectory();
                ErrorPane.showMessageDialog(L10n.getString("session_launcher.error_config_directory", directory));
                System.exit(0);
            }
        }
    }
}
