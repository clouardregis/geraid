package fr.gecoraid;

import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import java.util.Locale;

public class GeCoRaid {
    public static final String SOFTWARE_NAME = "GeCoRaid";

    public static void main( String[] args ) {
        Locale locale = new Locale.Builder()
                .setLanguage("fr")
                .setRegion("FR")
                .build();
        L10n.setLocale(locale);
        setLookAndFeel();
        SwingUtilities.invokeLater(SessionLauncher::init);
    }

    private static void setLookAndFeel() {
        if (!UIManager.getLookAndFeel().getID().equals("Aqua")) {
            // try to use Nimbus unless on macOS
            try {
                for (UIManager.LookAndFeelInfo info : UIManager.getInstalledLookAndFeels()) {
                    if ("Nimbus".equals(info.getName())) {
                        NimbusTheme.loadTheme();
                        UIManager.setLookAndFeel(info.getClassName());
                        break;
                    }
                }
            } catch (Exception ignored) {
                // Use the default Look and Feel
            }
        }
    }
}