package fr.gecoraid;

import javax.swing.UIManager;
import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public class L10n {
    private L10n() {
    }

    private static ResourceBundle _language;

    public static String getString( final String key ) {
        return _language.getString(key);
    }

    public static String getString( final String key, Object... arguments ) {
        final String pattern = _language.getString(key);
        final String filteredPattern = pattern.replace("'", "''");
        return MessageFormat.format(filteredPattern, arguments);
    }

    public static void setLocale( Locale locale ) {
        Locale.setDefault(locale);
        UIManager.getDefaults().setDefaultLocale(Locale.FRANCE);
        _language = ResourceBundle.getBundle("messages", locale);
        setDefaultLocaleToSwing(_language);
    }

    static void setDefaultLocaleToSwing( ResourceBundle language ) {
        UIManager.put("OptionPane.yesButtonText", language.getString("optionpane.yesButtonText"));
        UIManager.put("OptionPane.noButtonText", language.getString("optionpane.noButtonText"));
        UIManager.put("OptionPane.cancelButtonText", language.getString("optionpane.cancelButtonText"));
        UIManager.put("OptionPane.okButtonText", language.getString("optionpane.okButtonText"));
        UIManager.put("FileChooser.openDialogTitleText", language.getString("filechooser.openDialogTitleText"));
        UIManager.put("FileChooser.saveDialogTitleText", language.getString("filechooser.saveDialogTitleText"));
        UIManager.put("FileChooser.lookInLabelText", language.getString("filechooser.lookInLabelText"));
        UIManager.put("FileChooser.filesOfTypeLabelText", language.getString("filechooser.filesOfTypeLabelText"));
        UIManager.put("FileChooser.lookInLabelText", language.getString("filechooser.lookInLabelText"));
        UIManager.put("FileChooser.upFolderToolTipText", language.getString("filechooser.upFolderToolTipText"));
        UIManager.put("FileChooser.fileNameLabelText", language.getString("filechooser.fileNameLabelText"));
        UIManager.put("FileChooser.homeFolderToolTipText", language.getString("filechooser.homeFolderToolTipText"));
        UIManager.put("FileChooser.newFolderToolTipText", language.getString("filechooser.newFolderToolTipText"));
        UIManager.put("FileChooser.listViewButtonToolTipText", language.getString("filechooser.listViewButtonToolTipText"));
        UIManager.put("FileChooser.detailsViewButtonToolTipText", language.getString("filechooser.detailsViewButtonToolTipText"));
        UIManager.put("FileChooser.saveButtonText", language.getString("filechooser.saveButtonText"));
        UIManager.put("FileChooser.openButtonText", language.getString("filechooser.openButtonText"));
        UIManager.put("FileChooser.cancelButtonText", language.getString("filechooser.cancelButtonText"));
        UIManager.put("FileChooser.saveButtonToolTipText", language.getString("filechooser.saveButtonToolTipText"));
        UIManager.put("FileChooser.openButtonToolTipText", language.getString("filechooser.openButtonToolTipText"));
        UIManager.put("FileChooser.cancelButtonToolTipText", language.getString("filechooser.cancelButtonToolTipText"));
        UIManager.put("FileChooser.fileNameHeaderText", language.getString("filechooser.fileNameHeaderText"));
        UIManager.put("FileChooser.fileSizeHeaderText", language.getString("filechooser.fileSizeHeaderText"));
        UIManager.put("FileChooser.fileTypeHeaderText", language.getString("filechooser.fileTypeHeaderText"));
        UIManager.put("FileChooser.fileDateHeaderText", language.getString("filechooser.fileDateHeaderText"));
        UIManager.put("FileChooser.newFolderButtonText", language.getString("filechooser.newFolderButtonText"));
        UIManager.put("FileChooser.acceptAllFileFilterText", language.getString("filechooser.acceptAllFileFilterText"));
        UIManager.put("FileChooser.openDialogTitleText", language.getString("filechooser.openDialogTitleText"));
    }
}