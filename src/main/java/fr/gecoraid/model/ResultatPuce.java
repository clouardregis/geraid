package fr.gecoraid.model;

import fr.gecoraid.tools.TimeManager;

import java.util.Vector;

// TODO refactor

public class ResultatPuce implements Cloneable {
    private Parcours _parcours = null;
    private Etape _etape = null;
    private Equipe _equipe = null;
    private Puce _puce = new Puce();

    public int getPositionPoste( int code ) {
        return _puce.getPositionPoste(code);
    }

    public Vector<String> getTemps() {
        return _puce.getTemps();
    }

    public Vector<Integer> getCodes() {
        return _puce.getCodes();
    }

    public boolean existeCode( int code ) {
        for (int i = 0; i < getCodes().size(); i++) {
            if (getCodes().get(i) == code) {
                return true;
            }
        }
        return false;
    }

    public Parcours getParcours() {
        return _parcours;
    }

    public void setParcours( Parcours parcours ) {
        _parcours = parcours;
    }

    public Etape getEtape() {
        return _etape;
    }

    public void setEtape( Etape etape ) {
        _etape = etape;
    }

    public Equipe getEquipe() {
        return _equipe;
    }

    public void setEquipe( Equipe equipe ) {
        _equipe = equipe;
    }

    public Puce getPuce() {
        return _puce;
    }

    public void setPuce( Puce puce ) {
        _puce = puce;
    }

    public String toCSV() {
        StringBuilder tampon = new StringBuilder(_parcours.getName())
                .append(";")
                .append(_etape.getName())
                .append(";")
                .append(_equipe.getDossard())
                .append(";")
                .append(_equipe.getNom())
                .append(";")
                .append(_equipe.getCategorie().getShortName())
                .append(";")
                .append(_equipe.getIdPuce())
                .append(";")
                .append(TimeManager.fullTime(_puce.getStarttime().getTime()))
                .append(";")
                .append(TimeManager.fullTime(_puce.getFinishtime().getTime()));
        for (int i = 0; i < _puce.getPartiels().length; i++) {
            tampon.append(";")
                    .append(_puce.getPartiels()[i].getCode())
                    .append(";")
                    .append(TimeManager.fullTime(_puce.getPartiels()[i].getTime()));
        }
        return tampon.toString();
    }
}
