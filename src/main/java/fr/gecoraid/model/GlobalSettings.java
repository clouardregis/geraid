package fr.gecoraid.model;

import fr.gecoraid.L10n;

// TODO Revoir les visibilites
public class GlobalSettings {
    private boolean _isShortIndividualResultPrinting = true;
    private boolean _isShortGlobalResultPrinting = false;
    private boolean _resultAutoScrolling = false;
    private int _autoScrollingSpeedInS = 20;
    private String _clubName = L10n.getString("settings.club_name.template");
    private String _raidDirectoryPath = "";
    private String _workingDirectoryPath = "";
    private Categories _categoriesTemplate;
    private int _penaltyPointsDefault = 0;
    private int _penaltyTimeDefault = 0;
    private int _mispunchPenaltyPoints = 0;
    private int _mispunchPenaltyTimeInS = 0;

    public String getResultAutoScrollingAsInt() {
        return _resultAutoScrolling ? "1" : "0";
    }

    public String getIsShortIndividualResultPrintingAsInt() {
        return _isShortIndividualResultPrinting ? "1" : "0";
    }

    public String getIsShortGlobalResultPrintingAsInt() {
        return _isShortGlobalResultPrinting ? "1" : "0";
    }

    public String getClubName() {
        return _clubName;
    }

    public void setClubName( String clubName ) {
        _clubName = clubName;
    }

    public String getRaidFolderPath() {
        return _raidDirectoryPath;
    }

    public void setRaidDirectoryPath( String raidDirectoryPath ) {
        _raidDirectoryPath = raidDirectoryPath;
    }

    public String getWorkingDirectoryPath() {
        return _workingDirectoryPath;
    }

    public void setWorkingDirectoryPath( String dossier ) {
        _workingDirectoryPath = dossier;
    }

    public Categories getCategoriesTemplate() {
        if (_categoriesTemplate == null) {
            _createDefaultSettingsFile();
        }
        return _categoriesTemplate;
    }

    public void setCategoriesTemplate( Categories categories ) {
        _categoriesTemplate = categories;
    }

    public int getAutoScrollingPauseDurationInS() {
        return _autoScrollingSpeedInS * 1000;
    }

    public int getAutoScrollingSpeedInS() {
        return _autoScrollingSpeedInS;
    }

    public void setAutoScrollingSpeedInS( int speedInS ) {
        _autoScrollingSpeedInS = speedInS;
    }

    public int getPenaltyPointsDefault() {
        return _penaltyPointsDefault;
    }

    public void setPenaltyPointsDefault( int points ) {
        _penaltyPointsDefault = points;
    }

    public int getPenaltyTimeDefault() {
        return _penaltyTimeDefault;
    }

    public void setPenaltyTimeDefault( int temps ) {
        _penaltyTimeDefault = temps;
    }

    public int getMispunchPenaltyPoints() {
        return _mispunchPenaltyPoints;
    }

    public void setMispunchPenaltyPoints( int points ) {
        _mispunchPenaltyPoints = points;
    }

    public int getMispunchPenaltyTimeInS() {
        return _mispunchPenaltyTimeInS;
    }

    public void setMispunchPenaltyTimeInS( int temps ) {
        _mispunchPenaltyTimeInS = temps;
    }

    public boolean isResultAutoScrollingEnabled() {
        return _resultAutoScrolling;
    }

    public void setResultAutoScrolling( boolean value ) {
        _resultAutoScrolling = value;
    }

    public boolean isShortIndividualResultPrinting() {
        return _isShortIndividualResultPrinting;
    }

    public void setShortIndividualResultPrinting( boolean rr ) {
        _isShortIndividualResultPrinting = rr;
    }

    public boolean isShortGlobalResultPrinting() {
        return _isShortGlobalResultPrinting;
    }

    public void setShortGlobalResultPrinting( boolean value ) {
        _isShortGlobalResultPrinting = value;
    }

    private void _createDefaultSettingsFile() {
        _categoriesTemplate = new Categories();
        _categoriesTemplate.addCategorie(new Categorie(L10n.getString("settings.category.man"), "MAS"));
        _categoriesTemplate.addCategorie(new Categorie(L10n.getString("settings.category.woman"), "FEM"));
        _categoriesTemplate.addCategorie(new Categorie(L10n.getString("settings.category.mixed"), "MIX"));
    }
}
