package fr.gecoraid.model;

import fr.gecoraid.desktop.widget.ErrorPane;

import javax.swing.JOptionPane;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Vector;

// TODO mettre une visibilite package -> voir pourquoi on ne peut pas
// TODO refactor code
public class CsvEpreuves {
    public static void exporter( Epreuves es, String fichier ) {
        File chemin = new File(fichier);
        try (Writer monFichier = new OutputStreamWriter(Files.newOutputStream(chemin.toPath()), StandardCharsets.UTF_8)) {
            String tampon = "";
            monFichier.write(tampon, 0, tampon.length());
            //monFichier . newLine ( ) ;
            for (int i = 0; i < es.getSize(); i++) {
                tampon = es.getEpreuves().get(i).toCSV();
                monFichier.write(tampon, 0, tampon.length());
                monFichier.write(System.lineSeparator());
            }
        } catch (IOException e) {
            // TODO L10n.getString()
            JOptionPane.showMessageDialog(null, "Erreur d'écriture du fichier d'export : " + e.getClass().getName() + ", " + e.getMessage());
        }
    }

    public static void importer( GlobalSettings globalSettings, Etape p, String fichier ) {
        File chemin = new File(fichier);
        String chaine;
        String[] tampon;
        Vector<Integer> lignes = new Vector<>();
        int ligne = 1;
        Vector<String> codes = new Vector<>();

        try {
            if (!chemin.exists()) {
                chemin.createNewFile();
            }
            BufferedReader monFichier = new BufferedReader(new FileReader(chemin));
            //monFichier . readLine ( );
            while ((chaine = monFichier.readLine()) != null) {
                ligne++;
                tampon = chaine.trim().split(";");
                if (tampon.length > 0 && !tampon[0].isEmpty() && !p.existeNomEpreuve(tampon[0])) {
                    Epreuve eq = new Epreuve();
                    eq.setNom(tampon[0]);
                    for (int i = 1; i < tampon.length; i++) {
                        try {
                            Balise balise = new Balise(Integer.parseInt(tampon[i]));
                            balise.setPoints(globalSettings.getPenaltyPointsDefault());
                            balise.setTemps(globalSettings.getPenaltyTimeDefault());
                            balise.setPointsPosteManquant(globalSettings.getMispunchPenaltyPoints());
                            balise.setTempsPosteManquant(globalSettings.getMispunchPenaltyTimeInS());
                            eq.getBalises().addBalise(balise);
                        } catch (NumberFormatException e) {
                            codes.add(tampon[i]);
                        }
                    }
                    p.getEpreuves().addEpreuve(eq);
                } else {
                    lignes.add(ligne);
                }
            }
            monFichier.close();
            if (!lignes.isEmpty()) {
                // TODO L10n.getString()
                StringBuilder message = new StringBuilder("Certaines épreuves n'ont pu être importées :\nLignes ");
                for (Integer integer : lignes) {
                    message.append(integer).append(",");
                }
                // TODO L10n.getString()
                message.append("\nVérifiez que ces épreuves ont un nom ou qu'elles n'existent pas déjà.\n");
                if (!codes.isEmpty()) {
                    // TODO L10n.getString()
                    message.append("\nLes codes suivants ne sont pas conformes (31 à 255) :\n");
                }
                for (String code : codes) {
                    message.append(code).append(",");
                }
                // TODO L10n.getString()
                JOptionPane.showMessageDialog(null, message.toString(), "Import des épreuves", JOptionPane.OK_OPTION);
            }
        } catch (IOException e) {
            // TODO L10n.getString()
            ErrorPane.showMessageDialog("Erreur lors de l'import : " + e.getClass().getName() + ", " + e.getMessage());
        }
    }
}
