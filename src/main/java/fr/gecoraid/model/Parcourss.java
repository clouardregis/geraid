package fr.gecoraid.model;

import java.util.Vector;

// TODO refactor -> SOLID
public class Parcourss {
    private final Vector<Parcours> _parcourss = new Vector<>();

    public Vector<Parcours> getParcourss() {
        return _parcourss;
    }

    public void addParcours( Parcours p ) {
        if (_absenceNom(p.getName())) {
            _parcourss.add(p);
        }
    }

    public boolean existeParcours( String nom, Parcours parcours ) {
        for (Parcours parcourss : _parcourss) {
            if (parcourss.getName().toLowerCase().compareTo(nom.trim().toLowerCase()) == 0
                    && !parcourss.equals(parcours)) {
                return true;
            }
        }
        return false;
    }

    public boolean existeParcoursAvecMemeCasse( String nom, Parcours parcours ) {
        for (Parcours parcourss : _parcourss) {
            if (parcourss.getName().compareTo(nom.trim()) == 0
                    && (!parcourss.equals(parcours))) {
                return true;

            }
        }
        return false;
    }

    public void removeParcours( Parcours p ) {
        _parcourss.remove(p);
    }

    public Parcours getParcoursDeNom( String nom ) {
        for (Parcours parcourss : _parcourss) {
            if (parcourss.getName().compareTo(nom) == 0) {
                return parcourss;
            }
        }
        return null;
    }

    public int getSize() {
        return _parcourss.size();
    }

    public String getNomAIndex( int i ) {
        return _parcourss.get(i).getName();
    }

    public void clearParcours() {
        _parcourss.clear();
    }

    public boolean addEquipe( Parcours p, Equipe e ) {
        return p.getEquipes().addEquipe(e);
    }

    public boolean existeIdPuce( String idPuce, Equipe e ) {
        for (Parcours parcourss : _parcourss) {
            if (parcourss.existeIdPuce(idPuce, e)) {
                return true;
            }
        }
        return false;
    }

    public boolean existePuce( String idPuce ) {
        for (Parcours parcourss : _parcourss) {
            if (parcourss.existeIdPuce(idPuce)) {
                return true;
            }
        }
        return false;
    }

    public Parcours getParcoursIdPuce( String idPuce ) {
        for (Parcours parcourss : _parcourss) {
            if (parcourss.existeIdPuce(idPuce)) {
                return parcourss;
            }
        }
        return null;
    }

    public String toStringParcours() {
        StringBuilder string = new StringBuilder();
        for (Parcours parcourss : _parcourss) {
            string.append(parcourss.toStringParcours());
        }
        return string.toString();
    }

    public Parcours getParcoursDeEpreuve( Epreuve epreuve ) {
        for (Parcours parcourss : _parcourss) {
            if (parcourss.existeEpreuve(epreuve)) {
                return parcourss;
            }
        }
        return null;
    }

    public Parcours getParcours( Equipe equipe ) {
        for (Parcours parcourss : _parcourss) {
            if (parcourss.existeEquipe(equipe)) {
                return parcourss;
            }
        }
        return null;
    }

    public Parcours getParcoursDeEtape( Etape etape ) {
        for (Parcours parcourss : _parcourss) {
            if (parcourss.existeEpreuve(etape)) {
                return parcourss;
            }
        }
        return null;
    }

    private boolean _absenceNom( String nom ) {
        for (Parcours parcourss : _parcourss) {
            if (parcourss.getName().compareTo(nom.trim()) == 0) {
                return false;
            }
        }
        return true;
    }
}
