package fr.gecoraid.model;

// TODO refactor -> SOLID
public class Equipe {
    private final Raid raid;
    private String _nom = "";
    private Categorie _categorie;
    private String _dossard = "";
    private String _idPuce = "";
    private boolean _nonClassee;
    private boolean _absent;
    private Raiders _raiders = new Raiders();

    public Equipe( Raid raid ) {
        this.raid = raid;
    }

    public boolean isNonClassee() {
        return _nonClassee;
    }

    public void setUnclassified( boolean nC ) {
        _nonClassee = nC;
    }

    public String getNonClasseeAsInteger() {
        return (isNonClassee()) ? "1" : "0";
    }

    public String getAbsenceAsInteger() {
        return (isAbsent()) ? "1" : "0";
    }

    public String getNonClasseeString() {
        return (isNonClassee()) ? "NC" : "";
    }

    public String getNomEtRaiders(  ) {
        StringBuilder retour = new StringBuilder(getNom());
        if (raid.isAfficherNomsEquipiers()) {
            for (int i = 0; i < _raiders.getSize(); i++) {
                retour.append("\n");
                retour.append(_raiders.getRaiders().get(i).getNom());
                retour.append(" ");
                retour.append(_raiders.getRaiders().get(i).getPrenom());
            }
        }
        return retour.toString();
    }

    public String getNom() {
        return _nom;
    }

    public void setNom( String nom ) {
        _nom = nom;
    }

    public Categorie getCategorie() {
        return _categorie;
    }

    public void setCategorie( Categorie categorie ) {
        _categorie = categorie;
    }

    public String getDossard() {
        return _dossard;
    }

    public void setDossard( String dossard ) {
        _dossard = dossard;
    }

    public String getIdPuce() {
        return _idPuce;
    }

    public void setIdPuce( String idPuce ) {
        _idPuce = idPuce;
    }

    public String toString() {
        StringBuilder retour = new StringBuilder(_dossard + " ");
        retour.append(_nom);
        retour.append(" ").append(_idPuce);
        if (_categorie != null) {
            retour.append(" ").append(_categorie.getShortName());
        }
        retour.append(" ").append(getNonClasseeString());
        if (isAbsent()) {
            retour.append(" (ABS)");
        }
        return retour.toString();
    }

    public String toCSV() {
        StringBuilder tampon = new StringBuilder(_dossard);
        tampon.append(";");
        tampon.append(_nom);
        tampon.append(";");
        tampon.append(_idPuce);
        tampon.append(";");
        tampon.append(_categorie.getShortName());
        for (int i = 0; i < _raiders.getSize(); i++) {
            tampon.append(";");
            tampon.append(_raiders.getRaiders().get(i).getNom());
            tampon.append(";");
            tampon.append(_raiders.getRaiders().get(i).getPrenom());
        }
        return tampon.toString();
    }

    public Raiders getRaiders() {
        return _raiders;
    }

    public void setRaiders( Raiders raiders ) {
        _raiders = raiders;
    }

    public boolean isAbsent() {
        return _absent;
    }

    public void setAbsent( boolean value ) {
        _absent = value;
    }

    public boolean isArrived( Parcours selectedParcours, Etape selectedEtape ) {
        return raid.isEquipeArrived(this, selectedParcours, selectedEtape);
    }
}
