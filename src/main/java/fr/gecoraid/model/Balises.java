package fr.gecoraid.model;

import java.util.List;
import java.util.Vector;

// TODO refactor -> SOLID
public class Balises implements Cloneable {
    private Vector<Balise> _balises = new Vector<>();

    public Object clone() {
        try {
            Balises p = (Balises) super.clone();
            p._balises = new Vector<>();
            for (Balise balise : _balises) {
                p.getBalises().add((Balise) balise.clone());
            }
            return p;
        } catch (CloneNotSupportedException cnse) {
            // Ne devrait jamais arriver car nous implémentons l'interface Cloneable
        }
        return null;
    }

    public int getTotalPointsBalises() {
        int retour = 0;
        for (Balise balise : _balises) {
            retour = retour + balise.getPoints();
        }
        return retour;
    }

    public Vector<Balise> getBalises() {
        return _balises;
    }

    public void setBalises( Vector<Balise> balises ) {
        _balises = balises;
    }

    public boolean addBalise( Balise e ) {
        _balises.add(e);
        return true;
    }

    public void removeBalise( Balise e ) {
        _balises.remove(e);
    }

    public int getSize() {
        return _balises.size();
    }

    public void upBalise( Balise b ) {
        int index = _balises.indexOf(b);
        if (index > 0) {
            _balises.insertElementAt(b, index - 1);
            _balises.removeElementAt(index + 1);
        }
    }

    public void downBalise( Balise b ) {
        int index = _balises.indexOf(b);
        if (index < _balises.size() - 1) {
            _balises.insertElementAt(b, index + 2);
            _balises.removeElementAt(index);
        }
    }

    public int getNumeroDerniereBalise() {
        if (getSize() > 0) {
            return _balises.get(getSize() - 1).getNumero();
        }
        return 30;
    }

    public int getNumeroPremiereBalise() {
        if (getSize() > 0) {
            return _balises.get(0).getNumero();
        }
        return 30;
    }

    public int getDernierNumero() {
        int retour = 30;
        for (Balise balise : _balises) {
            if (balise.getNumero() > retour) {
                retour = balise.getNumero();
            }
        }
        return retour;
    }

    public String toStringBalises() {
        StringBuilder retour = new StringBuilder("<ul>");
        for (Balise balise : _balises) {
            retour.append(balise.toStringBalise());
        }
        retour.append("</ul>");
        return retour.toString();
    }

    public void addBalises( List<Integer> list, int points, int temps, int pointsPM, int tempsPM ) {
        for (Integer numero : list) {
            Balise balise = new Balise(numero);
            balise.setPoints(points);
            balise.setTemps(temps);
            balise.setPointsPosteManquant(pointsPM);
            balise.setTempsPosteManquant(tempsPM);
            _balises.add(balise);
        }
    }
}
