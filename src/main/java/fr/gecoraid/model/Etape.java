package fr.gecoraid.model;

import fr.gecoraid.tools.TimeManager;

import java.awt.Point;
import java.util.Date;

/**
 * Une étape représente une division d'un parcours comportant plusieurs épreuves
 * à l'issue de laquelle il faut vider les pointeurs SPORTident.
 * Une étape est par exemple une journée sur un raid de plusieurs jours.
 */
// TODO refactor -> SOLID
public class Etape implements Cloneable {
    private String _nom = "";
    private boolean _fini = false;
    private Epreuves _epreuves = new Epreuves();
    private TypeDepart _type = TypeDepart.GROUPE;
    private Date _heureDepart = new Date(9 * 3600 * 1000L); // 9:00
    private TypeLimite _typeLimite = TypeLimite.SANSLIMITE;
    private Date _heureLimite = new Date(0);
    private Date _tempsLimite = new Date(0);
    private int _intervalPenalite = 5;
    private int _penalite = 0;
    private int _penaliteTemps = 0;
    private boolean _gelDansLimiteTemps = false;

    public String getName() {
        return _nom;
    }

    public TypeDepart getType() {
        return _type;
    }

    public void setType( TypeDepart type ) {
        _type = type;
    }

    public Date getHeureDepart() {
        return _heureDepart;
    }

    public void setHeureDepart( Date heureDepart ) {
        _heureDepart = heureDepart;
    }

    public void setNom( String nom ) {
        _nom = nom;
    }

    public int getPenaliteTemps() {
        return _penaliteTemps;
    }

    public void setPenaliteTemps( int penaliteTemps ) {
        _penaliteTemps = penaliteTemps;
    }

    public Object clone() {
        try {
            Etape p = (Etape) super.clone();
            p._epreuves = (Epreuves) _epreuves.clone();
            return p;
        } catch (CloneNotSupportedException cnse) {
            // Ne devrait jamais arriver car nous implémentons
            // l'interface Cloneable
        }
        return null;
    }

    public int getTotalPointsEtape() {
        return _epreuves.getTotalPointsEpreuves();
    }

    public boolean isFirstEpreuve( Epreuve e ) {
        return _epreuves.getEpreuves().get(0).equals(e); // TODO BUG get(0) alors que getEpreuves est vide
    }

    public boolean isLastEpreuve( Epreuve e ) {
        boolean retour = false;
        for (int i = 0; i < _epreuves.getSize(); i++) {
            if (_epreuves.getEpreuves().get(i).equals(e)) {
                if (i + 1 < _epreuves.getSize()) {
                    if (_epreuves.getEpreuves().get(i + 1).isHorsChrono()) {
                        return true;
                    }
                } else if (i + 1 == _epreuves.getSize()) {
                    return true;
                }
            }
        }
        return retour;
    }

    public int getfirstCodeNextEpreuve( Epreuve e ) {
        for (int i = 0; i < _epreuves.getSize(); i++) {
            if (_epreuves.getEpreuves().get(i).equals(e)) {
                return _epreuves.getEpreuves().get(i + 1).getBalises().getNumeroPremiereBalise();
            }
        }
        return 0;
    }

    public int getLastCodePreviousEpreuve( Epreuve e ) {
        for (int i = 0; i < _epreuves.getSize(); i++) {
            if (_epreuves.getEpreuves().get(i).equals(e)) {
                return _epreuves.getEpreuves().get(i - 1).getBalises().getNumeroDerniereBalise();
            }
        }
        return 0;
    }

    public Balises getBalisesEpreuveSuivante( Epreuve e ) {
        for (int i = 0; i < _epreuves.getSize(); i++) {
            if (_epreuves.getEpreuves().get(i).equals(e)
                    && (i + 1 < _epreuves.getSize())) {
                return _epreuves.getEpreuves().get(i + 1).getBalises();
            }
        }
        return null;
    }

    public Balises getBalisesEpreuvePrcedente( Epreuve e ) {
        for (int i = 0; i < _epreuves.getSize(); i++) {
            if (_epreuves.getEpreuves().get(i).equals(e)
                    && (i - 1 >= 0)) {
                return _epreuves.getEpreuves().get(i - 1).getBalises();
            }
        }
        return null;
    }

    public String toString() {
        StringBuilder retour = new StringBuilder(_nom);
        // TODO L10n.getString()
        if (_fini) {
            retour.append(" (terminé)");
        } else {
            retour.append(" (en cours)");
        }
        return retour.toString();
    }

    public Epreuves getEpreuves() {
        return _epreuves;
    }

    public void setEpreuves( Epreuves epreuves ) {
        _epreuves = epreuves;
    }

    public boolean isFini() {
        return _fini;
    }

    public String getFini() {
        if (isFini()) {
            return "1";
        }
        return "0";
    }

    public void setFini( boolean fini ) {
        _fini = fini;
    }

    public TypeLimite getTypeLimite() {
        return _typeLimite;
    }

    public void setTypeLimite( TypeLimite typeLimite ) {
        _typeLimite = typeLimite;
    }

    public Date getHeureLimite() {
        return _heureLimite;
    }

    public void setHeureLimite( Date heureLimite ) {
        _heureLimite = heureLimite;
    }

    public Date getTempsLimite() {
        return _tempsLimite;
    }

    public void setTempsLimite( Date tempsLimite ) {
        _tempsLimite = tempsLimite;
    }

    public int getIntervalPenalite() {
        return _intervalPenalite;
    }

    public void setIntervalPenalite( int intervalPenalite ) {
        _intervalPenalite = intervalPenalite;
    }

    public int getPenalite() {
        return _penalite;
    }

    public void setPenalite( int penalite ) {
        _penalite = penalite;
    }

    public boolean existeEpreuve( String nom, Epreuve e ) {
        return _epreuves.existeEpreuve(nom, e);
    }

    public boolean existeNomEpreuve( String nom ) {
        return _epreuves.existeNomEpreuve(nom);
    }

    public String toStringEtape() {
        // TODO L10n.getString()
        StringBuilder retour = new StringBuilder();
        retour.append("<li><i>Étape : ")
                .append(_nom)
                .append("<br>");
        retour.append("Départ : ")
                .append(_type.getValue());
        if (_type.equals(TypeDepart.GROUPE)) {
            retour.append(" à ")
                    .append(TimeManager.fullTime(_heureDepart));
        }
        retour.append("<br>");
        switch (_typeLimite) {
            case SANSLIMITE:
                retour.append("Sans limite");
                break;
            case AVECLIMITETEMPS:
                retour.append("Avec limite de temps à ")
                        .append(TimeManager.fullTime(_tempsLimite))
                        .append(" et pénalité de ")
                        .append(_penalite)
                        .append(" pts et ")
                        .append(TimeManager.fullTime(_penaliteTemps * 60000L))
                        .append(" par ")
                        .append(_intervalPenalite)
                        .append(" mn de dépassement");
                break;
            case AVECLIMITEHORAIRE:
                retour.append("Avec limite horaire à ")
                        .append(TimeManager.fullTime(_heureLimite))
                        .append(" et pénalité de ").append(_penalite)
                        .append(" pts et ")
                        .append(TimeManager.fullTime(_penaliteTemps * 60000L))
                        .append(" par ")
                        .append(_intervalPenalite)
                        .append(" mn de dépassement");
                break;
            default:
                break;
        }
        retour.append("</i><br>");
        retour.append(_epreuves.toStringEpreuves());
        retour.append("</li>");
        return retour.toString();
    }

    public boolean existeEpreuve( Epreuve epreuve ) {
        for (int i = 0; i < _epreuves.getSize(); i++) {
            if (_epreuves.getEpreuves().get(i).equals(epreuve)) {
                return true;
            }
        }
        return false;
    }

    public boolean isGelDansLimiteTemps() {
        return _gelDansLimiteTemps;
    }

    public String getGelDansLimiteTemps() {
        if (isGelDansLimiteTemps()) {
            return "1";
        }
        return "0";
    }

    public void setGelDansLimiteTemps( boolean gelDansLimiteTemps ) {
        _gelDansLimiteTemps = gelDansLimiteTemps;
    }

    public boolean estPresqueEgal( Etape etape ) {
        Point p = compare(_nom, etape.getName());
        double d = (double) p.y / p.x;
        return d == 1;

    }

    private Point compare( String nom1, String nom2 ) {
        Point retour = new Point(0, 0);
        if (nom1.length() > nom2.length()) {
            retour.x = nom1.length();
            retour.y = nom1.length();
        } else {
            retour.x = nom2.length();
            retour.y = nom2.length();
        }
        for (int i = 0; i < retour.x; i++) {
            if (nom1.charAt(i) != nom2.charAt(i)) {
                retour.y = i;
                break;
            }
        }
        return retour;
    }
}
