package fr.gecoraid.model;

public enum TypeDepart {
    // TODO L10n.getString()
    GROUPE("Groupé"), BOITIER("Au boîtier");

    private final String value;

    TypeDepart( String value ) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return value;
    }

}
