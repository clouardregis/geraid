package fr.gecoraid.model;

import java.util.Vector;

// TODO refactor -> SOLID
public class ListeCodeOkPm {
    private Vector<CodeOkPm> liste = new Vector<>();
    private Vector<Integer> _codesEnPlus = new Vector<>();

    public Vector<CodeOkPm> getListe() {
        return liste;
    }

    public void setListe( Vector<CodeOkPm> liste ) {
        this.liste = liste;
    }

    public void add( CodeOkPm c ) {
        liste.add(c);
    }

    public void add( int[] codes, boolean[] ok, String[] temps ) {
        for (int i = 0; i < codes.length; i++) {
            liste.add(new CodeOkPm(codes[i], ok[i], temps[i]));
        }
    }

    public String toHtml() {
        StringBuilder retour = new StringBuilder("<html><body>");
        int nbPm = getNbPm();
        if (nbPm > 0) {
            // TODO L10n.getString()
            retour.append("<font color=red>  ").append(nbPm).append(" PM : </font>");
        }
        if (!liste.get(liste.size() - 1).isoK()) {
            retour.append("<font color=red> " + " AAA" + " </font>");
        }
        String code;
        for (int i = 0; i < liste.size(); i++) {
            if (liste.get(i).getCode() == 16) {
                code = "D";
            } else if (liste.get(i).getCode() == 15) {
                code = "A";
            } else {
                code = liste.get(i).getCode() + "";
            }
            if (liste.get(i).isoK()) {
                retour.append("<font color=green> ").append(code).append(" </font>");
            } else {
                retour.append("<font color=red> ").append(code).append(" </font>");
            }
        }

        retour.append("<br>");
        if (!_codesEnPlus.isEmpty()) {
            retour.append("<font color=blue> Codes en plus : ");
            for (int i = 0; i < _codesEnPlus.size(); i++) {
                retour.append(_codesEnPlus.get(i)).append(" ");
            }
            retour.append("</font>");
        }

        retour.append("</body></html>");
        return retour.toString();
    }

    private int getNbPm() {
        int retour = 0;
        for (int i = 0; i < liste.size(); i++) {
            if (!liste.get(i).isoK()) {
                retour++;
            }
        }
        return retour;
    }

    public Vector<Integer> getCodesEnPlus() {
        return _codesEnPlus;
    }

    public void setCodesEnPlus( Vector<Integer> codesEnPlus ) {
        _codesEnPlus = codesEnPlus;
    }

}
