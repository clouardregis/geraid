package fr.gecoraid.model;

public enum TypeVisualisationEpreuve implements IVisualisation {
    // TODO L10n.getString()
    SIMPLE("Simple"),
    AVEC_BALISES("Avec balises");

    private final String value;

    TypeVisualisationEpreuve( String value ) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return value;
    }
}
