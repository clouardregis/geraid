package fr.gecoraid.model;

public enum TypeVisualisationParcours implements IVisualisation {
    // TODO L10n.getString()
    SIMPLE("Simple"),
    AVEC_ETAPE("Avec étapes");

    private final String value;

    TypeVisualisationParcours( String value ) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return value;
    }
}
