package fr.gecoraid.model;

import fr.gecoraid.desktop.widget.ErrorPane;

import javax.swing.JOptionPane;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Vector;

public class CsvEquipes {
    // TODO  move to model? in a subpackage Teams
    // TODO clean code
    public static void exportAllTeams( Raid grd, String fichier ) {
        File chemin = new File(fichier);
        try (Writer monFichier = new OutputStreamWriter(Files.newOutputStream(chemin.toPath()), StandardCharsets.UTF_8)) {
            // TODO L10n.getString()
            String tampon = "Dossard;Nom;Puce;Categorie";
            monFichier.write(tampon, 0, tampon.length());
            monFichier.write(System.lineSeparator());
            for (int j = 0; j < grd.getParcourss().getSize(); j++) {
                Parcours p = grd.getParcourss().getParcourss().get(j); // TODO Diffère du dessous
                Equipes es = p.getEquipes();
                for (int i = 0; i < es.getSize(); i++) {
                    tampon = es.getEquipes().get(i).toCSV();
                    monFichier.write(tampon, 0, tampon.length());
                    monFichier.write(System.lineSeparator());
                }
            }
        } catch (IOException e) {
            // TODO L10n.getString()
            // TODO beep
            ErrorPane.showMessageDialog("Erreur d'écriture du fichier d'export : " + e.getClass().getName() + ", " + e.getMessage());
        }
    }

    // TODO même chose qu'au-dessus mais avec des arguements différents -> ici 1 parcours -> 1 liste d'équipe
    // TODO au-dessus tous les parcours -> toutes leslistes d'équipes
    public static void exportTeams( Equipes es, String fichier ) {
        File chemin = new File(fichier);
        try (Writer monFichier = new OutputStreamWriter(Files.newOutputStream(chemin.toPath()), StandardCharsets.UTF_8)) {
            // TODO L10n.getString()
            String tampon = "Dossard;Nom;Puce;Categorie";
            monFichier.write(tampon, 0, tampon.length());
            monFichier.write(System.lineSeparator());
            for (int i = 0; i < es.getSize(); i++) {
                tampon = es.getEquipes().get(i).toCSV();
                monFichier.write(tampon, 0, tampon.length());
                monFichier.write(System.lineSeparator());
            }
        } catch (IOException e) {
            // TODO L10n.getString()
            // TODO beep
            ErrorPane.showMessageDialog("Erreur d'écriture du fichier d'export : " + e.getClass().getName() + ", " + e.getMessage());
        }
    }

    public static void importer( Raid g, Parcours p, String fichier ) {
        File chemin = new File(fichier);
        String chaine;
        String[] tampon;
        Vector<Integer> lignes = new Vector<>();
        int ligne = 1;

        try {
            if (!chemin.exists()) {
                chemin.createNewFile();
            }
            BufferedReader monFichier = new BufferedReader(new FileReader(chemin));
            p.getEquipes().getEquipes().clear();
            monFichier.readLine();
            while ((chaine = monFichier.readLine()) != null) {
                ligne++;
                tampon = chaine.trim().split(";");
                if (tampon.length > 1 && !tampon[1].isEmpty() && g.existeCategorie(tampon[3])) {
                    Equipe eq = new Equipe(g);
                    eq.setDossard(tampon[0]);
                    eq.setNom(tampon[1]);
                    eq.setIdPuce(tampon[2]);
                    eq.setCategorie(g.getCategories().getCategorie(tampon[3]));
                    for (int i = 0; i < (tampon.length - 4) / 2; i++) {
                        eq.getRaiders().addRaider(new Raider(tampon[4 + i * 2], tampon[5 + i * 2]));
                    }
                    p.getEquipes().addEquipe(eq);
                } else {
                    lignes.add(ligne);
                }
            }
            monFichier.close();
            if (!lignes.isEmpty()) {
                // TODO L10n.getString()
                StringBuilder message = new StringBuilder("Certaines équipes n'ont pu être importées :\nLignes ");
                for (Integer integer : lignes) {
                    message.append(integer).append(",");
                }
                // TODO L10n.getString()
                message.append("\nVérifiez que ces équipes ont un nom et une catégorie conforme (nom court).");
                // TODO L10n.getString()
                JOptionPane.showMessageDialog(null, message.toString(), "Import des équipes", JOptionPane.OK_OPTION);
            }
        } catch (IOException e) {
            // TODO L10n.getString()
            ErrorPane.showMessageDialog("Erreur d'import : " + e.getClass().getName() + ", " + e.getMessage());
        }
    }
}