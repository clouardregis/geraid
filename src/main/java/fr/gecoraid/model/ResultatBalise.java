package fr.gecoraid.model;

// TODO refactor -> SOLID
public class ResultatBalise {
    private int _points = 0;
    private long _temps = 0;
    private boolean _pm = false;
    private String _time = "";
    private int _code = 0;

    public String getTime() {
        return _time;
    }

    public void setTime( String time ) {
        _time = time;
    }

    public int getCode() {
        return _code;
    }

    public void setCode( int code ) {
        _code = code;
    }

    public int getPoints() {
        return _points;
    }

    public void setPoints( int points ) {
        _points = points;
    }

    public long getTemps() {
        return _temps;
    }

    public void setTemps( long temps ) {
        _temps = temps;
    }

    public boolean isPm() {
        return _pm;
    }

    public void setPm( boolean pm ) {
        _pm = pm;
    }
}
