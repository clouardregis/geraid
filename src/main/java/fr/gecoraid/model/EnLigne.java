package fr.gecoraid.model;

import java.util.Vector;

// TODO put in the convenient folder
public class EnLigne {
    // TODO Refactor
    private final int[] _codesATrouver;
    private final Vector<Integer> _codesPuce;
    private final Vector<String> _tempsPuce;
    private int[][] _matrice;
    private int _coutLevenshtein = 0;
    private boolean[] _okpm;
    private String[] _temps;

    public EnLigne( int[] codesATrouver, Vector<Integer> codesPuce, Vector<String> tempsPuce ) {
        _codesATrouver = codesATrouver;
        _codesPuce = codesPuce;
        _tempsPuce = tempsPuce;
        suppCodesInutiles();
        initMatrice();
        levenshtein();
        okPm();
    }

    private void initMatrice() {
        _matrice = new int[_codesATrouver.length + 1][_codesPuce.size() + 1];
        for (int i = 0; i < _codesATrouver.length + 1; i++) {
            _matrice[i][0] = i;
        }
        for (int j = 0; j < _codesPuce.size() + 1; j++) {
            _matrice[0][j] = j;
        }
    }

    private void levenshtein() {
        int cout;
        for (int i = 0; i < _codesATrouver.length; i++) {
            for (int j = 0; j < _codesPuce.size(); j++) {
                cout = (_codesATrouver[i]) == _codesPuce.get(j) ? 0 : 1;
                _matrice[i + 1][j + 1] = minimum(1 + _matrice[i + 1][j], 1 + _matrice[i][j + 1], cout + _matrice[i][j]);
            }
        }
        _coutLevenshtein = _matrice[_codesATrouver.length][_codesPuce.size()];
    }

    private int minimum( int a, int b, int c ) {
        int retour;
        retour = Math.min(a, Math.min(b, c));
        return retour;
    }

    private void okPm() {
        _okpm = new boolean[_codesATrouver.length];
        _temps = new String[_codesATrouver.length];
        int i = 0;
        int j = 0;

        while (i < _codesATrouver.length && j < _codesPuce.size()) {
            if (j == _codesPuce.size()) {
                _okpm[i] = false;
                // TODO L10n.getString()
                _temps[i] = "PM";
                i++;
            } else if (i == _codesATrouver.length) {
                j++;
            } else if (_matrice[i + 1][j + 1] == _matrice[i][j]) {
                _okpm[i] = true;
                _temps[i] = _tempsPuce.get(j);
                i++;
                j++;
            } else if (!existeCode(_codesATrouver[i], j + 1)) {
                _okpm[i] = false;
                // TODO L10n.getString()
                _temps[i] = "PM";
                i++;
            } else if (existeCode(_codesATrouver[i], j + 1)) {
                if (_matrice[i][j + 1] > _coutLevenshtein) {
                    _okpm[i] = false;
                    // TODO L10n.getString()
                    _temps[i] = "PM";
                    i++;
                    j--;
                } else {
                    j++;
                }
            }
        }
    }

    private void suppCodesInutiles() {
        int i = _codesPuce.size() - 1;
        while (i >= 0) {
            if (!isCode(_codesPuce.get(i))) {
                _codesPuce.remove(i);
                _tempsPuce.remove(i);
            }
            i--;
        }
    }

    private boolean isCode( int code ) {
        boolean retour = false;
        for (int j : _codesATrouver) {
            if (code == j) {
                retour = true;
                break;
            }
        }
        return retour;
    }

    public boolean[] getOkPm() {
        return _okpm;
    }

    @SuppressWarnings("unused")
    private void verifMatrice() {
        for (int i = 0; i < _codesATrouver.length + 1; i++) {
            for (int j = 0; j < _codesPuce.size() + 1; j++) {
                if (j == _codesPuce.size()) {
                    System.out.println(_matrice[i][j]);
                } else {
                    System.out.print(_matrice[i][j] + "  ");
                }
            }
        }
        System.out.println(_coutLevenshtein + "");
    }

    @SuppressWarnings("unused")
    private void lireOkPm() {
        for (int i = 0; i < _codesATrouver.length; i++) {
            System.out.println(_codesATrouver[i] + " : " + _okpm[i]);
        }
    }

    // TODO mettre en test
//    public static void main( String[] args ) {
//        int codes[] = {31, 32, 33, 34, 32, 35, 36, 32, 37, 38, 39};
//        Vector<Integer> trace = new Vector<Integer>();
//        Vector<String> temps = new Vector<String>();
//        trace.add(31);
//        temps.add("10:00:00");
//        trace.add(33);
//        temps.add("10:00:00");
//        trace.add(34);
//        temps.add("10:00:00");
//        trace.add(32);
//        temps.add("10:00:00");
//        trace.add(35);
//        temps.add("10:00:00");
//        trace.add(36);
//        temps.add("10:00:00");
//        trace.add(32);
//        temps.add("10:00:00");
//        trace.add(37);
//        temps.add("10:00:00");
//        trace.add(38);
//        temps.add("10:00:00");
//        trace.add(39);
//        temps.add("10:00:00");
//        new EnLigne(codes, trace, temps);
//    }

    public String[] getTemps() {
        return _temps;
    }

    private boolean existeCode( int code, int index ) {
        for (int j = index; j < _codesPuce.size(); j++) {
            if (_codesPuce.get(j) == code) {
                return true;
            }
        }

        return false;
    }
}
