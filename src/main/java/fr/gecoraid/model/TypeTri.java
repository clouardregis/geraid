package fr.gecoraid.model;

public enum TypeTri {
    // TODO L10n.getString()
    DOSSARD("Dossard"),
    NOM("Nom d'équipe"),
    PUCE("Puce"),
    CATEGORIE("Catégorie");

    private final String value;

    TypeTri( String value ) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return value;
    }

}
