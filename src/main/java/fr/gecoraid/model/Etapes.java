package fr.gecoraid.model;

import java.util.Vector;

// TODO refactor -> SOLID
public class Etapes implements Cloneable {
    private Vector<Etape> _etapes = new Vector<>();

    public Object clone() {
        try {
            Etapes p = (Etapes) super.clone();
            p._etapes = new Vector<>();
            for (Etape etape : _etapes) {
                p.getEtapes().add((Etape) etape.clone());
            }
            return p;
        } catch (CloneNotSupportedException cnse) {
            // Ne devrait jamais arriver car nous implémentons l'interface Cloneable
        }
        return null;
    }

    public Vector<Etape> getEtapes() {
        return _etapes;
    }

    public void addEtape( Etape e ) {
        if (_existeNom(e) == null) {
            _etapes.add(e);
        }
    }

    private Etape _existeNom( Etape e ) {
        for (Etape etape : _etapes) {
            if (etape != e && etape.getName().compareTo(e.getName().trim()) == 0) {
                return etape;
            }
        }
        return null;
    }

    public boolean existeEtape( String nom, Etape e ) {
        for (Etape etape : _etapes) {
            if (etape.getName().toUpperCase().compareTo(nom.trim().toUpperCase()) == 0
                    && (!etape.equals(e))) {
                return true;
            }
        }
        return false;
    }

    public void removeEtape( Etape e ) {
        _etapes.remove(e);
    }

    public int getSize() {
        return _etapes.size();
    }

    public String toStringEtapes() {
        StringBuilder retour = new StringBuilder("<ul>");
        for (Etape etape : _etapes) {
            retour.append(etape.toStringEtape());
        }
        retour.append("</ul>");
        return retour.toString();
    }
}
