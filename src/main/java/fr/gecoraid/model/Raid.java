package fr.gecoraid.model;

import fr.gecoraid.L10n;
import fr.gecoraid.tools.TimeManager;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

// TODO refactor -> SOLID
public class Raid {
    private String _savingFile = "";
    private boolean _afficherNomsEquipiers = false;
    private String _raidName = L10n.getString("settings.raid_name.template");

    private final ZeroTime zeroHour = new ZeroTime(0, 0); // TODO Prendre autre chose qu'un point qui n'est pas très explicite pour stocker une heure
    private final Parcourss _parcourss = new Parcourss();
    private final ResultatsPuce _resultatsPuce = new ResultatsPuce();
    private final Penalites _penalites = new Penalites();
    private final List<String> _entetes = new ArrayList<>();
    private final List<PenalitesIndividuelles> _data = new ArrayList<>();
    private Equipes _toutesLesEquipes = new Equipes();
    private Categories _categories = new Categories();
    private String _pageHeader = "";
    private String _pageFooter = L10n.getString("settings.page_footer.template");

    public String getRaidName() {
        return _raidName;
    }

    public void setRaidName( String nom ) {
        _raidName = nom;
    }

    public String getSavingFile() {
        return _savingFile;
    }

    public void setSavingFile( String savingFile ) {
        _savingFile = savingFile;
    }

    public String getPageHeader() {
        return _pageHeader;
    }

    public void setPageHeader( String text ) {
        _pageHeader = text;
    }

    public String getPageFooter() {
        return _pageFooter;
    }

    public void setPageFooter( String text ) {
        _pageFooter = text;
    }

    public Parcourss getParcourss() {
        return _parcourss;
    }

    public void addParcours( Parcours p ) {
        _parcourss.addParcours(p);
    }

    public boolean existeParcours( String nom, Parcours parcours ) {
        return _parcourss.existeParcours(nom, parcours);
    }

    public boolean existeParcoursAvecMemeCasse( String nom, Parcours parcours ) {
        return _parcourss.existeParcoursAvecMemeCasse(nom, parcours);
    }

    public void removeParcours( Parcours p ) {
        _parcourss.removeParcours(p);
        _resultatsPuce.effacerParcours(p);
    }


    public boolean addResultatPuce( ResultatPuce p ) {
        return _resultatsPuce.addResultatPuce(p);
    }

    public void removeResultatPuce( ResultatPuce p ) {
        _resultatsPuce.removeResultatPuce(p);
    }


    public void clear() {
        _parcourss.clearParcours();
        _resultatsPuce.clearResultats();
        _penalites.getPenalites().clear();
        _data.clear();
        // TODO ici
        _categories.getCategories().clear();
        _savingFile = "";
    }

    public boolean addEquipe( Parcours p, Equipe e ) {
        return _parcourss.addEquipe(p, e);
    }

    public Equipe editEquipe( Equipes es, Equipe e ) {
        return es.editEquipe(e);
    }

    public boolean existeIdPuce( String idPuce, Equipe e ) {
        return _parcourss.existeIdPuce(idPuce, e);
    }

    public void removeEquipe( Equipes es, Equipe e ) {
        es.removeEquipe(e);
        _resultatsPuce.effacerEquipe(e);
    }

    public void setCategories( Categories categorie ) {
        _categories = categorie;
    }

    public void addCategorie( Categorie cat ) {
        _categories.addCategorie(cat);
    }

    public Categories getCategories() {
        return _categories;
    }

    public boolean existeCategorie( String nomCourt ) {
        return _categories.existeCategorie(nomCourt);
    }

    public ResultatPuce getEquipe( String idEquipe ) {
        ResultatPuce result = new ResultatPuce();
        for (int i = 0; i < _parcourss.getSize(); i++) {
            Equipe equipe = _parcourss.getParcourss().get(i).getEquipe(idEquipe);
            if (equipe != null) {
                Etape etape = _parcourss.getParcourss().get(i).getEtapeEnCours();
                if (etape != null) {
                    result.setParcours(_parcourss.getParcourss().get(i));
                    result.setEtape(etape);
                    result.setEquipe(equipe);
                    return result;
                }
            }
        }

        return result;
    }

    public Equipe getEquipePuce( String idEquipe ) {
        Equipe retour = null;

        for (int i = 0; i < _parcourss.getSize(); i++) {
            Equipe equipe = _parcourss.getParcourss().get(i).getEquipe(idEquipe);
            if (equipe != null) {
                return equipe;
            }
        }

        return retour;
    }

    public ResultatsPuce getResultatsPuce() {
        return _resultatsPuce;
    }

    public ResultatPuce existeResultatPuce( ResultatPuce rp ) {
        return _resultatsPuce.existeResultatPuce(rp);
    }

    public boolean existeResultatPuce( Equipe e ) {
        return _resultatsPuce.existeResultatPuce(e);
    }

    public boolean existePuce( String idPuce ) {
        return _parcourss.existePuce(idPuce);
    }

    public Parcours getParcoursIdPuce( String idPuce ) {
        return _parcourss.getParcoursIdPuce(idPuce);
    }

    public Parcours getParcours( Epreuve epreuve ) {
        return _parcourss.getParcoursDeEpreuve(epreuve);
    }

    public Equipe getEquipeSansPuce( Parcours p, int index ) {
        Equipe retour = null;
        Equipes eqs = p.getEquipes();
        for (int i = index; i < eqs.getSize(); i++) {
            if (eqs.getEquipes().get(i).getIdPuce().isEmpty()) {
                return eqs.getEquipes().get(i);
            }
        }

        return retour;
    }

    public String[] getEntetes( Etape e ) {
        editEntetes(e);
        return _entetes.toArray(new String[0]);
    }

    public Object[][] getData( Parcours p, Etape e ) {
        editData(p, e);
        Object[][] retour = new Object[_data.size()][_entetes.size()];
        for (int i = 0; i < _data.size(); i++) {
            retour[i][0] = _data.get(i).getDossard();
            retour[i][1] = _data.get(i).getEquipe();
            retour[i][2] = _data.get(i).getPuce();
            for (int j = 3; j < _entetes.size(); j = j + 2) {
                retour[i][j] = _data.get(i).getPoints().get((j - 3) / 2);
                retour[i][j + 1] = _data.get(i).getTemps().get((j - 3) / 2);
            }
        }
        return retour;
    }

    public Penalite getPenalite( Etape e, int index ) {
        Penalite retour = null;
        int tmp = 0;
        for (int i = 0; i < _penalites.getPenalites().size(); i++) {
            if (_penalites.getPenalites().get(i).getEtape().equals(e)) {
                if (tmp == index) {
                    return _penalites.getPenalites().get(i);
                } else {
                    tmp++;
                }
            }
        }

        return retour;
    }

    public PenaliteIndividuelle getPenaliteIndividuelle( Etape e, int indexPen, String puce ) {
        Penalite p = getPenalite(e, indexPen);
        return p.getPenaliteIndividuelle(puce);
    }

    public String exportParcoursAsHTML() {
        return "<!doctype html><html lang=\"fr-FR\"><body>" + _parcourss.toStringParcours() + "</body></html>";
    }

    public boolean isEquipeArrived( Equipe equipe, Parcours selectedParcours, Etape selectedEtape ) {
        return _resultatsPuce.isArrived(equipe, selectedParcours, selectedEtape);
    }

    public void removeResultatPuce( Equipe equipe ) {
        _resultatsPuce.effacerEquipe(equipe);
    }

    public Parcours getParcours( Etape etape ) {
        return _parcourss.getParcoursDeEtape(etape);
    }

    public Equipes getToutesLesEquipes( String debutNomEquipe ) {
        if (debutNomEquipe.trim().isEmpty()) {
            return getToutesLesEquipes();
        }
        _toutesLesEquipes = new Equipes();
        for (int i = 0; i < _parcourss.getSize(); i++) {
            _toutesLesEquipes.getEquipes().addAll(_parcourss.getParcourss().get(i).getEquipes().getEquipes(debutNomEquipe));
        }
        return _toutesLesEquipes;
    }

    public Equipes getToutesLesEquipes() {
        _toutesLesEquipes = new Equipes();
        for (int i = 0; i < _parcourss.getSize(); i++) {
            _toutesLesEquipes.getEquipes().addAll(_parcourss.getParcourss().get(i).getEquipes().getEquipes());
        }
        return _toutesLesEquipes;
    }

    public boolean isAfficherNomsEquipiers() {
        return _afficherNomsEquipiers;
    }

    public void setAfficherNomsEquipiers( boolean value ) {
        _afficherNomsEquipiers = value;
    }

    public Penalites getPenalites() {
        return _penalites;
    }

    public ZeroTime getZeroTime() {
        return zeroHour;
    }

    private void editEntetes( Etape e ) {
        _entetes.clear();
        _entetes.add(L10n.getString("settings.category.bib"));
        _entetes.add(L10n.getString("settings.category.team_name"));
        _entetes.add(L10n.getString("settings.category.chip"));
        Vector<String> penaltyCount = _penalites.getPenaltyCount(e);
        for (String nbPenalite : penaltyCount) {
            _entetes.add(nbPenalite + L10n.getString("settings.category.penalty_points"));
            _entetes.add(nbPenalite + L10n.getString("settings.category.penalty_time"));
        }
    }

    private void editData( Parcours p, Etape e ) {
        _data.clear();
        for (int i = 0; i < p.getNombreEquipes(); i++) {
            PenalitesIndividuelles pi = new PenalitesIndividuelles();
            pi.setDossard(p.getEquipes().getEquipes().get(i).getDossard());
            pi.setPuce(p.getEquipes().getEquipes().get(i).getIdPuce());
            pi.setEquipe(p.getEquipes().getEquipes().get(i).getNom());
            _data.add(pi);
        }
        for (int i = 0; i < _penalites.getPenalites().size(); i++) {
            if (_penalites.getPenalites().get(i).getEtape().equals(e)) {
                for (PenalitesIndividuelles datum : _data) {
                    if (_penalites.getPenalites().get(i).aPuce(datum.getPuce())) {
                        datum.getPoints().add(_penalites.getPenalites().get(i).getPoints(datum.getPuce()) + "");
                        datum.getTemps().add(TimeManager.fullTime(_penalites.getPenalites().get(i).getTemps(datum.getPuce()) * 1000L));
                    } else {
                        datum.getPoints().add("");
                        datum.getTemps().add("");
                    }
                }
            }
        }
    }
}
