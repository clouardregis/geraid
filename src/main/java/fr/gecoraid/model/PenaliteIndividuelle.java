package fr.gecoraid.model;

public class PenaliteIndividuelle {
    private String _puce = "";
    private int _point = 0;
    private int _temps = 0;

    public String getPuce() {
        return _puce;
    }

    public void setPuce( String puce ) {
        _puce = puce;
    }

    public int getPoint() {
        return _point;
    }

    public void setPoint( int point ) {
        if (getTemps() < 0) {
            _point = Math.abs(point);
        } else if (getTemps() == 0) {
            _point = point;
        } else {
            _point = -Math.abs(point);
        }
    }

    public int getTemps() {
        return _temps;
    }

    public void setTemps( int temps ) {
        if (getPoint() < 0) {
            _temps = Math.abs(temps);
        } else if (getPoint() == 0) {
            _temps = temps;
        } else {
            _temps = -Math.abs(temps);
        }
    }
}
