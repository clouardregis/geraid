package fr.gecoraid.model;

import fr.gecoraid.tools.TimeManager;

import java.util.Date;
import java.util.Vector;

/**
 * Une épreuve d’une étape se caractérise par la découverte d’une succession de balises
 * dans une configuration imposée (course à pied, VTT, au score, en ligne,…).
 */
// TODO refactor -> SOLID
public class Epreuve implements Cloneable {
    private static final Vector<String> _entetes = new Vector<>();
    private Balises _balises = new Balises();
    private String _nom = "";
    private TypeLimite _typeLimite = TypeLimite.SANSLIMITE;
    private Date _heureLimite = new Date(0);
    private Date _tempsLimite = new Date(0);
    private int _multiplicateurTemps = 1;
    private int _intervalPenalite = 5;
    private int _penalite = 0;
    private int _penaliteTemps = 0;
    private boolean _chronometree = false; // true s'il faut chronométrer cette épreuve
    private boolean _courseEnLigne = false; // true si c'est une épreuve type course en ligne
    private boolean _debutEpreuve = false; // l'épreuve commence avec la dernière balise de l'épreuve précédente == true
    private boolean _finEpreuve = false; // l'épreuve se termine avec la première balise de l'épreuve suivante == true
    private boolean _horsChrono = false; // Si l'épreuve est effectuée après l'arrivée. Permet d'utiliser le boitier arrivée pour une autre épreuve que la dernière
    private boolean _avantEpreuveSuivante = false; // Si true = chaque poste de cette épreuve doit être fait avant ceux de l'épreuve suivante sinon PM
    private boolean _apresEpreuvePrecedente = false; // Si true = chaque poste de cette épreuve doit être fait après ceux de l'épreuve précédente sinon PM
    private boolean _exclusiveWithNextEpreuve = false; // If true, consider only the epreuve giving the best score between this one and the next.

    public Object clone() {
        try {
            Epreuve p = (Epreuve) super.clone();
            p._balises = (Balises) _balises.clone();
            return p;
        } catch (CloneNotSupportedException cnse) {
            // Ne devrait jamais arriver car nous implémentons l'interface Cloneable
            cnse.printStackTrace(System.err);
        }
        return null;
    }

    public int getTotalPointsEpreuve() {
        return _balises.getTotalPointsBalises();
    }

    public int getMultiplicateurTemps() {
        return _multiplicateurTemps;
    }

    public void setMultiplicateurTemps( int multiplicateurTemps ) {
        _multiplicateurTemps = multiplicateurTemps;
    }

    public int[] getCodes() {
        int[] codes = new int[getNbBalises()];
        for (int i = 0; i < _balises.getSize(); i++) {
            codes[i] = _balises.getBalises().get(i).getNumero();
        }
        return codes;
    }

    public boolean isChronometree() {
        return _chronometree;
    }

    public void setChronometree( boolean chronometree ) {
        _chronometree = chronometree;
    }

    public String getChrono() {
        return isChronometree() ? "1" : "0";
    }

    public int getPenaliteTemps() {
        return _penaliteTemps;
    }

    public void setPenaliteTemps( int penaliteTemps ) {
        _penaliteTemps = penaliteTemps;
    }

    public String getName() {
        return _nom;
    }

    public void setNom( String nom ) {
        _nom = nom;
    }

    public String toString() {
        return _nom;
    }

    public Balises getBalises() {
        return _balises;
    }

    public void setBalises( Balises balises ) {
        _balises = balises;
    }

    public TypeLimite getTypeLimite() {
        return _typeLimite;
    }

    public void setTypeLimite( TypeLimite typeLimite ) {
        _typeLimite = typeLimite;
    }

    public Date getHeureLimite() {
        return _heureLimite;
    }

    public void setHeureLimite( Date heureLimite ) {
        _heureLimite = heureLimite;
    }

    public Date getTempsLimite() {
        return _tempsLimite;
    }

    public void setTempsLimite( Date tempsLimite ) {
        _tempsLimite = tempsLimite;
    }

    public int getIntervalPenalite() {
        return _intervalPenalite;
    }

    public void setIntervalPenalite( int intervalPenalite ) {
        _intervalPenalite = intervalPenalite;
    }

    public int getPenalite() {
        return _penalite;
    }

    public void setPenalite( int penalite ) {
        _penalite = penalite;
    }

    public int getNbBalises() {
        return _balises.getSize();
    }

    public boolean isCourseEnLigne() {
        return _courseEnLigne;
    }

    public void setCourseEnLigne( boolean courseEnLigne ) {
        _courseEnLigne = courseEnLigne;
    }

    public boolean isDebutEpreuve() {
        return _debutEpreuve;
    }

    public void setDebutEpreuve( boolean debutEpreuve ) {
        _debutEpreuve = debutEpreuve;
    }

    public boolean isFinEpreuve() {
        return _finEpreuve;
    }

    public void setFinEpreuve( boolean finEpreuve ) {
        _finEpreuve = finEpreuve;
    }

    public String getDebutEpreuve() {
        return isDebutEpreuve() ? "1" : "0";
    }

    public String getFinEpreuve() {
        return isFinEpreuve() ? "1" : "0";
    }

    public String getLigne() {
        return isCourseEnLigne() ? "1" : "0";
    }

    public int getDernierNumero() {
        return getBalises().getDernierNumero();
    }

    public int getDernierNumeroBalises() {
        return getBalises().getNumeroDerniereBalise();
    }

    public int getPremierNumeroBalises() {
        return getBalises().getNumeroPremiereBalise();
    }

    public boolean isHorsChrono() {
        return _horsChrono;
    }

    public void setHorsChrono( boolean horsChrono ) {
        _horsChrono = horsChrono;
    }

    public String getHorsChrono() {
        return (isHorsChrono()) ? "1" : "0";
    }

    public boolean isExclusiveWithNextEpreuve() {
        return _exclusiveWithNextEpreuve;
    }

    public void setExclusiveWithNextEpreuve( boolean exclusiveWithNextEpreuve ) {
        this._exclusiveWithNextEpreuve = exclusiveWithNextEpreuve;
    }

    public String getExclusiveWithNextEpreuve() {
        return isExclusiveWithNextEpreuve() ? "1" : "0";
    }

    public static String[] getEntetes( Epreuve epreuve, TypeVisualisationEpreuve tv ) {
        editEntetes(epreuve, tv);
        return _entetes.toArray(new String[0]);
    }

    private static void editEntetes( Epreuve epreuve, TypeVisualisationEpreuve tv ) {
        // TODO revoir l'encodage du texte HTML (problème Widnows/Linux)
        // Visualisation simple
        _entetes.clear();
        // Classement
        // TODO L10n.getString()
        _entetes.add("Clt");
        _entetes.add("Dossard");
        _entetes.add("Puce");
        _entetes.add("Nom équipe");
        _entetes.add("Catégorie");
        _entetes.add("Tps final");
        _entetes.add("Pts finaux");
        // Temps
        _entetes.add("Tps course");
        _entetes.add("Bonification");
        _entetes.add("Pénalités");
        _entetes.add("Dépassement");
        _entetes.add("Gel");
        // Points
        _entetes.add("Bonification");
        _entetes.add("Pénalités");
        _entetes.add("Dépassement");
        // Gestion des autres visualisations
        if (!tv.equals(TypeVisualisationEpreuve.SIMPLE)
                && (epreuve != null)) {
            for (int i = 0; i < epreuve.getBalises().getSize(); i++) {
                _entetes.add("Code");
                _entetes.add("Ok/Pm"); // heure de passage ou PM
                _entetes.add("Points");
                _entetes.add("Temps");
            }

        }
    }

    public String toStringEpreuve( Epreuves epreuves ) {
        StringBuilder retour = new StringBuilder();
        // TODO L10n.getString()
        retour.append("<li><code>Épreuve : ")
                .append(_nom)
                .append("<br>");
        switch (_typeLimite) {
            case SANSLIMITE:
                retour.append("Sans limite");
                break;
            case AVECLIMITETEMPS:
                retour.append("Avec limite de temps à ")
                        .append(TimeManager.fullTime(_tempsLimite))
                        .append(" et pénalité de ")
                        .append(_penalite)
                        .append(" pts et ")
                        .append(TimeManager.fullTime(_penaliteTemps * 60000L))
                        .append(" par ")
                        .append(_intervalPenalite)
                        .append(" mn de dépassement");
                break;
            case AVECLIMITEHORAIRE:
                retour.append("Avec limite horaire à ")
                        .append(TimeManager.fullTime(_heureLimite))
                        .append(" et pénalité de ")
                        .append(_penalite)
                        .append(" pts et ")
                        .append(TimeManager.fullTime(_penaliteTemps * 60000L))
                        .append(" par ")
                        .append(_intervalPenalite)
                        .append(" mn de dépassement");
                break;
            default:
                break;
        }
        retour.append("<br>");
        if (_chronometree) {
            retour.append("Epreuve chronométrée entre ");
            if (epreuves.isFirstEpreuve(this)) {
                retour.append("le départ et ");
            } else if (_debutEpreuve) {
                retour.append("la balise ").append(epreuves.getLastCodeEpreuvePrecedente(this)).append(" et ");
            } else {
                retour.append("la balise ").append(_balises.getNumeroPremiereBalise()).append(" et ");
            }
            if (epreuves.isLastEpreuveAvantArrivee(this)) {
                retour.append("l'arrivée.");
            } else if (_finEpreuve) {
                retour.append("la balise ").append(epreuves.getFirstCodeEpreuveSuivante(this));
            } else {
                retour.append("la balise ").append(_balises.getNumeroDerniereBalise()).append(".");
            }
            retour.append("<br>");
        }
        if (_apresEpreuvePrecedente) {
            retour.append("Postes à valider après l'épreuve précédente.<br>");
        }
        if (_avantEpreuveSuivante) {
            retour.append("Postes à valider avant l'épreuve suivante.<br>");
        }
        if (_multiplicateurTemps > 1) {
            retour.append("Temps de l'épreuve X ").append(_multiplicateurTemps).append(".<br>");
        }
        if (_courseEnLigne) {
            retour.append("Postes à valider dans l'ordre.<br>");
        }
        if (_horsChrono) {
            retour.append("Epreuve effectuée après l'arrivée.<br>");
        }
        retour.append("</code>");
        retour.append(_balises.toStringBalises());
        retour.append("</li>");
        return retour.toString();
    }

    public boolean isAvantEpreuveSuivante() {
        return _avantEpreuveSuivante;
    }

    public String getAvantEpreuveSuivante() {
        return (isAvantEpreuveSuivante()) ? "1" : "0";
    }

    public void setAvantEpreuveSuivante( boolean avantEpreuveSuivante ) {
        _avantEpreuveSuivante = avantEpreuveSuivante;
    }

    public boolean isApresEpreuvePrecedente() {
        return _apresEpreuvePrecedente;
    }

    public String getApresEpreuvePrecedente() {
        return (isApresEpreuvePrecedente()) ? "1" : "0";
    }

    public void setApresEpreuvePrecedente( boolean apresEpreuvePrecedente ) {
        _apresEpreuvePrecedente = apresEpreuvePrecedente;
    }

    public String toCSV() {
        StringBuilder string = new StringBuilder(_nom);
        for (int i = 0; i < _balises.getSize(); i++) {
            string.append(";")
                    .append(_balises.getBalises().get(i).getNumero());
        }
        return string.toString();
    }
}
