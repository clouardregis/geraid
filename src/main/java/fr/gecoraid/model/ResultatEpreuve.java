package fr.gecoraid.model;

import fr.gecoraid.tools.TimeManager;

import java.util.Date;
import java.util.Vector;

// TODO refactor -> SOLID
public class ResultatEpreuve {
    private final Epreuve _epreuve;
    private int totalPoints = 0; // ce qui est retenue pour le classement après retrait ou ajout divers
    private int totalPointsFinal = 0;
    private int totalPointsPenalite = 0;
    private int _totalPointsDepassement = 0;
    private long totalTemps = 0; // ce qui est retenue pour le classement après retrait ou ajout divers
    private long totalTempsFinal = 0;
    private long totalGelChrono = 0;
    private long totaltempsBonif = 0;
    private long totaltempsPenalite = 0;
    private long totaltempsdepassement = 0;
    private long _totalTempsMultiplicateur = 0;
    private Vector<ResultatBalise> _resultatBalises = new Vector<ResultatBalise>();
    private Date _depart = null;
    private Date _arrivee = null;
    private final int[] _codesATrouver;
    private boolean[] _okPm;
    private String[] _temps;
    private boolean used = true; // False if excluded.

    public ResultatEpreuve( Epreuve e, ResultatPuce rp ) {
        _epreuve = e;
        // récupération des codes à trouver
        _codesATrouver = e.getCodes();
        // récupération des codes de la puce
        Vector<Integer> codesPuce = rp.getCodes();
        Vector<String> tempsPuce = rp.getTemps();
        // calcul des OK et PM
        _okPm = new boolean[_codesATrouver.length];
        _temps = new String[_codesATrouver.length];
        if (e.isCourseEnLigne()) {
            EnLigne el = new EnLigne(_codesATrouver, codesPuce, tempsPuce);
            _okPm = el.getOkPm();
            _temps = el.getTemps();
        } else {
            AuScore as = new AuScore(_codesATrouver, codesPuce, tempsPuce);
            _okPm = as.getOkPm();
            _temps = as.getTemps();
        }
        // Calcul de la position par rapport aux épreuves précédentes et suivantes
        if (e.isAvantEpreuveSuivante()) {
            avantEpreuveSuivante(e, rp);
        }
        if (e.isApresEpreuvePrecedente()) {
            apresEpreuvePrecedente(e, rp);
        }
        // calcul du temps de course de l'épreuve
        if (e.isChronometree()) {
            // départ de l'épreuve
            if (rp.getEtape().isFirstEpreuve(e)) {
                switch (rp.getEtape().getType()) {
                    case GROUPE:
                        _depart = rp.getEtape().getHeureDepart();
                        break;
                    case BOITIER:
                        _depart = rp.getPuce().getStarttime();
                        break;

                    default:
                        break;
                }
            } else {
                if (e.isDebutEpreuve()) {
                    _depart = rp.getPuce().getTimePartielLastCode(rp.getEtape().getLastCodePreviousEpreuve(e));
                } else {
                    if (_codesATrouver.length > 0) {
                        _depart = rp.getPuce().getTimePartielFirstCode(_codesATrouver[0]);
                    } else {
                        _depart = TimeManager.NO_TIME;
                    }
                }
            }

            // Arrivée de l'épreuve
            if (rp.getEtape().isLastEpreuve(e)) {
                _arrivee = rp.getPuce().getFinishtime();
            } else {
                if (e.isFinEpreuve()) {
                    _arrivee = rp.getPuce().getTimePartielFirstCode(rp.getEtape().getfirstCodeNextEpreuve(e));
                } else {
                    if (_codesATrouver.length > 0) {
                        if (isDoubleCode(_codesATrouver[0])) {
                            _arrivee = rp.getPuce().getTimePartielLastCode(_codesATrouver[_codesATrouver.length - 1]);
                        } else {
                            _arrivee = rp.getPuce().getTimePartielFirstCode(_codesATrouver[_codesATrouver.length - 1]);
                        }
                    } else {
                        _arrivee = TimeManager.NO_TIME;
                    }
                }
            }
            if (e.isHorsChrono()) {
                if (e.isFinEpreuve()) {
                    _arrivee = rp.getPuce().getTimePartielFirstCode(rp.getEtape().getfirstCodeNextEpreuve(e));
                } else {
                    if (isDoubleCode(_codesATrouver[0])) {
                        _arrivee = rp.getPuce().getTimePartielLastCode(_codesATrouver[_codesATrouver.length - 1]);
                    } else {
                        _arrivee = rp.getPuce().getTimePartielFirstCode(_codesATrouver[_codesATrouver.length - 1]);
                    }
                }
            }

            // durée de l'épreuve
            if (_depart != null && _arrivee != null) {
                totalTempsFinal = _arrivee.getTime() - _depart.getTime();
            }
        }

        // Calcul des résultats en fonction des balises trouvées
        calculResultatBalises(e, rp);
        calculPointsTemps();
        calculDepassement(e, rp);
        // derniers calculs
        totalPoints = totalPointsFinal + totalPointsPenalite - _totalPointsDepassement;
        totalTemps = totalTempsFinal + totaltempsBonif * 1000 + totaltempsPenalite * 1000 + totaltempsdepassement * 60000 - totalGelChrono;
        _totalTempsMultiplicateur = totalTemps * (_epreuve.getMultiplicateurTemps() - 1);
        totalTemps = totalTemps * _epreuve.getMultiplicateurTemps();
        //totalPointsMultiplicateur = totalPoints * (epreuve.getMultiplicateurTemps() - 1);
        //totalPoints = totalPoints * epreuve.getMultiplicateurTemps();
    }

    private void calculDepassement( Epreuve e, ResultatPuce rp ) {
        switch (e.getTypeLimite()) {
            case AVECLIMITEHORAIRE:
                penaliteDepassementHoraire(e);
                break;
            case AVECLIMITETEMPS:
                penaliteDepassementTemps(e);
                break;

            default:
                break;
        }
    }

    private void penaliteDepassementHoraire( Epreuve e ) {
        if (_arrivee != null) {
            long diff = _arrivee.getTime() - e.getHeureLimite().getTime();
            if (diff > 0) {
                float minutes = (float) diff / 60000 / e.getIntervalPenalite();
                int mnEntamee = (int) Math.ceil(minutes);
                totaltempsdepassement = (long) mnEntamee * e.getPenaliteTemps();
                _totalPointsDepassement = mnEntamee * e.getPenalite();
            }
        }
    }

    private void penaliteDepassementTemps( Epreuve e ) {
        long diff = totalTempsFinal - e.getTempsLimite().getTime();
        if (diff > 0) {
            float minutes = (float) diff / 60000 / e.getIntervalPenalite();
            int mnEntamee = (int) Math.ceil(minutes);
            totaltempsdepassement = (long) mnEntamee * e.getPenaliteTemps();
            _totalPointsDepassement = mnEntamee * e.getPenalite();
        }
    }

    private void calculPointsTemps() {
        for (ResultatBalise resultatBalise : _resultatBalises) {
            if (resultatBalise.getPoints() > 0) {
                totalPointsFinal = totalPointsFinal + resultatBalise.getPoints();
            } else {
                totalPointsPenalite = totalPointsPenalite + resultatBalise.getPoints();
            }
            if (resultatBalise.getTemps() > 0) {
                totaltempsPenalite = totaltempsPenalite + resultatBalise.getTemps();
            } else {
                totaltempsBonif = totaltempsBonif + resultatBalise.getTemps();
            }
        }
    }

    private void calculResultatBalises( Epreuve e, ResultatPuce rp ) {
        // Calcul par balise
        // Init Gestion du gel
        boolean chronoEnCours = true;
        long arretChrono = 0;
        for (int i = 0; i < _codesATrouver.length; i++) {
            ResultatBalise rb = new ResultatBalise();
            Balise balise = e.getBalises().getBalises().get(i);
            rb.setCode(balise.getNumero());
            if (_okPm[i]) {
                rb.setPoints(balise.getPoints());
                rb.setTemps(balise.getTemps());
                rb.setTime(_temps[i]);
            } else {
                rb.setPm(true);
                rb.setPoints(balise.getPointsPosteManquant());
                rb.setTemps(balise.getTempsPosteManquant());
            }
            _resultatBalises.add(rb);
            // gestion du gel du chrono
            if (balise.isArreterChrono()) {
                chronoEnCours = false;
                //arretChrono = rp.getPuce().getTimePartielFirstCode(balise.getNumero()).getTime();
                arretChrono = TimeManager.safeParse(_temps[i]).getTime();
            }
            if (balise.isDemarerChrono() && !chronoEnCours) {
                chronoEnCours = true;
                //totalGelChrono = totalGelChrono + rp.getPuce().getTimePartielFirstCode(balise.getNumero()).getTime() - arretChrono;
                totalGelChrono = totalGelChrono + TimeManager.safeParse(_temps[i]).getTime() - arretChrono;
            }
        }
    }

    private boolean isDoubleCode( int code ) {
        return _codesATrouver[0] == _codesATrouver[_codesATrouver.length - 1];
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public long getTotaltempsdepassement() {
        return totaltempsdepassement;
    }

    public void setTotaltempsdepassement( long totaltempsdepassement ) {
        this.totaltempsdepassement = totaltempsdepassement;
    }

    public void setTotalPoints( int totalPoints ) {
        this.totalPoints = totalPoints;
    }

    public int getTotalPointsFinal() {
        return totalPointsFinal;
    }

    public void setTotalPointsFinal( int totalPointsFinal ) {
        this.totalPointsFinal = totalPointsFinal;
    }

    public int getTotalPointsPenalite() {
        return totalPointsPenalite;
    }

    public void setTotalPointsPenalite( int totalPointsPenalite ) {
        this.totalPointsPenalite = totalPointsPenalite;
    }

    public int getTotalPointsDepassement() {
        return _totalPointsDepassement;
    }

    public void setTotalPointsDepassement( int totalPointsDepassement ) {
        _totalPointsDepassement = totalPointsDepassement;
    }

    public long getTotalTemps() {
        return totalTemps;
    }

    public void setTotalTemps( long totalTemps ) {
        this.totalTemps = totalTemps;
    }

    public long getTotalTempsFinal() {
        return totalTempsFinal;
    }

    public void setTotalTempsFinal( long totalTempsFinal ) {
        this.totalTempsFinal = totalTempsFinal;
    }

    public long getTotalGelChrono() {
        return totalGelChrono;
    }

    public void setTotalGelChrono( long totalGelChrono ) {
        this.totalGelChrono = totalGelChrono;
    }

    public long getTotaltempsPenalite() {
        return totaltempsPenalite;
    }

    public void setTotaltempsPenalite( long totaltempsPenalite ) {
        this.totaltempsPenalite = totaltempsPenalite;
    }

    public long getTotaltempsBonif() {
        return totaltempsBonif;
    }

    public void setTotaltempsBonif( long totaltempsBonif ) {
        this.totaltempsBonif = totaltempsBonif;
    }

    public Vector<ResultatBalise> getResultatBalises() {
        return _resultatBalises;
    }

    public void setResultatBalises( Vector<ResultatBalise> resultatBalises ) {
        _resultatBalises = resultatBalises;
    }

    public boolean isUsed() {
        return used;
    }

    public void setUsed( boolean used ) {
        this.used = used;
    }

    private void avantEpreuveSuivante( Epreuve e, ResultatPuce rp ) {
        Balises balises = rp.getEtape().getBalisesEpreuveSuivante(e);
        if (balises != null) {
            for (int i = 0; i < _okPm.length; i++) {
                if (_okPm[i]) {
                    int positionPosteATrouver = rp.getPositionPoste(_codesATrouver[i]);
                    for (int j = 0; j < balises.getBalises().size(); j++) {
                        int positionPosteSuivant = rp.getPositionPoste(balises.getBalises().get(j).getNumero());
                        if (positionPosteSuivant > -1 && positionPosteATrouver > positionPosteSuivant) {
                            _okPm[i] = false;
                        }
                    }
                }
            }
        }
    }

    private void apresEpreuvePrecedente( Epreuve e, ResultatPuce rp ) {
        Balises balises = rp.getEtape().getBalisesEpreuvePrcedente(e);
        if (balises != null) {
            for (int i = 0; i < _okPm.length; i++) {
                if (_okPm[i]) {
                    int positionPosteATrouver = rp.getPositionPoste(_codesATrouver[i]);
                    for (int j = 0; j < balises.getBalises().size(); j++) {
                        int positionPostePrecedent = rp.getPositionPoste(balises.getBalises().get(j).getNumero());
                        if (positionPostePrecedent > -1 && positionPosteATrouver < positionPostePrecedent) {
                            _okPm[i] = false;
                        }
                    }
                }
            }
        }
    }

    public Epreuve getEpreuve() {
        return _epreuve;
    }

    public int[] getCodesATrouver() {
        return _codesATrouver;
    }

    public boolean[] getOkPm() {
        return _okPm;
    }

    public String[] getTemps() {
        return _temps;
    }

    public long getTotalTempsMultiplicateur() {
        return _totalTempsMultiplicateur;
    }
}
