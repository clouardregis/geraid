package fr.gecoraid.model;

import java.util.Collections;
import java.util.Vector;

// TODO refactor
public class ResultatsParcoursEquipe {
    private final Vector<ResultatParcoursEquipe> _resultat = new Vector<>();

    public Vector<ResultatParcoursEquipe> getResultat() {
        return _resultat;
    }

    public ResultatsParcoursEquipe() {
        //this.geRaid = g;
    }

    public void ajouterResultat( ResultatPuce rp, int points, int temps ) {
        getResultatParcoursEquipe(rp).ajouterResultat(rp, points, temps);
    }

    private ResultatParcoursEquipe getResultatParcoursEquipe( ResultatPuce rp ) {
        for (ResultatParcoursEquipe resultatParcoursEquipe : _resultat) {
            if (resultatParcoursEquipe.getEquipe().equals(rp.getEquipe())) {
                return resultatParcoursEquipe;
            }
        }
        ResultatParcoursEquipe retour = new ResultatParcoursEquipe(rp.getParcours(), rp.getEquipe());
        _resultat.add(retour);
        return retour;
    }

    public int getSize() {
        return _resultat.size();
    }

    public void trier( boolean puceParEquipe ) {
        Collections.sort(_resultat);
        if (puceParEquipe) {
            simplifier();
        }
    }

    public void simplifier() {
        for (int i = 0; i < _resultat.size(); i++) {
            String dossard = _resultat.get(i).getEquipe().getDossard();
            if (dossard.compareTo("") != 0) {
                if (existeDossard(i + 1, dossard)) {
                    _resultat.remove(i);
                    i--;
                }
            }
        }
    }

    private boolean existeDossard( int index, String dossard ) {
        for (int i = index; i < _resultat.size(); i++) {
            if (_resultat.get(i).getEquipe().getDossard().compareTo(dossard) == 0) {
                return true;
            }
        }
        return false;
    }
}
