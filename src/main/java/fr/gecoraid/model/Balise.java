package fr.gecoraid.model;

import fr.gecoraid.tools.TimeManager;

/**
 * Une balise est un poste matérialisé par un boîtier SPORTident qui permet généralement d’engranger
 * des points, des bonifications ou des pénalités en temps. GeRaid permet également
 * d’attribuer à certaines balises des fonctions de contrôle.
 */
// TODO refactor -> SOLID
public class Balise implements Cloneable {
    private int _numero = 31;
    private boolean _demarerChrono = false;
    private boolean _arreterChrono = false;
    private int _points = 0;
    private int _temps = 0;
    private int _pointsPm = 0;
    private int _tempsPm = 0;

    public Balise() {
    }

    public Balise( int numero ) {
        _numero = numero;
    }

    public Object clone() {
        try {
            return super.clone();
        } catch (CloneNotSupportedException cnse) {
            // Ne devrait jamais arriver car nous implémentons
            // l'interface Cloneable
            cnse.printStackTrace(System.err);
        }
        return null;
    }

    public int getNumero() {
        return _numero;
    }

    public void setNumero( int numero ) {
        _numero = numero;
    }

    @Override
    public String toString() {
        // TODO L10n.getString()
        StringBuilder retour = new StringBuilder("Balise " + _numero + " : ");
        if (_points != 0) {
            // TODO L10n.getString()
            retour.append(_points).append(" pts");
            if (_temps > 0) {
                retour.append(" / ").append(TimeManager.fullTime(_temps * 1000L));
            }
            if (_temps < 0) {
                retour.append(" / ").append(TimeManager.fullTime(_temps * 1000L));
            }
        } else if (_temps != 0) {
            if (_temps > 0) {
                retour.append("  ").append(TimeManager.fullTime(_temps * 1000L));
            }
            if (_temps < 0) {
                retour.append("  ").append(TimeManager.fullTime(_temps * 1000L));
            }
        }
        if (_pointsPm != 0) {
            // TODO L10n.getString()
            retour.append(" (Si PM : ").append(_pointsPm).append(" pts");
            if (_tempsPm > 0) {
                retour.append(" / ").append(TimeManager.fullTime(_tempsPm * 1000L));
            }
            if (_tempsPm < 0) {
                retour.append(" / ").append(TimeManager.fullTime(_tempsPm * 1000L));
            }
            retour.append(")");
        } else if (_tempsPm != 0) {
            retour.append(" (Si PM : ");
            if (_tempsPm > 0) {
                retour.append(TimeManager.fullTime(_tempsPm * 1000L));
            }
            if (_tempsPm < 0) {
                retour.append(TimeManager.fullTime(_tempsPm * 1000L));
            }
            retour.append(")");
        }
        if (_demarerChrono) {
            // TODO L10n.getString()
            retour.append(" avec arrêt du gel du chrono");
        }
        if (_arreterChrono) {
            // TODO L10n.getString()
            retour.append(" avec démarrage du gel du chrono");
        }
        return retour.toString();
    }

    public boolean isDemarerChrono() {
        return _demarerChrono;
    }

    public void setDemarerChrono( boolean demarerChrono ) {
        _demarerChrono = demarerChrono;
    }

    public boolean isArreterChrono() {
        return _arreterChrono;
    }

    // TODO arret et depart du chronometre sur les parties gelées
    // TODO permettre de passer par la balise 1 puis 2 ou 2 puis 1.
    public void setArreterChrono( boolean arreterChrono ) {
        _arreterChrono = arreterChrono;
    }

    public int getPoints() {
        return _points;
    }

    public void setPoints( int points ) {
        _points = points;
    }

    public int getTemps() {
        return _temps;
    }

    public void setTemps( int temps ) {
        _temps = temps;
    }

    public void setPointsPosteManquant( int pointsPm ) {
        _pointsPm = pointsPm;
    }

    public int getPointsPosteManquant() {
        return _pointsPm;
    }

    public void setTempsPosteManquant( int tempsPm ) {
        _tempsPm = tempsPm;
    }

    public int getTempsPosteManquant() {
        return _tempsPm;
    }

    public String toStringBalise() {
        return "<li>" + this + "</li>";
    }
}
