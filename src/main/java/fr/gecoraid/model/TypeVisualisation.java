package fr.gecoraid.model;

public enum TypeVisualisation implements IVisualisation {
    // TODO L10n.getString()
    SIMPLE("Simple"),
    AVEC_EPREUVE("Avec épreuves"),
    COMPLET("Totale");

    private final String value;

    TypeVisualisation( String value ) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return value;
    }
}
