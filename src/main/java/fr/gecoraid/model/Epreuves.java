package fr.gecoraid.model;

import java.util.Vector;

// TODO refactor -> SOLID
public class Epreuves implements Cloneable {
    private Vector<Epreuve> _epreuves = new Vector<>();

    public Object clone() {
        try {
            Epreuves p = (Epreuves) super.clone();
            p._epreuves = new Vector<>();
            for (Epreuve epreuve : _epreuves) {
                p.getEpreuves().add((Epreuve) epreuve.clone());
            }
            return p;
        } catch (CloneNotSupportedException cnse) {
            // Ne devrait jamais arriver car nous implémentons
            // l'interface Cloneable
            cnse.printStackTrace(System.err);
            return null;
        }
    }

    public int getTotalPointsEpreuves() {
        int retour = 0;
        for (Epreuve epreuve : _epreuves) {
            retour = retour + epreuve.getTotalPointsEpreuve();
        }
        return retour;
    }

    public Vector<Epreuve> getEpreuves() {
        return _epreuves;
    }

    public void upEpreuve( Epreuve b ) {
        int index = _epreuves.indexOf(b);
        if (index > 0) {
            _epreuves.add(index - 1, b);
            _epreuves.remove(index + 1);
        }
    }

    public void downEpreuve( Epreuve b ) {
        int index = _epreuves.indexOf(b);
        if (index < _epreuves.size() - 1) {
            _epreuves.add(index + 2, b);
            _epreuves.remove(index);
        }
    }

    public void addEpreuve( Epreuve e ) {
        if (existeNom(e) == null) {
            _epreuves.add(e);
        }
    }

    private Epreuve existeNom( Epreuve e ) {
        for (Epreuve epreuve : _epreuves) {
            if (epreuve != e && epreuve.getName().compareTo(e.getName().trim()) == 0) {
                return epreuve;
            }
        }
        return null;
    }

    public boolean existeEpreuve( String nom, Epreuve e ) {
        for (Epreuve epreuve : _epreuves) {
            if (epreuve.getName().toUpperCase().compareTo(nom.trim().toUpperCase()) == 0) {
                if (!epreuve.equals(e)) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean existeNomEpreuve( String nom ) {
        for (Epreuve epreuve : _epreuves) {
            if (epreuve.getName().toUpperCase().compareTo(nom.trim().toUpperCase()) == 0) {
                return true;
            }
        }
        return false;
    }

    public void removeEpreuve( Epreuve e ) {
        _epreuves.remove(e);
    }

    public int getSize() {
        return _epreuves.size();
    }

    public int getDernierNumero() {
        int retour = 30;
        for (Epreuve epreuve : _epreuves) {
            if (epreuve.getDernierNumero() > retour) {
                retour = epreuve.getDernierNumero();
            }
        }
        return retour;
    }

    public String toStringEpreuves() {
        StringBuilder retour = new StringBuilder("<ul>");
        for (Epreuve epreuve : _epreuves) {
            retour.append(epreuve.toStringEpreuve(this));
        }
        retour.append("</ul>");
        return retour.toString();
    }

    public Vector<Integer> getCodesEnPlus( ResultatPuce rp ) {
        Vector<Integer> retour = new Vector<>();
        final Partial[] partiels = rp.getPuce().getPartiels();
        for (Partial partiel : partiels) {
            if (!existeCode(partiel.getCode())) {
                retour.add(partiel.getCode());
            }
        }
        return retour;
    }

    public boolean existeCode( int code ) {
        for (Epreuve epreuve : _epreuves) {
            for (int j = 0; j < epreuve.getBalises().getSize(); j++) {
                if (epreuve.getBalises().getBalises().get(j).getNumero() == code) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean isFirstEpreuve( Epreuve e ) {
        if (!_epreuves.isEmpty()) {
            return e.equals(_epreuves.get(0));
        }
        return false;
    }

    public int getLastCodeEpreuvePrecedente( Epreuve e ) {
        for (int i = 0; i < _epreuves.size(); i++) {
            if (e.equals(_epreuves.get(i))) {
                return _epreuves.get(i - 1).getDernierNumeroBalises();
            }
        }
        return 30;
    }

    public boolean isLastEpreuveAvantArrivee( Epreuve epreuve ) {
        if (epreuve.equals(_epreuves.get(getSize() - 1)) && !epreuve.isHorsChrono()) {
            return true;
        }
        if (getRangEpreuve(epreuve) < _epreuves.size() - 1) {
            return !epreuve.isHorsChrono() && _epreuves.get(getRangEpreuve(epreuve) + 1).isHorsChrono();
        }
        return false;
    }

    public int getRangEpreuve( Epreuve e ) {
        for (int i = 0; i < _epreuves.size(); i++) {
            if (e.equals(_epreuves.get(i))) {
                return i;
            }
        }
        return -1;
    }

    public int getFirstCodeEpreuveSuivante( Epreuve epreuve ) {
        for (int i = 0; i < _epreuves.size(); i++) {
            if (epreuve.equals(_epreuves.get(i))) {
                return _epreuves.get(i + 1).getPremierNumeroBalises();
            }
        }
        return 30;
    }
}
