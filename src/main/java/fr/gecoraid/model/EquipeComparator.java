package fr.gecoraid.model;

import java.util.Comparator;

// TODO refactor -> SOLID
public class EquipeComparator implements Comparator<Equipe> {
    private static final int COMPARE_BY_DOSSARD = 0;
    private static final int COMPARE_BY_NOMEQUIPE = 1;
    private static final int COMPARE_BY_PUCE = 2;
    private static final int COMPARE_BY_CATEGORIE = 3;

    private final TypeTri _compareMode;

    public EquipeComparator( TypeTri compareMode ) {
        _compareMode = compareMode;
    }

    @Override
    public int compare( Equipe o1, Equipe o2 ) {
        switch (_compareMode) {
            case DOSSARD:
                if (o1.getDossard().compareTo(o2.getDossard()) > 0) {
                    return 1;
                } else {
                    return -1;
                }

            case NOM:
                if (o1.getNom().compareTo(o2.getNom()) > 0) {
                    return 1;
                } else {
                    return -1;
                }

            case CATEGORIE:
                if (o1.getCategorie().getShortName().compareTo(o2.getCategorie().getShortName()) >= 0) {
                    return 1;
                } else {
                    return -1;
                }

            case PUCE:
                if (o1.getIdPuce().compareTo(o2.getIdPuce()) > 0) {
                    return 1;
                } else {
                    return -1;
                }

            default:
                return 0;
        }
    }

}
