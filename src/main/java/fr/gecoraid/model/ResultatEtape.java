package fr.gecoraid.model;

import fr.gecoraid.tools.TimeManager;

import java.util.Vector;

// TODO refactor
public class ResultatEtape {
    private final Categorie _categorie;
    private final TypeVisualisation _tv;
    private final Vector<String> _entetes = new Vector<>();
    private final Vector<ResultatEquipe> _data = new Vector<>();
    private final Etape _etape;
    private final Raid _geRaid;

    public Raid getGeRaid() {
        return _geRaid;
    }

    public Etape getEtape() {
        return _etape;
    }

    public Categorie getCategorie() {
        return _categorie;
    }

    public ResultatEtape( Raid g, Etape e, Categorie c, TypeVisualisation tv, boolean puceParEquipe ) {
        _geRaid = g;
        _etape = e;
        _categorie = c;
        _tv = tv;
        editEntetes();
        editData(puceParEquipe);
    }

    public String getHeaderAsHtml() {
        // TODO L10n.getString()
        return "<table><tr align=center style='font-weight: bold;'>" + "<td>" + "Clt" + "</td>" +
                "<td>" + "Dossard" + "</td>" +
                "<td>" + "Nom équipe" + "</td>" +
                "<td>" + "Catégorie" + "</td>" +
                "<td>" + "Pts total" + "</td>" +
                "<td>" + "Tps final" + "</td>" +
                "</tr>";
    }

    private void editEntetes() {
        // Visualisation simple
        _entetes.clear();
        // Classement
        // TODO L10n.getString()
        _entetes.add("Clt");
        _entetes.add("Dossard");
        _entetes.add("Puce");
        _entetes.add("Nom équipe");
        _entetes.add("Catégorie");
        _entetes.add("Tps final");
        _entetes.add("Pts finaux");
        // Temps
        _entetes.add("Départ");
        _entetes.add("Arrivée");
        _entetes.add("Tps course");
        _entetes.add("Bonification");
        _entetes.add("Pénalités");
        _entetes.add("Dépassement");
        _entetes.add("Gel");
        _entetes.add("Pénalités hors étape"); // 14
        // Points
        _entetes.add("Bonification");
        _entetes.add("Pénalités");
        _entetes.add("Dépassement");
        _entetes.add("Pénalités hors étape"); // 18
        // Gestion des autres visualisations
        if (_tv != null && !_tv.equals(TypeVisualisation.SIMPLE)) {
            if (_etape != null) {
                for (int i = 0; i < _etape.getEpreuves().getSize(); i++) {
                    // Visualisation avec épreuves
                    Epreuve e = _etape.getEpreuves().getEpreuves().get(i);
                    _entetes.add(e.getName());
                    // Classement
                    _entetes.add("Tps final");
                    _entetes.add("Pts finaux");
                    // Temps
                    _entetes.add("Tps course");
                    _entetes.add("Bonification");
                    _entetes.add("Pénalités");
                    _entetes.add("Dépassement");
                    _entetes.add("Gel");
                    // Points
                    _entetes.add("Bonification");
                    _entetes.add("Pénalités");
                    _entetes.add("Dépassement");

                    // Visualisation complète
                    if (_tv.equals(TypeVisualisation.COMPLET)) {
                        for (int j = 0; j < e.getBalises().getSize(); j++) {
                            _entetes.add("Code");
                            _entetes.add("Ok/Pm"); // heure de passage ou PM
                            _entetes.add("Points");
                            _entetes.add("Temps");
                        }
                    }
                }
            }
        }
    }

    public String[] getEntetes() {
        return _entetes.toArray(new String[0]);
    }

    private void editData( boolean puceParEquipe ) {
        for (int i = 0; i < _geRaid.getResultatsPuce().getSize(); i++) {
            ResultatPuce rp = _geRaid.getResultatsPuce().getResultatsPuce().get(i);
            if (rp.getParcours().estPresqueEgal(_geRaid.getParcours(_etape)) && rp.getEtape().estPresqueEgal(_etape)) {
                if (_geRaid.getCategories().getCategories().contains(_categorie)) {
                    if (rp.getEquipe().getCategorie().equals(_categorie)) {
                        ajouterResultat(rp);
                    }
                } else {
                    ajouterResultat(rp);
                }
            }
        }
        if (puceParEquipe) {
            simplifier();
        }
        trierNC();
        faireClassement();
    }

    private void ajouterResultat( ResultatPuce rp ) {
        int points = _geRaid.getPenalites().getTotalPointPenalite(_geRaid.getParcours(_etape), _etape, rp.getEquipe().getIdPuce());
        int temps = _geRaid.getPenalites().getTotalTempsPenalite(_geRaid.getParcours(_etape), _etape, rp.getEquipe().getIdPuce());
        ResultatEquipe re = new ResultatEquipe(rp, points, temps);
        int index = indexAdd(re.getTotalPoints(), re.getTotalTemps());
        if (index == -1) {
            _data.add(re);
        } else {
            _data.add(index, re);
        }
    }

    public String getDataAsHtml() {
        StringBuilder retour = new StringBuilder();
        for (int i = 0; i < _data.size(); i++) {
            retour.append("<tr style='");
            if (i % 2 == 0) {
                retour.append("background-color: #FFFFE0;'");
            } else {
                retour.append("background-color: #E0FFFF;'");
            }
            retour.append(" align=center>");
            if (_data.get(i).getClassement() == -1) {
                // TODO L10n.getString()
                retour.append("<td>NC</td>");
            } else {
                retour.append("<td>").append(_data.get(i).getClassement()).append("</td>");
            }
            retour.append("<td>").append(_data.get(i).getDossardEquipe()).append("</td>");
            retour.append("<td>").append(_data.get(i).getNomEquipe()).append("</td>");
            retour.append("<td>").append(_data.get(i).getCategorieEquipe()).append("</td>");
            retour.append("<td>").append(_data.get(i).getTotalPoints()).append("</td>");
            retour.append("<td>").append(TimeManager.fullTime(_data.get(i).getTotalTemps())).append("</td>");
            retour.append("</tr>");
        }
        retour.append("</table>");

        return retour.toString();
    }

    public Object[][] getData() {
        Object[][] retour = new Object[_data.size()][_entetes.size()];
        for (int i = 0; i < _data.size(); i++) {
            for (int j = 0; j < _entetes.size(); j++) {
                try {
                    retour[i][j] = _data.get(i).toVector(_tv).get(j);
                } catch (ArrayIndexOutOfBoundsException e) {
                    // Was unused.
                    retour[i][j] = "";
                }
            }
        }
        return retour;
    }

    public Object[][] getData( Epreuve e, TypeVisualisationEpreuve tv ) {
        Object[][] retour = new Object[_data.size()][Epreuve.getEntetes(e, tv).length];
        for (int i = 0; i < _data.size(); i++) {
            for (int j = 0; j < Epreuve.getEntetes(e, tv).length; j++) {
                retour[i][j] = _data.get(i).toVector(e, tv).get(j);
            }
        }
        return retour;
    }

    private int indexAdd( int pts, long tps ) {
        return indexFirstTemps(indexFirstPoints(pts), pts, tps);
    }

    private int indexFirstPoints( int pts ) {
        for (int i = 0; i < _data.size(); i++) {
            if (_data.get(i).getTotalPoints() <= pts) {
                return i;
            }
        }
        return 0;
    }

    private int indexFirstTemps( int index, int pts, long tps ) {
        for (int i = index; i < _data.size(); i++) {
            if (_data.get(i).getTotalPoints() == pts && _data.get(i).getTotalTemps() >= tps) {
                return i;
            }
            if (_data.get(i).getTotalPoints() < pts) {
                return i;
            }
        }

        return -1;
    }

    private void faireClassement() {
        for (int i = 0; i < _data.size(); i++) {
            if (_data.get(i).getClassement() != -1) {
                _data.get(i).setClassement(i + 1);
            }
        }
    }

    private void trierNC() {
        int index = 0;
        for (int i = 0; i < _data.size(); i++) {
            if (_data.get(index).getClassement() == -1) {
                _data.add(_data.get(index));
                _data.remove(index);
            } else {
                index++;
            }
        }
    }

    public void simplifier() {
        for (int i = 0; i < _data.size(); i++) {
            String dossard = _data.get(i).getDossardEquipe();
            if (dossard.compareTo("") != 0) {
                if (existeDossard(i + 1, dossard)) {
                    _data.remove(i);
                    i--;
                }
            }
        }
    }

    private boolean existeDossard( int index, String dossard ) {
        for (int i = index; i < _data.size(); i++) {
            if (_data.get(i).getDossardEquipe().compareTo(dossard) == 0) {
                return true;
            }
        }
        return false;
    }
}
