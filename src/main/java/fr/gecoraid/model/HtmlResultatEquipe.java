package fr.gecoraid.model;

import fr.gecoraid.desktop.widget.ErrorPane;

import java.io.File;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;

public class HtmlResultatEquipe {
    // TODO clean code
    public static void save( ResultatEquipe resultatEquipe, String fichier, Raid raid, boolean resultatReduit ) {
        File file = new File(fichier);
        try (Writer fileWriter = new OutputStreamWriter(Files.newOutputStream(file.toPath()), StandardCharsets.UTF_8)) {
            // TODO factoriser l'entête html
            StringBuilder html = new StringBuilder("<!doctype html><html lang=\"fr-FR\"><head><meta charset=\"UTF-8\"><meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\"></head><body>");
            if (resultatReduit) {
                html.append("<font size=2>");
                html.append(raid.getPageHeader()).append("<br>");
                html.append("<b>").append(raid.getRaidName()).append("</b></font><br><br>");
                html.append(resultatEquipe.toHtmlReduit());
                html.append("<br><font size=2>").append(raid.getPageFooter()).append("</font><br>");
                // TODO L10n.getString()
                html.append("<b>Chronométré avec GeCoRaid</b>");
            } else {
                html.append("<font size=1>");
                html.append(raid.getPageHeader()).append("<br>");
                html.append(resultatEquipe.toHtml());
                html.append(raid.getPageFooter()).append("<br>");
                html.append("</font>");
            }
            html.append("</body></html>");
            fileWriter.write(html.toString(), 0, html.length());
            fileWriter.write(System.lineSeparator());
        } catch (IOException e) {
            // TODO L10n.getString()
            ErrorPane.showMessageDialog("Erreur d'écriture du fichier en html : " + e.getClass().getName() + ", " + e.getMessage());
        }
    }
}
