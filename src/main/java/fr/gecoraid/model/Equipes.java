package fr.gecoraid.model;

import java.util.Collections;
import java.util.Vector;

// TODO refactor -> SOLID
public class Equipes {
    private Vector<Equipe> _equipes = new Vector<>();

    public Equipes() {
    }

    public int getNbMaxEquipiers() {
        int retour = 0;
        for (Equipe equipe : _equipes) {
            if (equipe.getRaiders().getSize() > retour) {
                retour = equipe.getRaiders().getSize();
            }
        }
        return retour;
    }

    public Vector<Equipe> getEquipes() {
        return _equipes;
    }

    public void setEquipes( Vector<Equipe> equipes ) {
        _equipes = equipes;
    }

    public boolean addEquipe( Equipe equipe ) {
        boolean retour = false;
        if (existeIdPuce(equipe) == null) {
            _equipes.add(equipe);
            Collections.sort(_equipes, new EquipeComparator(TypeTri.DOSSARD));
            retour = true;
        }
        return retour;
    }

    private Equipe existeIdPuce( Equipe e ) {
        Equipe retour = null;
        for (Equipe equipe : _equipes) {
            if (equipe.getIdPuce().compareTo("") != 0) {
                if (equipe != e && equipe.getIdPuce().compareTo(e.getIdPuce().trim()) == 0) {
                    return equipe;
                }
            }
        }
        return retour;
    }

    public boolean existeIdPuce( String idPuce, Equipe e ) {
        boolean retour = false;
        for (Equipe equipe : _equipes) {
            if (equipe.getIdPuce().compareTo("") != 0) {
                if (equipe.getIdPuce().compareTo(idPuce.trim()) == 0) {
                    if (!equipe.equals(e)) {
                        return true;
                    }
                }
            }
        }

        return retour;
    }

    public boolean existeIdPuce( String idPuce ) {
        boolean retour = false;
        for (Equipe equipe : _equipes) {
            if (equipe.getIdPuce().compareTo("") != 0) {
                if (equipe.getIdPuce().compareTo(idPuce.trim()) == 0) {
                    return true;
                }
            }
        }

        return retour;
    }

    public Equipe editEquipe( Equipe e ) {
        Equipe retour = null;
        if (existeIdPuce(e) == null) {
            retour = e;
        }
        return retour;
    }

    public void removeEquipe( Equipe e ) {
        _equipes.remove(e);
    }

    public int getSize() {
        return _equipes.size();
    }

    public Equipe getEquipeIdPuce( String idPuce ) {
        for (Equipe equipe : _equipes) {
            if (equipe.getIdPuce().compareTo(idPuce.trim()) == 0) {
                return equipe;
            }
        }

        return null;
    }

    public void trierEquipes( TypeTri mode ) {
        Collections.sort(_equipes, new EquipeComparator(mode));
    }

    public void numeroterEquipes( String prefixe, int premierNumero, String suffixe ) {
        for (int i = 0; i < getSize(); i++) {
            _equipes.get(i).setDossard(prefixe + (premierNumero + i) + suffixe);
        }
    }

    public Vector<Equipe> getEquipes( String debutNomEquipe ) {
        Vector<Equipe> retour = new Vector<>();
        for (Equipe equipe : _equipes) {
            if (equipe.getNom().toUpperCase().startsWith(debutNomEquipe)) {
                retour.add(equipe);
            }
        }
        return retour;
    }
}
