package fr.gecoraid.model;

import fr.gecoraid.tools.TimeManager;

import java.awt.Point;

// TODO refactor -> SOLID
public class Parcours implements Cloneable {
    private String _nom;
    private Equipes _equipes = new Equipes();
    private Etapes _etapes = new Etapes();

    public Parcours( String nom ) {
        _nom = nom;
    }

    public int getNombreMaximumEquipiers() {
        return _equipes.getNbMaxEquipiers();
    }

    @Override
    public Object clone() {
        try {
            Parcours parcours = (Parcours) super.clone();
            parcours._etapes = (Etapes) _etapes.clone();
            parcours._nom = _nom + " -" + TimeManager.getDateHeureMinuteSeconte();
            parcours._equipes = new Equipes();
            return parcours;
        } catch (CloneNotSupportedException e) {
            // Ne devrait jamais arriver car nous implémentons l'interface Cloneable
            return null;
        }
    }

    public String getName() {
        return _nom;
    }

    public void setNom( String nom ) {
        _nom = nom;
    }

    public Equipes getEquipes() {
        return _equipes;
    }

    public void setEquipes( Equipes equipes ) {
        _equipes = equipes;
    }

    public Etapes getEtapes() {
        return _etapes;
    }

    public void setEtapes( Etapes etapes ) {
        _etapes = etapes;
    }

    public String toString() {
        return _nom;
    }

    public Equipe getEquipe( String idEquipe ) {
        for (int i = 0; i < _equipes.getSize(); i++) {
            if (_equipes.getEquipes().get(i).getIdPuce().compareTo(idEquipe) == 0) {
                return _equipes.getEquipes().get(i);
            }
        }
        return null;
    }

    public Etape getEtape( String nom ) {
        for (int i = 0; i < _etapes.getSize(); i++) {
            if (_etapes.getEtapes().get(i).getName().compareTo(nom) == 0) {
                return _etapes.getEtapes().get(i);
            }
        }
        return null;
    }

    public Etape getEtapeEnCours() {
        for (int i = 0; i < _etapes.getSize(); i++) {
            if (!_etapes.getEtapes().get(i).isFini()) {
                return _etapes.getEtapes().get(i);
            }
        }
        return null;
    }

    public boolean existeIdPuce( String idPuce, Equipe e ) {
        return _equipes.existeIdPuce(idPuce, e);
    }

    public boolean existeIdPuce( String idPuce ) {
        return _equipes.existeIdPuce(idPuce);
    }

    public boolean existeEtape( String nom, Etape e ) {
        return _etapes.existeEtape(nom, e);
    }

    public Equipe getEquipeIdPuce( String idPuce ) {
        return _equipes.getEquipeIdPuce(idPuce);
    }

    public int getIndexEtape( Etape e ) {
        for (int i = 0; i < _etapes.getSize(); i++) {
            if (_etapes.getEtapes().get(i).equals(e)) {
                return i;
            }
        }
        return -1;
    }

    public int getNombreEquipes() {
        return _equipes.getSize();
    }

    public int getNombreEquipesPresentes() {
        int retour = 0;
        for (int i = 0; i < _equipes.getSize(); i++) {
            if (!_equipes.getEquipes().get(i).isAbsent()) {
                retour++;
            }
        }
        return retour;
    }

    public String toStringParcours() {
        return "<b>Parcours : " + _nom + "</b>" + _etapes.toStringEtapes();
    }

    public boolean existeEpreuve( Epreuve epreuve ) {
        for (int i = 0; i < _etapes.getSize(); i++) {
            if (_etapes.getEtapes().get(i).existeEpreuve(epreuve)) {
                return true;
            }
        }
        return false;
    }

    public boolean existeEpreuve( Etape etape ) {
        for (int i = 0; i < _etapes.getSize(); i++) {
            if (_etapes.getEtapes().get(i).equals(etape)) {
                return true;
            }
        }
        return false;
    }

    public boolean estPresqueEgal( Parcours parcours ) {
        Point p = _compare(_nom, parcours.getName());
        double d = (double) p.y / p.x;
        return d > 0.75;

    }

    private Point _compare( String nom1, String nom2 ) {
        Point retour = new Point(0, 0);
        if (nom1.length() > nom2.length()) {
            retour.x = nom2.length();
            retour.y = nom2.length();
        } else {
            retour.x = nom1.length();
            retour.y = nom1.length();
        }
        for (int i = 0; i < retour.x; i++) {
            if (nom1.charAt(i) != nom2.charAt(i)) {
                retour.y = i;
                break;
            }
        }
        return retour;
    }

    public boolean existeEquipe( Equipe equipe ) {
        for (int i = 0; i < _equipes.getSize(); i++) {
            if (_equipes.getEquipes().contains(equipe)) {
                return true;
            }
        }
        return false;
    }
}
