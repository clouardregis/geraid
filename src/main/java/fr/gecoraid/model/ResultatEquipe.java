package fr.gecoraid.model;

import fr.gecoraid.tools.TimeManager;

import java.io.File;
import java.util.Date;
import java.util.Vector;

// TODO refactor -> SOLID
public class ResultatEquipe {
    private ResultatPuce rp;
    private int totalPoints = 0; // ce qui est retenue pour le classement après retrait ou ajout divers
    private int totalPointsFinal = 0;
    private int totalPointsPenalite = 0;
    private int totalPointsDepassement = 0;
    private final int totalPointsPenalitesHorsEpreuves;
    private Date depart = null;
    private final Date arrivee;
    private long totalTemps = 0; // ce qui est retenue pour le classement après retrait ou ajout divers
    private final long totalTempsFinal;
    private long totalGelChrono = 0;
    private long totaltempsPenalite = 0;
    private long totaltempsBonif = 0;
    private int _classement = 0;
    private int penaliteDepassementEtapePoints = 0;
    private long penaliteDepassementEtapeTemps = 0;
    private final int totalTempsPenalitesHorsEpreuves;
    private final String puce;
    private final String dossardEquipe;
    private final String nomEquipe;
    private final String categorieEquipe;
    private final ResultatEpreuves resultatEpreuves;
    private final ListeCodeOkPm listeCodeOkPm = new ListeCodeOkPm();

    public ResultatEquipe( ResultatPuce rp, int points, int temps ) {
        this.rp = rp;
        totalPointsPenalitesHorsEpreuves = points;
        totalTempsPenalitesHorsEpreuves = temps;
        // Classement NC
        if (rp.getEquipe().isNonClassee()) {
            _classement = -1;
        }
        // Init identification de l'équipe
        dossardEquipe = rp.getEquipe().getDossard();
        puce = rp.getEquipe().getIdPuce();
        //nomEquipe = rp.getEquipe().getNom();
        nomEquipe = rp.getEquipe().getNomEtRaiders();
        categorieEquipe = rp.getEquipe().getCategorie().getLongName();
        // Calcul du temps de course
        switch (rp.getEtape().getType()) {
            case GROUPE:
                depart = rp.getEtape().getHeureDepart();
                listeCodeOkPm.getListe().add(new CodeOkPm(16, true, ""));
                break;
            case BOITIER:
                depart = rp.getPuce().getStarttime();
                // A tester si le départ n'est pas poinçonné
                if (depart.getTime() < 0) {
                    listeCodeOkPm.getListe().add(new CodeOkPm(16, false, ""));
                } else {
                    listeCodeOkPm.getListe().add(new CodeOkPm(16, true, ""));
                }
                break;

            default:
                break;
        }
        arrivee = rp.getPuce().getFinishtime();
        totalTempsFinal = arrivee.getTime() - depart.getTime();

        // Calcul des résultats des épreuves
        resultatEpreuves = new ResultatEpreuves();
        long totaltempsMultiplicateur = 0;
        for (int i = 0; i < rp.getEtape().getEpreuves().getSize(); i++) {
            Epreuve e = rp.getEtape().getEpreuves().getEpreuves().get(i);
            // Création du résultat de l'épreuve
            resultatEpreuves.addResultatEpreuve(e, rp);
            ResultatEpreuve re = resultatEpreuves.getResultatEpreuves().get(i);
            if (e.isExclusiveWithNextEpreuve()) {
                i++;
                Epreuve e2 = rp.getEtape().getEpreuves().getEpreuves().get(i);
                resultatEpreuves.addResultatEpreuve(e2, rp);
                ResultatEpreuve re2 = resultatEpreuves.getResultatEpreuves().get(i);
                if (re.getTotalPoints() > re2.getTotalPoints()) {
                    re2.setUsed(false);
                } else if (re.getTotalPoints() < re2.getTotalPoints()) {
                    re.setUsed(false);
                    re = re2;
                } else if (re.getTotalTemps() < re2.getTotalTemps()) {
                    re2.setUsed(false);
                } else {
                    re.setUsed(false);
                    re = re2;
                }
            }
            // Edition de la liste des codes Ok
            listeCodeOkPm.add(re.getCodesATrouver(),
                    re.getOkPm(),
                    re.getTemps());
            // Cumul des points
            totalPoints = totalPoints + re.getTotalPoints();
            totalPointsFinal = totalPointsFinal + re.getTotalPointsFinal();
            totalPointsPenalite = totalPointsPenalite + re.getTotalPointsPenalite();
            totalPointsDepassement = totalPointsDepassement + re.getTotalPointsDepassement();
            // Cumul des temps
            totalGelChrono = totalGelChrono + re.getTotalGelChrono();
            totaltempsBonif = totaltempsBonif + re.getTotaltempsBonif();
            totaltempsPenalite = totaltempsPenalite + re.getTotaltempsPenalite() + re.getTotaltempsdepassement() * 60;
            totaltempsMultiplicateur = totaltempsMultiplicateur + re.getTotalTempsMultiplicateur();
        }
        // si l'arrivée n'est pas poinçonnée
        if (arrivee.getTime() > 172800000 || arrivee.getTime() < 0) {
            listeCodeOkPm.getListe().add(new CodeOkPm(15, false, ""));
        } else {
            listeCodeOkPm.getListe().add(new CodeOkPm(15, true, ""));
        }
        // Calcul des pénalités de dépassement
        switch (rp.getEtape().getTypeLimite()) {
            case AVECLIMITEHORAIRE:
                penaliteDepassementHoraire();
                break;
            case AVECLIMITETEMPS:
                penaliteDepassementTemps();
                break;

            default:
                break;
        }
        // calculs finaux
        //totaltempsPenalite = totaltempsPenalite + totalTempsPenalitesHorsEpreuves;
        totalTemps = totalTempsFinal + totaltempsPenalite * 1000 + totalTempsPenalitesHorsEpreuves * 1000L + totaltempsBonif * 1000 - totalGelChrono + penaliteDepassementEtapeTemps * 60000 + totaltempsMultiplicateur;
        totalPointsPenalite = totalPointsPenalite + totalPointsDepassement;
        totalPoints = totalPoints - penaliteDepassementEtapePoints + totalPointsPenalitesHorsEpreuves;

        // edition des postes en plus
        listeCodeOkPm.setCodesEnPlus(rp.getEtape().getEpreuves().getCodesEnPlus(rp));
    }

    public int getTotalPoints() {
        return totalPoints;
    }

    public long getTotalTemps() {
        return totalTemps;
    }

    private void penaliteDepassementHoraire() {
        long diff = arrivee.getTime() - rp.getEtape().getHeureLimite().getTime();
        if (diff > 0) {
            float minutes = (float) diff / 60000 / rp.getEtape().getIntervalPenalite();
            int mnEntamee = (int) Math.ceil(minutes);
            penaliteDepassementEtapePoints = mnEntamee * rp.getEtape().getPenalite();
            penaliteDepassementEtapeTemps = (long) mnEntamee * rp.getEtape().getPenaliteTemps();
        }
    }

    private void penaliteDepassementTemps() {
        long diff = totalTempsFinal - rp.getEtape().getTempsLimite().getTime();
        if (rp.getEtape().isGelDansLimiteTemps()) {
            diff = diff - totalGelChrono;
        }
        if (diff > 0) {
            float minutes = (float) diff / 60000 / rp.getEtape().getIntervalPenalite();
            int mnEntamee = (int) Math.ceil(minutes);
            penaliteDepassementEtapePoints = mnEntamee * rp.getEtape().getPenalite();
            penaliteDepassementEtapeTemps = (long) mnEntamee * rp.getEtape().getPenaliteTemps();
        }
    }

    public int getClassement() {
        return _classement;
    }

    public void setClassement( int value ) {
        _classement = value;
    }

    public ResultatPuce getRp() {
        return rp;
    }

    public void setRp( ResultatPuce rp ) {
        this.rp = rp;
    }

    public Vector<String> toVector( TypeVisualisation tv ) {
        Vector<String> retour = new Vector<>();
        // Visualisation simple
        if (_classement == -1) {
            retour.add("NC");
        } else {
            retour.add(_classement + "");
        }
        retour.add(dossardEquipe);
        retour.add(puce);
        retour.add(nomEquipe);
        retour.add(categorieEquipe);
        retour.add(TimeManager.fullTime(totalTemps));
        retour.add(totalPoints + "");
        // Temps
        retour.add(TimeManager.fullTime(depart));
        retour.add(TimeManager.fullTime(arrivee));
        retour.add(TimeManager.fullTime(totalTempsFinal));
        retour.add(TimeManager.fullTime(totaltempsBonif * 1000));
        retour.add(TimeManager.fullTime(totaltempsPenalite * 1000));
        retour.add(TimeManager.fullTime(penaliteDepassementEtapeTemps * 60000));
        retour.add(TimeManager.fullTime(totalGelChrono));
        retour.add(TimeManager.fullTime(totalTempsPenalitesHorsEpreuves * 1000L));
        // Points
        retour.add(totalPointsFinal + "");
        retour.add(totalPointsPenalite + "");
        retour.add(penaliteDepassementEtapePoints + "");
        retour.add(totalPointsPenalitesHorsEpreuves + "");
        // Autres visualisations
        if (!tv.equals(TypeVisualisation.SIMPLE)) {
            // Visualisation avec épreuves
            for (int i = 0; i < resultatEpreuves.getResultatEpreuves().size(); i++) {
                // Visualisation avec épreuves
                ResultatEpreuve re = resultatEpreuves.getResultatEpreuves().get(i);
                if (!re.isUsed()) {
                    continue;
                }
                retour.add(""); // case vide du nom de l'épreuve
                // Classement
                retour.add(TimeManager.fullTime(re.getTotalTemps()));
                retour.add(re.getTotalPoints() + "");
                // Temps
                retour.add(TimeManager.fullTime(re.getTotalTempsFinal()));
                retour.add(TimeManager.fullTime(re.getTotaltempsBonif() * 1000));
                retour.add(TimeManager.fullTime(re.getTotaltempsPenalite() * 1000));
                retour.add(TimeManager.fullTime(re.getTotaltempsdepassement() * 60000));
                retour.add(TimeManager.fullTime(re.getTotalGelChrono()));
                // Points
                retour.add(re.getTotalPointsFinal() + "");
                retour.add(re.getTotalPointsPenalite() + "");
                retour.add(re.getTotalPointsDepassement() + "");
                // Visualisation complète
                if (tv.equals(TypeVisualisation.COMPLET)) {
                    for (int j = 0; j < re.getResultatBalises().size(); j++) {
                        ResultatBalise rb = re.getResultatBalises().get(j);
                        retour.add(rb.getCode() + "");
                        if (!rb.isPm()) {
                            //retour.add("Ok");
                            retour.add(rb.getTime());
                        } else {
                            // TODO L10n.getString()=
                            retour.add("Pm");
                        }
                        retour.add(rb.getPoints() + "");
                        if (rb.getTemps() < 0) {
                            retour.add(TimeManager.fullTime(rb.getTemps() * 1000));
                        } else {
                            retour.add(TimeManager.fullTime(rb.getTemps() * 1000));
                        }
                    }
                }
            }
        }
        return retour;
    }

    public void saveHtml( Raid raid, boolean resultatReduit ) {
        HtmlResultatEquipe.save(this, "temp.html", raid, resultatReduit);
    }

    public String toHtml() {
        // TODO revoir le HTML
        StringBuilder retour = new StringBuilder();

        // Identification
        // TODO L10n.getString()
        retour.append("<table><tr><td><table style=\"font-size:5px;\">");
        retour.append("<tr><td>Parcours : ").append(rp.getParcours().getName()).append("</td></tr>");
        retour.append("<tr><td>Étape : ").append(rp.getEtape().getName()).append("</td></tr></table>");
        // Equipe
        retour.append("<td><table style=\"font-size:5px;\">Équipe : ").append(rp.getEquipe().getNom()).append("</td></tr>");
        retour.append("<tr><td>Dossard : ").append(rp.getEquipe().getDossard()).append("</td></tr>");
        for (int i = 0; i < rp.getEquipe().getRaiders().getSize(); i++) {
            retour.append("<tr><td>Raider").append(i + 1).append(" : ").append(rp.getEquipe().getRaiders().getRaiders().get(i).toString()).append("</td></tr>");
        }
        retour.append("</table></td></tr></table>");
        //  résultats globaux de l'étape
        //retour.append("<br>");
        retour.append("<font size=1><b>Résultats de l'étape :</b><br>");
        retour.append("Temps final : ");
        retour.append(TimeManager.fullTime(totalTemps)).append("<br>");
        retour.append("Total des points : ");
        retour.append(totalPoints).append("<br>");
        // Détails de l'étape
        // Temps et points
        retour.append("<b>Détails de l'étape :</b><br></font><table style=\"font-size:5px;\">");
        retour.append("<tr><td>Heure de départ : ").append(TimeManager.fullTime(depart)).append("</td>");
        retour.append("<td>Bonification de temps : ");
        retour.append(TimeManager.fullTime(totaltempsBonif * 1000)).append("</td>");
        retour.append("<td>Bonification de points : ");
        retour.append(totalPointsFinal).append("</td></tr>");
        retour.append("<tr><td>Heure d'arrivée : ").append(TimeManager.fullTime(arrivee)).append("</td>");
        retour.append("<td>Pénalité de temps : ");
        retour.append(TimeManager.fullTime(totaltempsPenalite * 1000)).append("</td>");
        retour.append("<td>Pénalité de points : ");
        retour.append(totalPointsPenalite).append("</td></tr>");
        retour.append("<tr><td>Temps de course : ");
        retour.append(TimeManager.fullTime(totalTempsFinal)).append("</td>");
        retour.append("<td>Gel du chrono : ");
        retour.append(TimeManager.fullTime(totalGelChrono)).append("</td>");
        retour.append("<td>Pénalité de dépassement de l'étape : ");
        retour.append(totalPointsDepassement).append("</td></tr>");
        retour.append("<tr><td></td>");
        retour.append("<td>Pénalité de dépassement de l'étape : ");
        retour.append(TimeManager.fullTime(penaliteDepassementEtapeTemps * 1000)).append("</td></tr></table></font>");

        // par épreuve
        for (int i = 0; i < resultatEpreuves.getResultatEpreuves().size(); i++) {
            ResultatEpreuve re = resultatEpreuves.getResultatEpreuves().get(i);
            if (!re.isUsed()) {
                continue;
            }

            retour.append("<font size=1><b>Épreuve : ").append(rp.getEtape().getEpreuves().getEpreuves().get(i).getName()).append("</b><br><table style=\"font-size:5px;\">");
            retour.append("<tr><td>Temps final : ");
            retour.append(TimeManager.fullTime(re.getTotalTemps())).append("</td>");
            retour.append("<td>Bonification de temps : ");
            retour.append(TimeManager.fullTime(re.getTotaltempsBonif() * 1000)).append("</td>");
            retour.append("<td>Bonification de points : ");
            retour.append(re.getTotalPointsFinal()).append("</td></tr>");
            retour.append("<tr><td>Total des points : ");
            retour.append(re.getTotalPoints()).append("</td>");
            retour.append("<td>Pénalité de temps : ");
            retour.append(TimeManager.fullTime(re.getTotaltempsPenalite() * 1000)).append("</td>");
            retour.append("<td>Pénalité de points : ");
            retour.append(re.getTotalPointsPenalite()).append("</td></tr>");
            // récapitulatif temps et points
            retour.append("<tr><td>Temps de course : ");
            retour.append(TimeManager.fullTime(re.getTotalTempsFinal())).append("</td>");
            retour.append("<td>Gel du chrono : ");
            retour.append(TimeManager.fullTime(re.getTotalGelChrono())).append("</td>");
            retour.append("<td>Pénalité de dépassement de l'épreuve : ");
            retour.append(re.getTotalPointsDepassement()).append("</td></tr>");
            retour.append("<tr><td></td>");
            retour.append("<td>Pénalité de dépassement de l'épreuve : ");
            retour.append(TimeManager.fullTime(re.getTotaltempsdepassement() * 1000)).append("</td></tr></table>");

            // Détails par balise
            retour.append("<table style=\"font-size:5px;\"><tr><td>Code</td><td>Ok-Pm</td><td>Points</td><td>Temps</td><td>Code</td><td>Ok-Pm</td><td>Points</td><td>Temps</td>" +
                    "<td>Code</td><td>Ok-Pm</td><td>Points</td><td>Temps</td><td>Code</td><td>Ok-Pm</td><td>Points</td><td>Temps</td></tr>");
            for (int j = 0; j < resultatEpreuves.getResultatEpreuves().get(i).getResultatBalises().size(); j = j + 4) {
                retour.append("<tr>");
                for (int k = 0; k < 4; k++) {
                    if (resultatEpreuves.getResultatEpreuves().get(i).getResultatBalises().size() > j + k) {
                        ResultatBalise rb = resultatEpreuves.getResultatEpreuves().get(i).getResultatBalises().get(j + k);
                        retour.append("<td>").append(rb.getCode()).append("</td><td>");
                        if (rb.isPm()) {
                            retour.append("PM</td>");
                        } else {
                            retour.append("OK</td>");
                        }
                        retour.append("<td>").append(rb.getPoints()).append("</td><td>").append(TimeManager.fullTime(rb.getTemps() * 1000)).append("</td>");
                    }
                }
                retour.append("</tr>");
            }
            retour.append("</table></font>");

        }
        return retour.toString();
    }

    public String toHtmlReduit() {
        // TODO L10n.getString()
        StringBuilder retour = new StringBuilder();
        // Identification
        retour.append("<b>Parcours :</b> ").append(rp.getParcours().getName()).append("<br>");
        retour.append("<b>Étape :</b> ").append(rp.getEtape().getName()).append("<br>");
        // Equipe
        retour.append("<b>catégorie :</b> ").append(rp.getEquipe().getCategorie().getLongName()).append("<br>");
        retour.append("<b>Équipe :</b> ").append(rp.getEquipe().getNom()).append("<br>");
        retour.append("<b>Dossard :</b> ").append(rp.getEquipe().getDossard()).append("<br>");
        retour.append("<b>Puce :</b> ").append(rp.getEquipe().getIdPuce()).append("<br>");
        for (int i = 0; i < rp.getEquipe().getRaiders().getSize(); i++) {
            retour.append("<b>Raider").append(i + 1).append(" :</b> ").append(rp.getEquipe().getRaiders().getRaiders().get(i).toString()).append("<br>");
        }
        //  résultats globaux de l'étape
        //retour.append("<br>");
        retour.append("<br>");
        retour.append("<b>Départ :</b> ");
        retour.append(TimeManager.fullTime(depart)).append("<br>");
        retour.append("<b>Arrivée :</b> ");
        retour.append(TimeManager.fullTime(arrivee)).append("<br>");
        retour.append("<b>Temps de course :</b> ");
        retour.append(TimeManager.fullTime(arrivee.getTime() - depart.getTime())).append("<br>");
        retour.append("<b>Temps final :</b> ");
        retour.append(TimeManager.fullTime(totalTemps)).append("<br>");
        if (totalPointsFinal != 0) {
            retour.append("<b>Points avant Bonif/Penal:</b> ");
            retour.append(totalPointsFinal).append(File.separator).append(rp.getEtape().getTotalPointsEtape()).append("<br>");
        }
        if (totalPointsPenalite != 0) {
            retour.append("<b>Pénalité de points :</b> ");
            retour.append(totalPointsPenalite).append("<br>");
        }
        if (totalPointsDepassement != 0) {
            retour.append("<b>Pénalité dépassement étape :</b> ");
            retour.append(totalPointsDepassement).append("<br>");
        }
        if (totalPoints != 0) {
            retour.append("<b>Total des points :</b> ");
            retour.append(totalPoints).append("<br>");
        }
        if (totaltempsBonif != 0) {
            retour.append("<b>Bonification de temps :</b> ");
            retour.append(TimeManager.fullTime(totaltempsBonif * 1000)).append("<br>");
        }
        if (totaltempsPenalite != 0) {
            retour.append("<b>Pénalité de temps :</b> ");
            retour.append(TimeManager.fullTime(totaltempsPenalite * 1000)).append("<br>");
        }
        if (totalGelChrono != 0) {
            retour.append("<b>Gel du chrono :</b> ");
            retour.append(TimeManager.fullTime(totalGelChrono)).append("<br>");
        }
        if (penaliteDepassementEtapeTemps != 0) {
            retour.append("<b>Pénalité dépassement étape :</b> ");
            retour.append(TimeManager.fullTime(penaliteDepassementEtapeTemps * 1000)).append("<br>");
        }
        retour.append("<br>");

        // par épreuve

        int index = 0;
        for (int i = 0; i < resultatEpreuves.getResultatEpreuves().size(); i++) {
            if (!resultatEpreuves.getResultatEpreuves().get(i).isUsed()) {
                continue;
            }

            retour.append(rp.getEtape().getEpreuves().getEpreuves().get(i).getName()).append("<br>");

            // Détails par balise
            // TODO L10n.getString()
            retour.append("<table cellspacing=0 style='font-size:7pt;'><tr align=center><th>code</th><th>temps</th><th>code</th><th>temps</th><th>code</th><th>temps</th></tr>");
            for (int j = 0; j < resultatEpreuves.getResultatEpreuves().get(i).getResultatBalises().size(); j = j + 3) {
                retour.append("<tr align=center><td height=0><b>").append(listeCodeOkPm.getListe().get(index + j + 1).getCode()).append("</b></td>");
                retour.append("<td height=0>").append(listeCodeOkPm.getListe().get(index + j + 1).getTemps()).append("</td>");
                if (j + 1 < resultatEpreuves.getResultatEpreuves().get(i).getResultatBalises().size()) {
                    retour.append("<td height=0><b>").append(listeCodeOkPm.getListe().get(index + j + 2).getCode()).append("</b></td>");
                    retour.append("<td height=0>").append(listeCodeOkPm.getListe().get(index + j + 2).getTemps()).append("</td>");
                }
                if (j + 2 < resultatEpreuves.getResultatEpreuves().get(i).getResultatBalises().size()) {
                    retour.append("<td height=0><b>").append(listeCodeOkPm.getListe().get(index + j + 3).getCode()).append("</b></td>");
                    retour.append("<td height=0>").append(listeCodeOkPm.getListe().get(index + j + 3).getTemps()).append("</td>");
                }
                retour.append("</tr>");
            }
            index = index + resultatEpreuves.getResultatEpreuves().get(i).getResultatBalises().size();
            retour.append("</table>");

        }

    /*
    // balises complètes
    retour.append("<table cellspacing=0 style='font-size:7pt;'><tr align=center><th>code</th><th>temps</th><th>code</th><th>temps</th><th>code</th><th>temps</th></tr>");
    for(int i=1; i<listeCodeOkPm.getListe().size()-1; i=i+3)
    {
      retour.append("<tr align=center><td height=0><b>" + listeCodeOkPm.getListe().get(i).getCode() + "</b></td>");
      retour.append("<td height=0>" + listeCodeOkPm.getListe().get(i).getTemps() + "</td>");
      if(i+1<listeCodeOkPm.getListe().size()-1)
      {
        retour.append("<td height=0><b>" + listeCodeOkPm.getListe().get(i+1).getCode() + "</b></td>");
        retour.append("<td height=0>" + listeCodeOkPm.getListe().get(i+1).getTemps() + "</td>");
      }
      if(i+2<listeCodeOkPm.getListe().size()-1)
      {
        retour.append("<td height=0><b>" + listeCodeOkPm.getListe().get(i+2).getCode() + "</b></td>");
        retour.append("<td height=0>" + listeCodeOkPm.getListe().get(i+2).getTemps() + "</td>");
      }
      retour.append("</tr>");
    }
    retour.append("</table>");
    */

        return retour.toString();
    }

    public Vector<String> toVector( Epreuve e, TypeVisualisationEpreuve tv ) {
        Vector<String> retour = new Vector<>();
        // Visualisation simple
        if (_classement == -1) {
            retour.add("NC");
        } else {
            retour.add(_classement + "");
        }
        retour.add(dossardEquipe);
        retour.add(puce);
        retour.add(nomEquipe);
        retour.add(categorieEquipe);
        // Visualisation avec épreuves
        for (int i = 0; i < resultatEpreuves.getResultatEpreuves().size(); i++) {
            // Visualisation avec épreuves
            ResultatEpreuve re = resultatEpreuves.getResultatEpreuves().get(i);
            if (!re.isUsed()) {
                continue;
            }

            //if(re.epreuve.equals(e))
            if (re.getEpreuve().getName().compareTo(e.getName()) == 0) {
                // Classement
                retour.add(TimeManager.fullTime(re.getTotalTemps()));
                retour.add(re.getTotalPoints() + "");
                // Temps
                retour.add(TimeManager.fullTime(re.getTotalTempsFinal()));
                retour.add(TimeManager.fullTime(re.getTotaltempsBonif() * 1000));
                retour.add(TimeManager.fullTime(re.getTotaltempsPenalite() * 1000));
                retour.add(TimeManager.fullTime(re.getTotaltempsdepassement() * 60000));
                retour.add(TimeManager.fullTime(re.getTotalGelChrono()));
                // Points
                retour.add(re.getTotalPointsFinal() + "");
                retour.add(re.getTotalPointsPenalite() + "");
                retour.add(re.getTotalPointsDepassement() + "");
                // Visualisation complète
                if (tv.equals(TypeVisualisationEpreuve.AVEC_BALISES)) {
                    for (int j = 0; j < re.getResultatBalises().size(); j++) {
                        ResultatBalise rb = re.getResultatBalises().get(j);
                        retour.add(rb.getCode() + "");
                        if (!rb.isPm()) {
                            //retour.add("Ok");
                            retour.add(rb.getTime());
                        } else {
                            // TODO L10n.getString()
                            retour.add("Pm");
                        }
                        retour.add(rb.getPoints() + "");
                        if (rb.getTemps() < 0) {
                            retour.add(TimeManager.fullTime(rb.getTemps() * 1000));
                        } else {
                            retour.add(TimeManager.fullTime(rb.getTemps() * 1000));
                        }
                    }
                }
            }
        }

        return retour;
    }

    public String getDossardEquipe() {
        return dossardEquipe;
    }

    public String getNomEquipe() {
        return nomEquipe;
    }

    public String getCategorieEquipe() {
        return categorieEquipe;
    }

    public ListeCodeOkPm getListeCodeOkPm() {
        return listeCodeOkPm;
    }
}