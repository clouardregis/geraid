package fr.gecoraid.model;

public enum TypeBalise {
    // TODO L10n.getString()
    BONIFICATION("Bonification"), CONTROLE("Contrôle");

    private final String value;

    TypeBalise( String value ) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return value;
    }

}
