package fr.gecoraid.model;

// TODO refactor -> SOLID
public class CodeOkPm {
    private int _code = 0;
    private boolean _oK = false;
    private String _temps = "PM";

    public CodeOkPm( int code, boolean ok, String temps ) {
        _code = code;
        _oK = ok;
        if (temps != null) {
            _temps = temps;
        }
    }

    public int getCode() {
        return _code;
    }

    public void setCode( int code ) {
        _code = code;
    }

    public boolean isoK() {
        return _oK;
    }

    public void setoK( boolean oK ) {
        _oK = oK;
    }

    public String getTemps() {
        return _temps;
    }

    public void setTemps( String temps ) {
        _temps = temps;
    }
}
