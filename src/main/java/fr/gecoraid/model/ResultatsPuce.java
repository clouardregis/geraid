package fr.gecoraid.model;

import java.util.Vector;

// TODO refactor
public class ResultatsPuce implements Cloneable {
    private final Vector<ResultatPuce> _resultatsPuce = new Vector<>();

    public Vector<ResultatPuce> getResultatsPuce() {
        return _resultatsPuce;
    }

//    public void setResultatsPuce( Vector<ResultatPuce> resultatsPuce ) {
//        _resultatsPuce = resultatsPuce;
//    }

    public boolean addResultatPuce( ResultatPuce p ) {
        boolean retour = false;
        _resultatsPuce.add(p);
        retour = true;
        return retour;
    }

    public int getSize() {
        return _resultatsPuce.size();
    }

    public void clearResultats() {
        _resultatsPuce.clear();
    }

    public void removeResultatPuce( ResultatPuce rp ) {
        _resultatsPuce.remove(rp);
    }

    public ResultatPuce existeResultatPuce( ResultatPuce rp ) {
        for (ResultatPuce resultatPuce : _resultatsPuce) {
            if (compare(resultatPuce, rp)) {
                return resultatPuce;
            }
        }
        return null;
    }

    private boolean compare( ResultatPuce rp1, ResultatPuce rp2 ) {
        return rp1.getEtape().equals(rp2.getEtape())
                && rp1.getEquipe().equals(rp2.getEquipe());
    }

    public void effacerParcours( Parcours p ) {
        int i = 0;
        while (i < _resultatsPuce.size()) {
            if (_resultatsPuce.get(i).getParcours().equals(p)) {
                _resultatsPuce.remove(i);
            } else {
                i++;
            }
        }
    }

    public void effacerEquipe( Equipe e ) {
        int i = 0;
        while (i < _resultatsPuce.size()) {
            if (_resultatsPuce.get(i).getEquipe().equals(e)) {
                _resultatsPuce.remove(i);
            } else {
                i++;
            }
        }
    }

    public void effacerEtape( Etape e ) {
        int i = 0;
        while (i < _resultatsPuce.size()) {
            if (_resultatsPuce.get(i).getEtape().equals(e)) {
                _resultatsPuce.remove(i);
            } else {
                i++;
            }
        }
    }

    public Vector<Parcours> getParcours() {
        Vector<Parcours> retour = new Vector<>();
        for (ResultatPuce resultatPuce : _resultatsPuce) {
            if (!retour.contains(resultatPuce.getParcours())) {
                retour.add(resultatPuce.getParcours());
            }
        }
        return retour;
    }

    public Vector<Etape> getEtapes( Parcours p ) {
        Vector<Etape> retour = new Vector<>();
        for (ResultatPuce resultatPuce : _resultatsPuce) {
            if (resultatPuce.getParcours().equals(p)
                    && !retour.contains(resultatPuce.getEtape())) {
                retour.add(resultatPuce.getEtape());
            }
        }
        return retour;
    }

    public Vector<Equipe> getEquipes( Etape e ) {
        Vector<Equipe> retour = new Vector<>();
        for (ResultatPuce resultatPuce : _resultatsPuce) {
            if (resultatPuce.getEtape().equals(e)
                    && !retour.contains(resultatPuce.getEquipe())) {
                retour.add(resultatPuce.getEquipe());
            }
        }
        return retour;
    }

    public ResultatPuce getResultatPuce( Etape et, Equipe eq ) {
        for (ResultatPuce resultatPuce : _resultatsPuce) {
            if (resultatPuce.getEtape().equals(et)
                    && resultatPuce.getEquipe().equals(eq)) {
                return resultatPuce;
            }
        }
        return null;
    }

    public boolean isArrived( Equipe equipe, Parcours parcours, Etape etape ) {
        boolean result = false;
        for (ResultatPuce resultatPuce : _resultatsPuce) {
            if (resultatPuce.getEtape().equals(etape)
                    && resultatPuce.getEquipe().equals(equipe)
                    && resultatPuce.getParcours().equals(parcours)) {
                return true;
            }
        }
        return result;
    }

    public ResultatPuce getResultatPuce( Parcours p, Etape e, String puce ) {
        for (ResultatPuce resultatPuce : _resultatsPuce) {
            if (p.equals(resultatPuce.getParcours())
                    && e.equals(resultatPuce.getEtape())
                    && resultatPuce.getEquipe().getIdPuce().compareTo(puce) == 0) {
                return resultatPuce;
            }
        }
        return null;
    }

    public boolean existeResultatPuce( Equipe e ) {
        for (ResultatPuce resultatPuce : _resultatsPuce) {
            if (resultatPuce.getEquipe().equals(e)) {
                return true;
            }
        }
        return false;
    }
// TODO not used
//    public boolean existeCodePuce( int code, String puce ) {
//        for (ResultatPuce resultatPuce : _resultatsPuce) {
//            if (puce.compareTo(resultatPuce.getPuce().getIdPuce()) == 0) {
//                if (resultatPuce.existeCode(code)) {
//                    return true;
//                }
//            }
//        }
//        return false;
//    }
}
