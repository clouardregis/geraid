package fr.gecoraid.model;

import java.util.Vector;

// TODO refactor -> SOLID
public class ResultatEpreuves {
    private Vector<ResultatEpreuve> _resultatEpreuves;

    public ResultatEpreuves() {
        _resultatEpreuves = new Vector<>();
    }

    public Vector<ResultatEpreuve> getResultatEpreuves() {
        return _resultatEpreuves;
    }

    public void addResultatEpreuve( Epreuve e, ResultatPuce rp ) {
        _resultatEpreuves.add(new ResultatEpreuve(e, rp));
    }

    public void setResultatEpreuves( Vector<ResultatEpreuve> resultatEpreuves ) {
        _resultatEpreuves = resultatEpreuves;
    }
}
