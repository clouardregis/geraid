package fr.gecoraid.model;

import fr.gecoraid.tools.TimeManager;

import java.util.Date;


public class Partial implements Cloneable {
    private int _code = 31;
    private Date _time = new Date(9 * 3600 * 1000L);// 9:00:00

    public Partial() {

    }

    public int getCode() {
        return _code;
    }

    public void setCode( int code ) {
        _code = code;
    }

    public Date getTime() {
        return _time;
    }

    public void setTime( Date time ) {
        _time = time;
    }
// TODO L10n.getString()
    public String toString() {
        return "Poste : " + _code + " - " + TimeManager.fullTime(_time);
    }
}
