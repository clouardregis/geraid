package fr.gecoraid.model;

import java.util.Vector;

public class Penalites {
    private Vector<Penalite> penalites = new Vector<>();

    public Penalites() {
    }

    public void effacerEtape( Etape e ) {
        int i = 0;
        while (i < penalites.size()) {
            if (penalites.get(i).getEtape().equals(e)) {
                penalites.remove(i);
            } else {
                i++;
            }
        }
    }

    public Vector<Penalite> getPenalites() {
        return penalites;
    }

    public void setPenalites( Vector<Penalite> penalites ) {
        this.penalites = penalites;
    }

    public Vector<String> getPenaltyCount( Etape e ) {
        Vector<String> retour = new Vector<String>();

        for (Penalite penalite : penalites) {
            if (penalite.getEtape().equals(e)) {
                retour.add(penalite.getNom());
            }
        }

        return retour;
    }

    public int getTotalPointPenalite( Parcours p, Etape e, String puce ) {
        int retour = 0;
        for (Penalite penalite : penalites) {
            if (penalite.getParcours().estPresqueEgal(p) && penalite.getEtape().estPresqueEgal(e)) {
                for (int j = 0; j < penalite.getPenalites().size(); j++) {
                    if (penalite.getPenalites().get(j).getPuce().compareTo(puce) == 0) {
                        retour = retour + penalite.getPenalites().get(j).getPoint();
                    }
                }
            }
        }
        return retour;
    }

    public int getTotalTempsPenalite( Parcours p, Etape e, String puce ) {
        int retour = 0;
        for (Penalite penalite : penalites) {
            if (penalite.getParcours().estPresqueEgal(p) && penalite.getEtape().estPresqueEgal(e)) {
                for (int j = 0; j < penalite.getPenalites().size(); j++) {
                    if (penalite.getPenalites().get(j).getPuce().compareTo(puce) == 0) {
                        retour = retour + penalite.getPenalites().get(j).getTemps();
                    }
                }
            }
        }
        return retour;
    }
}
