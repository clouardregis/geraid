package fr.gecoraid.model;

import java.util.Vector;

// TODO mettre une visibilite package
// TODO Refactor and clean code
// TODO put in the convenient folder
public class AuScore {
    private final int[] _codesATrouver;
    private final Vector<Integer> _codesPuce;
    private final Vector<String> _tempsPuce;
    private boolean[] _matrice;
    private boolean[] _okpm;
    private String[] _temps;
    private int _index = 0;

    public AuScore( int[] codesATrouver, Vector<Integer> codesPuce, Vector<String> tempsPuce ) {
        _codesATrouver = codesATrouver;
        _codesPuce = codesPuce;
        _tempsPuce = tempsPuce;
        initMatrice();
        okPm();
    }

    private void initMatrice() {
        _matrice = new boolean[_codesPuce.size()];
        for (int i = 0; i < _codesPuce.size(); i++) {
            _matrice[i] = false;
        }
    }

    private void okPm() {
        _okpm = new boolean[_codesATrouver.length];
        _temps = new String[_codesATrouver.length];
        for (int i = 0; i < _codesATrouver.length; i++) {
            _okpm[i] = isCode(_codesATrouver[i]);
            if (_index >= 0) {
                _temps[i] = _tempsPuce.get(_index);
            } else {
                // TODO I18n
                _temps[i] = "PM";
            }
        }
    }

    private boolean isCode( int code ) {
        _index = -1;
        boolean retour = false;
        for (int i = 0; i < _codesPuce.size(); i++) {
            if (code == _codesPuce.get(i) && !_matrice[i]) {
                _matrice[i] = true;
                _index = i;
                return true;
            }
        }
        return retour;
    }

    public boolean[] getOkPm() {
        return _okpm;
    }

    @SuppressWarnings("unused")
    private void lireOkPm() {
        for (int i = 0; i < _codesATrouver.length; i++) {
            System.out.println(_codesATrouver[i] + " : " + _okpm[i]);
        }
    }

    // TODO Faire en test
//    public static void main( String[] args ) {
//        @SuppressWarnings("unused")
//        int codes[] = {31, 32, 33, 34, 35, 32, 36, 37, 40, 32, 38, 39, 31};
//        Vector<Integer> trace = new Vector<Integer>();
//        trace.add(31);
//        trace.add(32);
//        trace.add(33);
//        trace.add(34);
//        trace.add(35);
//        trace.add(32);
//        trace.add(45);
//        trace.add(36);
//        trace.add(37);
//        trace.add(40);
//        trace.add(32);
//        trace.add(38);
//        trace.add(39);
//        //new AuScore(codes, trace);
//    }

    public String[] getTemps() {
        return _temps;
    }
}
