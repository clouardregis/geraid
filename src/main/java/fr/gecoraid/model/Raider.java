package fr.gecoraid.model;

// TODO refactor -> SOLID
public class Raider {
    private String _nom = "";
    private String _prenom = "";

    public Raider() {

    }

    public Raider( String nom, String prenom ) {
        setLastname(nom);
        setFirstname(prenom);
    }

    public String getNom() {
        return _nom;
    }

    public void setLastname( String nom ) {
        _nom = nom.trim();
    }

    public String getPrenom() {
        return _prenom;
    }

    public void setFirstname( String prenom ) {
        _prenom = prenom.trim();
    }

    public String toString() {
        return _nom + " " + _prenom;
    }
}
