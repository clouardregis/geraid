package fr.gecoraid.model;

import java.util.Vector;

// TODO refactor -> SOLID
public class Categories {
    private final Vector<Categorie> _categories = new Vector<>();

    public Vector<Categorie> getCategories() {
        return _categories;
    }

    public void addCategorie( Categorie cat ) {
        if (!existeCategorie(cat.getLongName(), cat.getShortName())) {
            _categories.add(cat);
        }
    }

    public int getSize() {
        return _categories.size();
    }

    public Categorie getCategorie( String nomCourt ) {
        for (Categorie category : _categories) {
            if (category.getShortName().compareTo(nomCourt) == 0) {
                return category;
            }
        }
        return null;
    }

    public String[][] getData() {
        String[][] retour = new String[_categories.size()][2];
        for (int i = 0; i < _categories.size(); i++) {
            retour[i][0] = _categories.get(i).getLongName();
            retour[i][1] = _categories.get(i).getShortName();
        }
        return retour;
    }

    public void removeCategorie( String nomLong ) {
        for (int i = 0; i < _categories.size(); i++) {
            if (nomLong.compareTo(_categories.get(i).getLongName()) == 0) {
                _categories.remove(_categories.get(i));
                return;
            }
        }
    }

    public boolean existeCategorie( String nomLong, String nomCourt ) {
        for (Categorie category : _categories) {
            if (nomLong.compareTo(category.getLongName()) == 0
                    && nomCourt.compareTo(category.getShortName()) == 0) {
                return true;
            }
        }
        return false;
    }

    public boolean existeCategorie( String nomCourt ) {
        for (Categorie category : _categories) {
            if (nomCourt.compareTo(category.getShortName()) == 0) {
                return true;
            }
        }
        return false;
    }
}
