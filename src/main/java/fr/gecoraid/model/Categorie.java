package fr.gecoraid.model;

// TODO refactor -> SOLID
public class Categorie {
    private String _nomLong;
    private String _nomCourt;

    public Categorie( String nomLong, String nomCourt ) {
        _nomLong = nomLong;
        _nomCourt = nomCourt;
    }

    public String getLongName() {
        return _nomLong;
    }

    public void setNomLong( String nomLong ) {
        _nomLong = nomLong;
    }

    public String getShortName() {
        return _nomCourt;
    }

    public void setNomCourt( String nomCourt ) {
        _nomCourt = nomCourt;
    }

    public String toString() {
        return _nomLong;
    }
}
