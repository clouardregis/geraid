package fr.gecoraid.model;

import fr.gecoraid.tools.TimeManager;

// TODO refactor
public class ResultatParcoursEquipe implements Comparable<ResultatParcoursEquipe> {
    private int _classement = 0;
    private final Parcours parcours;
    private Equipe equipe;
    private int _totalPoints = 0;
    private long _tempsCompense = 0;
    private final String[] etape;
    private final String[] _points;
    private final String[] _temps;
    private int nbEtapes = 0;

    public ResultatParcoursEquipe( Parcours p, Equipe e ) {
        this.parcours = p;
        this.equipe = e;
        if (equipe.isNonClassee()) {
            _classement = -1;
        }
        int nbEtape = p.getEtapes().getSize();
        etape = new String[nbEtape];
        _points = new String[nbEtape];
        _temps = new String[nbEtape];
        initTableaux();
    }

    private void initTableaux() {
        for (int i = 0; i < etape.length; i++) {
            etape[i] = "";
            _points[i] = "";
            _temps[i] = "";
        }
    }

    public Equipe getEquipe() {
        return equipe;
    }

    public void setEquipe( Equipe equipe ) {
        this.equipe = equipe;
    }

    public void ajouterResultat( ResultatPuce rp, int pts, int tps ) {
        ResultatEquipe resultat = new ResultatEquipe(rp, pts, tps);
        _totalPoints = _totalPoints + resultat.getTotalPoints();
        _tempsCompense = _tempsCompense + resultat.getTotalTemps();
        int index = parcours.getIndexEtape(rp.getEtape());
        etape[index] = "";
        _points[index] = resultat.getTotalPoints() + "";
        _temps[index] = TimeManager.fullTime(resultat.getTotalTemps());
        setNbEtapes();
    }

    private void setNbEtapes() {
        for (String temp : _temps) {
            if (temp.compareTo("") != 0) {
                nbEtapes++;
            }
        }
    }

    @Override
    public int compareTo( ResultatParcoursEquipe o ) {
        if (_totalPoints < o._totalPoints) {
            return 1;
        }
        if (_totalPoints == o._totalPoints) {
            if (nbEtapes < o.nbEtapes) {
                return 1;
            } else if (o.nbEtapes < nbEtapes) {
                return -1;
            } else if (_tempsCompense > o._tempsCompense) {
                return 1;
            } else {
                return -1;
            }
        }
        return -1;
    }

    public int getClassement() {
        return _classement;
    }

    public void setClassement( int value ) {
        _classement = value;
    }

    public String getEtape( int index ) {
        return etape[index];
    }

    public String getPoints( int index ) {
        return _points[index];
    }

    public String getTemps( int index ) {
        return _temps[index];
    }

    public int getTotalPoints() {
        return _totalPoints;
    }

    public long getTempsCompense() {
        return _tempsCompense;
    }
}
