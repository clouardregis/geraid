package fr.gecoraid.model;

import java.util.Vector;

// TODO refactor -> SOLID
public class PenalitesIndividuelles {
    private String dossard = "";
    private String puce = "";
    private String equipe = "";
    private Vector<String> points = new Vector<>();
    private Vector<String> temps = new Vector<>();

    public String getDossard() {
        return dossard;
    }

    public void setDossard( String dossard ) {
        this.dossard = dossard;
    }

    public String getPuce() {
        return puce;
    }

    public void setPuce( String puce ) {
        this.puce = puce;
    }

    public String getEquipe() {
        return equipe;
    }

    public void setEquipe( String equipe ) {
        this.equipe = equipe;
    }

    public Vector<String> getPoints() {
        return points;
    }

    public void setPoints( Vector<String> points ) {
        this.points = points;
    }

    public Vector<String> getTemps() {
        return temps;
    }

    public void setTemps( Vector<String> temps ) {
        this.temps = temps;
    }
}
