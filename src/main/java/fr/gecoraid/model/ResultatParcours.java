package fr.gecoraid.model;

import fr.gecoraid.tools.TimeManager;

import java.util.Vector;

// TODO refactor
public class ResultatParcours {
    private final Raid _geRaid;
    private final Parcours _parcours;
    private final Categorie _categorie;
    private final TypeVisualisationParcours tv;
    private final Vector<String> _entetes = new Vector<>();
    private final ResultatsParcoursEquipe _rpes = new ResultatsParcoursEquipe();
    private int _nbRaiders = 0;
    private final boolean _visuRaiders = false;

    public ResultatParcours( Raid g, Parcours p, Categorie c, TypeVisualisationParcours tv, boolean puceParEquipe ) {
        _geRaid = g;
        _parcours = p;
        _categorie = c;
        this.tv = tv;
        editEntetes();
        editData(puceParEquipe);
    }

    public String getHeaderAsHtml() {
        // TODO L10n.getString()
        StringBuilder retour = new StringBuilder("<table ><tr align=center style='font-weight: bold;'>");
        retour.append("<td>" + "Clt" + "</td>");
        retour.append("<td>" + "Dossard" + "</td>");
        retour.append("<td>" + "Nom équipe" + "</td>");
        retour.append("<td>" + "Catégorie" + "</td>");
        retour.append("<td>" + "Pts total" + "</td>");
        retour.append("<td>" + "Tps final" + "</td>");
        if (_parcours != null) {
            for (int i = 0; i < _parcours.getEtapes().getSize(); i++) {
                retour.append("<td>").append(_parcours.getEtapes().getEtapes().get(i).getName()).append("</td>");
                retour.append("<td>" + "Points" + "</td>");
                retour.append("<td>" + "Temps" + "</td>");
            }
        }
        retour.append("</tr>");

        return retour.toString();
    }

    private void editEntetes() {
        _entetes.clear();
        // TODO L10n.getString()
        _entetes.add("Clt");
        _entetes.add("Dossard");
        _entetes.add("Puce");
        _entetes.add("Nom équipe");
        _entetes.add("Catégorie");
        _entetes.add("Pts total");
        _entetes.add("Tps final");
        if (!tv.equals(TypeVisualisation.SIMPLE)) {
            if (_parcours != null) {
                if (tv.equals(TypeVisualisationParcours.AVEC_ETAPE)) {
                    for (int i = 0; i < _parcours.getEtapes().getSize(); i++) {
                        _entetes.add(_parcours.getEtapes().getEtapes().get(i).getName());
                        _entetes.add("Points");
                        _entetes.add("Temps");
                    }
                }
            }
        }
    }

    public String[] getEntetes() {
        return _entetes.toArray(new String[0]);
    }

    private void editData( boolean puceParEquipe ) {
        for (int i = 0; i < _geRaid.getResultatsPuce().getSize(); i++) {
            ResultatPuce rp = _geRaid.getResultatsPuce().getResultatsPuce().get(i);
            //if(rp.getParcours().equals(parcours))
            if (rp.getParcours().estPresqueEgal(_parcours)) {
                int points = _geRaid.getPenalites().getTotalPointPenalite(rp.getParcours(), rp.getEtape(), rp.getEquipe().getIdPuce());
                int temps = _geRaid.getPenalites().getTotalTempsPenalite(rp.getParcours(), rp.getEtape(), rp.getEquipe().getIdPuce());

                if (_geRaid.getCategories().getCategories().contains(_categorie)) {
                    if (rp.getEquipe().getCategorie().equals(_categorie)) {
                        _rpes.ajouterResultat(rp, points, temps);
                        if (_visuRaiders) {
                            _nbRaiders = _nbRaiders + rp.getEquipe().getRaiders().getSize();
                        }
                    }
                } else {
                    _rpes.ajouterResultat(rp, points, temps);
                    if (_visuRaiders) {
                        _nbRaiders = _nbRaiders + rp.getEquipe().getRaiders().getSize();
                    }
                }
            }
        }
        _rpes.trier(puceParEquipe);
        trierNC();
        faireClassement();
    }

    public String getDataAsHtml() {
        StringBuilder retour = new StringBuilder();
        for (int i = 0; i < _rpes.getSize(); i++) {
            retour.append("<tr style='");
            if (i % 2 == 0) {
                retour.append("background-color: #FFFFE0;'");
            } else {
                retour.append("background-color: #E0FFFF;'");
            }
            retour.append(" align=center>");
            if (_rpes.getResultat().get(i).getClassement() == -1) {
                // TODO L10n.getString()
                retour.append("<td>NC</td>");
            } else {
                retour.append("<td>").append(_rpes.getResultat().get(i).getClassement()).append("</td>");
            }
            retour.append("<td>").append(_rpes.getResultat().get(i).getEquipe().getDossard()).append("</td>");
            retour.append("<td>").append(_rpes.getResultat().get(i).getEquipe().getNomEtRaiders()).append("</td>");
            retour.append("<td>").append(_rpes.getResultat().get(i).getEquipe().getCategorie().getLongName()).append("</td>");
            retour.append("<td>").append(_rpes.getResultat().get(i).getTotalPoints()).append("</td>");
            retour.append("<td>").append(TimeManager.fullTime(_rpes.getResultat().get(i).getTempsCompense())).append("</td>");
            for (int j = 0; j < _parcours.getEtapes().getSize(); j++) {
                retour.append("<td></td>");
                retour.append("<td>").append(_rpes.getResultat().get(i).getPoints(j)).append("</td>");
                retour.append("<td>").append(_rpes.getResultat().get(i).getTemps(j)).append("</td>");
            }
            retour.append("</tr>");
        }
        retour.append("</table>");

        return retour.toString();
    }

    public Object[][] getData() {
        Object[][] retour = new Object[_rpes.getSize() + _nbRaiders][_entetes.size()];
        int indexRaider = 0;
        for (int i = 0; i < _rpes.getSize(); i++) {
            if (_rpes.getResultat().get(i).getClassement() == -1) {
                retour[i + indexRaider][0] = "NC";
            } else {
                retour[i + indexRaider][0] = _rpes.getResultat().get(i).getClassement();
            }
            retour[i + indexRaider][1] = _rpes.getResultat().get(i).getEquipe().getDossard();
            retour[i + indexRaider][2] = _rpes.getResultat().get(i).getEquipe().getIdPuce();
            retour[i + indexRaider][3] = _rpes.getResultat().get(i).getEquipe().getNomEtRaiders();
            retour[i + indexRaider][4] = _rpes.getResultat().get(i).getEquipe().getCategorie().getLongName();
            retour[i + indexRaider][5] = _rpes.getResultat().get(i).getTotalPoints();
            retour[i + indexRaider][6] = TimeManager.fullTime(_rpes.getResultat().get(i).getTempsCompense());
            int index = 0;
            for (int j = 7; j < _entetes.size(); j = j + 3) {
                retour[i + indexRaider][j] = _rpes.getResultat().get(i).getEtape(index);
                retour[i + indexRaider][j + 1] = _rpes.getResultat().get(i).getPoints(index);
                retour[i + indexRaider][j + 2] = _rpes.getResultat().get(i).getTemps(index);
                index++;
            }
            Raiders raiders = _rpes.getResultat().get(i).getEquipe().getRaiders();
            for (int k = 0; k < raiders.getSize(); k++) {
                if (_visuRaiders) {
                    indexRaider++;
                    retour[i + indexRaider][2] = raiders.getRaiders().get(k).toString();
                }
            }
        }
        return retour;
    }

    private void faireClassement() {
        for (int i = 0; i < _rpes.getSize(); i++) {
            if (_rpes.getResultat().get(i).getClassement() != -1) {
                _rpes.getResultat().get(i).setClassement(i + 1);
            }
        }
    }

    private void trierNC() {
        int index = 0;
        for (int i = 0; i < _rpes.getSize(); i++) {
            if (_rpes.getResultat().get(index).getClassement() == -1) {
                _rpes.getResultat().add(_rpes.getResultat().get(index));
                _rpes.getResultat().remove(index);
            } else {
                index++;
            }
        }
    }

    public Parcours getParcours() {
        return _parcours;
    }

    public Categorie getCategorie() {
        return _categorie;
    }

    public Raid getGeRaid() {
        return _geRaid;
    }
}
