package fr.gecoraid.model;

import java.util.Vector;

/**
 * Une pénalité hors épreuves est une pénalité ou une bonification qui est attribuée à une équipe
 * pour une étape. Cette fonctionnalité permet de gérer les pénalités de sécurité et toutes les
 * épreuves qui ne peuvent pas être gérées par le système SPORTident.
 */
// TODO Refactor
public class Penalite {
    private String nom = "Nouveau nom";
    private Parcours parcours = null;
    private Etape etape = null;
    private Vector<PenaliteIndividuelle> penalites = new Vector<>();

    public Penalite() {

    }

    public String getNom() {
        return nom;
    }

    public void setNom( String nom ) {
        this.nom = nom;
    }

    public Parcours getParcours() {
        return parcours;
    }

    public void setParcours( Parcours parcours ) {
        this.parcours = parcours;
    }

    public Etape getEtape() {
        return etape;
    }

    public void setEtape( Etape etape ) {
        this.etape = etape;
    }

    public Vector<PenaliteIndividuelle> getPenalites() {
        return penalites;
    }

    public void setPenalites( Vector<PenaliteIndividuelle> penalites ) {
        this.penalites = penalites;
    }

    public void addPenaliteIndividuelle( PenaliteIndividuelle p ) {
        if (p.getPuce().compareTo("") != 0) {
            supprimePenalitePuce(p.getPuce());
            penalites.add(p);
        }
    }

    private void supprimePenalitePuce( String puce ) {
        for (int i = 0; i < penalites.size(); i++) {
            if (penalites.get(i).getPuce().compareTo(puce) == 0) {
                penalites.remove(i);
                return;
            }
        }
    }

    public boolean aPuce( String puce ) {
        boolean retour = false;
        for (int i = 0; i < penalites.size(); i++) {
            if (penalites.get(i).getPuce().compareTo(puce) == 0) {
                return true;
            }
        }

        return retour;
    }

    public int getPoints( String puce ) {
        int retour = 0;
        for (int i = 0; i < penalites.size(); i++) {
            if (penalites.get(i).getPuce().compareTo(puce) == 0) {
                return penalites.get(i).getPoint();
            }
        }
        return retour;
    }

    public int getTemps( String puce ) {
        int retour = 0;
        for (int i = 0; i < penalites.size(); i++) {
            if (penalites.get(i).getPuce().compareTo(puce) == 0) {
                return penalites.get(i).getTemps();
            }
        }
        return retour;
    }

    public PenaliteIndividuelle getPenaliteIndividuelle( String puce ) {
        PenaliteIndividuelle retour = null;
        for (int i = 0; i < penalites.size(); i++) {
            if (puce.compareTo(penalites.get(i).getPuce()) == 0) {
                return penalites.get(i);
            }
        }
        return retour;
    }
}
