package fr.gecoraid.model;

public class ZeroTime {
    public  int hours;
    public  int minutes;

    public ZeroTime( int hours, int minutes ) {
        this.hours = hours;
        this.minutes = minutes;
    }
}
