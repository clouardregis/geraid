package fr.gecoraid.model;


import fr.gecoraid.tools.TimeManager;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.Vector;

// TODO refactor -> SOLID
public class Puce implements Cloneable {
    private String _idPuce = "";
    private Date _starttime = TimeManager.NO_TIME;

    private Date _finishtime = TimeManager.NO_TIME;

    private Date _erasetime = TimeManager.NO_TIME;

    private Date _controltime = TimeManager.NO_TIME;

    private Date _readtime = TimeManager.NO_TIME;

    private Partial[] partiels = new Partial[0];

    public int getPositionPoste( int code ) {
        int retour = -1;
        for (int i = 0; i < partiels.length; i++) {
            if (partiels[i].getCode() == code) {
                return i;
            }
        }
        return retour;
    }

    public Vector<String> getTemps() {
        Vector<String> temps = new Vector<>();
        for (Partial partiel : partiels) {
            temps.add(TimeManager.fullTime(partiel.getTime()));
        }
        return temps;
    }

    public Vector<Integer> getCodes() {
        Vector<Integer> codes = new Vector<>();
        for (Partial partiel : partiels) {
            codes.add(partiel.getCode());
        }
        return codes;
    }

    public String getIdPuce() {
        return _idPuce;
    }

    public void setIdPuce( String idPuce ) {
        _idPuce = idPuce;
    }

    public Date getStarttime() {
        return _starttime;
    }

    public void setStarttime( Date starttime ) {
        _starttime = starttime;
    }

    public Date getFinishtime() {
        return _finishtime;
    }

    public void setFinishtime( Date finishtime ) {
        _finishtime = finishtime;
    }

    public Date getErasetime() {
        return _erasetime;
    }

    public void setErasetime( Date erasetime ) {
        _erasetime = erasetime;
    }

    public Date getControltime() {
        return _controltime;
    }

    public void setControltime( Date controltime ) {
        _controltime = controltime;
    }

    public Date getReadtime() {
        return _readtime;
    }

    public void setReadtime( Date readtime ) {
        _readtime = readtime;
    }

    public Partial[] getPartiels() {
        return partiels;
    }

    public void setPartiels( Partial[] partiels ) {
        this.partiels = partiels;
        //garderXpremiersPostes(30);
    }

    public String toStringEnTete() {
        // TODO L10n.getString()
        return "Effacer : " + TimeManager.fullTime(_erasetime) + ", " +
                "Controle : " + TimeManager.fullTime(_controltime) + ", " +
                "Départ : " + TimeManager.fullTime(_starttime) + ", " +
                "Arrivée : " + TimeManager.fullTime(_finishtime) + ", " +
                "Lecture : " + TimeManager.fullTime(_readtime) + ", ";
    }

    public String toStringPartiels() {
        StringBuilder retour = new StringBuilder();
        // TODO L10n.getString()
        retour.append("Départ : ").append(TimeManager.fullTime(_starttime)).append("\n");
        for (Partial partiel : partiels) {
            retour.append(partiel.toString());
        }
        // TODO L10n.getString()
        retour.append("Arrivée : ").append(TimeManager.fullTime(_finishtime));
        return retour.toString();
    }

    public void removePartiel( Partial p ) {
        Partial[] ps = new Partial[partiels.length - 1];
        int index = indexOfPartiel(p);
        System.arraycopy(partiels, 0, ps, 0, index);
        if (partiels.length != index) {
            System.arraycopy(partiels, index + 1, ps, index, partiels.length - index - 1);
        }
        setPartiels(ps);
    }

    private int indexOfPartiel( Partial p ) {
        for (int i = 0; i < partiels.length; i++) {
            if (partiels[i].equals(p)) {
                return i;
            }
        }
        return -1;
    }

    public void upBalise( Partial p ) {
        List<Partial> l = Arrays.asList(partiels);
        List<Partial> v = new Vector<>(l);
        int index = l.indexOf(p);
        if (index > 0) {
            v.add(index - 1, p);
            v.remove(index + 1);
        }
        v.toArray(partiels);
    }

    public void downBalise( Partial p ) {
        List<Partial> l = Arrays.asList(partiels);
        List<Partial> v = new Vector<>(l);
        int index = l.indexOf(p);
        if (index < partiels.length - 1) {
            v.add(index + 2, p);
            v.remove(index);
        }
        v.toArray(partiels);
    }

    public Date getTimePartielFirstCode( int code ) {
        Date retour = TimeManager.NO_TIME;
        for (int i = 0; i < partiels.length; i++) {
            if (code == partiels[i].getCode()) {
                return partiels[i].getTime();
            }
        }
        return retour;
    }

    // on récupère le partiel du deuxième code trouvé
    public Date getTimePartielLastCode( int code ) {
        Date retour = TimeManager.NO_TIME;
        for (int i = partiels.length - 1; i > -1; i--) {
            if (code == partiels[i].getCode()) {
                return partiels[i].getTime();
            }
        }
        return retour;
    }
}
