package fr.gecoraid.model;

import java.util.Vector;

// TODO refactor -> SOLID
public class Raiders {
    private Vector<Raider> _raiders = new Vector<>();

    public Raiders() {

    }

    public Vector<Raider> getRaiders() {
        return _raiders;
    }

    public void setRaiders( Vector<Raider> raiders ) {
        _raiders = raiders;
    }

    public int getSize() {
        return _raiders.size();
    }

    public Object[][] getData() {
        Object[][] retour = new Object[_raiders.size()][2];
        for (int i = 0; i < _raiders.size(); i++) {
            retour[i][0] = _raiders.get(i).getNom();
            retour[i][1] = _raiders.get(i).getPrenom();
        }

        return retour;
    }

    public void addRaider( Raider r ) {
        _raiders.add(r);
    }

    public Raider getRaider( String nom, String prenom ) {
        Raider retour = null;
        for (Raider raider : _raiders) {
            if (raider.getNom().trim().compareTo(nom.trim()) == 0 &&
                    raider.getPrenom().trim().compareTo(prenom.trim()) == 0) {
                retour = raider;
                break;
            }
        }
        return retour;
    }

    public void removeRaider( String nom, String prenom ) {
        for (int i = 0; i < _raiders.size(); i++) {
            if (_raiders.get(i).getNom().trim().compareTo(nom.trim()) == 0 &&
                    _raiders.get(i).getPrenom().trim().compareTo(prenom.trim()) == 0) {
                _raiders.remove(i);
                break;
            }
        }
    }
}
