package fr.gecoraid.model;

public enum TypeLimite {
    // TODO L10n.getString()
    SANSLIMITE("Sans limite"),
    AVECLIMITETEMPS("Avec limite de temps"),
    AVECLIMITEHORAIRE("Avec limite horaire");

    private final String value;

    TypeLimite( String value ) {
        this.value = value;
    }

    public String getValue() {
        return this.value;
    }

    @Override
    public String toString() {
        return value;
    }

}
