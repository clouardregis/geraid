/*
 * Released by SPORTident under the CC BY 3.0 license.
 *
 * This work is licensed under the Creative Commons Attribution 3.0 Unported License.
 * To view a copy of this license, visit http://creativecommons.org/licenses/by/3.0/
 * or send a letter to Creative Commons, 444 Castro Street, Suite 900,
 * Mountain View, California, 94041, USA.
 */
package de.sportident;

public class CRCCalculator {
    private static final int POLY = 0x8005;
    private static final int BITF = 0x8000;

    private CRCCalculator() {
    }

    public static int crc( byte[] buffer ) {
        int ptr = 0;
        int tmp = (short) (buffer[ptr++] << 8 | (buffer[ptr++] & 0xFF));
        if (buffer.length > 2) {
            int val; // 16 Bit
            for (int i = buffer.length / 2; i > 0; i--) { // only even counts !!! and more than 4
                if (i > 1) {
                    val = buffer[ptr++] << 8 | (buffer[ptr++] & 0xFF);
                } else {
                    if (buffer.length % 2 == 1) {
                        val = buffer[buffer.length - 1] << 8;
                    } else {
                        val = 0; // last value with 0 // last 16 bit value
                    }
                }

                for (int j = 0; j < 16; j++) {
                    if ((tmp & BITF) != 0) {
                        tmp <<= 1;
                        if ((val & BITF) != 0) {
                            tmp++; // rotate carry
                        }
                        tmp ^= POLY;
                    } else {
                        tmp <<= 1;
                        if ((val & BITF) != 0) {
                            tmp++; // rotate carry
                        }
                    }
                    val <<= 1;
                }
            }
        }
        return tmp & 0xFFFF;
    }
}
