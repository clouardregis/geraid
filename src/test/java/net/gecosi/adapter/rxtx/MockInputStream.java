/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.adapter.rxtx;

import java.io.InputStream;

public class MockInputStream extends InputStream {
    private byte[] _input;

    public void setInput( byte[] input ) {
        _input = input;
    }

    @Override
    public int read() {
        return 0;
    }

    @Override
    public int read( byte[] answer, int offset, int len ) {
        int length = Math.min(_input.length, len);
        System.arraycopy(_input, 0, answer, offset, length);
        return length;
    }
}
