/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.adapter.rxtx;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoInteractions;

import com.fazecast.jSerialComm.SerialPortEvent;
import net.gecosi.internal.SportIdentLogger;
import net.gecosi.internal.SportIdentMessage;
import net.gecosi.internal.SportIdentMessageQueue;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class RxtxCommReaderTest {
    private MockInputStream _inputStream;
    @Mock
    private SportIdentMessageQueue _messageQueue;
    @Mock
    private SerialPortEvent _triggerEvent;
    private AutoCloseable _closeable;

    @BeforeEach
    public void setUp() {
        _closeable = MockitoAnnotations.openMocks(this);
        SportIdentLogger.open();
        _inputStream = new MockInputStream();
    }

    @AfterEach
    public void releaseMocks() throws Exception {
        _closeable.close();
    }

    public RxtxCommReader subject() {
        return new RxtxCommReader(_inputStream, _messageQueue);
    }

    @Test
    void nominalCase() {
        byte[] testInput = new byte[]{0x02, (byte) 0xF0, 0x03, 0x00, 0x01, 0x4D, 0x0D, 0x11, 0x03};
        testReaderOutput(new byte[][]{testInput}, testInput, subject());
    }

    @Test
    void messageInTwoFragments() {
        byte[] testInput1 = new byte[]{0x02, (byte) 0xF0, 0x03};
        byte[] testInput2 = new byte[]{0x00, 0x01, 0x4D, 0x0D, 0x11, 0x03};
        byte[] testOutput = new byte[]{0x02, (byte) 0xF0, 0x03, 0x00, 0x01, 0x4D, 0x0D, 0x11, 0x03};
        testReaderOutput(new byte[][]{testInput1, testInput2}, testOutput, subject());
    }

    @Test
    void messageInMultipleFragments() {
        byte[] testInput1 = new byte[]{0x02, (byte) 0xF0, 0x03};
        byte[] testInput2 = new byte[]{0x00, 0x01, 0x4D, 0x0D};
        byte[] testInput3 = new byte[]{0x11, 0x03};
        byte[] testOutput = new byte[]{0x02, (byte) 0xF0, 0x03, 0x00, 0x01, 0x4D, 0x0D, 0x11, 0x03};
        testReaderOutput(new byte[][]{testInput1, testInput2, testInput3}, testOutput, subject());
    }

    @Test
    void firstFragmentWithoutLengthPrefix() {
        byte[] testInput1 = new byte[]{0x02};
        byte[] testInput2 = new byte[]{(byte) 0xF0, 0x03, 0x00, 0x01, 0x4D, 0x0D};
        byte[] testInput3 = new byte[]{0x11, 0x03};
        byte[] testOutput = new byte[]{0x02, (byte) 0xF0, 0x03, 0x00, 0x01, 0x4D, 0x0D, 0x11, 0x03};
        testReaderOutput(new byte[][]{testInput1, testInput2, testInput3}, testOutput, subject());
    }

    @Test
    void zeroDataMessage() {
        byte[] testInput1 = new byte[]{0x02};
        byte[] testInput2 = new byte[]{(byte) 0xF0, 0x00, (byte) 0xF0, 0x00, 0x03};
        byte[] testOutput = new byte[]{0x02, (byte) 0xF0, 0x00, (byte) 0xF0, 0x00, 0x03};
        testReaderOutput(new byte[][]{testInput1, testInput2}, testOutput, subject());
    }

    @Test
    void emptyMessage() {
        RxtxCommReader subject = subject();
        _inputStream.setInput(new byte[0]);
        subject.serialEvent(_triggerEvent);
        verifyNoInteractions(_messageQueue);
    }

    @Test
    void shortMessage() {
        byte[] testInput = new byte[]{0x15};
        testReaderOutput(new byte[][]{testInput}, testInput, subject());
    }

    @Test
    synchronized void timeoutResetsAccumulator() {
        try {
            RxtxCommReader subject = new RxtxCommReader(_inputStream, _messageQueue, 1);
            byte[] testInput1 = new byte[]{0x02, (byte) 0xF0, 0x03};
            _inputStream.setInput(testInput1);
            subject.serialEvent(_triggerEvent);

            wait(2);
            byte[] testInput2 = new byte[]{0x00, 0x01, 0x4D, 0x0D, 0x11, 0x03};
            _inputStream.setInput(testInput2);
            subject.serialEvent(_triggerEvent);

            verifyNoInteractions(_messageQueue);

            wait(2);
            byte[] testInput = new byte[]{0x02, (byte) 0xF0, 0x03, 0x00, 0x01, 0x4D, 0x0D, 0x11, 0x03};
            testReaderOutput(new byte[][]{testInput}, testInput, subject);
        } catch (InterruptedException e) {
            fail();
        }
    }

    @Test
    synchronized void tooLongFragmentResetsAccumulator() {
        RxtxCommReader subject = new RxtxCommReader(_inputStream, _messageQueue, 1);
        byte[] testInput1 = new byte[]{0x02, (byte) 0xF0, 0x03};
        _inputStream.setInput(testInput1);
        subject.serialEvent(_triggerEvent);

        byte[] testInput2 = new byte[]{0x00, 0x01, 0x4D, 0x0D, 0x11, 0x03, (byte) 0xFF};
        _inputStream.setInput(testInput2);
        subject.serialEvent(_triggerEvent);

        verifyNoInteractions(_messageQueue);

        try {
            wait(2);
            byte[] testInput = new byte[]{0x02, (byte) 0xF0, 0x03, 0x00, 0x01, 0x4D, 0x0D, 0x11, 0x03};
            testReaderOutput(new byte[][]{testInput}, testInput, subject);
        } catch (InterruptedException e) {
            fail();
        }
    }

    private void testReaderOutput( byte[][] testInputs, byte[] expectedOutput, RxtxCommReader subject ) {
        try {
            for (byte[] testInput : testInputs) {
                _inputStream.setInput(testInput);
                subject.serialEvent(_triggerEvent);
            }
            ArgumentCaptor<SportIdentMessage> message = ArgumentCaptor.forClass(SportIdentMessage.class);
            verify(_messageQueue).put(message.capture());
            assertArrayEquals(expectedOutput, message.getValue().sequence());
        } catch (InterruptedException e) {
            fail();
        }
    }

}
