/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.internal;

import net.gecosi.CommStatus;
import net.gecosi.SportIdentHandler;
import net.gecosi.dataframe.SportIdent6DataFrame;
import net.gecosi.dataframe.SportIdent8PlusDataFrame;
import net.gecosi.dataframe.SportIdentDataFrame;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static net.gecosi.SiMessageFixtures.no_ext_protocol_answer;
import static net.gecosi.SiMessageFixtures.ok_ext_protocol_answer;
import static net.gecosi.SiMessageFixtures.si6_192_punches_answer;
import static net.gecosi.SiMessageFixtures.si6_64_punches_answer;
import static net.gecosi.SiMessageFixtures.siCard10_b0_data;
import static net.gecosi.SiMessageFixtures.siCard10_b4_data;
import static net.gecosi.SiMessageFixtures.siCard10_detected;
import static net.gecosi.SiMessageFixtures.siCard5_data;
import static net.gecosi.SiMessageFixtures.siCard5_detected;
import static net.gecosi.SiMessageFixtures.siCard5_removed;
import static net.gecosi.SiMessageFixtures.siCard6_192p_b0_data;
import static net.gecosi.SiMessageFixtures.siCard6_192p_b2_data;
import static net.gecosi.SiMessageFixtures.siCard6_192p_b3_data;
import static net.gecosi.SiMessageFixtures.siCard6_192p_b6_data;
import static net.gecosi.SiMessageFixtures.siCard6_192p_b7_data;
import static net.gecosi.SiMessageFixtures.siCard6_detected;
import static net.gecosi.SiMessageFixtures.startup_answer;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.verify;

class SportIdentDriverTest {

    @Mock
    private SportIdentHandler _sportIdentHandler;
    private AutoCloseable closeable;

    @BeforeEach
    void setUp() {
        closeable = MockitoAnnotations.openMocks(this);
        SportIdentLogger.open();
    }

    @AfterEach
    void releaseMocks() throws Exception {
        closeable.close();
    }

    private void testRunDriver( SportIdentDriver driver ) throws InterruptedException {
        driver.start();
        Thread.sleep(250);
        driver.interrupt();
    }

    @Test
    void startupProtocol_succeeds() throws Exception {
        SportIdentPort sportIdentPort = new MockCommPort(new SportIdentMessage[]{startup_answer, ok_ext_protocol_answer, si6_64_punches_answer});
        testRunDriver(new SportIdentDriver(sportIdentPort, _sportIdentHandler));

        InOrder inOrder = inOrder(_sportIdentHandler);
        inOrder.verify(_sportIdentHandler).notify(CommStatus.STARTING);
        inOrder.verify(_sportIdentHandler).notify(CommStatus.READY);
    }

    @Test
    void startupProtocol_failsOnTimeout() throws Exception {
        SportIdentPort sportIdentPort = new MockCommPort();
        testRunDriver(new SportIdentDriver(sportIdentPort, _sportIdentHandler));

        InOrder inOrder = inOrder(_sportIdentHandler);
        Thread.sleep(200);
        inOrder.verify(_sportIdentHandler).notify(CommStatus.STARTING);
        inOrder.verify(_sportIdentHandler).notifyError(eq(CommStatus.FATAL_ERROR), anyString());
        inOrder.verify(_sportIdentHandler).notify(CommStatus.OFF);
    }

    @Test
    void startupProtocol_failsOnExtendedProtocolCheck() throws Exception {
        SportIdentPort sportIdentPort = new MockCommPort(new SportIdentMessage[]{startup_answer, no_ext_protocol_answer});
        testRunDriver(new SportIdentDriver(sportIdentPort, _sportIdentHandler));

        InOrder inOrder = inOrder(_sportIdentHandler);
        Thread.sleep(200);
        inOrder.verify(_sportIdentHandler).notify(CommStatus.STARTING);
        inOrder.verify(_sportIdentHandler).notifyError(eq(CommStatus.FATAL_ERROR), anyString());
        inOrder.verify(_sportIdentHandler).notify(CommStatus.OFF);
    }

    @Test
    void readSiCard5() throws Exception {
        SportIdentPort sportIdentPort = new MockCommPort(new SportIdentMessage[]{startup_answer, ok_ext_protocol_answer, si6_64_punches_answer, siCard5_detected, siCard5_data, siCard5_removed});
        testRunDriver(new SportIdentDriver(sportIdentPort, _sportIdentHandler));

        verify(_sportIdentHandler).notify(any(SportIdentDataFrame.class));
    }

    @Test
    void siCard5_removedBeforeRead() throws Exception {
        SportIdentPort sportIdentPort = new MockCommPort(new SportIdentMessage[]{startup_answer, ok_ext_protocol_answer, si6_64_punches_answer, siCard5_detected, siCard5_removed});
        testRunDriver(new SportIdentDriver(sportIdentPort, _sportIdentHandler));

        verify(_sportIdentHandler).notify(CommStatus.PROCESSING_ERROR);
    }

    @Test
    void readSiCard6_192Punches() throws Exception {
        SportIdentPort sportIdentPort = new MockCommPort(new SportIdentMessage[]{startup_answer, ok_ext_protocol_answer, si6_192_punches_answer, siCard6_detected, siCard6_192p_b0_data, siCard6_192p_b6_data, siCard6_192p_b7_data, siCard6_192p_b2_data, siCard6_192p_b3_data, siCard5_removed});
        testRunDriver(new SportIdentDriver(sportIdentPort, _sportIdentHandler));
        ArgumentCaptor<SportIdent6DataFrame> si6Arg = ArgumentCaptor.forClass(SportIdent6DataFrame.class);
        verify(_sportIdentHandler).notify(si6Arg.capture());
        SportIdentDataFrame si6Data = si6Arg.getValue().startingAt(0);

        assertEquals(101, si6Data.getNbPunches());
        assertEquals(31, si6Data.getPunches()[0].code());
        assertEquals(634, si6Data.getPunches()[100].code());
    }

    @Test
    void readSiCard10_192Punches() throws Exception {
        SportIdentPort sportIdentPort = new MockCommPort(new SportIdentMessage[]{startup_answer, ok_ext_protocol_answer, si6_192_punches_answer, siCard10_detected, siCard10_b0_data, siCard10_b4_data, siCard5_removed});
        testRunDriver(new SportIdentDriver(sportIdentPort, _sportIdentHandler));
        ArgumentCaptor<SportIdent8PlusDataFrame> si10Arg = ArgumentCaptor.forClass(SportIdent8PlusDataFrame.class);
        verify(_sportIdentHandler).notify(si10Arg.capture());
        SportIdentDataFrame si10Data = si10Arg.getValue().startingAt(0);

        assertEquals(3, si10Data.getNbPunches());
        assertEquals(42, si10Data.getPunches()[0].code());
        assertEquals(32, si10Data.getPunches()[2].code());
    }
}
