/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.internal;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class SportIdentMessageTest {
    private final SportIdentMessage _message = new SportIdentMessage(new byte[]{0x02, (byte) 0xF0, 0x01, 0x4D, 0x6D, 0x0A, 0x03});
    private final SportIdentMessage _crcFail = new SportIdentMessage(new byte[]{0x02, (byte) 0xF0, 0x01, 0x4D, 0x00, 0x00, 0x03});
    private final SportIdentMessage _badStart = new SportIdentMessage(new byte[]{0x00, (byte) 0xF0, 0x01, 0x4D, 0x00, 0x00, 0x03});
    private final SportIdentMessage _badEnd = new SportIdentMessage(new byte[]{0x02, (byte) 0xF0, 0x01, 0x4D, 0x00, 0x00, 0x00});
    private final SportIdentMessage _badCmd = new SportIdentMessage(new byte[]{0x02, (byte) 0x00, 0x01, 0x4D, 0x00, 0x00, 0x03});

    @Test
    void commandByte() {
        assertEquals((byte) 0xF0, _message.commandByte());
    }

    @Test
    void extractCRC() {
        assertEquals(0x6D0A, _message.extractCRC());
    }

    @Test
    void computeCRC() {
        assertEquals(0x6D0A, _message.computeCRC());
    }

    @Test
    void check() {
        assertTrue(_message.check((byte) 0xF0));
    }

    @Test
    void check_failsOnCrcError() {
        assertFalse(_crcFail.check((byte) 0xF0));
    }

    @Test
    void check_failsOnBadStartOrBadEnd() {
        assertFalse(_badStart.check((byte) 0xF0));
        assertFalse(_badEnd.check((byte) 0xF0));
    }

    @Test
    void check_failsOnBadCommand() {
        assertFalse(_badCmd.check((byte) 0xF0));
    }
}
