/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.internal;

import java.util.Arrays;

public class MockCommPort implements SportIdentPort {

    private final MockComm _comm;

    private final SportIdentMessageQueue _messageQueue;

    public MockCommPort() {
        this(new SportIdentMessage[0]);
    }

    public MockCommPort( SportIdentMessage[] sportIdentMessages ) {
        _comm = new MockComm();
        _messageQueue = new SportIdentMessageQueue(sportIdentMessages.length + 1, 1);
        _messageQueue.addAll(Arrays.asList(sportIdentMessages));

    }

    public SportIdentMessageQueue createMessageQueue() {
        return _messageQueue;
    }

    public CommWriter createWriter() {
        return _comm;
    }

    public static class MockComm implements CommWriter {
        public void write( SportIdentMessage message ) {
        }
    }

    public void setupHighSpeed() {
    }

    public void setupLowSpeed() {
    }

    public void close() {
        // TODO test always closed/called
    }
}
