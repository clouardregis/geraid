/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.internal;

import net.gecosi.CommStatus;
import net.gecosi.SportIdentHandler;
import net.gecosi.SiMessageFixtures;
import net.gecosi.dataframe.SportIdent5DataFrame;
import net.gecosi.dataframe.SportIdent6DataFrame;
import net.gecosi.dataframe.SportIdent8PlusDataFrame;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InOrder;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.concurrent.TimeoutException;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;

class SportIdentDriverStateTest {

    private SportIdentMessageQueue queue;

    @Mock
    private CommWriter writer;

    @Mock
    private SportIdentHandler _sportIdentHandler;
    private AutoCloseable closeable;

    @BeforeEach
    void setUp() {
        closeable = MockitoAnnotations.openMocks(this);
        queue = new SportIdentMessageQueue(10, 1);
        SportIdentDriverState.setSportIdentCard6_192PunchesMode(false);
        SportIdentLogger.open();
    }

    @AfterEach
    void releaseMocks() throws Exception {
        closeable.close();
    }

    @Test
    void STARTUP_CHECK() throws Exception {
        queue.add(SiMessageFixtures.startup_answer);
        SportIdentDriverState nextState = SportIdentDriverState.STARTUP_CHECK.receive(queue, writer, _sportIdentHandler);

        assertEquals(SportIdentDriverState.CONFIG_CHECK, nextState);
        verify(writer).write(SportIdentMessage.get_protocol_configuration);
    }

    @Test
    void STARTUP_CHECK_throwsTimeoutException() {
        assertThrows(TimeoutException.class, () -> SportIdentDriverState.STARTUP_CHECK.receive(queue, writer, _sportIdentHandler));
    }

    @Test
    void STARTUP_CHECK_throwsInvalidMessage() {
        queue.add(SportIdentMessage.ack_sequence);
        assertThrows(InvalidMessage.class, () -> SportIdentDriverState.STARTUP_CHECK.receive(queue, writer, _sportIdentHandler));
    }

    @Test
    void CONFIG_CHECK() throws Exception {
        queue.add(SiMessageFixtures.ok_ext_protocol_answer);
        SportIdentDriverState nextState = SportIdentDriverState.CONFIG_CHECK.receive(queue, writer, _sportIdentHandler);
        assertEquals(SportIdentDriverState.SI6_CARDBLOCKS_SETTING, nextState);
    }

    @Test
    void CONFIG_CHECK_failsOnExtendedProtocol() throws Exception {
        queue.add(SiMessageFixtures.no_ext_protocol_answer);
        SportIdentDriverState nextState = SportIdentDriverState.CONFIG_CHECK.receive(queue, writer, _sportIdentHandler);
        assertEquals(SportIdentDriverState.EXTENDED_PROTOCOL_ERROR, nextState);
    }

    @Test
    void CONFIG_CHECK_failsOnHandshakeMode() throws Exception {
        queue.add(SiMessageFixtures.no_handshake_answer);
        SportIdentDriverState nextState = SportIdentDriverState.CONFIG_CHECK.receive(queue, writer, _sportIdentHandler);
        assertEquals(SportIdentDriverState.HANDSHAKE_MODE_ERROR, nextState);
    }

    @Test
    void SI6_CARDBLOCKS_SETTING_64PunchesMode() throws Exception {
        queue.add(SiMessageFixtures.si6_64_punches_answer);
        SportIdentDriverState nextState = SportIdentDriverState.SI6_CARDBLOCKS_SETTING.receive(queue, writer, _sportIdentHandler);

        verify(_sportIdentHandler).notify(CommStatus.ON);
        assertFalse(SportIdentDriverState.sport_ident_card6_192PunchesMode());
        assertEquals(SportIdentDriverState.DISPATCH_READY, nextState);
    }

    @Test
    void SI6_CARDBLOCKS_SETTING_192PunchesMode() throws Exception {
        queue.add(SiMessageFixtures.si6_192_punches_answer);
        SportIdentDriverState nextState = SportIdentDriverState.SI6_CARDBLOCKS_SETTING.receive(queue, writer, _sportIdentHandler);

        verify(_sportIdentHandler).notify(CommStatus.ON);
        assertTrue(SportIdentDriverState.sport_ident_card6_192PunchesMode());
        assertEquals(SportIdentDriverState.DISPATCH_READY, nextState);
    }

    @Test
    void DISPATCH_READY() throws Exception {
        queue.add(SiMessageFixtures.siCard5_detected);
        SportIdentDriverState.DISPATCH_READY.receive(queue, writer, _sportIdentHandler);
        verify(_sportIdentHandler).notify(CommStatus.READY);
    }

    @Test
    void DISPATCH_READY_dispatchesSiCard5() throws Exception {
        queue.add(SiMessageFixtures.siCard5_detected);
        SportIdentDriverState.DISPATCH_READY.receive(queue, writer, _sportIdentHandler);
        verify(writer).write(SportIdentMessage.read_sport_ident_card_5);
    }

    @Test
    void RETRIEVE_SICARD_5_DATA() throws Exception {
        queue.add(SiMessageFixtures.siCard5_data);
        SportIdentDriverState nextState = SportIdentDriverState.RETRIEVE_SPORT_IDENT_CARD_5_DATA.retrieve(queue, writer, _sportIdentHandler);

        verify(writer).write(SportIdentMessage.ack_sequence);
        verify(_sportIdentHandler).notify(any(SportIdent5DataFrame.class));
        assertEquals(SportIdentDriverState.WAIT_SPORT_IDENT_CARD_REMOVAL, nextState);
    }

    @Test
    void RETRIEVE_SICARD_5_DATA_earlySiCardRemovalFallbackToDispatchReady() throws Exception {
        queue.add(SiMessageFixtures.siCard5_removed);
        SportIdentDriverState nextState = SportIdentDriverState.RETRIEVE_SPORT_IDENT_CARD_5_DATA.retrieve(queue, writer, _sportIdentHandler);
        verify(_sportIdentHandler).notify(CommStatus.PROCESSING_ERROR);
        assertEquals(SportIdentDriverState.DISPATCH_READY, nextState);
    }

    @Test
    void RETRIEVE_SICARD_5_DATA_timeoutFallbackToDispatchReady() throws Exception {
        SportIdentDriverState nextState = SportIdentDriverState.RETRIEVE_SPORT_IDENT_CARD_5_DATA.retrieve(queue, writer, _sportIdentHandler);
        verify(_sportIdentHandler).notify(CommStatus.PROCESSING_ERROR);
        assertEquals(SportIdentDriverState.DISPATCH_READY, nextState);
    }

    @Test
    void DISPATCH_READY_dispatchesSiCard6() throws Exception {
        queue.add(SiMessageFixtures.siCard6_detected);
        SportIdentDriverState.DISPATCH_READY.receive(queue, writer, _sportIdentHandler);
        verify(writer).write(SportIdentMessage.read_sport_ident_card_6_b0);
    }

    @Test
    void DISPATCH_READY_dispatchesSiCard6Star() throws Exception {
        queue.add(SiMessageFixtures.siCard6Star_detected);
        SportIdentDriverState.DISPATCH_READY.receive(queue, writer, _sportIdentHandler);
        verify(writer).write(SportIdentMessage.read_sport_ident_card_6_b0);
    }

    @Test
    void DISPATCH_READY_dispatchesSiCard6In192PunchesMode() throws Exception {
        SportIdentDriverState.setSportIdentCard6_192PunchesMode(true);

        queue.add(SiMessageFixtures.siCard6_detected);
        SportIdentDriverState.DISPATCH_READY.receive(queue, writer, _sportIdentHandler);
        verify(writer).write(SportIdentMessage.read_sport_ident_card_6_b0);
    }

    @Test
    void RETRIEVE_SICARD_6_DATA() throws Exception {
        queue.add(SiMessageFixtures.siCard6_b0_data);
        queue.add(SiMessageFixtures.siCard6_b6_data);
        queue.add(SiMessageFixtures.siCard6_b7_data);
        SportIdentDriverState nextState = SportIdentDriverState.RETRIEVE_SPORT_IDENT_CARD_6_DATA.retrieve(queue, writer, _sportIdentHandler);

        verify(writer).write(SportIdentMessage.read_sport_ident_card_6_b0);
        verify(writer).write(SportIdentMessage.read_sport_ident_card_6_b6);
        verify(writer, never()).write(SportIdentMessage.read_sport_ident_card_6_b7);
        verify(writer).write(SportIdentMessage.ack_sequence);
        verify(_sportIdentHandler).notify(any(SportIdent6DataFrame.class));
        assertEquals(SportIdentDriverState.WAIT_SPORT_IDENT_CARD_REMOVAL, nextState);
    }

    @Test
    void RETRIEVE_SICARD_6_8BLOCKS_DATA() throws Exception {
        queue.add(SiMessageFixtures.siCard6_192p_b0_data);
        queue.add(SiMessageFixtures.siCard6_192p_b1_data);
        queue.add(SiMessageFixtures.siCard6_192p_b2_data);
        queue.add(SiMessageFixtures.siCard6_192p_b3_data);
        queue.add(SiMessageFixtures.siCard6_192p_b4_data);
        queue.add(SiMessageFixtures.siCard6_192p_b5_data);
        queue.add(SiMessageFixtures.siCard6_192p_b6_data);
        queue.add(SiMessageFixtures.siCard6_192p_b7_data);
        SportIdentDriverState nextState = SportIdentDriverState.RETRIEVE_SPORT_IDENT_CARD_6_DATA.retrieve(queue, writer, _sportIdentHandler);

        InOrder inOrder = inOrder(writer);
        inOrder.verify(writer).write(SportIdentMessage.read_sport_ident_card_6_b0);
        inOrder.verify(writer).write(SportIdentMessage.read_sport_ident_card_6_b6);
        inOrder.verify(writer).write(SportIdentMessage.read_sport_ident_card_6_b7);
        inOrder.verify(writer).write(SportIdentMessage.read_sport_ident_card_6_plus_b2);
        inOrder.verify(writer).write(SportIdentMessage.read_sport_ident_card_6_plus_b3);
        verify(writer, never()).write(SportIdentMessage.read_sport_ident_card_6_plus_b4);
        verify(writer, never()).write(SportIdentMessage.read_sport_ident_card_6_plus_b5);
        inOrder.verify(writer).write(SportIdentMessage.ack_sequence);
        verify(_sportIdentHandler).notify(any(SportIdent6DataFrame.class));
        assertEquals(SportIdentDriverState.WAIT_SPORT_IDENT_CARD_REMOVAL, nextState);
    }

    @Test
    void DISPATCH_READY_dispatchesSiCard8() throws Exception {
        queue.add(SiMessageFixtures.siCard8_detected);
        SportIdentDriverState.DISPATCH_READY.receive(queue, writer, _sportIdentHandler);
        verify(writer).write(SportIdentMessage.read_sport_ident_card_8_plus_b0);
    }

    @Test
    void RETRIEVE_SICARD_8_9_DATA() throws Exception {
        queue.add(SiMessageFixtures.siCard9_b0_data);
        queue.add(SiMessageFixtures.siCard9_b1_data);
        SportIdentDriverState nextState = SportIdentDriverState.RETRIEVE_SPORT_IDENT_CARD_8_9_DATA.retrieve(queue, writer, _sportIdentHandler);

        verify(writer).write(SportIdentMessage.read_sport_ident_card_8_plus_b0);
        verify(writer).write(SportIdentMessage.read_sport_ident_card_8_plus_b1);
        verify(writer).write(SportIdentMessage.ack_sequence);
        verify(_sportIdentHandler).notify(any(SportIdent8PlusDataFrame.class));
        assertEquals(SportIdentDriverState.WAIT_SPORT_IDENT_CARD_REMOVAL, nextState);
    }

    @Test
    void DISPATCH_READY_dispatchesSiCard10() throws Exception {
        queue.add(SiMessageFixtures.siCard10_detected);
        SportIdentDriverState.DISPATCH_READY.receive(queue, writer, _sportIdentHandler);
        verify(writer).write(SportIdentMessage.read_sport_ident_card_10_plus_b0);
    }

    @Test
    void DISPATCH_READY_dispatchesSiCard11() throws Exception {
        queue.add(SiMessageFixtures.siCard11_detected);
        SportIdentDriverState.DISPATCH_READY.receive(queue, writer, _sportIdentHandler);
        verify(writer).write(SportIdentMessage.read_sport_ident_card_10_plus_b0);
    }

    @Test
    void DISPATCH_READY_dispatchesSiCard10PlusIn192PunchesMode() throws Exception {
        SportIdentDriverState.setSportIdentCard6_192PunchesMode(true);

        queue.add(SiMessageFixtures.siCard10_detected);
        SportIdentDriverState.DISPATCH_READY.receive(queue, writer, _sportIdentHandler);
        verify(writer).write(SportIdentMessage.read_sport_ident_card_10_plus_b0);
    }

    @Test
    void RETRIEVE_SICARD_10_PLUS_DATA() throws Exception {
        queue.add(SiMessageFixtures.siCard10_b0_data);
        queue.add(SiMessageFixtures.siCard10_b4_data);
        queue.add(SiMessageFixtures.siCard10_b5_data);
        queue.add(SiMessageFixtures.siCard10_b6_data);
        queue.add(SiMessageFixtures.siCard10_b7_data);
        SportIdentDriverState nextState = SportIdentDriverState.RETRIEVE_SPORT_IDENT_CARD_10_PLUS_DATA.retrieve(queue, writer, _sportIdentHandler);

        InOrder inOrder = inOrder(writer);
        inOrder.verify(writer).write(SportIdentMessage.read_sport_ident_card_10_plus_b0);
        inOrder.verify(writer).write(SportIdentMessage.read_sport_ident_card_10_plus_b4);
        verify(writer, never()).write(SportIdentMessage.read_sport_ident_card_10_plus_b5);
        verify(writer, never()).write(SportIdentMessage.read_sport_ident_card_10_plus_b6);
        verify(writer, never()).write(SportIdentMessage.read_sport_ident_card_10_plus_b7);
        inOrder.verify(writer).write(SportIdentMessage.ack_sequence);
        verify(_sportIdentHandler).notify(any(SportIdent8PlusDataFrame.class));
        assertEquals(SportIdentDriverState.WAIT_SPORT_IDENT_CARD_REMOVAL, nextState);
    }

    @Test
    void RETRIEVE_SICARD_10_PLUS_DATA_192_MODE() throws Exception {
        queue.add(SiMessageFixtures.siCard10_b0_data);
        queue.add(SiMessageFixtures.siCard10_b1_data);
        queue.add(SiMessageFixtures.siCard10_b2_data);
        queue.add(SiMessageFixtures.siCard10_b3_data);
        queue.add(SiMessageFixtures.siCard10_b4_data);
        queue.add(SiMessageFixtures.siCard10_b5_data);
        queue.add(SiMessageFixtures.siCard10_b6_data);
        queue.add(SiMessageFixtures.siCard10_b7_data);
        SportIdentDriverState nextState = SportIdentDriverState.RETRIEVE_SPORT_IDENT_CARD_10_PLUS_DATA.retrieve(queue, writer, _sportIdentHandler);

        verify(writer).write(SportIdentMessage.read_sport_ident_card_10_plus_b0);
        verify(writer).write(SportIdentMessage.read_sport_ident_card_10_plus_b4);
        verify(writer, never()).write(SportIdentMessage.read_sport_ident_card_10_plus_b5);
        verify(writer, never()).write(SportIdentMessage.read_sport_ident_card_10_plus_b6);
        verify(writer, never()).write(SportIdentMessage.read_sport_ident_card_10_plus_b7);
        verify(writer).write(SportIdentMessage.ack_sequence);
        verify(_sportIdentHandler).notify(any(SportIdent8PlusDataFrame.class));
        assertEquals(SportIdentDriverState.WAIT_SPORT_IDENT_CARD_REMOVAL, nextState);
    }

    @Test
    void WAIT_SICARD_REMOVAL() throws Exception {
        queue.add(SiMessageFixtures.siCard5_removed);
        SportIdentDriverState nextState = SportIdentDriverState.WAIT_SPORT_IDENT_CARD_REMOVAL.receive(queue, writer, _sportIdentHandler);
        assertEquals(SportIdentDriverState.DISPATCH_READY, nextState);
    }

    @Test
    void WAIT_SICARD_REMOVAL_timeoutFallbackToDispatchReady() throws Exception {
        SportIdentDriverState nextState = SportIdentDriverState.WAIT_SPORT_IDENT_CARD_REMOVAL.receive(queue, writer, _sportIdentHandler);
        assertEquals(SportIdentDriverState.DISPATCH_READY, nextState);
    }
}
