/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.dataframe;

import net.gecosi.internal.SportIdentMessage;
import org.junit.jupiter.api.Test;

import static net.gecosi.SiMessageFixtures.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Si11DataFrameTest {

    @Test
    void getSiCardNumber() {
        assertEquals("9993810", subject9993810().getSiNumber());
    }

    @Test
    void getSiCardSeries() {
        assertEquals("SiCard 10/11/SIAC", subject9993810().getSiSeries());
    }

    @Test
    void getStartTime() {
        assertEquals(SportIdentDataFrame.NO_TIME, subject9993810().getStartTime());
    }

    @Test
    void getFinishTime() {
        assertEquals(205497000L, subject9993810().getFinishTime());
    }

    @Test
    void getCheckTime() {
        assertEquals(SportIdentDataFrame.NO_TIME, subject9993810().getCheckTime());
    }

    @Test
    void getNbPunches() {
        assertEquals(4, subject9993810().getNbPunches());
    }

    @Test
    void getPunches() {
        SportIdentPunch[] punches = subject9993810().getPunches();
        assertEquals(38, punches[0].code());
        assertEquals(54903000L, punches[0].timestamp());
        assertEquals(42, punches[1].code());
        assertEquals(132543000L, punches[1].timestamp());
        assertEquals(38, punches[2].code());
        assertEquals(132554000L, punches[2].timestamp());
        assertEquals(32, punches[3].code());
        assertEquals(134912000L, punches[3].timestamp());
    }

    private SportIdentDataFrame subject9993810() {
        return new SportIdent8PlusDataFrame(new SportIdentMessage[]{
                siCard11_b0_data, siCard11_b4_data, siCard11_b5_data, siCard11_b6_data, siCard11_b7_data}).startingAt(0);
    }

}
