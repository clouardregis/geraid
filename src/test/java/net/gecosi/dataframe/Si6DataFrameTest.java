/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.dataframe;

import net.gecosi.internal.SportIdentMessage;
import org.junit.jupiter.api.Test;

import static net.gecosi.SiMessageFixtures.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Si6DataFrameTest {
    @Test
    void getSiCardNumber() {
        assertEquals("821003", subject821003().getSiNumber());
    }

    @Test
    void getSiCardSeries() {
        assertEquals("SiCard 6", subject821003().getSiSeries());
    }

    @Test
    void getStartTime() {
        assertEquals(35565000L, subject821003().getStartTime());
    }

    @Test
    void getFinishTime() {
        assertEquals(35652000L, subject821003().getFinishTime());
    }

    @Test
    void getCheckTime() {
        assertEquals(35563000L, subject821003().getCheckTime());
    }

    @Test
    void getNbPunches() {
        assertEquals(5, subject821003().getNbPunches());
    }

    @Test
    void getPunches() {
        SportIdentPunch[] punches = subject821003().getPunches();
        assertEquals(31, punches[0].code());
        assertEquals(35589000L, punches[0].timestamp());
        assertEquals(32, punches[1].code());
        assertEquals(35602000L, punches[1].timestamp());
        assertEquals(35, punches[4].code());
        assertEquals(35635000L, punches[4].timestamp());
    }

    private SportIdentDataFrame subject821003() {
        return new SportIdent6DataFrame(new SportIdentMessage[]{siCard6_b0_data, siCard6_b6_data, siCard6_b7_data}).startingAt(0);
    }
}
