/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.dataframe;

import net.gecosi.SiMessageFixtures;
import net.gecosi.internal.SportIdentMessage;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Si8DataFrameTest {

    @Test
    void getSiCardNumber() {
        assertEquals("2005331", subject2005331().getSiNumber());
    }

    @Test
    void getSiCardSeries() {
        assertEquals("SiCard 8", subject2005331().getSiSeries());
    }

    @Test
    void getStartTime() {
        assertEquals(71950000L, subject2005331().getStartTime());
    }

    @Test
    void getFinishTime() {
        assertEquals(71992000L, subject2005331().getFinishTime());
    }

    @Test
    void getCheckTime() {
        assertEquals(71949000L, subject2005331().getCheckTime());
    }

    @Test
    void getNbPunches() {
        assertEquals(1, subject2005331().getNbPunches());
    }

    @Test
    void getPunches() {
        SportIdentPunch[] punches = subject2005331().getPunches();
        assertEquals(31, punches[0].code());
        assertEquals(71970000L, punches[0].timestamp());
    }

    private SportIdentDataFrame subject2005331() {
        return new SportIdent8PlusDataFrame(new SportIdentMessage[]{SiMessageFixtures.siCard8_b0_data, SiMessageFixtures.siCard8_b1_data}).startingAt(0);
    }

}
