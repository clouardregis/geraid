/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.dataframe;

import net.gecosi.SiMessageFixtures;
import net.gecosi.internal.SportIdentMessage;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SportIdent5DataFrameTest {
    @Test
    void getSiCardNumber() {
        assertEquals("304243", subject304243().getSiNumber());
        assertEquals("36353", subject36353().getSiNumber());
    }

    @Test
    void getSiCardSeries() {
        assertEquals("SiCard 5", subject36353().getSiSeries());
    }

    @Test
    void getStartTime() {
        assertEquals(1234000L, subject36353().getStartTime());
    }

    @Test
    void getFinishTime() {
        assertEquals(4321000L, subject36353().getFinishTime());
    }

    @Test
    void getCheckTime() {
        assertEquals(4444000L, subject36353().getCheckTime());
    }

    @Test
    void getNbPunches() {
        assertEquals(36, subject36353().getNbPunches());
    }

    @Test
    void getPunches() {
        SportIdentPunch[] punches = subject36353().getPunches();
        assertEquals(36, punches[0].code());
        assertEquals(40059000L, punches[0].timestamp());
        assertEquals(59, punches[9].code());
        assertEquals(40104000L, punches[9].timestamp());
    }

    @Test
    void getPunches_with36Punches() {
        SportIdentPunch[] punches = subject36353().getPunches();
        assertEquals(31, punches[30].code());
        assertEquals(SportIdent5DataFrame.NO_TIME, punches[30].timestamp());
        assertEquals(36, punches[35].code());
        assertEquals(SportIdent5DataFrame.NO_TIME, punches[35].timestamp());
    }

    @Test
    void getPunches_withZeroHourShift() {
        SportIdentDataFrame subject = subject36353().startingAt(41400000L);
        assertEquals(44434000L, subject.getStartTime());
        assertEquals(47521000L, subject.getFinishTime());
        SportIdentPunch[] punches = subject.getPunches();
        assertEquals(83304000L, punches[9].timestamp());
        assertEquals(SportIdent5DataFrame.NO_TIME, punches[10].timestamp());
        assertEquals(SportIdent5DataFrame.NO_TIME, punches[35].timestamp());
    }

    @Test
    void getPunches_withoutStartTime() {
        SportIdentDataFrame subject = subject304243();
        assertEquals(SportIdentDataFrame.NO_TIME, subject.getStartTime());
        assertEquals(43704000L, subject.getFinishTime());
        SportIdentPunch[] punches = subject.getPunches();
        assertEquals(40059000L, punches[0].timestamp());
        assertEquals(40104000L, punches[9].timestamp());
    }

    @Test
    void getPunches_withoutStartTime_withMidnightShift() {
        SportIdentDataFrame subject = subject304243().startingAt(82800000L);
        assertEquals(SportIdentDataFrame.NO_TIME, subject.getStartTime());
        assertEquals(86904000L, subject.getFinishTime());
        SportIdentPunch[] punches = subject.getPunches();
        assertEquals(83259000L, punches[0].timestamp());
        assertEquals(83304000L, punches[9].timestamp());
    }

    @Test
    void advanceTimePast_doesNotChangeTimeWhen() {
        SportIdent5DataFrame subject = (SportIdent5DataFrame) subject36353();
        assertEquals(1000L, subject.advanceTimePast(1000, 0), "Time already past Ref");
        assertEquals(1000L, subject.advanceTimePast(1000, 1500), "Time less than 1 hour behind Ref");
        assertEquals(1000L, subject.advanceTimePast(1000, SportIdentDataFrame.NO_TIME), "No time for Ref");
    }

    @Test
    void advanceTimePast_advancesBy12HoursStep() {
        SportIdent5DataFrame subject = (SportIdent5DataFrame) subject36353();
        assertEquals(43200000L, subject.advanceTimePast(0, 3600001));
        assertEquals(133200000L, subject.advanceTimePast(3600000, 100000000L)); // + 36 hours
    }

    @Test
    void advanceTimePast_returnsNoTime() {
        SportIdent5DataFrame subject = (SportIdent5DataFrame) subject36353();
        assertEquals(SportIdentDataFrame.NO_TIME, subject.advanceTimePast(0xEEEE * 1000, 0), "Time is unknown");
    }

    private SportIdentDataFrame subject304243() {
        return new SportIdent5DataFrame(SiMessageFixtures.siCard5_data).startingAt(0);
    }

    private SportIdentDataFrame subject36353() {
        return new SportIdent5DataFrame(new SportIdentMessage(new byte[]{0x02, (byte) 0xB1, (byte) 0x82, 0x00, 0x01, (byte) 0xAA, 0x2E, 0x00, 0x01, (byte) 0x8E, (byte) 0x01, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x65, 0x10, (byte) 0x93, (byte) 0x04, (byte) 0xD2, (byte) 0x10, (byte) 0xE1, 0x25, 0x56, (byte) 0x11, (byte) 0x5C, 0x28, 0x03, (byte) 0xA6, 0x00, 0x07, 0x1F, 0x24, (byte) 0x9C, 0x7B, 0x26, (byte) 0x9C, (byte) 0x8C, 0x22, (byte) 0x9C, (byte) 0x8D, 0x28, (byte) 0x9C, (byte) 0x8F, 0x34, (byte) 0x9C, (byte) 0x9B, 0x20, 0x36, (byte) 0x9C, (byte) 0x9F, 0x33, (byte) 0x9C, (byte) 0xA1, 0x35, (byte) 0x9C, (byte) 0xA2, 0x3C, (byte) 0x9C, (byte) 0xA7, 0x3B, (byte) 0x9C, (byte) 0xA8, 0x21, 0x00, (byte) 0xEE, (byte) 0xEE, 0x00, (byte) 0xEE, (byte) 0xEE, 0x00, (byte) 0xEE, (byte) 0xEE, 0x00, (byte) 0xEE, (byte) 0xEE, 0x00, (byte) 0xEE, (byte) 0xEE, 0x22, 0x00, (byte) 0xEE, (byte) 0xEE, 0x00, (byte) 0xEE, (byte) 0xEE, 0x00, (byte) 0xEE, (byte) 0xEE, 0x00, (byte) 0xEE, (byte) 0xEE, 0x00, (byte) 0xEE, (byte) 0xEE, 0x23, 0x00, (byte) 0xEE, (byte) 0xEE, 0x00, (byte) 0xEE, (byte) 0xEE, 0x00, (byte) 0xEE, (byte) 0xEE, 0x00, (byte) 0xEE, (byte) 0xEE, 0x00, (byte) 0xEE, (byte) 0xEE, 0x24, 0x00, (byte) 0xEE, (byte) 0xEE, 0x00, (byte) 0xEE, (byte) 0xEE, 0x00, (byte) 0xEE, (byte) 0xEE, 0x00, (byte) 0xEE, (byte) 0xEE, 0x00, (byte) 0xEE, (byte) 0xEE})).startingAt(0);
    }
}
