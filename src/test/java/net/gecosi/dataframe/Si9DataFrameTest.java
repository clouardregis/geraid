/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.dataframe;

import net.gecosi.SiMessageFixtures;
import net.gecosi.internal.SportIdentMessage;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class Si9DataFrameTest {

    @Test
    void getSiCardNumber() {
        assertEquals("1061511", subject1061511().getSiNumber());
    }

    @Test
    void getSiCardSeries() {
        assertEquals("SiCard 9", subject1061511().getSiSeries());
    }

    @Test
    void getStartTime() {
        assertEquals(40561000L, subject1061511().getStartTime());
    }

    @Test
    void getFinishTime() {
        assertEquals(44143000L, subject1061511().getFinishTime());
    }

    @Test
    void getCheckTime() {
        assertEquals(40359000L, subject1061511().getCheckTime());
    }

    @Test
    void getNbPunches() {
        assertEquals(19, subject1061511().getNbPunches());
    }

    @Test
    void getPunches() {
        SportIdentPunch[] punches = subject1061511().getPunches();
        assertEquals(134, punches[0].code());
        assertEquals(40679000L, punches[0].timestamp());
        assertEquals(158, punches[8].code());
        assertEquals(42667000L, punches[8].timestamp());
        assertEquals(200, punches[18].code());
        assertEquals(44131000L, punches[18].timestamp());
    }

    private SportIdentDataFrame subject1061511() {
        return new SportIdent8PlusDataFrame(new SportIdentMessage[]{SiMessageFixtures.siCard9_b0_data, SiMessageFixtures.siCard9_b1_data}).startingAt(0);
    }

}
