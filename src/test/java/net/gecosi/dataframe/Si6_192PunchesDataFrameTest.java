/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.dataframe;

import static net.gecosi.SiMessageFixtures.siCard6_192p_b0_data;
import static net.gecosi.SiMessageFixtures.siCard6_192p_b2_data;
import static net.gecosi.SiMessageFixtures.siCard6_192p_b3_data;
import static net.gecosi.SiMessageFixtures.siCard6_192p_b6_data;
import static net.gecosi.SiMessageFixtures.siCard6_192p_b7_data;
import static org.junit.jupiter.api.Assertions.assertEquals;

import net.gecosi.internal.SportIdentMessage;

import org.junit.jupiter.api.Test;

class Si6_192PunchesDataFrameTest {

    @Test
    void getSiCardNumber() {
        assertEquals("821003", subject821003_192p().getSiNumber());
    }

    @Test
    void getSiCardSeries() {
        assertEquals("SiCard 6", subject821003_192p().getSiSeries());
    }

    @Test
    void getStartTime() {
        assertEquals(36752000L, subject821003_192p().getStartTime());
    }

    @Test
    void getFinishTime() {
        assertEquals(48684000L, subject821003_192p().getFinishTime());
    }

    @Test
    void getCheckTime() {
        assertEquals(36750000L, subject821003_192p().getCheckTime());
    }

    @Test
    void getNbPunches() {
        assertEquals(101, subject821003_192p().getNbPunches());
    }

    @Test
    void getPunches() {
        SportIdentPunch[] punches = subject821003_192p().getPunches();
        assertEquals(31, punches[0].code());
        assertEquals(36762000L, punches[0].timestamp());
        assertEquals(32, punches[1].code());
        assertEquals(36764000L, punches[1].timestamp());
        assertEquals(33, punches[32].code());
        assertEquals(36851000L, punches[32].timestamp());
        assertEquals(34, punches[63].code());
        assertEquals(36940000L, punches[63].timestamp());
        assertEquals(35, punches[99].code());
        assertEquals(37040000L, punches[99].timestamp());

        assertEquals(634, punches[100].code());
        assertEquals(48625000L, punches[100].timestamp());
    }

    private SportIdentDataFrame subject821003_192p() {
        return new SportIdent6DataFrame(new SportIdentMessage[]{siCard6_192p_b0_data, siCard6_192p_b6_data, siCard6_192p_b7_data,
                siCard6_192p_b2_data, siCard6_192p_b3_data}).startingAt(0);
    }
}
