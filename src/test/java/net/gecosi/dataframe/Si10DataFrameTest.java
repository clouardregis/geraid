/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi.dataframe;

import net.gecosi.internal.SportIdentMessage;
import org.junit.jupiter.api.Test;

import static net.gecosi.SiMessageFixtures.*;
import static org.junit.jupiter.api.Assertions.assertEquals;

class Si10DataFrameTest {

    @Test
    void getSiCardNumber() {
        assertEquals("7773810", subject7773810().getSiNumber());
    }

    @Test
    void getSiCardSeries() {
        assertEquals("SiCard 10/11/SIAC", subject7773810().getSiSeries());
    }

    @Test
    void getStartTime() {
        assertEquals(SportIdentDataFrame.NO_TIME, subject7773810().getStartTime());
    }

    @Test
    void getFinishTime() {
        assertEquals(SportIdentDataFrame.NO_TIME, subject7773810().getFinishTime());
    }

    @Test
    void getCheckTime() {
        assertEquals(SportIdentDataFrame.NO_TIME, subject7773810().getCheckTime());
    }

    @Test
    void getNbPunches() {
        assertEquals(3, subject7773810().getNbPunches());
    }

    @Test
    void getPunches() {
        SportIdentPunch[] punches = subject7773810().getPunches();
        assertEquals(42, punches[0].code());
        assertEquals(45706000L, punches[0].timestamp());
        assertEquals(38, punches[1].code());
        assertEquals(45713000L, punches[1].timestamp());
        assertEquals(32, punches[2].code());
        assertEquals(48397000L, punches[2].timestamp());
    }

    private SportIdentDataFrame subject7773810() {
        return new SportIdent8PlusDataFrame(new SportIdentMessage[]{
                siCard10_b0_data, siCard10_b4_data, siCard10_b5_data, siCard10_b6_data, siCard10_b7_data}).startingAt(0);
    }

}
