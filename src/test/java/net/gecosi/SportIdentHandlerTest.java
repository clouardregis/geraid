/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package net.gecosi;

import net.gecosi.dataframe.SportIdent5DataFrame;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.verify;

class SportIdentHandlerTest {
    @Mock
    private SportIdentListener listener;

    @Mock
    private SportIdent5DataFrame siCard5;
    private AutoCloseable closeable;

    @BeforeEach
    public void setup() {
        closeable = MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    public void releaseMocks() throws Exception {
        closeable.close();
    }

    @Test
    void notifySiCard5() {
        SportIdentHandler sportIdentHandler = new SportIdentHandler(listener);
        sportIdentHandler.setZeroHourInMilliseconds(10000L);
        sportIdentHandler.notify(siCard5);
        verify(siCard5).startingAt(10000L);
    }

}
