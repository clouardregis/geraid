package fr.gecoraid.desktop.setting_menu;

import org.junit.jupiter.api.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertArrayEquals;
import static org.junit.jupiter.api.Assertions.assertEquals;

class RecentFileHandlerTest {

    @Test
    void test_remove_extension_from_filename() {
        String actual = RecentFileHandler.removeExtension("toto avec espace.grd");
        assertEquals("toto avec espace", actual);
    }

    @Test
    void test_get_filenames_from_file_path() {
        String tmpdir = System.getProperty("java.io.tmpdir");
        final String[] filePaths = new String[]{
                tmpdir + File.separator + "directory" + File.separator + "raid 1.grd",
                tmpdir + File.separator + "sub directory" + File.separator + "long raid name 2.grd",
        };
        final String[] expected = new String[]{
                "raid 1",
                "long raid name 2",
        };
        final String[] actual = RecentFileHandler.getFileNamesFromFilePath(filePaths);
        assertArrayEquals(expected, actual);
    }

    @Test
    void test_and_duplicate_file() {
        //noinspection ConstantConditions
        String filename = getClass().getResource("/.gecoraid/recent_projects").getFile();
        File file = new File(filename);
        final String[] expected = RecentFileHandler._getRecentFilePaths(
                file.getParent(),
                file.getName());
        final String newFile = "/root/directory/exemple/ctdnf18.grd";
        RecentFileHandler._addToRecentFiles(newFile, file.getParent(), file.getName());
        final String[] actual = RecentFileHandler._getRecentFilePaths(
                file.getParent(),
                file.getName());
//        RecentFileHandler._removeFromRecentFiles(newFile, file.getParent(), file.getName());
        assertArrayEquals(expected, actual);
    }

    @Test
    void test_add_and_remove_recent_files() {
        //noinspection ConstantConditions
        String filename = getClass().getResource("/.gecoraid/recent_projects").getFile();
        File file = new File(filename);
        final String[] actual1 = RecentFileHandler._getRecentFilePaths(
                file.getParent(),
                file.getName());
        String[] expected1 = new String[]{
                "/root/directory/exemple/ctdnf18.grd",
                "/root/directory 1/ctdnf18 2024-1-25 13h17mn28s.grd",
                "c:\\dev\\licenses\\windows\\raid 1.grd"
        };
        assertArrayEquals(expected1, actual1);

        final String newFile = "/toto/titi";
        RecentFileHandler._addToRecentFiles(newFile, file.getParent(), file.getName());
        String[] expected2 = new String[]{
                newFile,
                "/root/directory/exemple/ctdnf18.grd",
                "/root/directory 1/ctdnf18 2024-1-25 13h17mn28s.grd",
                "c:\\dev\\licenses\\windows\\raid 1.grd"
        };
        final String[] actual2 = RecentFileHandler._getRecentFilePaths(
                file.getParent(),
                file.getName());
        assertArrayEquals(expected2, actual2);

        RecentFileHandler._removeFromRecentFiles(newFile, file.getParent(), file.getName());
        final String[] actual3 = RecentFileHandler._getRecentFilePaths(
                file.getParent(),
                file.getName());
        assertArrayEquals(expected1, actual3);
    }


    private static String readFileToString( String filename ) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        String ls = System.lineSeparator();
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }
        reader.close();
        return stringBuilder.toString();
    }
}