package fr.gecoraid.desktop.setting_menu;

import fr.gecoraid.L10n;
import fr.gecoraid.model.Categorie;
import fr.gecoraid.model.GlobalSettings;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Locale;
import java.util.Vector;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class RaidHandlerTest {
    @Test
    void test_bad_xml_file_throw_exception() {
        L10n.setLocale(Locale.FRENCH);
        //noinspection ConstantConditions
        String filename = getClass().getResource("/.gecoraid/recent_projects").getFile();
        GlobalSettings globalSettings = new GlobalSettings();
        assertThrows(
                SAXException.class,
                () -> GlobalSettingsFile._readGlobalSettingFromXmlFile(globalSettings, filename, "/toto"),
                "Expected SAXException() to throw, but it didn't"
        );
    }

    @Test
    void test_good_xml_file() throws IOException, ParserConfigurationException, SAXException {
        //noinspection ConstantConditions
        String filename = getClass().getResource("/.gecoraid/config.xml").getFile();
        L10n.setLocale(Locale.FRENCH);
        GlobalSettings globalSettings = new GlobalSettings();
        GlobalSettingsFile._readGlobalSettingFromXmlFile(globalSettings, filename, "/toto");
        assertEquals("club", globalSettings.getClubName());
        assertEquals("/toto", globalSettings.getRaidFolderPath());
        assertEquals("", globalSettings.getWorkingDirectoryPath());
        assertEquals("1", globalSettings.getIsShortIndividualResultPrintingAsInt());
        assertEquals("0", globalSettings.getIsShortGlobalResultPrintingAsInt());
        assertEquals("0", globalSettings.getResultAutoScrollingAsInt());
        assertEquals(30, globalSettings.getAutoScrollingSpeedInS());
        assertEquals(10, globalSettings.getPenaltyPointsDefault());
        assertEquals(-3661, globalSettings.getPenaltyTimeDefault());
        assertEquals(-10, globalSettings.getMispunchPenaltyPoints());
        assertEquals(3721, globalSettings.getMispunchPenaltyTimeInS());

        assertEquals(3, globalSettings.getCategoriesTemplate().getSize());
        Vector<Categorie> categories = globalSettings.getCategoriesTemplate().getCategories();
        assertEquals("Homme", categories.get(0).getLongName());
        assertEquals("MAS", categories.get(0).getShortName());
        assertEquals("Femme", categories.get(1).getLongName());
        assertEquals("FEM", categories.get(1).getShortName());
        assertEquals("Mixte", categories.get(2).getLongName());
        assertEquals("MIX", categories.get(2).getShortName());
    }

    @Test
    void test_write_global_settings_in_xml() throws Exception {
        L10n.setLocale(Locale.FRENCH);
        GlobalSettings globalSettings = new GlobalSettings();
        globalSettings.setClubName("toto");
        Document document = GlobalSettingsFile._saveGlobalSettingsToXml(globalSettings);
        final Node tag = document.getElementsByTagName("nom").item(0);
        String name = tag.getFirstChild().getTextContent().trim();
        assertEquals("toto", name);
    }
}