package fr.gecoraid.desktop.setting_menu;

import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

class PlatformSettingsHandlerTest {
    @Test
    void test_get_global_settings_directory() {
        String globalSettingsDirectory = PlatformSettingsHandler.getGlobalSettingsDirectory();
        assertTrue(globalSettingsDirectory.endsWith(".gecoraid"));
    }

    @Test
    void test_create_directory_when_impossible() {
        assertThrows(
                IOException.class,
                () -> PlatformSettingsHandler._createGlobalSettingsDirectoryIfNeeded("\0/gecoraid/toto"),
                "Expected IOException() to throw, but it didn't"
        );
    }

    @SuppressWarnings({"ResultOfMethodCallIgnored"})
    @Test
    void test_create_directory_when_possible() throws IOException {
        String temporaryDirectory = System.getProperty("java.io.tmpdir") + File.separator + "toto";
        PlatformSettingsHandler._createGlobalSettingsDirectoryIfNeeded(temporaryDirectory);
        File file = new File(temporaryDirectory);
        file.delete();
    }
}