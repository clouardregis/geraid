package fr.gecoraid.desktop.raid_file;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.RaidFileXmlReader;
import fr.gecoraid.model.Raid;
import org.junit.jupiter.api.Test;

import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RaidFileXmlReaderTest {
    @Test
    void test_read_raid_file_when_nominal() {
        L10n.setLocale(Locale.FRENCH);
        //noinspection ConstantConditions
        String filename = getClass().getResource("/ctdnf18.grd").getFile();
        Raid raid = new Raid();
        RaidFileXmlReader.readRaidFile(raid, filename);
        assertEquals(4, raid.getZeroTime().hours);
        assertEquals(2, raid.getZeroTime().minutes);

        assertEquals(3, raid.getCategories().getSize());
        assertEquals("Masculin", raid.getCategories().getCategories().get(0).getLongName());
        assertEquals("F", raid.getCategories().getCategories().get(1).getShortName());

        assertEquals(3, raid.getCategories().getSize());
        assertEquals("Masculin", raid.getCategories().getCategories().get(0).getLongName());
        assertEquals("F", raid.getCategories().getCategories().get(1).getShortName());

        assertEquals(3, raid.getParcourss().getSize());

        assertEquals(214, raid.getResultatsPuce().getSize());

        assertEquals(1, raid.getPenalites().getPenalites().size());
    }
}