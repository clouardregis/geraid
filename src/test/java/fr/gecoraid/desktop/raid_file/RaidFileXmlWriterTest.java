package fr.gecoraid.desktop.raid_file;

import fr.gecoraid.L10n;
import fr.gecoraid.desktop.RaidFileXmlReader;
import fr.gecoraid.desktop.RaidFileXmlWriter;
import fr.gecoraid.model.Raid;
import org.junit.jupiter.api.Test;
import org.w3c.dom.Document;

import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.StringWriter;
import java.util.Locale;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RaidFileXmlWriterTest {
    @Test
    void test_read_and_write_raid() throws ParserConfigurationException, TransformerException, IOException {
        L10n.setLocale(Locale.FRENCH);
        //noinspection ConstantConditions
        String filename = getClass().getResource("/ctdnf18.grd").getFile();
        Raid raid = new Raid();
        RaidFileXmlReader.readRaidFile(raid, filename);
        Document document = RaidFileXmlWriter.buildDocumentFromRaid(raid);

        String originalFile = readFileToString(filename);
        String output = writeDocumentToString(document);
        // Escape version
        assertEquals(originalFile.substring(93, 593), output.substring(41, 541));
    }

    private static String writeDocumentToString( Document document ) throws TransformerException {
        TransformerFactory tf = TransformerFactory.newInstance();
        Transformer transformer = tf.newTransformer();
        transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");
        StringWriter writer = new StringWriter();
        transformer.transform(new DOMSource(document), new StreamResult(writer));
        return writer.getBuffer().toString();
    }

    private static String readFileToString( String filename ) throws IOException {
        BufferedReader reader = new BufferedReader(new FileReader(filename));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        String ls = System.lineSeparator();
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line);
            stringBuilder.append(ls);
        }
        reader.close();
        return stringBuilder.toString();
    }
}