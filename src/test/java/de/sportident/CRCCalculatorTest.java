/**
 * Copyright (c) 2013 Simon Denier
 * Licensed under The MIT License
 */
package de.sportident;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class CRCCalculatorTest {

    @Test
    void testSampleCrc() {
        byte[] sample = new byte[]{
                (byte) 0x53, (byte) 0x00, (byte) 0x05, (byte) 0x01,
                (byte) 0x0F, (byte) 0xB5, (byte) 0x00, (byte) 0x00,
                (byte) 0x1E, (byte) 0x08
        };
        int expectedCrc = 0x2C12;
        assertEquals(expectedCrc, CRCCalculator.crc(sample));
    }

    @Test
    void testStartupCrc() {
        int expectedCrc = 0x6D0A;
        byte[] sample = new byte[]{(byte) 0xF0, 0x01, 0x4D};
        assertEquals(expectedCrc, CRCCalculator.crc(sample));
    }

    @Test
    void testAnswerStartupCrc() {
        int expectedCrc = 0x8B12;
        byte[] sample = new byte[]{(byte) 0xF0, 0x03, 0x01, 0x01, 0x4D};
        assertEquals(expectedCrc, CRCCalculator.crc(sample));
    }

    @Test
    void test2AnswerStartupCrc() {
        int expectedCrc = 0x0D11;
        byte[] sample = new byte[]{(byte) 0xF0, 0x03, 0x00, 0x01, 0x4D};
        assertEquals(expectedCrc, CRCCalculator.crc(sample));
    }

    @Test
    void testGetProtocolModeCrc() {
        int expectedCrc = 0x0414;
        byte[] sample = new byte[]{(byte) 0x83, 0x02, 0x74, 0x01};
        assertEquals(expectedCrc, CRCCalculator.crc(sample));
    }

    @Test
    void testGetCardBlocksCrc() {
        int expectedCrc = 0x1611;
        byte[] sample = new byte[]{(byte) 0x83, 0x02, 0x33, 0x01};
        assertEquals(expectedCrc, CRCCalculator.crc(sample));
    }

    @Test
    void testReadSiCard5() {
        int expectedCrc = 0xB100;
        byte[] sample = new byte[]{(byte) 0xB1, 0x00};
        assertEquals(expectedCrc, CRCCalculator.crc(sample));
    }

    @Test
    void testReadSiCard6Bx() {
        int[] expectedCrc = new int[]{0x460A, 0x440A, 0x450A, 0x420A, 0x430A, 0x400A, 0x410A};
        byte[] sample_byte = new byte[]{0x00, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07};
        for (int i = 0; i < expectedCrc.length; i++) {
            byte[] sample = new byte[]{(byte) 0xE1, 0x01, sample_byte[i]};
            assertEquals(expectedCrc[i], CRCCalculator.crc(sample), "CRC failed on Block " + sample_byte[i]);
        }
    }

    @Test
    void testReadSiCard6B8() {
        int expectedCrc = 0x4E0A;
        byte[] sample = new byte[]{(byte) 0xE1, 0x01, 0x08};
        assertEquals(expectedCrc, CRCCalculator.crc(sample));
    }

    @Test
    void testReadSiCard8B0() {
        int expectedCrc = 0xE209;
        byte[] sample = new byte[]{(byte) 0xEF, 0x01, 0x00};
        assertEquals(expectedCrc, CRCCalculator.crc(sample));
    }

    @Test
    void testReadSiCard8B1() {
        int expectedCrc = 0xE309;
        byte[] sample = new byte[]{(byte) 0xEF, 0x01, 0x01};
        assertEquals(expectedCrc, CRCCalculator.crc(sample));
    }

    @Test
    void testReadSiCard8Bx() {
        int[] expectedCrc = new int[]{0xE609, 0xE709, 0xE409, 0xE509};
        byte[] sample_byte = new byte[]{0x04, 0x05, 0x06, 0x07};
        for (int i = 0; i < expectedCrc.length; i++) {
            byte[] sample = new byte[]{(byte) 0xEF, 0x01, sample_byte[i]};
            assertEquals(expectedCrc[i], CRCCalculator.crc(sample), "CRC failed on Block " + sample_byte[i]);
        }
    }

    @Test
    void testReadSiCard10B8() {
        int expectedCrc = 0xEA09;
        byte[] sample = new byte[]{(byte) 0xEF, 0x01, 0x08};
        assertEquals(expectedCrc, CRCCalculator.crc(sample));
    }
}
